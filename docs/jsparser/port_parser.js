// Decode decodes an array of bytes into an object.
//  - fPort contains the LoRaWAN fPort number
//  - bytes is an array of bytes, e.g. [225, 230, 255, 0]
// The function must return an object, e.g. {"temperature": 22.5}
function Decode(fPort, bytes) {
	var obj_ret = {};
  switch (fPort)
  {
    case 12:
      {
        var iSCNR;
        var iRemaining;
        var iVTemp;
        iSCNR = 0;
		do{
			iRemaining = bytes.length - iSCNR;
			switch (bytes[iSCNR])
			{
			case 1: //CENTRALINA_LORAWAN_ANSW_STATUS_SYS
				{
					obj_ret.stato_sys = {};
					iSCNR++;
					obj_ret.stato_sys.stato= bytes[iSCNR];
					iSCNR++;
					obj_ret.stato_sys.curr_sched= bytes[iSCNR];
					iSCNR++;
					obj_ret.stato_sys.ev_sched= bytes[iSCNR];
					iSCNR++;
				}
				break;
			case 2: //CENTRALINA_LORAWAN_ANSW_STATUS_OUTS
				{
					obj_ret.stato_out = {};
					iSCNR++;
					obj_ret.stato_out.ev_open= bytes[iSCNR];
					iSCNR++;
					obj_ret.stato_out.ev_bad= bytes[iSCNR];
					iSCNR++;
				}
				break;
			case 11: //CENTRALINA_LORAWAN_ANSW_VALUE_IN1
				{
					obj_ret.value_in1 = {};
					iSCNR++;
					iVTemp = bytes[iSCNR];
					iVTemp <<= 8;
					iSCNR++;
					iVTemp |= bytes[iSCNR];
					iSCNR++;
					
					obj_ret.value_in1.value 	= iVTemp & 0x3FF;
					obj_ret.value_in1.pullup 	= iVTemp & 0x1000 ? true : false;
					obj_ret.value_in1.ty4_20mA 	= iVTemp & 0x2000 ? true : false;
				}
				break;
			case 12: //CENTRALINA_LORAWAN_ANSW_VALUE_IN2
				{
					obj_ret.value_in2 = {};
					iSCNR++;
					iVTemp = bytes[iSCNR];
					iVTemp <<= 8;
					iSCNR++;
					iVTemp |= bytes[iSCNR];
					iSCNR++;
					
					obj_ret.value_in2.value 	= iVTemp & 0x3FF;
					obj_ret.value_in2.pullup 	= iVTemp & 0x1000 ? true : false;
					obj_ret.value_in2.ty4_20mA 	= iVTemp & 0x2000 ? true : false;
				}
				break;
			case 20: //CENTRALINA_LORAWAN_ANSW_VALUE_CNTR
				{
					obj_ret.flux = {};
					iSCNR++;
					iVTemp = bytes[iSCNR];
					iVTemp <<= 8;
					iSCNR++;
					iVTemp |= bytes[iSCNR];
					iSCNR++;
					
					obj_ret.flux.value 	= iVTemp;
				}
				break;
			default:
				iRemaining = -1;
				break;
			}
		
		}while(iRemaining > 0);
      }
      break;
  }
  return obj_ret;
}

