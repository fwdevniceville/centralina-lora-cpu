// Decode decodes an array of bytes into an object.
//  - fPort contains the LoRaWAN fPort number
//  - bytes is an array of bytes, e.g. [225, 230, 255, 0]
// The function must return an object, e.g. {"temperature": 22.5}
function Decode(fPort, bytes) {
	var obj_ret = {};
  switch (fPort)
  {
    case 12:
      {
        var iSCNR;
        var iRemaining;
        var iVTemp;
        iSCNR = 0;
		do{
			iRemaining = bytes.length - iSCNR;
			switch (bytes[iSCNR])
			{
			case 1: //CENTRALINA_LORAWAN_ANSW_STATUS_SYS
				{
					obj_ret.stato_sys = {};
					iSCNR++;
					obj_ret.stato_sys.stato= bytes[iSCNR];
					iSCNR++;
					obj_ret.stato_sys.curr_sched= bytes[iSCNR];
					iSCNR++;
					obj_ret.stato_sys.ev_sched= bytes[iSCNR];
					iSCNR++;
				}
				break;
			case 2: //CENTRALINA_LORAWAN_ANSW_STATUS_OUTS
				{
					obj_ret.stato_out = {};
					iSCNR++;
					obj_ret.stato_out.ev_open = {};
					obj_ret.stato_out.ev_open.raw = bytes[iSCNR];
					obj_ret.stato_out.ev_open.out_0 = obj_ret.stato_out.ev_open.raw & 0x01 ? true : false;
					obj_ret.stato_out.ev_open.out_1 = obj_ret.stato_out.ev_open.raw & 0x02 ? true : false;
					obj_ret.stato_out.ev_open.out_2 = obj_ret.stato_out.ev_open.raw & 0x04 ? true : false;
					obj_ret.stato_out.ev_open.out_3 = obj_ret.stato_out.ev_open.raw & 0x08 ? true : false;
					
					iSCNR++;
					obj_ret.stato_out.ev_bad_unplugged = {};
					obj_ret.stato_out.ev_bad_unplugged.raw= (bytes[iSCNR] >> 0) & 0x0f;
					obj_ret.stato_out.ev_bad_unplugged.out_0 = obj_ret.stato_out.ev_bad_unplugged.raw & 0x01 ? true : false;
					obj_ret.stato_out.ev_bad_unplugged.out_1 = obj_ret.stato_out.ev_bad_unplugged.raw & 0x02 ? true : false;
					obj_ret.stato_out.ev_bad_unplugged.out_2 = obj_ret.stato_out.ev_bad_unplugged.raw & 0x04 ? true : false;
					obj_ret.stato_out.ev_bad_unplugged.out_3 = obj_ret.stato_out.ev_bad_unplugged.raw & 0x08 ? true : false;
					
					obj_ret.stato_out.ev_bad_shotcut = {};
					obj_ret.stato_out.ev_bad_shotcut.raw= (bytes[iSCNR] >> 4) & 0x0f;
					obj_ret.stato_out.ev_bad_shotcut.out_0 = obj_ret.stato_out.ev_bad_shotcut.raw & 0x01 ? true : false;
					obj_ret.stato_out.ev_bad_shotcut.out_1 = obj_ret.stato_out.ev_bad_shotcut.raw & 0x02 ? true : false;
					obj_ret.stato_out.ev_bad_shotcut.out_2 = obj_ret.stato_out.ev_bad_shotcut.raw & 0x04 ? true : false;
					obj_ret.stato_out.ev_bad_shotcut.out_3 = obj_ret.stato_out.ev_bad_shotcut.raw & 0x08 ? true : false;



					iSCNR++;
				}
				break;
			case 11: //CENTRALINA_LORAWAN_ANSW_VALUE_IN1
				{
					obj_ret.value_in1 = {};
					iSCNR++;
					iVTemp = bytes[iSCNR];
					iVTemp <<= 8;
					iSCNR++;
					iVTemp |= bytes[iSCNR];
					iSCNR++;
					
					obj_ret.value_in1.value 	= iVTemp & 0x3FF;
					obj_ret.value_in1.pullup 	= iVTemp & 0x1000 ? true : false;
					obj_ret.value_in1.ty4_20mA 	= iVTemp & 0x2000 ? true : false;
				}
				break;
			case 12: //CENTRALINA_LORAWAN_ANSW_VALUE_IN2
				{
					obj_ret.value_in2 = {};
					iSCNR++;
					iVTemp = bytes[iSCNR];
					iVTemp <<= 8;
					iSCNR++;
					iVTemp |= bytes[iSCNR];
					iSCNR++;
					
					obj_ret.value_in2.value 	= iVTemp & 0x3FF;
					obj_ret.value_in2.pullup 	= iVTemp & 0x1000 ? true : false;
					obj_ret.value_in2.ty4_20mA 	= iVTemp & 0x2000 ? true : false;
				}
				break;
			case 20: //CENTRALINA_LORAWAN_ANSW_VALUE_CNTR
				{
					obj_ret.flux = {};
					iSCNR++;
					iVTemp = bytes[iSCNR];
					iVTemp <<= 8;
					iSCNR++;
					iVTemp |= bytes[iSCNR];
					iSCNR++;
					
					obj_ret.flux.value 	= iVTemp;
				}
				break;
			default:
				iRemaining = -1;
				break;
			}
		
		}while(iRemaining > 0);
      }
      break;
  }
  return obj_ret;
}

