/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     

/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId tastSupervisorHandle;
uint32_t tastSupervisorBuffer[ 128 ];
osStaticThreadDef_t tastSupervisorControlBlock;
osThreadId taskInterfaceHandle;
uint32_t taskInterfaceBuffer[ 128 ];
osStaticThreadDef_t taskInterfaceControlBlock;
osThreadId taskComunicatioHandle;
uint32_t taskComunicatioBuffer[ 512 ];
osStaticThreadDef_t taskComunicatioControlBlock;
osThreadId taskSchedulerHandle;
uint32_t taskSchedulerBuffer[ 256 ];
osStaticThreadDef_t taskSchedulerControlBlock;
osThreadId taskIntComHandle;
uint32_t taskIntComBuffer[ 256 ];
osStaticThreadDef_t taskIntComControlBlock;
osMutexId mutexIntComFunctionsCallHandle;
osStaticMutexDef_t mutexIntComFunctionsCallControlBlock;
osMutexId mutexIntComHandle;
osStaticMutexDef_t mutexIntComControlBlock;

/* USER CODE BEGIN Variables */

/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartSupervisorTask(void const * argument);
void StartInterfaceTask(void const * argument);
void StartComunicationTask(void const * argument);
void StartSchedulerTask(void const * argument);
void StartIntComTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

/* Pre/Post sleep processing prototypes */
void PreSleepProcessing(uint32_t *ulExpectedIdleTime);
void PostSleepProcessing(uint32_t *ulExpectedIdleTime);

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* Hook prototypes */
void configureTimerForRunTimeStats(void);
unsigned long getRunTimeCounterValue(void);
void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName);

/* USER CODE BEGIN 1 */
/* Functions needed when configGENERATE_RUN_TIME_STATS is on */
__weak void configureTimerForRunTimeStats(void)
{

}

__weak unsigned long getRunTimeCounterValue(void)
{
return 0;
}
/* USER CODE END 1 */

/* USER CODE BEGIN 4 */
__weak void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName)
{
   /* Run time stack overflow checking is performed if
   configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook function is
   called if a stack overflow is detected. */
}
/* USER CODE END 4 */

/* USER CODE BEGIN PREPOSTSLEEP */
__weak void PreSleepProcessing(uint32_t *ulExpectedIdleTime)
{
/* place for user code */ 
}

__weak void PostSleepProcessing(uint32_t *ulExpectedIdleTime)
{
/* place for user code */
}
/* USER CODE END PREPOSTSLEEP */

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];
  
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}                   
/* USER CODE END GET_IDLE_TASK_MEMORY */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* Create the mutex(es) */
  /* definition and creation of mutexIntComFunctionsCall */
  osMutexStaticDef(mutexIntComFunctionsCall, &mutexIntComFunctionsCallControlBlock);
  mutexIntComFunctionsCallHandle = osMutexCreate(osMutex(mutexIntComFunctionsCall));

  /* definition and creation of mutexIntCom */
  osMutexStaticDef(mutexIntCom, &mutexIntComControlBlock);
  mutexIntComHandle = osMutexCreate(osMutex(mutexIntCom));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of tastSupervisor */
  osThreadStaticDef(tastSupervisor, StartSupervisorTask, osPriorityNormal, 0, 128, tastSupervisorBuffer, &tastSupervisorControlBlock);
  tastSupervisorHandle = osThreadCreate(osThread(tastSupervisor), NULL);

  /* definition and creation of taskInterface */
  osThreadStaticDef(taskInterface, StartInterfaceTask, osPriorityLow, 0, 128, taskInterfaceBuffer, &taskInterfaceControlBlock);
  taskInterfaceHandle = osThreadCreate(osThread(taskInterface), NULL);

  /* definition and creation of taskComunicatio */
  osThreadStaticDef(taskComunicatio, StartComunicationTask, osPriorityNormal, 0, 512, taskComunicatioBuffer, &taskComunicatioControlBlock);
  taskComunicatioHandle = osThreadCreate(osThread(taskComunicatio), NULL);

  /* definition and creation of taskScheduler */
  osThreadStaticDef(taskScheduler, StartSchedulerTask, osPriorityNormal, 0, 256, taskSchedulerBuffer, &taskSchedulerControlBlock);
  taskSchedulerHandle = osThreadCreate(osThread(taskScheduler), NULL);

  /* definition and creation of taskIntCom */
  osThreadStaticDef(taskIntCom, StartIntComTask, osPriorityHigh, 0, 256, taskIntComBuffer, &taskIntComControlBlock);
  taskIntComHandle = osThreadCreate(osThread(taskIntCom), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* StartSupervisorTask function */
__weak void StartSupervisorTask(void const * argument)
{

  /* USER CODE BEGIN StartSupervisorTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartSupervisorTask */
}

/* StartInterfaceTask function */
__weak void StartInterfaceTask(void const * argument)
{
  /* USER CODE BEGIN StartInterfaceTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartInterfaceTask */
}

/* StartComunicationTask function */
__weak void StartComunicationTask(void const * argument)
{
  /* USER CODE BEGIN StartComunicationTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartComunicationTask */
}

/* StartSchedulerTask function */
__weak void StartSchedulerTask(void const * argument)
{
  /* USER CODE BEGIN StartSchedulerTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartSchedulerTask */
}

/* StartIntComTask function */
__weak void StartIntComTask(void const * argument)
{
  /* USER CODE BEGIN StartIntComTask */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartIntComTask */
}

/* USER CODE BEGIN Application */
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
