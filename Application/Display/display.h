/*
 * display.h
 *
 *  Created on: 24 apr 2018
 *      Author: daniele_parise
 */

#ifndef DISPLAY_DISPLAY_H_
#define DISPLAY_DISPLAY_H_

#define DISPLAY_OPTIONS_SHOW_MINUS	(0x01)
#define DISPLAY_OPTIONS_SHOW_PLUS	(0x02)
#define DISPLAY_OPTIONS_SHOW_LOBATT	(0x04)
#define DISPLAY_OPTIONS_SHOW_OVER	(0x08)

void displayOut(const char *aFRAME,uint8_t uiOptions);
void displayOutOverride(bool bOVERRIDE,const uint8_t auiFRAME[8]);


#endif /* DISPLAY_DISPLAY_H_ */
