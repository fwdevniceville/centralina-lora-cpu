/*
 * display.c
 *
 *  Created on: 24 apr 2018
 *      Author: daniele_parise
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "stm32l0xx_hal.h"
#include "display.h"

typedef struct tagLCD_DIGIT{
	int8_t	aiSegPos[10];
}LCD_DIGIT;

typedef struct tagLCD_FRAME{
	LCD_DIGIT	aDigits[5];
}LCD_FRAME;

#if 1

const LCD_FRAME stLCD_FRAME = {
		{
			{{-1,	10,	10,	-1,	-1,	-1,	-1,	6,	-1,	-1}}, // 5a da destra
			{{28,	29,	22,	23,	5,	15,	14,	48,	-1,	-1}}, // 4a da destra
			{{34,	35,	21,	1,	2,	33,	32,	20,	31,	-1}}, // 3a da destra
			{{27,	17,	39,	18,	19,	26,	25,	38,	24,	-1}}, // 2a da destra
			{{8,	9,	16,	36,	37,	7,	50,	-1,	-1, -1}}, // 1a da destra
		}
};

#else

const LCD_FRAME stLCD_FRAME = {
		{
			{{-1,	10,	10,	-1,	-1,	-1,	-1,	6,	-1,	-1}}, // 5a da destra
			{{28,	29,	22,	23,	5,	15,	14,	48,	-1,	-1}}, // 4a da destra
			{{34,	35,	21,	1,	2,	33,	32,	20,	31,	-1}}, // 3a da destra
			{{27,	17,	39,	18,	19,	26,	25,	38,	24,	-1}}, // 2a da destra
			{{8,	9,	16,	36,	37,	7,	30,	-1,	-1, -1}}, // 1a da destra
		}
};

#endif

#define SEG_A	(0x01)
#define SEG_B	(0x02)
#define SEG_C	(0x04)
#define SEG_D	(0x08)
#define SEG_E	(0x10)
#define SEG_F	(0x20)
#define SEG_G	(0x40)
#define SEG_DOT	(0x80)
#define SEG_COL	(0x100)


const uint8_t achCharMap[] ={
		SEG_E|SEG_F|SEG_A|SEG_B|SEG_C|SEG_G,	// A
		SEG_E|SEG_F|SEG_G|SEG_C|SEG_D,			// b
		SEG_E|SEG_F|SEG_A|SEG_D,				// C
		SEG_E|SEG_G|SEG_B|SEG_C|SEG_D,			// d
		SEG_E|SEG_F|SEG_A|SEG_D|SEG_G,			// E
		SEG_E|SEG_F|SEG_A|SEG_G,				// F
		SEG_E|SEG_F|SEG_A|SEG_C|SEG_D,			// G
		SEG_E|SEG_F|SEG_B|SEG_C|SEG_G,			// H
		SEG_E|SEG_F,							// I
		SEG_B|SEG_C|SEG_D|SEG_E,				// J
		0,										// K
		SEG_E|SEG_F|SEG_D,						// L
		SEG_E|SEG_F|SEG_A|SEG_B|SEG_C,			// M
		SEG_E|SEG_C|SEG_G,						// n
		SEG_E|SEG_C|SEG_D|SEG_G,				// o
		SEG_E|SEG_F|SEG_A|SEG_B|SEG_G,			// P
		0,										// Q
		SEG_E|SEG_G,							// r
		SEG_F|SEG_A|SEG_C|SEG_D|SEG_G,			// S
		SEG_E|SEG_F|SEG_D|SEG_G,				// t
		SEG_E|SEG_C|SEG_D,						// u
		SEG_E|SEG_C|SEG_D,						// v
		SEG_E|SEG_C|SEG_D,						// w
		0,										// x
		SEG_E|SEG_F|SEG_B|SEG_G,				// y
		SEG_E|SEG_A|SEG_B|SEG_G|SEG_D			// z
};

const uint8_t achDigitMap[] ={
		SEG_E|SEG_F|SEG_A|SEG_B|SEG_C|SEG_D,		// 0
		SEG_B|SEG_C,								// 1
		SEG_E|SEG_A|SEG_B|SEG_G|SEG_D,				// 2
		SEG_A|SEG_B|SEG_C|SEG_D|SEG_G,				// 3
		SEG_F|SEG_B|SEG_C|SEG_G,					// 4
		SEG_F|SEG_A|SEG_C|SEG_D|SEG_G,				// 5
		SEG_E|SEG_F|SEG_A|SEG_C|SEG_D|SEG_G,		// 6
		SEG_A|SEG_B|SEG_C,							// 7
		SEG_E|SEG_F|SEG_A|SEG_B|SEG_C|SEG_D|SEG_G, 	// 8
		SEG_F|SEG_A|SEG_B|SEG_C|SEG_D|SEG_G, 		// 9
};

typedef struct tagDISPLAYOUT_CTX{
	int iOUT_ADD_BIT;
	bool	bOverride;
	uint8_t auiOverrideFRAME[8];
}DISPLAYOUT_CTX;

DISPLAYOUT_CTX stDISPLAYOUT_CTX;

uint64_t displayConvertLSeg2PSeg(uint16_t uiSegDigit,int iDX_DIGIT);

void displayOutOverride(bool bOVERRIDE,const uint8_t auiFRAME[8])
{
	stDISPLAYOUT_CTX.bOverride = bOVERRIDE;
	if (auiFRAME)
	{
		memcpy(&stDISPLAYOUT_CTX.auiOverrideFRAME[0],auiFRAME,8);
	}
}


void displayOut(const char *aFRAME,uint8_t uiOptions)
{
	uint64_t			uiOUT;
	uint64_t			uiOUT_PART;
	int			iSCNR_STR;
	int			iSCNR_DIGIT;
	int			iDISPLAY;
	bool		bAdvanceDigit;
	uiOUT = 0;
	iSCNR_STR 	= strlen(aFRAME);
	iSCNR_DIGIT = 4;
	while ((iSCNR_DIGIT >= 0) && (iSCNR_STR > 0))
	{
		char chCURR;
		iSCNR_STR--;
		chCURR = aFRAME[iSCNR_STR];

		bAdvanceDigit = true;
		iDISPLAY = -1;
		if ((chCURR >= 'A') && (chCURR <= 'Z'))
		{
			iDISPLAY = achCharMap[chCURR - 'A'];
		}
		else
		if ((chCURR >= 'a') && (chCURR <= 'z'))
		{
			iDISPLAY = achCharMap[chCURR - 'a'];
		}
		else
		if ((chCURR >= '0') && (chCURR <= '9'))
		{
			iDISPLAY = achDigitMap[chCURR - '0'];
		}
		else
		if (chCURR == ' ')
		{
			iDISPLAY = 0;
		}
		else
		if (chCURR == '-')
		{
			if (iSCNR_DIGIT == 0)
			{
				uiOptions |= DISPLAY_OPTIONS_SHOW_MINUS;
			}
			else
			{
				iDISPLAY = SEG_G;
			}
		}
		else
		{
			bAdvanceDigit = false;
			if (chCURR == '.')
			{
				iDISPLAY = SEG_DOT;
			}
			else if (chCURR == ':')
			{
				iDISPLAY = SEG_COL;
			}
		}

		if (iDISPLAY > 0)
		{
			uiOUT_PART = displayConvertLSeg2PSeg(iDISPLAY,iSCNR_DIGIT);
			uiOUT |= uiOUT_PART;
		}

		if (bAdvanceDigit)
		{
			iSCNR_DIGIT--;
		}
	}
	if (uiOptions & (DISPLAY_OPTIONS_SHOW_MINUS | DISPLAY_OPTIONS_SHOW_PLUS))
	{
		uiOUT |= 1UL << 11;
	}
	if (uiOptions & ( DISPLAY_OPTIONS_SHOW_PLUS ))
	{
		uiOUT |= 1UL << 12;
	}
	if (uiOptions & ( DISPLAY_OPTIONS_SHOW_LOBATT ))
	{
		uiOUT |= 1UL << 13;
	}
	if (uiOptions & ( DISPLAY_OPTIONS_SHOW_OVER ))
	{
		uiOUT |= 1UL << 13;
	}

	uiOUT |= 1ULL << stDISPLAYOUT_CTX.iOUT_ADD_BIT;

	if (stDISPLAYOUT_CTX.bOverride)
	{
		uiOUT = stDISPLAYOUT_CTX.auiOverrideFRAME[0];
		uiOUT <<=8;
		uiOUT |= stDISPLAYOUT_CTX.auiOverrideFRAME[1];
		uiOUT <<=8;
		uiOUT |= stDISPLAYOUT_CTX.auiOverrideFRAME[2];
		uiOUT <<=8;
		uiOUT |= stDISPLAYOUT_CTX.auiOverrideFRAME[3];
		uiOUT <<=8;
		uiOUT |= stDISPLAYOUT_CTX.auiOverrideFRAME[4];
		uiOUT <<=8;
		uiOUT |= stDISPLAYOUT_CTX.auiOverrideFRAME[5];
		uiOUT <<=8;
		uiOUT |= stDISPLAYOUT_CTX.auiOverrideFRAME[6];
		uiOUT <<=8;
		uiOUT |= stDISPLAYOUT_CTX.auiOverrideFRAME[7];
	}


	WRITE_REG(LCD->RAM[0],(uint32_t)uiOUT);
	WRITE_REG(LCD->RAM[1],(uint32_t)(uiOUT >> 32));
	WRITE_REG(LCD->RAM[2],LCD->RAM[0]);
	WRITE_REG(LCD->RAM[3],LCD->RAM[1]);

	WRITE_REG(LCD->SR,LCD_SR_UDR);

}


uint64_t displayConvertLSeg2PSeg(uint16_t uiSegDigit,int iDX_DIGIT)
{
	uint64_t	uiRET;
	unsigned	uiSCNR;
	int	iSEG;
	uiRET = 0;
	uiSCNR = 0;
	while (uiSCNR < 10)
	{
		if (uiSegDigit & ( 1 << uiSCNR))
		{
			iSEG = stLCD_FRAME.aDigits[iDX_DIGIT].aiSegPos[uiSCNR];
			if (iSEG >= 0)
			{
				uiRET |= 1ULL << iSEG;
			}
		}
		uiSCNR++;
	}
	return uiRET;
}
