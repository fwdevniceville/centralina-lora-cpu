/*
 * menu_func.h
 *
 *  Created on: 04 mar 2018
 *      Author: daniele_parise
 */

#ifndef MENU_MENU_FUNC_H_
#define MENU_MENU_FUNC_H_

typedef enum etagEMENU_ITEM_TYPE{
	MENU_ITEM_TYPE_NULL,
	MENU_ITEM_TYPE_TEXT_STATIC,
	MENU_ITEM_TYPE_TEXT_DYNAMIC,
	MENU_ITEM_TYPE_TEXT_READ,
	MENU_ITEM_TYPE_INT_READ_DEC,
	MENU_ITEM_TYPE_INT_READ_HEX,
	MENU_ITEM_TYPE_INT_RW_DEC,
	MENU_ITEM_TYPE_INT_RW_HEX
}EMENU_ITEM_TYPE;

typedef struct tagMENU_ITEM_MIN_MAX{
	int iMIN;
	int iMAX;
	int	iDPPos;
}MENU_ITEM_MIN_MAX;

#define MENU_ITEM_MAKE_ID(cm,cs,co,ac) ((((ac) & 0xF) << 0) | (((co) & 0xF) << 4) | (((cs) & 0xF) << 8) | (((cm) & 0xF) << 12))

#define MENU_ITEM_GET_ID_CM(id)		(((id)>>12) & 0xF)
#define MENU_ITEM_GET_ID_CS(id)		(((id)>> 8) & 0xF)
#define MENU_ITEM_GET_ID_CO(id)		(((id)>> 4) & 0xF)
#define MENU_ITEM_GET_ID_AC(id)		(((id)>> 0) & 0xF)

typedef struct tagMENU_ITEM{
	EMENU_ITEM_TYPE				eTYPE;
	int							iID;
	const char* 				strLABEL_VALUE;
	union {
		const void*					pGENERIC;
		const struct tagMENU_ITEM*	pSUB_MENU;
		MENU_ITEM_MIN_MAX*			pMIN_MAX;
	};
}MENU_ITEM;

typedef enum etagEMENU_EVENTS{
	MENU_EVENT_BTN_NONE,
	MENU_EVENT_BTN_NEXT,
	MENU_EVENT_BTN_ENTER,
	MENU_EVENT_BTN_EXIT,
	MENU_EVENT_BTN_CONFIRM,
}EMENU_EVENTS;

typedef struct tagMENU_CTX{
	EMENU_EVENTS 		eEVT_LAST;
//	EMENU_ITEM_TYPE		eCURRENT_ITEM_TYPE;
	uint8_t				auiLevels[8];
	int					iCurrentEditValue;
	int					iTmrEditPause;
	int					iTmrNoPressionEnter;
	uint8_t				uiCurrLevel;
	uint8_t				uiCurrentDigit;
	bool				bEditMode;
	bool				bEditModeACK;
	bool				bEditModeDigitACK;
	bool				bDisplayOn;
	bool				bIsDisplaingValues;
	bool				bIsEnterArmed;
}MENU_CTX;

extern MENU_CTX	stMENU_CTX;
extern const MENU_ITEM_MIN_MAX	stMENU_INT_MIN_MAX;



#define menuGetCurrentItemType()	(stMENU_CTX.eCURRENT_ITEM_TYPE)
#define menuIsDisplaingValues()		(stMENU_CTX.bIsDisplaingValues)

bool menuItemManageValue(int iMenuId,const char** pstrValue,bool bWrite);


void menuInit(void);
void menuDeinit(void);
void menuManage(const MENU_ITEM* pMenuBase, EMENU_EVENTS eEVT,uint16_t uiDeltaTLastCall,uint16_t uiPressionTime,char* strDISPLAY,uint8_t* puiDisplayOptions);
void menuEditModeExit(void);

typedef enum etagEMENU_ITEM_EVENT{
	MENU_ITEM_EVENT_REFRESH,
	MENU_ITEM_EVENT_BTN_ENTER,
	MENU_ITEM_EVENT_ENTER_FROM_PARENT,
	MENU_ITEM_EVENT_ENTER_FROM_SUB,
	MENU_ITEM_EVENT_EXIT_TO_SUB,
	MENU_ITEM_EVENT_EXIT
}EMENU_ITEM_EVENT;
bool menuItemEvent(EMENU_ITEM_EVENT eEvt,int iID,void* pValuePtr,uint8_t* puiDisplayOptions);

bool menuChangingEditValue(int iID,void* pValuePtr);
bool menuChangedEditValue(int iID,void* pValuePtr);

void menuPrintNumber_dec(char* strBuffer,int iValue, int iDPPos, int iDigitObscured,uint8_t* puiDisplayOptions);
void menuPrintNumber_hex(char* strBuffer,int iValue, int iDPPos, int iDigitObscured,uint8_t* puiDisplayOptions);
void menuPrintNumber_base(char* strBuffer,int iValue, int iDPPos, int iDigitObscured,int iBase);

#endif /* MENU_MENU_FUNC_H_ */
