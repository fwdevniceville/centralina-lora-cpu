/*
 * menu_func.c
 *
 *  Created on: 04 mar 2018
 *      Author: daniele_parise
 */


#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "menu_func.h"
#include <string.h>
#include <stdio.h>
#include "../Display/display.h"
#define USE_MENU_EDIT_SINGLE_DIGIT

MENU_CTX	stMENU_CTX;

void menuDeinit(void)
{
	stMENU_CTX.uiCurrLevel = 0;
}

void menuInit(void)
{
	stMENU_CTX.uiCurrLevel = 0;
}

void menuEditModeExit(void)
{
	stMENU_CTX.bEditMode = false;
	stMENU_CTX.bEditModeACK = false;
}

void menuReadValue(EMENU_EVENTS eEVT,uint16_t uiDeltaT,const MENU_ITEM* pMenuItem,char* strDISPLAY,uint8_t* puiDisplayOptions);
void menuEditValue(EMENU_EVENTS eEVT,uint16_t uiDeltaT,uint16_t uiPressionTime,const MENU_ITEM* pMenuItem,char* strDISPLAY,uint8_t* puiDisplayOptions);
int menuFindCurrentItem(const MENU_ITEM* pMenuBase,const MENU_ITEM** ppMenuItemFirst,const MENU_ITEM** ppMenuItem,int* piMenuItem);
int menuFindCurrentItem(const MENU_ITEM* pMenuBase,const MENU_ITEM** ppMenuItemFirst,const MENU_ITEM** ppMenuItem,int* piMenuItem)
{
	int iLevelScnr;
	int iMenuItemSel;
	iLevelScnr = 0;
	(*piMenuItem) = 0;
	(*ppMenuItem) = pMenuBase;
	while (iLevelScnr <= stMENU_CTX.uiCurrLevel)
	{
		(*ppMenuItem) = &pMenuBase[0];
		(*piMenuItem) = 0;
		iMenuItemSel = stMENU_CTX.auiLevels[iLevelScnr];
		while ((*piMenuItem) <= iMenuItemSel)
		{
			if ((*ppMenuItem)->eTYPE == MENU_ITEM_TYPE_NULL)
			{
				break;
			}
			if ((*piMenuItem) >= iMenuItemSel)
			{
				break;
			}
			(*ppMenuItem)++;
			(*piMenuItem)++;
		}
		stMENU_CTX.auiLevels[iLevelScnr] = (*piMenuItem);
		if (iLevelScnr >= stMENU_CTX.uiCurrLevel)
		{
			break;
		}
		if ((*ppMenuItem)->pSUB_MENU)
		{
			pMenuBase = (*ppMenuItem)->pSUB_MENU;
		}
		else
		{
			break;
		}
		iLevelScnr++;
	}

	(*ppMenuItemFirst) = pMenuBase;



	return iLevelScnr;

}

void menuManage(const MENU_ITEM* pMenuBase, EMENU_EVENTS eEVT,uint16_t uiDeltaTLastCall,uint16_t uiPressionTime,char* strDISPLAY,uint8_t* puiDisplayOptions)
{
	const MENU_ITEM* pMenuItemFirst;
	const MENU_ITEM* pMenuItem;

//	EMENU_ITEM_TYPE		eCURRENT_ITEM_TYPE;
	int iLevelScnr;
	int iMenuItem;
	bool	bDispValues;

	iLevelScnr = menuFindCurrentItem(pMenuBase,&pMenuItemFirst,&pMenuItem,&iMenuItem);


	if (stMENU_CTX.eEVT_LAST != eEVT)
	{
		EMENU_ITEM_EVENT	eITEM_EVENT;
		switch (eEVT)
		{
		case MENU_EVENT_BTN_NONE:
			{
			}
			break;
		case MENU_EVENT_BTN_NEXT:
			{
				if (
						(
								(pMenuItem->eTYPE != MENU_ITEM_TYPE_TEXT_STATIC)
								&&
								(pMenuItem->eTYPE != MENU_ITEM_TYPE_TEXT_DYNAMIC)
						)
						||
						((pMenuItem + 1)->eTYPE == MENU_ITEM_TYPE_NULL)
					)
				{
					stMENU_CTX.auiLevels[iLevelScnr] = 0;
					pMenuItem = &pMenuItemFirst[0];

				}
				else
				{
					pMenuItem++;
					iMenuItem++;
					stMENU_CTX.auiLevels[iLevelScnr] = iMenuItem;
				}
			}

			break;
		case MENU_EVENT_BTN_ENTER:
			{
				eITEM_EVENT = MENU_ITEM_EVENT_BTN_ENTER;
				if ((pMenuItem->eTYPE == MENU_ITEM_TYPE_TEXT_STATIC) && (pMenuItem->pSUB_MENU != NULL))
				{
					if (pMenuItem && (pMenuItem->iID))
					{
						menuItemEvent(MENU_ITEM_EVENT_EXIT_TO_SUB,pMenuItem->iID,(char*)pMenuItem->strLABEL_VALUE,puiDisplayOptions);
					}

					eITEM_EVENT = MENU_ITEM_EVENT_ENTER_FROM_PARENT;
					pMenuItem = pMenuItem->pSUB_MENU;
					stMENU_CTX.uiCurrLevel++;
					stMENU_CTX.auiLevels[stMENU_CTX.uiCurrLevel] = 0;
				}
				if (pMenuItem && (pMenuItem->iID))
				{
					menuItemEvent(eITEM_EVENT,pMenuItem->iID,(char*)pMenuItem->strLABEL_VALUE,puiDisplayOptions);
				}
			}
			break;
		case MENU_EVENT_BTN_CONFIRM:
			{
			}
			break;
		case MENU_EVENT_BTN_EXIT:
			{
				stMENU_CTX.bEditMode 	= false;
				stMENU_CTX.bEditModeACK = false;
				if (stMENU_CTX.uiCurrLevel > 0)
				{
					stMENU_CTX.uiCurrLevel--;
					if (pMenuItem->iID != 0)
					{
						menuItemEvent(MENU_ITEM_EVENT_EXIT,pMenuItem->iID,(char*)pMenuItem->strLABEL_VALUE,puiDisplayOptions);
					}
					iLevelScnr = menuFindCurrentItem(pMenuBase,&pMenuItemFirst,&pMenuItem,&iMenuItem);
					if (pMenuItem->iID != 0)
					{
						menuItemEvent(MENU_ITEM_EVENT_ENTER_FROM_SUB,pMenuItem->iID,(char*)pMenuItem->strLABEL_VALUE,puiDisplayOptions);
					}
				}
			}
			break;
		}
	}
	bDispValues = false;
//	eCURRENT_ITEM_TYPE = MENU_ITEM_TYPE_NULL;
	if (pMenuItem != NULL)
	{
//		eCURRENT_ITEM_TYPE = pMenuItem->eTYPE;
		menuItemEvent(MENU_ITEM_EVENT_REFRESH,pMenuItem->iID, (char*)pMenuItem->strLABEL_VALUE,puiDisplayOptions);
		switch (pMenuItem->eTYPE)
		{
		default:
			break;
		case MENU_ITEM_TYPE_TEXT_DYNAMIC:
			bDispValues = true;
		case MENU_ITEM_TYPE_TEXT_STATIC:
			strcpy(strDISPLAY,pMenuItem->strLABEL_VALUE);
			break;
		case MENU_ITEM_TYPE_TEXT_READ:
		case MENU_ITEM_TYPE_INT_READ_DEC:
		case MENU_ITEM_TYPE_INT_READ_HEX:
			menuReadValue(eEVT,uiDeltaTLastCall,pMenuItem,strDISPLAY,puiDisplayOptions);
			bDispValues = true;
			break;
		case MENU_ITEM_TYPE_INT_RW_DEC:
		case MENU_ITEM_TYPE_INT_RW_HEX:
			menuEditValue(eEVT,uiDeltaTLastCall,uiPressionTime,pMenuItem,strDISPLAY,puiDisplayOptions);
			bDispValues = true;
			break;
		}
	}
	stMENU_CTX.bIsDisplaingValues = bDispValues;
//	stMENU_CTX.eCURRENT_ITEM_TYPE = pMenuItem->eTYPE;
	stMENU_CTX.eEVT_LAST = eEVT;
}
const MENU_ITEM_MIN_MAX	stMENU_INT_MIN_MAX = {0x0000,0xFFFF,-1};

void menuReadValue(EMENU_EVENTS eEVT,uint16_t uiDeltaT,const MENU_ITEM* pMenuItem,char* strDISPLAY,uint8_t* puiDisplayOptions)
{
	const MENU_ITEM_MIN_MAX*			pMIN_MAX;
	int iDPPos;
	iDPPos = -1;
	pMIN_MAX = pMenuItem->pMIN_MAX;
	if (pMIN_MAX == NULL)
	{
		pMIN_MAX = &stMENU_INT_MIN_MAX;
	}
	iDPPos = pMIN_MAX->iDPPos;
	switch (pMenuItem->eTYPE)
	{
	default:
		break;
	case MENU_ITEM_TYPE_TEXT_READ:
		strncpy(strDISPLAY,pMenuItem->strLABEL_VALUE,6);
		break;
	case MENU_ITEM_TYPE_INT_READ_DEC:
		{
			int	iVALUE;
			iVALUE = *(int*)pMenuItem->strLABEL_VALUE;
			menuPrintNumber_dec(strDISPLAY,iVALUE,iDPPos,-1,puiDisplayOptions);
		}
		break;
	case MENU_ITEM_TYPE_INT_READ_HEX:
		{
			int	iVALUE;
			iVALUE = *(int*)pMenuItem->strLABEL_VALUE;
			menuPrintNumber_hex(strDISPLAY,iVALUE,-1,-1,puiDisplayOptions);
		}
		break;
	}
}


void menuEditValue(EMENU_EVENTS eEVT,uint16_t uiDeltaT,uint16_t uiPressionTime,const MENU_ITEM* pMenuItem,char* strDISPLAY,uint8_t* puiDisplayOptions)
{
	const MENU_ITEM_MIN_MAX*			pMIN_MAX;
	int 								iValue;
	int iDPPos;
	iDPPos = -1;
	iValue = 0;

	pMIN_MAX = pMenuItem->pMIN_MAX;
	if (pMIN_MAX == NULL)
	{
		pMIN_MAX = &stMENU_INT_MIN_MAX;
	}

	iDPPos = pMIN_MAX->iDPPos;

	if ((eEVT == MENU_EVENT_BTN_CONFIRM) && (uiPressionTime > 1000))
	{
		if (!stMENU_CTX.bEditModeACK)
		{
			if (stMENU_CTX.bEditMode)
			{
				void* pModified;
				pModified = NULL;
				switch (pMenuItem->eTYPE)
				{
				default:
					break;
				case MENU_ITEM_TYPE_INT_RW_DEC:
				case MENU_ITEM_TYPE_INT_RW_HEX:
					{
						if (*(int*)pMenuItem->strLABEL_VALUE != stMENU_CTX.iCurrentEditValue)
						{
							pModified = (void*)pMenuItem->strLABEL_VALUE;
						}
					}
					break;
				}
				if (pModified)
				{
					if (menuChangingEditValue(pMenuItem->iID,pModified))
					{
						*(int*)pMenuItem->strLABEL_VALUE = stMENU_CTX.iCurrentEditValue;
						menuChangedEditValue(pMenuItem->iID,pModified);
					}
				}
				stMENU_CTX.bEditMode = false;
			}
			else
			{
#ifdef USE_MENU_EDIT_SINGLE_DIGIT
				stMENU_CTX.uiCurrentDigit = 0;
#endif
				stMENU_CTX.iCurrentEditValue = 0;
				switch (pMenuItem->eTYPE)
				{
				default:
					break;
				case MENU_ITEM_TYPE_INT_RW_DEC:
				case MENU_ITEM_TYPE_INT_RW_HEX:
					stMENU_CTX.iCurrentEditValue = * (int*)pMenuItem->strLABEL_VALUE;
					break;
				}
				stMENU_CTX.bEditMode = true;
				stMENU_CTX.iTmrEditPause = 1000;
			}
			stMENU_CTX.bEditModeACK = true;
		}
	}
	else
	{
		stMENU_CTX.bEditModeACK = false;
	}
	if (eEVT == MENU_EVENT_BTN_ENTER)
	{
		stMENU_CTX.iTmrNoPressionEnter = 0;
	}
	else
	{
		stMENU_CTX.iTmrNoPressionEnter += uiDeltaT;
	}

	if (stMENU_CTX.bEditMode)
	{
#ifdef USE_MENU_EDIT_SINGLE_DIGIT
		uint8_t	auiDigitsValue[4];
		int		iTMP;
		int iMaxDigit;
		iMaxDigit = 10;
		switch (pMenuItem->eTYPE)
		{
		default:
		case MENU_ITEM_TYPE_INT_RW_DEC:
			break;
		case MENU_ITEM_TYPE_INT_RW_HEX:
			iMaxDigit = 16;
			break;
		}


		iTMP = stMENU_CTX.iCurrentEditValue;
		auiDigitsValue[0] = iTMP % iMaxDigit;
		iTMP /= iMaxDigit;
		auiDigitsValue[1] = iTMP % iMaxDigit;
		iTMP /= iMaxDigit;
		auiDigitsValue[2] = iTMP % iMaxDigit;
		iTMP /= iMaxDigit;
		auiDigitsValue[3] = iTMP % iMaxDigit;

		if (eEVT == MENU_EVENT_BTN_NEXT)
		{
			if (!stMENU_CTX.bEditModeDigitACK)
			{
				stMENU_CTX.bEditModeDigitACK = true;
				stMENU_CTX.uiCurrentDigit++;
				if (stMENU_CTX.uiCurrentDigit > 2)
				{
					stMENU_CTX.uiCurrentDigit = 0;
				}
			}
		}
		else
		{
			stMENU_CTX.bEditModeDigitACK = false;
		}
#endif

		if (stMENU_CTX.iTmrEditPause > 0)
		{
			stMENU_CTX.iTmrEditPause -= uiDeltaT;
		}
		else
		{
			int iIncrement;
			iIncrement = 0;
			if (eEVT == MENU_EVENT_BTN_ENTER)
			{
#ifdef USE_MENU_EDIT_SINGLE_DIGIT
				if (stMENU_CTX.bIsEnterArmed)
				{
					iIncrement=1;
				}
				stMENU_CTX.bIsEnterArmed = false;
#else
				int iDeltaRange;
//				iDeltaRange = pMIN_MAX->iMAX - pMIN_MAX->iMIN;
				iDeltaRange = pMIN_MAX->iMAX - stMENU_CTX.iCurrentEditValue;
				if ((iDeltaRange > 10) && (uiPressionTime > 1000))
				{
					if ((uiPressionTime > 10000) && (iDeltaRange > 1000))
					{
						iIncrement=100;
					}
					else
					if ((uiPressionTime > 5000) && (iDeltaRange > 100))
					{
						iIncrement=10;
					}
					else
					if ((uiPressionTime > 1000))
					{
						iIncrement = 1;
					}
					else
					if (uiPressionTime > 100)
					{
						iIncrement=1;
					}
				}
				else
				{
					if (stMENU_CTX.bIsEnterArmed)
					{
						iIncrement=1;
					}
					stMENU_CTX.bIsEnterArmed = false;
				}
#endif
			}
			else
			{
				stMENU_CTX.bIsEnterArmed = true;
			}

#ifdef USE_MENU_EDIT_SINGLE_DIGIT
			{

				auiDigitsValue[stMENU_CTX.uiCurrentDigit]+=iIncrement;
				if (auiDigitsValue[stMENU_CTX.uiCurrentDigit] >= iMaxDigit)
				{
					auiDigitsValue[stMENU_CTX.uiCurrentDigit] = 0;
				}

				stMENU_CTX.iCurrentEditValue = auiDigitsValue[3];
				stMENU_CTX.iCurrentEditValue *= iMaxDigit;
				stMENU_CTX.iCurrentEditValue += auiDigitsValue[2];
				stMENU_CTX.iCurrentEditValue *= iMaxDigit;
				stMENU_CTX.iCurrentEditValue += auiDigitsValue[1];
				stMENU_CTX.iCurrentEditValue *= iMaxDigit;
				stMENU_CTX.iCurrentEditValue += auiDigitsValue[0];
			}
#else
			stMENU_CTX.iCurrentEditValue += iIncrement;
#endif
			if (stMENU_CTX.iCurrentEditValue > pMIN_MAX->iMAX)
			{
				stMENU_CTX.iCurrentEditValue = pMIN_MAX->iMIN;
			}
			else
			if (stMENU_CTX.iCurrentEditValue < pMIN_MAX->iMIN)
			{
				stMENU_CTX.iCurrentEditValue = pMIN_MAX->iMIN;
			}
		}

		iValue = stMENU_CTX.iCurrentEditValue;
		if (stMENU_CTX.iTmrNoPressionEnter > 500)
		{
			stMENU_CTX.bDisplayOn = !stMENU_CTX.bDisplayOn;
		}
		else
		{
			stMENU_CTX.bDisplayOn = true;
		}
	}
	else
	{
		switch (pMenuItem->eTYPE)
		{
		default:
			break;
		case MENU_ITEM_TYPE_INT_RW_DEC:
		case MENU_ITEM_TYPE_INT_RW_HEX:
			iValue = *(int*)pMenuItem->strLABEL_VALUE;
			break;
		}
		stMENU_CTX.bDisplayOn = true;
	}

#ifdef USE_MENU_EDIT_SINGLE_DIGIT
	{
		int iObscuredDigit;
		iObscuredDigit = stMENU_CTX.uiCurrentDigit;
		if (stMENU_CTX.bDisplayOn)
		{
			iObscuredDigit = -1;
		}
		switch (pMenuItem->eTYPE)
		{
		default:
			break;
		case MENU_ITEM_TYPE_INT_RW_DEC:
			menuPrintNumber_dec(strDISPLAY,iValue,iDPPos,iObscuredDigit, puiDisplayOptions);
			break;
		case MENU_ITEM_TYPE_INT_RW_HEX:
			menuPrintNumber_hex(strDISPLAY,iValue,iDPPos,iObscuredDigit, puiDisplayOptions);
			break;
		}
	}
#else
	if (stMENU_CTX.bDisplayOn)
	{
		switch (pMenuItem->eTYPE)
		{
		default:
			break;
		case MENU_ITEM_TYPE_INT_RW_DEC:
			menuPrintNumber_dec(strDISPLAY,iValue,iDPPos,-1);
			break;
		case MENU_ITEM_TYPE_INT_RW_HEX:
			menuPrintNumber_hex(strDISPLAY,iValue,iDPPos,-1);
			break;
		}

//		sprintf(strDISPLAY,"%d",iValue);
	}
	else
	{
		strcpy(strDISPLAY,"");
	}
#endif

}

const char aMenucharMap[] =
{
		'0',
		'1',
		'2',
		'3',
		'4',
		'5',
		'6',
		'7',
		'8',
		'9',
		'A',
		'B',
		'C',
		'D',
		'E',
		'F'
};


#if 1

void menuPrintNumber_base(char* strBuffer,int iValueIn, int iDPPos, int iDigitObscured,int iBase)
{
	int ipos;
	int idiv;
	int idigit;
	int iNdigit;
	int iValue;
	char	chDig;
	bool bWriteZero;
	iNdigit = 3;
	bWriteZero = false;
	idiv = 1;
	ipos = 0;
	while (ipos < iNdigit)
	{
		idiv*=iBase;
		ipos++;
	}

	iValue = iValueIn;
	ipos = 0;
	if (iValue < 0)
	{
		strBuffer[0] = '-';
		ipos = 1;
		if (iDPPos >= 0)
		{
			iDPPos++;
		}
		iValue = -iValue;
	}

	while (idiv >= 1)
	{
		if (ipos == iDPPos)
		{
			strBuffer[ipos] = '.';
		}
		else
		{
			idigit = iValue / idiv;
			idigit %= iBase;
			if (iNdigit == iDigitObscured)
			{
				strBuffer[ipos] =' ';
			}
			else
			{
				strBuffer[ipos] =aMenucharMap[idigit];
			}
			idiv /=iBase;
			iNdigit--;
		}
		ipos++;
	}
	strBuffer[ipos] = 0;
}

void menuPrintNumber_dec(char* strBuffer,int iValue, int iDPPos, int iDigitObscured,uint8_t* puiDisplayOptions)
{
	if (iValue < 0)
	{
		iValue = -iValue;
		*puiDisplayOptions |= DISPLAY_OPTIONS_SHOW_MINUS;
	}

	menuPrintNumber_base(strBuffer,iValue,iDPPos,iDigitObscured,10);
}


void menuPrintNumber_hex(char* strBuffer,int iValue, int iDPPos, int iDigitObscured,uint8_t* puiDisplayOptions)
{
	iValue &= 0xFFFF;
	menuPrintNumber_base(strBuffer,iValue,iDPPos,iDigitObscured,16);
}

#else
void menuPrintNumber_dec(char* strBuffer,int iValue, int iDPPos, int iDigitObscured)
{
	int ipos;
	int idiv;
	int idigit;
	int iNdigit;
	char	chDig;
	bool bWriteZero;
	iNdigit = 3;
	ipos = 0;
	bWriteZero = false;
	idiv = 1000;
	while (idiv >= 1)
	{
		if (ipos == iDPPos)
		{
			strBuffer[ipos] = '.';
		}
		else
		{
			idigit = iValue / idiv;
			idigit %= 10;
			if (iNdigit == iDigitObscured)
			{
				strBuffer[ipos] =' ';
			}
			else
			{
				strBuffer[ipos] ='0' + idigit;
			}
			idiv /=10;
			iNdigit--;
		}
		ipos++;
	}
	strBuffer[ipos] = 0;
}

void menuPrintNumber_hex(char* strBuffer,int iValue, int iDPPos, int iDigitObscured)
{
	int ipos;
	int iNdigit;
	ipos = 3;
	iNdigit = 0;
	if (iDPPos >=0)
	{
		ipos++;
	}
	while (ipos >= 0)
	{
		if (ipos == iDPPos)
		{
			strBuffer[ipos] = '.';
		}
		else
		{
			if (iNdigit == iDigitObscured)
			{
				strBuffer[ipos] =' ';
			}
			else
			{
				strBuffer[ipos] = aMenucharMap[iValue & 0x0F];
			}
			iValue >>=4;
			iNdigit++;
		}
		ipos--;
	}
	strBuffer[4] = 0;
}
#endif
__weak bool menuChangingEditValue(int iID,void* pValuePtr)
{

	return true;
}

__weak bool menuChangedEditValue(int iID,void* pValuePtr)
{

	return true;
}
__weak bool menuItemEvent(EMENU_ITEM_EVENT eEvt,int iID,void* pValuePtr,uint8_t* puiDisplayOptions)
{

	return true;
}
