/*
 * SystemFunctions.c
 *
 *  Created on: 21 feb 2018
 *      Author: daniele_parise
 */



#include <stdbool.h>
#include "cmsis_os.h"
#include "hal_inc.h"

#include "Tasks/taskScheduler.h"
#include "Tasks/taskComunication.h"
#include "SystemFunctions.h"
#include "rtc.h"


void systemBuckBoostEnable(bool bEnable)
{
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, bEnable ? GPIO_PIN_SET : GPIO_PIN_RESET);
}


void SystemSettings(void)
{
}

void HW_RTC_IrqHandler(void);
void RTC_Alarm_IRQHandler(void)
{
//	HW_RTC_IrqHandler();
//	HAL_RTC_AlarmIRQHandler(&hrtc);
//	HAL_RTC_AlarmIRQHandler(&RtcHandle);
}

void TimerIrqHandler(void);

void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc)
{
  /* Prevent unused argument(s) compilation warning */
	UNUSED(hrtc);
//  taskInterfaceButtonPressed(TASKINTERFACE_BTN_EVT_TIME);
//	TimerIrqHandler( );

//	HW_RTC_IrqHandler();


  /* NOTE : This function should not be modified, when the callback is needed,
            the HAL_RTC_AlarmAEventCallback could be implemented in the user file
   */
}
#include <time.h>
#include <string.h>

#define DIVC(X,N)   ( ( (X) + (N) -1 ) / (N) )

#define  DAYS_IN_LEAP_YEAR (uint32_t) 366
#define  DAYS_IN_YEAR      (uint32_t) 365
#define  DAYS_IN_MONTH_CORRECTION_NORM     ((uint32_t) 0x99AAA0 )
#define  DAYS_IN_MONTH_CORRECTION_LEAP     ((uint32_t) 0x445550 )

uint64_t systemGetCurrentDateTime_ms(DateTime* t)
{
	uint64_t uiRet;
	DateTime	stDateInt;
	if (t == NULL)
	{
		t = &stDateInt;
	}
	uiRet = systemGetCurrentDateTime(t);
	uiRet*=1000;
	uiRet+= t->tm_msec;
	return uiRet;
}

uint64_t systemGetCurrentDateTime(DateTime* t)
{
	RTC_TimeTypeDef 	sTime;
	RTC_DateTypeDef 	sDate;
	uint32_t			first_read;
	DateTime	stDateInt;
	if (t == NULL)
	{
		t = &stDateInt;
	}

	sTime.SubSeconds = -1;
	do
	{
		first_read = sTime.SubSeconds;
		HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
		HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
	}
	while (first_read != sTime.SubSeconds);

	{
		int			iDayYear;
		uint32_t 	correction;
		correction = ( (sDate.Year % 4) == 0 ) ? DAYS_IN_MONTH_CORRECTION_LEAP : DAYS_IN_MONTH_CORRECTION_NORM ;

		iDayYear =( DIVC( (t->tm_mon -1 )*(30+31) ,2 ) - (((correction>> ((t->tm_mon - 1)*2) )&0x3)));

		iDayYear += (t->tm_mday - 1);

		memset(t,0,sizeof(struct tm));
		t->tm_year 	= sDate.Year + 2000;
		t->tm_mon 	= sDate.Month;
		t->tm_yday 	= iDayYear;
		t->tm_mday 	= sDate.Date;
		t->tm_wday 	= sDate.WeekDay;

		t->tm_hour	= sTime.Hours;
		t->tm_min 	= sTime.Minutes;
		t->tm_sec 	= sTime.Seconds;

// 		  msec = 1000 * (PREDIV_S - RTC_RTC_SSR.SS) / (PREDIV_S + 1)

		  /* Get subseconds structure field from the corresponding register*/
		correction = (uint32_t)(hrtc.Instance->SSR);
		  /* Get SecondFraction structure field from the corresponding register field*/
		iDayYear = (hrtc.Instance->PRER & RTC_PRER_PREDIV_S);

		correction = (iDayYear - correction);
		correction *= 1000;
		correction /= (iDayYear + 1);

		t->tm_msec	= correction;
		return systemDateTime2secondsFrom2K(t);
	}
	return 0;
}


uint32_t systemYear2DaysFrom2K(int iYear)
{
	uint32_t 	uiRetDays;
	iYear -= 2000;
	uiRetDays= DIVC( (DAYS_IN_YEAR*3 + DAYS_IN_LEAP_YEAR)* iYear , 4);
	return uiRetDays;
}

uint64_t systemYearYDay2secondsFrom2K(int iYear,int iYday)
{
	uint64_t 	uiRetSeconds;

	uiRetSeconds= systemYear2DaysFrom2K(iYear);
	uiRetSeconds+=iYday;
	uiRetSeconds*=SECONDS_IN_1DAY;
	return uiRetSeconds;
}

uint64_t systemYearMonthDay2secondsFrom2K(int iYear, int iMonth, int iMday)
{
	uint64_t 	uiRetSeconds;
	uint32_t 	correction;
	int			iCentYear;

	iCentYear = iYear - 2000;
	uiRetSeconds = 0;
	uiRetSeconds= DIVC( (DAYS_IN_YEAR*3 + DAYS_IN_LEAP_YEAR)* iCentYear , 4);

	correction = ( (iCentYear % 4) == 0 ) ? DAYS_IN_MONTH_CORRECTION_LEAP : DAYS_IN_MONTH_CORRECTION_NORM ;

	iMonth--;

	uiRetSeconds +=( DIVC( (iMonth )*(30 + 31) ,2 ) - (((correction >> ((iMonth)*2) ) & 0x3)));

	uiRetSeconds += (iMday - 1);

	uiRetSeconds*=SECONDS_IN_1DAY;

	return uiRetSeconds;
}

uint64_t systemDateTime2secondsFrom2K(DateTime* t)
{
	uint64_t 	uiRetSeconds;
	uint32_t 	correction;
	int			iYear;

	iYear = t->tm_year - 2000;
	uiRetSeconds = 0;
	uiRetSeconds= DIVC( (DAYS_IN_YEAR*3 + DAYS_IN_LEAP_YEAR)* iYear , 4);

	correction = ( (iYear % 4) == 0 ) ? DAYS_IN_MONTH_CORRECTION_LEAP : DAYS_IN_MONTH_CORRECTION_NORM ;

	uiRetSeconds +=( DIVC( (t->tm_mon -1 )*(30+31) ,2 ) - (((correction>> ((t->tm_mon - 1)*2) )&0x3)));

	uiRetSeconds += (t->tm_mday - 1);

	  /* convert from days to seconds */
	uiRetSeconds *= SECONDS_IN_1DAY;

	uiRetSeconds += ( ( uint32_t )t->tm_sec +
	                     ( ( uint32_t )t->tm_min * SECONDS_IN_1MINUTE ) +
	                     ( ( uint32_t )t->tm_hour * SECONDS_IN_1HOUR ) ) ;

	return uiRetSeconds;

}

void systemDateTimeGet(SYS_DATETIME_SET_GET* pSET)
{
	RTC_TimeTypeDef RTC_TimeStruct = {0};
	RTC_DateTypeDef RTC_DateStruct = {0};
	HAL_RTC_GetTime(&hrtc, &RTC_TimeStruct, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &RTC_DateStruct, RTC_FORMAT_BIN);

	pSET->Year		= RTC_DateStruct.Year;
	pSET->Month		= RTC_DateStruct.Month;
	pSET->MDay		= RTC_DateStruct.Date;
	pSET->WDay		= RTC_DateStruct.WeekDay;
	pSET->Hour		= RTC_TimeStruct.Hours;
	pSET->Minutes	= RTC_TimeStruct.Minutes;

}

bool systemDateTimeSet(const SYS_DATETIME_SET_GET* pSET)
{
	if (
			(SYS_TEST_IN(pSET->Year,0,99))
			&&
			(SYS_TEST_IN(pSET->Month,1,12))
			&&
			(SYS_TEST_IN(pSET->MDay,1,31))
			&&
			(SYS_TEST_IN(pSET->WDay,1,7))
			&&
			(SYS_TEST_IN(pSET->Hour,0,23))
			&&
			(SYS_TEST_IN(pSET->Minutes,0,59))
		)
	{
		RTC_TimeTypeDef RTC_TimeStruct = {0};
		RTC_DateTypeDef RTC_DateStruct = {0};


		//Monday 1st January 2016
		RTC_DateStruct.Year 	= pSET->Year;
		RTC_DateStruct.Month 	= pSET->Month;
		RTC_DateStruct.Date 	= pSET->MDay;
		RTC_DateStruct.WeekDay 	= pSET->WDay ? pSET->WDay : RTC_WEEKDAY_SUNDAY;
		HAL_RTC_SetDate(&hrtc, &RTC_DateStruct, RTC_FORMAT_BIN);

		//at 0:0:0
		RTC_TimeStruct.Hours 	= pSET->Hour;
		RTC_TimeStruct.Minutes	= pSET->Minutes;

//	RTC_TimeStruct.Seconds = 0;
//	RTC_TimeStruct.TimeFormat = 0;
//	RTC_TimeStruct.SubSeconds = 0;
//	RTC_TimeStruct.StoreOperation = RTC_DAYLIGHTSAVING_NONE;
//	RTC_TimeStruct.DayLightSaving = RTC_STOREOPERATION_RESET;

		HAL_RTC_SetTime(&hrtc, &RTC_TimeStruct, RTC_FORMAT_BIN);

		rtcSetSetted();

		return true;
	}
	return false;
}

