/*
 * gencom_hw.c
 *
 *  Created on: 20 dic 2018
 *      Author: sa2025
 */



#include <stdbool.h>
#include <stdint.h>
#include "../gencom/gencom.h"
#include "../gencom/gencomhl.h"
#include "../intcom/intcom.h"
#include <string.h>
#include "../hwbrd/hw_bridge.h"
#include "hal_inc.h"
#include "../FeeRTOS/FRT_BoardFunctions.h"



void gencom_hw_lpm_stop(GENCOM_CTX* pGENCOM_CTX, bool bTX,bool bEnable)
{
/*
	LPM_Id_t		eId;
	LPM_SetMode_t	eMode;

	if (pGENCOM_CTX == &stEXTCOM_CTX)
	{
		eId = bTX ? LPM_EXTCOM_TX_Id : LPM_EXTCOM_RX_Id;
	}
	else
//	if (pGENCOM_CTX == &stINTCOM_CTX)
	{
		eId = bTX ? LPM_INTCOM_TX_Id : LPM_INTCOM_RX_Id;
	}

	eMode = bEnable ? LPM_Enable : LPM_Disable;


	LPM_SetStopMode(eId , eMode );
*/
	EFRT_TASK_ORDER_ID eTaskOrderId;
	eTaskOrderId = FRT_TASK_ORDER_INTCOM_RX;
	if (bTX)
	{
		eTaskOrderId = FRT_TASK_ORDER_INTCOM_TX;
	}
	FRT_SetRunningRQST(eTaskOrderId,!bEnable);
}

void gencomhlFlushSendRequest(GENCOMHL_CTX* pGENCOMHL_CTX)
{
	/*
	if (pGENCOMHL_CTX == &stEXTCOMHL_CTX)
	{
		extcomhlFlushSendRequest();
	}
	else
	if (pGENCOMHL_CTX == &stINTCOMHL_CTX)
	{
		intcomhlFlushSendRequest();
	}
	*/
//	intcomhlFlushSendRequest();
}


//#define DEBUG_UART_STOP_MODE

void gencom_hwSignalStopMode(void)
{
#ifdef DEBUG_UART_STOP_MODE
	{
	    GPIO_InitTypeDef initStruct = { 0 };
	    __HAL_RCC_GPIOB_CLK_ENABLE();

	    CLEAR_BIT(GPIOB->ODR,GPIO_PIN_5);


	    initStruct.Mode 	=GPIO_MODE_ANALOG;
	    initStruct.Pull 	= GPIO_NOPULL;
	    initStruct.Speed 	= GPIO_SPEED_HIGH;
	    initStruct.Pin 	= GPIO_PIN_5;

	    HAL_GPIO_Init( GPIOB, &initStruct );
	}
#endif
}

void gencom_hwSignalResponse(void)
{
#ifdef DEBUG_UART_STOP_MODE
	{
	    GPIO_InitTypeDef initStruct = { 0 };
	    __HAL_RCC_GPIOB_CLK_ENABLE();

	    initStruct.Mode 	= GPIO_MODE_OUTPUT_PP;
	    initStruct.Pull 	= GPIO_NOPULL;
	    initStruct.Speed 	= GPIO_SPEED_HIGH;
	    initStruct.Pin 	= GPIO_PIN_5 ;

	    HAL_GPIO_Init( GPIOB, &initStruct );
	    SET_BIT(GPIOB->ODR,GPIO_PIN_5);

	}
#endif
}
