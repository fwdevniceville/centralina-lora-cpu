/*
 * coildrv.c
 *
 *  Created on: 27 apr 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include "coildrv.h"
#include "tim.h"

#include "stm32l0xx_hal.h"


void coildrvInit(void)
{
	coildrvInitIO(true);
	MX_TIM22_Init();
	WRITE_REG(TIM22->CCR2,0);
	WRITE_REG(TIM22->ARR,1023);
	HAL_TIM_PWM_Start(&htim22,TIM_CHANNEL_2);

}

#define COIL_DRV_GPIOA_PINS (GPIO_PIN_10)
#define COIL_DRV_GPIOB_PINS (GPIO_PIN_7)
#define COIL_DRV_GPIOD_PINS (GPIO_PIN_2)
#define COIL_DRV_GPIOE_PINS (GPIO_PIN_9	| GPIO_PIN_10 | GPIO_PIN_12	| GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15)
#define COIL_DRV_GPIOH_PINS (GPIO_PIN_9 | GPIO_PIN_10)

void coildrvInitIO(bool bON)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	static bool bInitiated = false;
	bool	bInitDevice;
	bInitDevice = true;


	if (bON)
	{
		if (bInitiated)
		{
			bInitDevice = false;

		}
		else
		{
			bInitiated = true;
			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
		}
	}
	else
	{
		bInitiated = false;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	}

	if (bInitDevice)
	{

		__HAL_RCC_GPIOH_CLK_ENABLE();

		HAL_GPIO_WritePin(GPIOA, COIL_DRV_GPIOA_PINS, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOB, COIL_DRV_GPIOB_PINS, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOD, COIL_DRV_GPIOD_PINS, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOE, COIL_DRV_GPIOE_PINS, GPIO_PIN_RESET);

		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;

		GPIO_InitStruct.Pin = COIL_DRV_GPIOA_PINS;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = COIL_DRV_GPIOB_PINS;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = COIL_DRV_GPIOD_PINS;
		HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = COIL_DRV_GPIOE_PINS;
		HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

		if (bInitiated)
		{
			GPIO_InitStruct.Mode 	= GPIO_MODE_INPUT;
			GPIO_InitStruct.Pull 	= GPIO_PULLUP;
		}
		GPIO_InitStruct.Pin 	= COIL_DRV_GPIOH_PINS;
		HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

		if (!bInitiated)
		{
			__HAL_RCC_GPIOH_CLK_DISABLE();
		}
	}
}

void coildrvFree(void)
{
	HAL_TIM_PWM_Stop(&htim22,TIM_CHANNEL_2);
	HAL_TIM_Base_DeInit(&htim22);
	coildrvInitIO(false);
}

void coildrvPowerLoad(int imVolts)
{
	int iPWM;

	// Max out = 1,229 * (160K/10K + 1) = 20.893V
	if (imVolts > 20893)
	{
		imVolts = 20893;
	}

	if (imVolts < 0)
	{
		imVolts = 0;
	}

	iPWM = (imVolts * 1023) / 20893;

	WRITE_REG(TIM22->CCR2,iPWM);
}

bool coildrvIsLatchingEV(void)
{
	int iCNTR;
	int iCNTR_TOT;

	iCNTR = 0;
	iCNTR_TOT = 1000;
	while (iCNTR_TOT > 0)
	{
		if (!READ_BIT(GPIOH->IDR,GPIO_PIN_9))
		{
			iCNTR++;
		}
		iCNTR_TOT--;
	}
	if (iCNTR > 500)
	{
		return true;
	}
	return false;
}

bool coildrvIsBoostEV(void)
{
	int iCNTR;
	int iCNTR_TOT;

	iCNTR = 0;
	iCNTR_TOT = 1000;
	while (iCNTR_TOT > 0)
	{
		if (!READ_BIT(GPIOH->IDR,GPIO_PIN_10))
		{
			iCNTR++;
		}
		iCNTR_TOT--;
	}
	if (iCNTR > 500)
	{
		return true;
	}
	return false;
}


void coildrv3V3_PWR_VLV_ON(void)
{
	CLEAR_BIT(GPIOA->ODR,GPIO_ODR_OD10);
}


void coildrv3V3_PWR_VLV_OFF(void)
{
	SET_BIT(GPIOA->ODR,GPIO_ODR_OD10);
}

void coildrv3V3_PWR_COM_ON(void)
{
	CLEAR_BIT(GPIOB->ODR,GPIO_PIN_7);
}


void coildrv3V3_PWR_COM_OFF(void)
{
	SET_BIT(GPIOB->ODR,GPIO_PIN_7);
}


void coildrvCOMM_HI(void)
{
	SET_BIT(GPIOE->ODR,GPIO_ODR_OD14);
}

void coildrvCOMM_LO(void)
{
	CLEAR_BIT(GPIOE->ODR,GPIO_ODR_OD14);
}

void coildrvPWR_HI(void)
{
	SET_BIT(GPIOE->ODR,GPIO_ODR_OD15);
}

void coildrvPWR_LO(void)
{
	CLEAR_BIT(GPIOE->ODR,GPIO_ODR_OD15);
}

void coildrvPWR2_EXT_HI(void)
{
	SET_BIT(GPIOD->ODR,GPIO_ODR_OD2);
}

void coildrvPWR2_EXT_LO(void)
{
	CLEAR_BIT(GPIOD->ODR,GPIO_ODR_OD2);
}


void coildrvOUT_POL(unsigned uiMASK)
{
	unsigned uiOUT_MASK;
	uiOUT_MASK  = 0;
	if (uiMASK & 0x01)	uiOUT_MASK  |= GPIO_ODR_OD9;
	if (uiMASK & 0x02)	uiOUT_MASK  |= GPIO_ODR_OD10;
	if (uiMASK & 0x04)	uiOUT_MASK  |= GPIO_ODR_OD12;
	if (uiMASK & 0x08)	uiOUT_MASK  |= GPIO_ODR_OD13;

	MODIFY_REG(GPIOE->ODR,
			GPIO_ODR_OD9 | GPIO_ODR_OD10 | GPIO_ODR_OD12 | GPIO_ODR_OD13,
			uiOUT_MASK
			);
}
