/*
 * adcreader.h
 *
 *  Created on: 28 apr 2018
 *      Author: daniele_parise
 */

#ifndef INOUT_ADCREADER_H_
#define INOUT_ADCREADER_H_


typedef enum etagEADCREADER_IN0_MODE{
	ADCREADER_IN0_MODE_IN,
	ADCREADER_IN0_MODE_ADC_5V,
	ADCREADER_IN0_MODE_ADC_4_20mA,
	ADCREADER_IN0_MODE_ADC_10V
}EADCREADER_IN0_MODE;

void adcreaderInit(EADCREADER_IN0_MODE eIN0MODE);
void adcreaderFree(void);

void adcreaderReadIN0Select(EADCREADER_IN0_MODE eMODE);
void adcreaderEnablePullUps(unsigned uiINMASK);


void adcreaderReadVBattEnable(bool bEnable);
void adcreaderReadCurrentEnable(bool bEnable);

bool adcreaderIsReadCurrentEnabled(void);
bool adcreaderIsReadVBattEnabled(void);
bool adcreaderIsVEXTPresent(void);




typedef enum etagEADCREADER_CHANNEL{
	ADCREADER_NULL,
	ADCREADER_IN0,
	ADCREADER_IN4,
	ADCREADER_VBATT,
	ADCREADER_CURRENT,
	ADCREADER_TSENSE,
	ADCREADER_VREFF
}EADCREADER_CHANNEL;

int adcreaderReadChannel(EADCREADER_CHANNEL eCHANNEL,bool bVREFIN_CORRECTION);
int adcreaderReadChannelMean(EADCREADER_CHANNEL eCH,int iSam_ms, int iDropSams, int* piDroppedMean,bool bVREFIN_CORRECTION);
int adcreaderReadCurrent(int iSam_ms, int iDropSams,int* piDroppedMean);
int adcreaderReadVBatt(int iSam_ms, int iDropSams,int* piDroppedMean);


#define ADCREADER_CHANNEL_FIRST_CURRENT_SAMPLES_BUFFER	(20)

typedef struct tagADCREADER_CHANNEL_CTX{
	struct {
		EADCREADER_CHANNEL eCH;
		uint8_t uiSam_ms;
		uint8_t uiDropSams;
		bool 	bVREFIN_CORRECTION;
	}stSettings;
	struct {
#ifdef DEBUG
		uint16_t	auiValues[ADCREADER_CHANNEL_FIRST_CURRENT_SAMPLES_BUFFER];
#endif
		uint32_t	uiMeanValueAcc;
		uint32_t	uiMeanDroppedValueAcc;
		uint16_t	uiMeanValuesCntr;
		uint16_t	uiMeanDroppedValuesCntr;
	}stProcess;
	struct {
		uint16_t uiMean;
		uint16_t uiDroppedMean;
	}stResult;
}ADCREADER_CHANNEL_CTX;

void adcreaderReadChannelsMean(ADCREADER_CHANNEL_CTX* aChannels, uint16_t uiChannelCounter );

#endif /* INOUT_ADCREADER_H_ */
