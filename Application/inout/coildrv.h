/*
 * coildrv.h
 *
 *  Created on: 27 apr 2018
 *      Author: daniele_parise
 */

#ifndef INOUT_COILDRV_H_
#define INOUT_COILDRV_H_

void coildrvInitIO(bool bON);
void coildrvInit(void);
void coildrvFree(void);
void coildrvPowerLoad(int imVolts);

void coildrv3V3_PWR_VLV_ON(void);
void coildrv3V3_PWR_VLV_OFF(void);

void coildrv3V3_PWR_COM_ON(void);
void coildrv3V3_PWR_COM_OFF(void);

void coildrvCOMM_HI(void);
void coildrvCOMM_LO(void);
void coildrvPWR_HI(void);
void coildrvPWR_LO(void);
void coildrvPWR2_EXT_HI(void);
void coildrvPWR2_EXT_LO(void);

void coildrvOUT_POL(unsigned uiMASK);
bool coildrvIsLatchingEV(void);
bool coildrvIsBoostEV(void);


#endif /* INOUT_COILDRV_H_ */
