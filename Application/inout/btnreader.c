/*
 * btnreader.c
 *
 *  Created on: 13 ago 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include "btnreader.h"

#include "stm32l0xx_hal.h"


void btnreaderSetup(bool bEnable)
{
	  GPIO_InitTypeDef GPIO_InitStruct;
	  __HAL_RCC_GPIOA_CLK_ENABLE();
	  __HAL_RCC_GPIOC_CLK_ENABLE();
	  __HAL_RCC_GPIOE_CLK_ENABLE();

	  GPIO_InitStruct.Mode = bEnable ? GPIO_MODE_INPUT : GPIO_MODE_ANALOG;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;

	  GPIO_InitStruct.Pin = GPIO_PIN_0;
	  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	  GPIO_InitStruct.Pin = GPIO_PIN_13;
	  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	  GPIO_InitStruct.Pin = GPIO_PIN_6;
	  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

}

void btnreaderRead(unsigned* pBtn1,unsigned* pBtn2,unsigned* pBtn3)
{
	*pBtn1 = READ_BIT(GPIOA->IDR,GPIO_PIN_0); // TST1
	*pBtn2 = READ_BIT(GPIOC->IDR,GPIO_PIN_13); // TST2
	*pBtn3 = READ_BIT(GPIOE->IDR,GPIO_PIN_6); // TST3

}

