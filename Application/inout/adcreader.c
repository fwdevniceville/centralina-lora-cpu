/*
 * adcreader.c
 *
 *  Created on: 28 apr 2018
 *      Author: daniele_parise
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "adc.h"
#include "adcreader.h"
#include "cmsis_os.h"

typedef struct tagADCREADER_CTX{
	EADCREADER_IN0_MODE	eADC_READ_MODE;
	int 				iVREFF_RAW_VAL;
#ifdef DEBUG
	int 				iTSENSE_RAW_VAL;
	int 				iVREFF_CAL_VAL;
	int 				iTEMP130_CAL_VAL;
	int 				iTEMP30_CAL_VAL;
#endif
	bool				bADC_CALIBRATED;
}ADCREADER_CTX;

ADCREADER_CTX	stADCREADER_CTX;


#define VREFINT_CAL_ADDR	((uint16_t*) ((uint32_t) 0x1FF80078))
#define TEMP130_CAL_ADDR	((uint16_t*) ((uint32_t) 0x1FF8007E))
#define TEMP30_CAL_ADDR		((uint16_t*) ((uint32_t) 0x1FF8007A))

#define VREFINT_CAL_VAL		(*VREFINT_CAL_ADDR)
#define TEMP130_CAL_VAL		(*TEMP130_CAL_ADDR)
#define TEMP30_CAL_VAL		(*TEMP30_CAL_ADDR)


void adcreaderInitIO(bool bON,EADCREADER_IN0_MODE eIN0MODE);

void adcreaderInit(EADCREADER_IN0_MODE eIN0MODE)
{
	stADCREADER_CTX.eADC_READ_MODE = eIN0MODE;
	adcreaderInitIO(true,eIN0MODE);
	MX_ADC_Init();
	HAL_ADC_Stop(&hadc);
	if (!stADCREADER_CTX.bADC_CALIBRATED)
	{
		stADCREADER_CTX.bADC_CALIBRATED = true;
		HAL_ADCEx_Calibration_Start(&hadc,ADC_SINGLE_ENDED);
	}

	stADCREADER_CTX.iVREFF_RAW_VAL		= adcreaderReadChannel(ADCREADER_VREFF,false);
#ifdef DEBUG
	stADCREADER_CTX.iTSENSE_RAW_VAL		= adcreaderReadChannel(ADCREADER_TSENSE,true);
	stADCREADER_CTX.iVREFF_CAL_VAL		= VREFINT_CAL_VAL;
	stADCREADER_CTX.iTEMP130_CAL_VAL	= TEMP130_CAL_VAL;
	stADCREADER_CTX.iTEMP30_CAL_VAL		= TEMP30_CAL_VAL;
#endif

}

void adcreaderFree(void)
{
	HAL_ADC_DeInit(&hadc);
	adcreaderInitIO(false,stADCREADER_CTX.eADC_READ_MODE);
}


void adcreaderInitIO(bool bON,EADCREADER_IN0_MODE eIN0MODE)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_11, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_10, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_5, GPIO_PIN_RESET);

	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
#ifdef CENTRALINA_PRTOTIPO_LAST
	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_7 | GPIO_PIN_8, GPIO_PIN_RESET);
#else
	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_7 , GPIO_PIN_RESET);
#endif

	GPIO_InitStruct.Pull 	= GPIO_NOPULL;
	GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Mode 	= GPIO_MODE_ANALOG;
	if (bON)
	{
		GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	}



	GPIO_InitStruct.Pin =
			GPIO_PIN_9  // Enable 4-20mA IN0 Read
				|
			GPIO_PIN_2  // PullUp IN0
				;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	// Enable 0-10V IN0 Read
	GPIO_InitStruct.Pin = GPIO_PIN_11;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	// Enable read current
	GPIO_InitStruct.Pin = GPIO_PIN_10;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);


	GPIO_InitStruct.Pin =
			GPIO_PIN_5	// Enable read BATT
				|
			GPIO_PIN_7 // PullUp IN1
#ifdef CENTRALINA_PRTOTIPO_LAST
				|
			GPIO_PIN_8 // PullUp IN2 - non usato
#else
#endif

			;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	if (eIN0MODE != ADCREADER_IN0_MODE_IN)
	{
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	}
	GPIO_InitStruct.Pin = GPIO_PIN_1;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pull 	= GPIO_NOPULL;
	GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Mode 	= GPIO_MODE_ANALOG;
	if (bON)
	{
#ifdef CENTRALINA_PRTOTIPO_LAST
		GPIO_InitStruct.Pull 	= GPIO_PULLUP;
#else
		GPIO_InitStruct.Pull 	= GPIO_NOPULL;
#endif
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	}
#ifdef CENTRALINA_PRTOTIPO_LAST
	GPIO_InitStruct.Pin = GPIO_PIN_11;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
#else
	GPIO_InitStruct.Pin = GPIO_PIN_8;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
#endif

	GPIO_InitStruct.Mode 	= GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull 	= GPIO_NOPULL;
	GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Pin 	= GPIO_PIN_4 | GPIO_PIN_6 | GPIO_PIN_7;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

void adcreaderReadVBattEnable(bool bEnable)
{
	MODIFY_REG(GPIOE->ODR, GPIO_PIN_5 , bEnable ? GPIO_PIN_5 : 0 );
}

void adcreaderReadCurrentEnable(bool bEnable)
{
	MODIFY_REG(GPIOD->ODR, GPIO_PIN_10 , bEnable ? GPIO_PIN_10 : 0 );
}

bool adcreaderIsReadCurrentEnabled(void)
{
	if (READ_BIT(GPIOD->IDR,GPIO_PIN_10))
	{
		return true;
	}
	return false;
}

bool adcreaderIsReadVBattEnabled(void)
{
	if (READ_BIT(GPIOE->IDR,GPIO_PIN_5))
	{
		return true;
	}
	return false;
}

bool adcreaderIsVEXTPresent(void)
{
#ifdef CENTRALINA_PRTOTIPO_LAST
	if (READ_BIT(GPIOA->IDR,GPIO_PIN_11))
#else
	if (READ_BIT(GPIOE->IDR,GPIO_PIN_8))
#endif
	{
		return false;
	}
	return true;
}

void adcreaderReadIN0Select(EADCREADER_IN0_MODE eMODE)
{
	switch (eMODE)
	{
	case ADCREADER_IN0_MODE_IN:
	case ADCREADER_IN0_MODE_ADC_5V:
		MODIFY_REG(GPIOB->ODR, GPIO_PIN_9 , 0);
		MODIFY_REG(GPIOC->ODR, GPIO_PIN_11 , 0);
		break;
	case ADCREADER_IN0_MODE_ADC_10V:
		MODIFY_REG(GPIOB->ODR, GPIO_PIN_9 , 0);
		MODIFY_REG(GPIOC->ODR, GPIO_PIN_11 , GPIO_PIN_11);
		break;
	case ADCREADER_IN0_MODE_ADC_4_20mA:
		MODIFY_REG(GPIOB->ODR, GPIO_PIN_9 , GPIO_PIN_9);
		MODIFY_REG(GPIOD->ODR, GPIO_PIN_11 ,  0 );
		break;
	}
}

void adcreaderEnablePullUps(unsigned uiINMASK)
{
	unsigned uiGPIOSET_B;
	unsigned uiGPIOSET_E;
	uiGPIOSET_B = 0;
	uiGPIOSET_E = 0;
	if (uiINMASK & 0x01)
	{
		uiGPIOSET_B |= GPIO_PIN_2;
	}
	if (uiINMASK & 0x02)
	{
		uiGPIOSET_E |= GPIO_PIN_7;
	}
	MODIFY_REG(GPIOB->ODR, GPIO_PIN_2, uiGPIOSET_B);
#ifdef CENTRALINA_PRTOTIPO_LAST
	MODIFY_REG(GPIOE->ODR, GPIO_PIN_7 | GPIO_PIN_8, uiGPIOSET_E);
#else
	MODIFY_REG(GPIOE->ODR, GPIO_PIN_7 , uiGPIOSET_E);
#endif
}


int adcreaderReadChannel(EADCREADER_CHANNEL eCHANNEL,bool bVREFIN_CORRECTION)
{
	ADC_ChannelConfTypeDef	sConfig;
	int	iRetValue;
	iRetValue = -1;
	sConfig.Channel 	= ADC_CHANNEL_1 | ADC_CHANNEL_6 | ADC_CHANNEL_7;
	sConfig.Rank 		= ADC_RANK_NONE;
	HAL_ADC_ConfigChannel(&hadc, &sConfig);

	sConfig.Channel = 0;
	switch (eCHANNEL)
	{
	case ADCREADER_IN0:
		sConfig.Channel = ADC_CHANNEL_1;
		break;
	case ADCREADER_IN4:
		sConfig.Channel = ADC_CHANNEL_4;
		break;
	case ADCREADER_VBATT:
		sConfig.Channel = ADC_CHANNEL_6;
		break;
	case ADCREADER_CURRENT:
		sConfig.Channel = ADC_CHANNEL_7;
		break;
	case ADCREADER_TSENSE:
		sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
		break;
	case ADCREADER_VREFF:
		sConfig.Channel = ADC_CHANNEL_VREFINT;
		break;
	}
	if (sConfig.Channel != 0)
	{
		sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
		HAL_ADC_ConfigChannel(&hadc, &sConfig);

		HAL_ADC_Start(&hadc);

		if (HAL_ADC_PollForConversion(&hadc,100) == HAL_OK)
		{
			iRetValue = HAL_ADC_GetValue(&hadc);
		}


		switch (eCHANNEL)
		{
		default:
			break;
		case ADCREADER_VREFF:
			bVREFIN_CORRECTION = false;
			break;
		}
		sConfig.Rank = ADC_RANK_NONE;
		HAL_ADC_ConfigChannel(&hadc, &sConfig);
		if (bVREFIN_CORRECTION)
		{
			if (stADCREADER_CTX.iVREFF_RAW_VAL > 0)
			{
				iRetValue *= VREFINT_CAL_VAL;
				iRetValue /= stADCREADER_CTX.iVREFF_RAW_VAL;
			}
		}
	}
	return iRetValue;
}
/*
 *

 VREFINT = 1.224V

#define VREFINT_CAL_ADDR    0x1FFFF7BA
#define TEMP130_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FF8007E))
#define TEMP30_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FF8007A))

VDDA = 3 V x VREFINT_CAL / VREFINT_DATA

  TS_CAL1 TS ADC raw data acquired at
temperature of 30 �C, VDDA= 3 V 0x1FF8 007A - 0x1FF8 007B
TS_CAL2 TS ADC raw data acquired at
temperature of 130 �C, VDDA= 3 V 0x1FF8 007E - 0x1FF8 007F

// Temperature sensor calibration value address
#define VDD_CALIB ((uint16_t) (300))
#define VDD_APPLI ((uint16_t) (330))
int32_t ComputeTemperature(uint32_t measure)
{
	int32_t temperature;
	temperature = ((measure * VDD_APPLI / VDD_CALIB) - (int32_t) *TEMP30_CAL_ADDR );
	temperature = temperature * (int32_t)(130 - 30);
	temperature = temperature / (int32_t)(*TEMP130_CAL_ADDR - *TEMP30_CAL_ADDR);
	temperature = temperature + 30;
	return(temperature);
}

 */


#ifdef DEBUG
#define ADCREADER_VALUES_FIRST_CURRENT_SAMPLES	(20)
typedef struct tagADCREADER_VALUES{
	struct {
		uint16_t	auiValues[ADCREADER_VALUES_FIRST_CURRENT_SAMPLES];
		int16_t		iMeanValue;
	}stLastMean;
}ADCREADER_VALUES;

ADCREADER_VALUES	stADCREADER_VALUES;
#endif

int adcreaderReadVBatt(int iSam_ms, int iDropSams,int* piDroppedMean)
{
	return adcreaderReadChannelMean(ADCREADER_VBATT,iSam_ms,iDropSams,piDroppedMean,true);
}

int adcreaderReadCurrent(int iSam_ms, int iDropSams, int* piDroppedMean)
{
	int iMean;
	iMean = adcreaderReadChannelMean(ADCREADER_CURRENT,iSam_ms,iDropSams,piDroppedMean,true);;
	iMean *= 1600;
	iMean /= 4096;
	if (piDroppedMean)
	{
		*piDroppedMean *= 1600;
		*piDroppedMean /= 4096;
	}

	return iMean;
}


void adcreaderReadChannelsMean(ADCREADER_CHANNEL_CTX* aChannels, uint16_t uiChannelCounter )
{
	uint16_t	uiSampleCurrent;
	uint16_t	uiChannelCurrent;
	uint16_t 	uiValue;
	bool		bContinue;

	if (uiChannelCounter > 0)
	{
		ADCREADER_CHANNEL_CTX* pChannel;
		uiValue = 0;
		uiSampleCurrent = 0;
		do
		{
			bContinue = false;
			uiChannelCurrent = 0;
			while (uiChannelCurrent < uiChannelCounter)
			{
				pChannel = &aChannels[uiChannelCurrent];
				if (pChannel->stSettings.eCH != ADCREADER_NULL)
				{
					if (uiSampleCurrent == 0)
					{
						memset(&pChannel->stProcess,0,sizeof(pChannel->stProcess));
						memset(&pChannel->stResult,0,sizeof(pChannel->stResult));
					}
					if (uiSampleCurrent < pChannel->stSettings.uiSam_ms)
					{
						bContinue = true;
						uiValue = adcreaderReadChannel(pChannel->stSettings.eCH,pChannel->stSettings.bVREFIN_CORRECTION);
#ifdef DEBUG
#ifdef ADCREADER_CHANNEL_FIRST_CURRENT_SAMPLES_BUFFER
						if (uiSampleCurrent < ADCREADER_CHANNEL_FIRST_CURRENT_SAMPLES_BUFFER)
						{
							pChannel->stProcess.auiValues[uiSampleCurrent] = uiValue;
						}
#endif
#endif

						if (pChannel->stProcess.uiMeanDroppedValuesCntr < pChannel->stSettings.uiDropSams)
						{
							pChannel->stProcess.uiMeanDroppedValuesCntr++;
							pChannel->stProcess.uiMeanDroppedValueAcc += uiValue;
						}
						else
						{
							pChannel->stProcess.uiMeanValueAcc+=uiValue;
							pChannel->stProcess.uiMeanValuesCntr++;
						}
					}
					else
					{
						if (pChannel->stProcess.uiMeanDroppedValuesCntr > 0)
						{
							pChannel->stResult.uiDroppedMean =pChannel->stProcess.uiMeanDroppedValueAcc / pChannel->stProcess.uiMeanDroppedValuesCntr;
						}
						if (pChannel->stProcess.uiMeanValuesCntr > 0)
						{
							pChannel->stResult.uiMean =pChannel->stProcess.uiMeanValueAcc / pChannel->stProcess.uiMeanValuesCntr;
						}
					}
				}
				uiChannelCurrent++;
			}
			if (bContinue)
			{
				osDelay(1);
			}
			uiSampleCurrent++;
		}
		while (bContinue);

	}
}


int adcreaderReadChannelMean(EADCREADER_CHANNEL eCH,int iSam_ms, int iDropSams, int* piDroppedMean,bool bVREFIN_CORRECTION)
{
	uint32_t	uiMeanValueAcc;
	uint32_t	uiMeanDroppedValueAcc;
	uint16_t	uiMeanValuesCntr;
	uint16_t	uiMeanDroppedValuesCntr;
	int			iMeanValue;
	int iSamples;
	int iValue;
	iSamples = 0;

	uiMeanValuesCntr		= 0;
	uiMeanValueAcc 			= 0;
	uiMeanDroppedValueAcc 	= 0;
	uiMeanDroppedValuesCntr	= 0;
	iMeanValue			= -1;
	if (piDroppedMean)
	{
		*piDroppedMean = -1;
	}

	while (iSamples < iSam_ms)
	{
		iValue = adcreaderReadChannel(eCH,bVREFIN_CORRECTION);
#ifdef DEBUG
		stADCREADER_VALUES.stLastMean.auiValues[iSamples] = iValue;
#endif

		if (iDropSams <= 0)
		{
			uiMeanValueAcc+=iValue;
			uiMeanValuesCntr++;
		}
		else
		{
			uiMeanDroppedValueAcc+=iValue;
			uiMeanDroppedValuesCntr++;
		}


		osDelay(1);
		iSamples++;
		iDropSams--;
	}
	if (uiMeanValuesCntr > 0)
	{
		uiMeanValueAcc /= uiMeanValuesCntr;
		iMeanValue = uiMeanValueAcc;
	}
	if (uiMeanDroppedValuesCntr > 0)
	{
		uiMeanDroppedValueAcc /= uiMeanDroppedValuesCntr;
		if (piDroppedMean)
		{
			*piDroppedMean = uiMeanDroppedValueAcc;
		}
	}

#ifdef DEBUG
	stADCREADER_VALUES.stLastMean.iMeanValue = iMeanValue;
#endif
	return iMeanValue;
}


