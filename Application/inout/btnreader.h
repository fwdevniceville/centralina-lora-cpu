/*
 * btnreader.h
 *
 *  Created on: 13 ago 2018
 *      Author: daniele_parise
 */

#ifndef INOUT_BTNREADER_H_
#define INOUT_BTNREADER_H_

void btnreaderSetup(bool bEnable);
void btnreaderRead(unsigned* pBtn1,unsigned* pBtn2,unsigned* pBtn3);

#endif /* INOUT_BTNREADER_H_ */
