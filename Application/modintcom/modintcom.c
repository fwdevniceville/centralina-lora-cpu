/*
 * modintcom.c
 *
 *  Created on: 12 mar 2018
 *      Author: daniele_parise
 */

#include <stdbool.h>
#include <stdint.h>

#include "../modintcom/modintcom.h"
#include "../intcom/intcom.h"
#include "../Utils/crc16.h"


bool modintcomOnBusRxCallBack(const uint8_t* abyBuffer, unsigned uiSize);
bool modintcomOnBusTxcCallBack(const uint8_t* abyBuffer, unsigned uiSize);
void modintcomProcessSendRequests(void);


#define MODINTCOMCTX_TEMP_BUFFER_SIZE	(128)

typedef struct tagMODINTCOMCTX{
	struct {
		uint8_t		auiBUFFER[MODINTCOMCTX_TEMP_BUFFER_SIZE];
		int			iSize;
	}stPENDING_TX_SEND;

	struct {
		uint8_t	auiBUFFER[4];
		int		iSize;
	}stPENDING_ACK_SEND;
	modintcom_recv_callback_t pfRecv;
	modintcom_sent_callback_t pfSent;
}MODINTCOMCTX;

MODINTCOMCTX	stMODINTCOMCTX;

void modintcomInit(modintcom_recv_callback_t pfRecv, modintcom_sent_callback_t pfSent)
{
	stMODINTCOMCTX.pfRecv = pfRecv;
	stMODINTCOMCTX.pfSent = pfSent;

	intcom_Init(modintcomOnBusRxCallBack,modintcomOnBusTxcCallBack);

}

void modintcomCheck(void)
{
	modintcomProcessSendRequests();
}

void modintcomAdjustBufferCRC(uint8_t* aBUFF,unsigned uiSize);
void modintcomAdjustBufferCRC(uint8_t* aBUFF,unsigned uiSize)
{
	uint16_t	uiCRC;
	uiCRC = calcCRC16(0xFFFF,aBUFF,uiSize);
	aBUFF[uiSize+0] = (uint8_t)uiCRC;
	aBUFF[uiSize+1] = (uint8_t)(uiCRC>>8);

}





bool modintcomOnBusRxCallBack(const uint8_t* abyBuffer, unsigned uiSize)
{
	if (uiSize > 2)
	{
		uint16_t	uiCRC;
		uint16_t	uiCRC_RCV;
		uiCRC = calcCRC16(0xFFFF,abyBuffer,uiSize - sizeof(uint16_t));
		uiCRC_RCV = abyBuffer[uiSize - 2];
		uiCRC_RCV |= (uint16_t)abyBuffer[uiSize - 1] << 8;
		if (uiCRC == uiCRC_RCV)
		{
			bool	bIsAck;
			bool	bAutoAck;
			uint8_t	uiCode;
			uiSize-=1;
			uiSize-=2;
			uiCode = abyBuffer[0] & MODINTCOM_CODE_CODE_MASK;
			bAutoAck 	= true;
			bIsAck 		= false;
			if (abyBuffer[0] & MODINTCOM_CODE_ACK_MASK)
			{
				uiSize-=1;
				bIsAck = true;
			}


			if (stMODINTCOMCTX.pfRecv)
			{
				bAutoAck = stMODINTCOMCTX.pfRecv(
						bIsAck,
						uiCode,
						bIsAck ? abyBuffer[1] : 0x00,
						&abyBuffer[bIsAck ? 2 : 1],
						uiSize
					);

			}
			if (bIsAck)
			{
			}
			else
			{
				if (bAutoAck)
				{
					stMODINTCOMCTX.stPENDING_ACK_SEND.auiBUFFER[0] 	= uiCode | MODINTCOM_CODE_ACK_MASK;
					stMODINTCOMCTX.stPENDING_ACK_SEND.auiBUFFER[1] 	= 0;
					stMODINTCOMCTX.stPENDING_ACK_SEND.iSize		= 4;
				}
				switch (uiCode)
				{
				case MODINTCOM_CODE_SET_LORA_PARAMS:
					break;
				case MODINTCOM_CODE_ENABLE_LORA:
					break;
				case MODINTCOM_CODE_SEND_UPLINK:
					break;
				case MODINTCOM_CODE_SEND_DOWNLINK:
					break;
				}
			}

		}

	}
	modintcomProcessSendRequests();
	return true;
}

void modintcomProcessSendRequests(void)
{
	if (intcom_IsSendFree())
	{
		if (stMODINTCOMCTX.stPENDING_ACK_SEND.iSize > 0)
		{
			modintcomAdjustBufferCRC(&stMODINTCOMCTX.stPENDING_ACK_SEND.auiBUFFER[0],2);
			intcom_Send(&stMODINTCOMCTX.stPENDING_ACK_SEND.auiBUFFER[0],4);
			stMODINTCOMCTX.stPENDING_ACK_SEND.iSize = 0;
		}
		else
		if (stMODINTCOMCTX.stPENDING_TX_SEND.iSize > 0)
		{
			modintcomAdjustBufferCRC(&stMODINTCOMCTX.stPENDING_TX_SEND.auiBUFFER[0],stMODINTCOMCTX.stPENDING_TX_SEND.iSize - 2);
			intcom_Send(&stMODINTCOMCTX.stPENDING_TX_SEND.auiBUFFER[0],stMODINTCOMCTX.stPENDING_TX_SEND.iSize);
			stMODINTCOMCTX.stPENDING_TX_SEND.iSize = 0;
		}
	}
}

bool modintcomOnBusTxcCallBack(const uint8_t* abyBuffer, unsigned uiSize)
{
	modintcomProcessSendRequests();
	return true;
}


void modintcomPulsEvent(void)
{
	if (stMODINTCOMCTX.stPENDING_TX_SEND.iSize <= 0)
	{
		stMODINTCOMCTX.stPENDING_TX_SEND.auiBUFFER[0] = 1;
		stMODINTCOMCTX.stPENDING_TX_SEND.auiBUFFER[1] = 2;
		stMODINTCOMCTX.stPENDING_TX_SEND.auiBUFFER[2] = 3;
		stMODINTCOMCTX.stPENDING_TX_SEND.auiBUFFER[3] = 4;
		stMODINTCOMCTX.stPENDING_TX_SEND.iSize = 6;

		modintcomProcessSendRequests();
	}

}
