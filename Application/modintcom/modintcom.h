/*
 * modintcom.h
 *
 *  Created on: 12 mar 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_MODINTCOM_MODINTCOM_H_
#define SRC_APPLICATION_MODINTCOM_MODINTCOM_H_

typedef	bool (*modintcom_recv_callback_t)(bool bIsACK, uint8_t uiCode, uint8_t uiAckResult,const uint8_t* abyBuffer, unsigned uiSize);
typedef	bool (*modintcom_sent_callback_t)(bool bIsACK, uint8_t uiCode,const uint8_t* abyBuffer, unsigned uiSize);

void modintcomInit(modintcom_recv_callback_t pfRecv, modintcom_sent_callback_t pfSent);

void modintcomCheck(void);

int modintcom_send(bool bIsACK, uint8_t uiCode, uint8_t uiAckResult,const uint8_t* abyBuffer, unsigned uiSize);

void modintcomPulsEvent(void);


#endif /* SRC_APPLICATION_MODINTCOM_MODINTCOM_H_ */
