#ifndef __INTCOM_H__
#define __INTCOM_H__

#include "hal_inc.h"

#include "../gencom/gencom.h"

void intcom_Init(gencom_rx_callback_t rxCallback,gencom_txc_callback_t txcCallback);
void intcom_DeInit(void);

void intcom_IoInit(void);
void intcom_IoDeInit(void);
  

int intcom_Send(unsigned uiCmd,unsigned uiSize, unsigned uiProgr,const uint8_t* uiSendBuffer);
int intcom_Recv(unsigned* puiCmd,unsigned* puiSize, unsigned* puiProgr,uint8_t* pBuffer,unsigned uiBufferSize);

void intcom_IRQHandler( void );
extern UART_HandleTypeDef UartIntcomHandle;

void intcom_process_lpm_rx(void);


bool intcom_IsSendFree(void);


extern GENCOM_CTX	stINTCOM_CTX;


#endif /* __intcom_H__*/

