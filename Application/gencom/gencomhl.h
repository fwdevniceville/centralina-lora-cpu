/*
 * gencomhl.h
 *
 *  Created on: 18 mar 2018
 *      Author: daniele_parise
 */

#ifndef INTCOM_GENCOMHL_H_
#define INTCOM_GENCOMHL_H_



typedef enum etagEGENCOMHL_SEND_STATUS{
	GENCOMHL_SEND_FREE,
	GENCOMHL_SEND_POST,
	GENCOMHL_SEND_POSTING,
	GENCOMHL_SEND_WAIT_ANSWER,
	GENCOMHL_SEND_COMPLETED,
	GENCOMHL_SEND_COMPLETED_IDLE
}EGENCOMHL_SEND_STATUS;

typedef enum etagEGENCOMHL_RECV_STATUS{
	GENCOMHL_RECV_WAIT,
	GENCOMHL_RECV_POST_ANSWER_WAIT,
	GENCOMHL_RECV_POST_ANSWER,
	GENCOMHL_RECV_POSTING_ANSWER
}EGENCOMHL_RECV_STATUS;



typedef enum etagEGENCOMHL_STATUS_RET{
	GENCOMHL_STATUS_SUCCESS	= (0),
	GENCOMHL_STATUS_INVAL,
	GENCOMHL_STATUS_BUSY,
	GENCOMHL_STATUS_TIMEOUT,
	GENCOMHL_STATUS_CANCELLED,
	GENCOMHL_STATUS_INCONSISTENT,
	GENCOMHL_STATUS_INVALID_ANSWER_LENGTH,
	GENCOMHL_STATUS_INVALID_SCHEDULE,
	GENCOMHL_STATUS_INVALID_COMMAND,
	GENCOMHL_STATUS_SCHEDULED,
	GENCOMHL_STATUS_QUEUED,
	GENCOMHL_STATUS_P_POST,
	GENCOMHL_STATUS_P_POSTING,
	GENCOMHL_STATUS_P_POSTING_TO,
	GENCOMHL_STATUS_P_WAIT_ANSW,
	GENCOMHL_STATUS_P_WAIT_ANSW_TO
}EGENCOMHL_STATUS_RET;


struct tagGENCOMHL_CTX;
struct tagGENCOMHL_MESSAGE_REQUEST;
struct tagGENCOMHL_MESSAGE_ANSWER;
typedef void (*gencomhl_send_callback_t)(struct tagGENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,EGENCOMHL_STATUS_RET eStautsRet, const struct tagGENCOMHL_MESSAGE_REQUEST* pRQST, const struct tagGENCOMHL_MESSAGE_ANSWER* pANSW);
typedef bool (*gencomhl_recv_callback_t)(struct tagGENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer);

#define GENCOMHL_ANSWER_MAX		(256)
#define GENCOMHL_REQUEST_MAX	(256)

typedef struct tagGENCOMHL_RQST_HDR{
	uint16_t	uiSIZE;
	uint8_t		uiCODE;
	uint8_t		uiRetries;
	uint8_t		uiOverrideProgr;
	uint8_t		uiSpare;
	uint32_t	uiTimeout;
}GENCOMHL_RQST_HDR;


typedef struct tagGENCOMHL_RQST_BASE{
	GENCOMHL_RQST_HDR	stHDR;
	uint8_t				auiPayLoad[2];
}GENCOMHL_RQST_BASE;

typedef struct tagGENCOMHL_RQST_BLOCK{
	GENCOMHL_RQST_HDR*	pRQST;
	unsigned			uiPayloadMax;
}GENCOMHL_RQST_BLOCK;

typedef struct tagGENCOMHL_REQUEST_SENDER_CTX{
	const GENCOMHL_RQST_BLOCK*  aRQSTS;
	uint16_t					uiRequestMax;
	uint16_t					uiRQS_SCNR;
	uint16_t					uiRQSTMASK_PENDING;
	uint16_t					uiRQSTMASK_SENDING;
	uint16_t					uiRQSTMASK_ERROR;
	uint16_t					uiRQSTMASK_SUCCESS;
}GENCOMHL_REQUEST_SENDER_CTX;

typedef struct tagGENCOMHL_MESSAGE_REQUEST{
	uint16_t	uiSIZE;
	uint8_t		uiCODE;
	uint8_t		uiPROGR;
	uint8_t 	aBuffer[GENCOMHL_REQUEST_MAX];
}GENCOMHL_MESSAGE_REQUEST;

typedef struct tagGENCOMHL_MESSAGE_ANSWER{
	uint16_t	uiSIZE;
	uint8_t		uiCODE;
	uint8_t		uiPROGR;
	uint8_t 	aBuffer[GENCOMHL_ANSWER_MAX];
}GENCOMHL_MESSAGE_ANSWER;

typedef struct tagGENCOMHL_CTX{
	struct{
		gencomhl_recv_callback_t	pfunc_recv;
		gencomhl_send_callback_t	pfunct_sent;
		void*						pctx_clbk;
	}stCLBK;
	GENCOMHL_REQUEST_SENDER_CTX*	pSENDER_SCHEDULER_CTX;
	struct tagGENCOM_CTX*			pGENCOM_CTX;
	struct{
		EGENCOMHL_SEND_STATUS	eSTATUS;
		EGENCOMHL_STATUS_RET	eStatusRetCode;
		int16_t		iTimeoutRunnung;
		uint16_t	uiTimeOut;
		uint8_t		uiReties;
		uint8_t		uiRetiesMax;
		uint8_t		uiRqstProgr;
		uint8_t		uiSpare;
		GENCOMHL_MESSAGE_REQUEST	stREQUEST;
		GENCOMHL_MESSAGE_ANSWER		stANSW;
	}stSEND;
	struct{
		EGENCOMHL_RECV_STATUS	eSTATUS;
		int32_t	iTimeoutRunnung;
		struct{
			uint8_t		uiCODE;
			uint8_t		uiPROGR;
			uint8_t		uiPROGR_LAST;
			uint8_t		uiSPARE;
			uint16_t	uiSIZE;
			uint8_t aBuffer[GENCOMHL_REQUEST_MAX];
		}stREQUEST;
		struct{
			uint8_t		uiCODE;
			uint8_t		uiPROGR;
			uint16_t	uiSIZE;
			uint8_t aBuffer[GENCOMHL_ANSWER_MAX];
		}stANSW_PENDING;
	}stRECV;
	struct{
		unsigned	uiNO_ACK;
	}stERRORS;
	bool	bACTIVE;
}GENCOMHL_CTX;

extern GENCOMHL_CTX	stGENCOMHL_CTX;

#define GENCOMHL_EVENT_RQST_RECV		(0x0001)
#define GENCOMHL_EVENT_RQST_SENT		(0x0002)
#define GENCOMHL_EVENT_ANSW_RECV		(0x0004)
#define GENCOMHL_EVENT_ANSW_SENT		(0x0008)
#define GENCOMHL_EVENT_RQST_POST		(0x0010)

#define GENCOMHL_CMD_ANSWER_MASK			(0x80)
#define GENCOMHL_CMD_CODE_MASK				(0x7f)



#define GENCOMHL_ANSW_CMD_OK				(0x00)
#define GENCOMHL_ANSW_CMD_WAIT_2ND_RESPONSE	(0xE1)
#define GENCOMHL_ANSW_CMD_UNK				(0xF1)
#define GENCOMHL_ANSW_CMD_INVALID_PARAMS	(0xF2)
#define GENCOMHL_ANSW_CMD_FAIL				(0xF3)
#define GENCOMHL_ANSW_ROUTER_BUSY			(0xF4)
#define GENCOMHL_ANSW_BAD_PROC				(0xFF)


void gencomhlInit(GENCOMHL_CTX* pGENCOMHL_CTX,struct tagGENCOM_CTX *pGENCOM_CTX,gencomhl_recv_callback_t pfunc_recv,gencomhl_send_callback_t pfunct, void* pctx_clbk,GENCOMHL_REQUEST_SENDER_CTX* pSENDER_CTX);
void gencomhlFree(GENCOMHL_CTX* pGENCOMHL_CTX);

int gencomhlExecComunicationSlot(GENCOMHL_CTX* pGENCOMHL_CTX,int deltaT_ms,unsigned uiEVT_MASK,bool* pbSytemRunning);

EGENCOMHL_STATUS_RET gencomhlPostSendRequest(GENCOMHL_CTX* pGENCOMHL_CTX,uint8_t uiOverrideProgr,uint8_t uiCmd,unsigned uiSizeHdr,const uint8_t* abyBufferHdr,unsigned uiSize,const uint8_t* abyBuffer,uint32_t uiTimeOut, unsigned uiRetiesMax);
EGENCOMHL_STATUS_RET gencomhlScheduleSendRequest(GENCOMHL_CTX* pGENCOMHL_CTX,int iSchedulerSlot,uint8_t uiOverrideProgr,uint8_t uiCmd,unsigned uiSizeHdr,const uint8_t* abyBufferHdr,unsigned uiSize,const uint8_t* abyBuffer,uint32_t uiTimeOut, unsigned uiRetiesMax);
EGENCOMHL_STATUS_RET gencomhlCompleteSendRequest(GENCOMHL_CTX* pGENCOMHL_CTX,uint8_t* puiRetStatus,unsigned* puiRetSize,uint8_t* abyRetBuffer,bool bCancelled);
bool gencomhlCheckAndStoreRecvFromBus(GENCOMHL_CTX* pGENCOMHL_CTX,uint8_t uiCmd, unsigned uiSize,unsigned uiPROGR,const uint8_t* abyBuffer,bool* pbIsAck);
EGENCOMHL_STATUS_RET gencomhlPostRecvAnswer(GENCOMHL_CTX* pGENCOMHL_CTX,uint8_t uiCmd,unsigned uiSize,unsigned uiPROGR,const uint8_t* abyBuffer);


bool gencomhlCheckAndSendPendingRequest(GENCOMHL_CTX* pGENCOMHL_CTX);

// To implement
void gencomhlFlushSendRequest(GENCOMHL_CTX* pGENCOMHL_CTX);

#endif /* GENCOMHL_GENCOMHL_H_ */
