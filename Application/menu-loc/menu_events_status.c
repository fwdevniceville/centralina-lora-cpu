/*
 * menu_tst.c
 *
 *  Created on: 08 ott 2018
 *      Author: sa2025
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "../menu/menu_func.h"
#include "menu_vars.h"
#include "menu_def.h"
#include "menu_def_lora.h"
#include "menu_def_tst.h"
#include "../Tasks/taskComunication.h"
#include "../Tasks/taskSupervisor.h"
#include "../Tasks/taskScheduler.h"
#include "../Storage/confdata.h"
#include "menu_events_status.h"
#include "menu_def_sts.h"
#include "stm32l0xx_hal.h"
#include "../SystemFunctions.h"

MENU_STATUS	stMENU_STATUS;

void menuStatusSetLoRaWANStatus(const char* strStatus)
{
	strncpy(stMENU_STATUS.strSTATUS_LORA,strStatus,sizeof(stMENU_STATUS.strSTATUS_LORA));
}

void menuStatusSetLinkStatus(EMENSTS_LORA_LINK_STATUS eSTS, int iRSSI, int iSNR)
{
	switch (eSTS)
	{
	case MENSTS_LORA_LINK_OK:
		stMENU_STATUS.iLORA_RX_RSSI	= iRSSI;
		stMENU_STATUS.iLORA_RX_SNR	= iSNR;
		menuPrintNumber_base(stMENU_STATUS.strLORA_RX_RSSI_TMP,iRSSI, -1, -1,10);
		menuPrintNumber_base(stMENU_STATUS.strLORA_RX_SNR_TMP,iSNR, 3, -1,10);
		break;
	case MENSTS_LORA_LINK_DUTYVIOLATION:
		strcpy(stMENU_STATUS.strLORA_RX_RSSI_TMP,"dyv");
		strcpy(stMENU_STATUS.strLORA_RX_SNR_TMP,"dyv");
		break;
	case MENSTS_LORA_LINK_TXBUSY:
		strcpy(stMENU_STATUS.strLORA_RX_RSSI_TMP,"tbu");
		strcpy(stMENU_STATUS.strLORA_RX_SNR_TMP,"tbu");
		break;
	}
}

bool menuItemEventStatusLoRaWan(EMENU_ITEM_EVENT eEvt,int iID,void* pValuePtr,uint8_t* puiDisplayOptions)
{
	switch (eEvt)
	{
	case MENU_ITEM_EVENT_BTN_ENTER:
		switch (iID)
		{
		case MENU_DEF_ITEM_ID_BASE_STATUS_LORA_RX_RSSI_EVAL:
		case MENU_DEF_ITEM_ID_BASE_STATUS_LORA_RX_SNR_EVAL:
			{
				strcpy(stMENU_STATUS.strLORA_RX_RSSI_TMP,"neu");
				strcpy(stMENU_STATUS.strLORA_RX_SNR_TMP,"neu");

				taskComunicationLoRaWanPulseConfirmedUplink();
			}
			break;
		}
		break;
	default:
		{
			switch (iID)
			{
			case MENU_DEF_ITEM_ID_BASE_STATUS_LORA_RESET:
				{
				}
				break;
			}
		}
		break;
	}
	return true;
}

bool menuChangedEditValueStatusLoRaWan(int iID, void* pValuePtr)
{
	switch (iID)
	{
	case MENU_DEF_ITEM_ID_BASE_STATUS_LORA_RESET:
		{
			if (stMENU_STATUS.iLORA_RESET)
			{
				stMENU_STATUS.iLORA_RESET = 0;
				taskComunicationLoRaWanReset();
			}
		}
		break;
	}
	return true;
}

bool menuChangedEditValueStatusSystem(int iID, void* pValuePtr)
{
	switch (iID)
	{
	case MENU_DEF_ITEM_ID_BASE_STATUS_SYSTEM_RESET:
		{
			if (stMENU_STATUS.iSYSTEM_RESET)
			{
				stMENU_STATUS.iSYSTEM_RESET = 0;
				taskSupervisorReboot();
			}
		}
		break;
	}
	return true;
}

bool menuItemEventStatus(EMENU_ITEM_EVENT eEvt,int iID,void* pValuePtr,uint8_t* puiDisplayOptions)
{
	unsigned	uiValue;

	switch (eEvt)
	{
	case MENU_ITEM_EVENT_EXIT:
		{
//			stMENU_STATUS.iCurrentDisplay = 0;
		}
		break;
	case MENU_ITEM_EVENT_ENTER_FROM_PARENT:
		{
			stMENU_STATUS.iCurrentDisplay = 0;
		}
		break;
	case MENU_ITEM_EVENT_BTN_ENTER:
		stMENU_STATUS.iCurrentDisplay++;
	case MENU_ITEM_EVENT_REFRESH:
		{
			memset(stMENU_STATUS.strSTATUS_BUFFER,0,sizeof(stMENU_STATUS.strSTATUS_BUFFER));
			if (stMENU_STATUS.iCurrentDisplay < 0)
			{
				stMENU_STATUS.iCurrentDisplay = 0;
			}
			switch (iID)
			{
			case MENU_DEF_ITEM_ID_BASE_STATUS_OUTS:
				{
					stMENU_STATUS.strSTATUS_BUFFER[0] = 'O';
					if (stMENU_STATUS.iCurrentDisplay >= 4)
					{
						stMENU_STATUS.iCurrentDisplay = 0;
					}
					stMENU_STATUS.strSTATUS_BUFFER[1] = '1' + stMENU_STATUS.iCurrentDisplay;
					uiValue = taskSchedulerGetOpenValves();
					stMENU_STATUS.strSTATUS_BUFFER[2] = (uiValue & (1 << stMENU_STATUS.iCurrentDisplay)) ? 'H' : 'L';
				}
				break;
			case MENU_DEF_ITEM_ID_BASE_STATUS_INS:
				{
					stMENU_STATUS.strSTATUS_BUFFER[0] = 'I';
					if (stMENU_STATUS.iCurrentDisplay >= 3)
					{
						stMENU_STATUS.iCurrentDisplay = 0;
					}
					stMENU_STATUS.strSTATUS_BUFFER[1] = '1' + stMENU_STATUS.iCurrentDisplay;
					stMENU_STATUS.strSTATUS_BUFFER[2] = stMENU_STATUS.iCurrentDisplay & 1 ? 'H' : 'L';
				}
				break;
			case MENU_DEF_ITEM_ID_BASE_STATUS_VBAT:
				{
					unsigned	uidVBatt;
					uidVBatt = stTASKCOMUNICATION_CTX.iMeasVBatt_mV / 100;
					stMENU_STATUS.strSTATUS_BUFFER[3] = 'V';
					stMENU_STATUS.strSTATUS_BUFFER[2] = '0' + (uidVBatt % 10);
					stMENU_STATUS.strSTATUS_BUFFER[1] = '.';
					uidVBatt /= 10;
					stMENU_STATUS.strSTATUS_BUFFER[0] = '0' + (uidVBatt % 10);

				}
				break;
			case MENU_DEF_ITEM_ID_BASE_STATUS_TIME:
				{
					DateTime	stDate;
					systemGetCurrentDateTime(&stDate);
					uiValue = 0;
					switch (stMENU_STATUS.iCurrentDisplay)
					{
					default :
						stMENU_STATUS.iCurrentDisplay = 0;
					case 0:
						stMENU_STATUS.strSTATUS_BUFFER[0] = 'H';
						uiValue = stDate.tm_hour;
						break;
					case 1:
						stMENU_STATUS.strSTATUS_BUFFER[0] = 'm';
						uiValue = stDate.tm_min;
						break;
					case 2:
						stMENU_STATUS.strSTATUS_BUFFER[0] = 'y';
						uiValue = stDate.tm_year;
						break;
					case 3:
						stMENU_STATUS.strSTATUS_BUFFER[0] = 'M';
						uiValue = stDate.tm_mon;
						break;
					case 4:
						stMENU_STATUS.strSTATUS_BUFFER[0] = 'd';
						uiValue = stDate.tm_mday;
						break;
					}
					uiValue = uiValue % 100;
					stMENU_STATUS.strSTATUS_BUFFER[1] = '0' + uiValue / 10;
					stMENU_STATUS.strSTATUS_BUFFER[2] = '0' + uiValue % 10;
				}
				break;
			}

		}
		break;
	}
	return true;
}
