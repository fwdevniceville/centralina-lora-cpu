/*
 * menu_vars.h
 *
 *  Created on: 23 mar 2018
 *      Author: daniele_parise
 */

#ifndef MENU_MENU_VARS_H_
#define MENU_MENU_VARS_H_

#include <stdint.h>
typedef struct tagMENU_VARS_LORA_CFG{
		int 	iNID1;
		int 	iCID1;
		int 	iCID2;

		int		iLORA_DATA_RATE;
		int		iLORA_ADR;
		int		iLORA_TX_POWER;
}MENU_VARS_LORA_CFG;

typedef struct tagMENU_VARS{
	char		strSUPPORT_TXT[20];
	int			iPOWER_LEVEL;
	unsigned	uiFLUX_CNTR;
	MENU_VARS_LORA_CFG	stLORA_CFG;
	struct{
		int	iVoltLoad_dV;
		int iTimeLoad_dS;
		int iTimeDischarge_mS;
	}stTEST_PARAMS;
}MENU_VARS;

extern MENU_VARS	stMENU_VARS;


#endif /* MENU_MENU_VARS_H_ */
