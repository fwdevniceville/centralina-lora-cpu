/*
 * menu_def.c
 *
 *  Created on: 04 mar 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "../menu/menu_func.h"
#include "menu_def.h"
#include "menu_def_sts.h"
#include "menu_def_tst.h"
#include "menu_def_lora.h"
#include "menu_vars.h"
#include "menu_ids.h"

MENU_VARS	stMENU_VARS;


const MENU_ITEM stVER = {
		MENU_ITEM_TYPE_TEXT_READ,
		0,
		"a02",
		{NULL}
};


const MENU_ITEM stPOWER = {
		MENU_ITEM_TYPE_INT_READ_DEC,
		0,
		(const char*)&stMENU_VARS.iPOWER_LEVEL,
		{NULL}
};


const MENU_ITEM aMENU_ITEMS_BASE[] =
{
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
				"VEr",
				{&stVER}
		},
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
				"STS",
				{&aMENU_ITEMS_BASE_STATUS}
		},
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
				"POW",
				{&stPOWER}
		},
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				MENU_DEF_ITEM_ID_BASE_LORA,
#ifdef MENU_DISPLAY_4_DIGITS
				"LORA",
#else
				"LOR",
#endif
				{aMENU_ITEMS_BASE_LORA}
		},
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
				"TST",
				{&aMENU_ITEMS_BASE_TEST}
		},
		{
				MENU_ITEM_TYPE_NULL,
				0,
				NULL,
				{NULL}
		}

};

