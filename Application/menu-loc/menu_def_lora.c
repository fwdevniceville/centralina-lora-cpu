/*
 * menu_def.c
 *
 *  Created on: 04 mar 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "../menu/menu_func.h"
#include "menu_def.h"
#include "menu_def_lora.h"
#include "menu_vars.h"
#include "menu_ids.h"


const MENU_ITEM_MIN_MAX	stLORA_ADR_MIN_MAX 		= {0	, 1		,-1};
const MENU_ITEM_MIN_MAX	stLORA_DR_MIN_MAX 		= {0	, 5		,-1};
const MENU_ITEM_MIN_MAX	stLORA_POWER_MIN_MAX 	= {1	, 20	,-1};

const MENU_ITEM stNID1 = {
		MENU_ITEM_TYPE_INT_RW_HEX,
		MENU_DEF_ITEM_ID_LORA_NID1,
		(const char*)&stMENU_VARS.stLORA_CFG.iNID1,
		{&stMENU_INT_MIN_MAX}
};

const MENU_ITEM stCID1 = {
		MENU_ITEM_TYPE_INT_RW_HEX,
		MENU_DEF_ITEM_ID_LORA_CID1,
		(const char*)&stMENU_VARS.stLORA_CFG.iCID1,
		{&stMENU_INT_MIN_MAX}
};

const MENU_ITEM stCID2 = {
		MENU_ITEM_TYPE_INT_RW_HEX,
		MENU_DEF_ITEM_ID_LORA_CID2,
		(const char*)&stMENU_VARS.stLORA_CFG.iCID2,
		{&stMENU_INT_MIN_MAX}
};

const MENU_ITEM stLORA_ADR = {
		MENU_ITEM_TYPE_INT_RW_HEX,
		MENU_DEF_ITEM_ID_LORA_ADR,
		(const char*)&stMENU_VARS.stLORA_CFG.iLORA_ADR,
		{&stLORA_ADR_MIN_MAX}
};

const MENU_ITEM stLORA_DR = {
		MENU_ITEM_TYPE_INT_RW_DEC,
		MENU_DEF_ITEM_ID_LORA_DR,
		(const char*)&stMENU_VARS.stLORA_CFG.iLORA_DATA_RATE,
		{&stLORA_DR_MIN_MAX}
};

const MENU_ITEM stLORA_POWER = {
		MENU_ITEM_TYPE_INT_RW_DEC,
		MENU_DEF_ITEM_ID_LORA_POWER,
		(const char*)&stMENU_VARS.stLORA_CFG.iLORA_TX_POWER,
		{&stLORA_POWER_MIN_MAX}
};

const MENU_ITEM aMENU_ITEMS_BASE_LORA_DEUI[] =
{
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
#ifdef MENU_DISPLAY_4_DIGITS
				"NID1",
#else
				"NI1",
#endif

				{&stNID1}
		},
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
#ifdef MENU_DISPLAY_4_DIGITS
				"CID1",
#else
				"CI1",
#endif
				{&stCID1}
		},
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
#ifdef MENU_DISPLAY_4_DIGITS
				"CID2",
#else
				"CI2",
#endif
				{&stCID2}
		},
		{
				MENU_ITEM_TYPE_NULL,
				0,
				NULL,
				{NULL}
		}
};

const MENU_ITEM aMENU_ITEMS_BASE_LORA_PARAMS[] =
{
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
				"ADR",
				{&stLORA_ADR}
		},
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
				"DR",
				{&stLORA_DR}
		},
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
				"POW",
				{&stLORA_POWER}
		},
		{
				MENU_ITEM_TYPE_NULL,
				0,
				NULL,
				{NULL}
		}
};


const MENU_ITEM aMENU_ITEMS_BASE_LORA[] =
{
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
#ifdef MENU_DISPLAY_4_DIGITS
				"DEUI",
#else
				"DUI",
#endif
				{&aMENU_ITEMS_BASE_LORA_DEUI}
		},
		{
				MENU_ITEM_TYPE_TEXT_STATIC,
				0,
				"PHY",
				{&aMENU_ITEMS_BASE_LORA_PARAMS}
		},
		{
				MENU_ITEM_TYPE_NULL,
				0,
				NULL,
				{NULL}
		}
};

