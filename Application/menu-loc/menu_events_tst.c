/*
 * menu_tst.c
 *
 *  Created on: 08 ott 2018
 *      Author: sa2025
 */

#include "cmsis_os.h"
#include <stdbool.h>
#include <stdint.h>
#include "../menu/menu_func.h"
#include "menu_vars.h"
#include "menu_def.h"
#include "menu_def_lora.h"
#include "menu_def_tst.h"
#include "../Tasks/taskComunication.h"
#include "../Tasks/taskSupervisor.h"
#include "../Tasks/taskScheduler.h"
#include "../Storage/confdata.h"

typedef enum etagESCHEDULER_TEST_STATO{
	SCHEDULER_TEST_INIT,
	SCHEDULER_TEST_GOTO_LOW_POWER,
	SCHEDULER_TEST_LOW_POWER,

	SCHEDULER_TEST_GOTO_RUNNING,
	SCHEDULER_TEST_RUNNING,

}ESCHEDULER_TEST_STATO;

typedef struct tagSCHEDULER_TEST{
	ESCHEDULER_TEST_STATO	eSTATO;
	unsigned uiOUT_KEEP_OPEN;
	unsigned uiOUT_OPEN;
	unsigned uiOUT_CLOSE;
	unsigned uiOUT_SCNR;
	unsigned uiOUT_CURR;

	unsigned uimVPULSE;
	unsigned uiTLOAD;
	unsigned uiTPULSE;

	unsigned uiIN_VALUES;
	unsigned uiIN_PULLUP;

	struct {
		int	iReading_IN1;
	}stADC_SETTING;
	struct {

		int i10V;
		int i4_20mA;
		int	iVBatt;
		int iCurr;
	}stADC_VALUES;



	bool	bLOADING;
	bool	bDISCHARGE;

	bool	bForceLatch;
	bool	bForceMonostabili;

}SCHEDULER_TEST;

SCHEDULER_TEST	stSCHEDULER_TEST;

bool menuItemEventTst(EMENU_ITEM_EVENT eEvt,int iID,void* pValuePtr,uint8_t* puiDisplayOptions)
{
	int iClass;
	int iNumber;
	int iAction;
	char* strDISPLAY;
	int	iINSPOS;
	uint16_t	uiMASK;
	strDISPLAY = (char*)pValuePtr;
	iClass 	= MENU_ITEM_GET_ID_CS(iID);
	iNumber = MENU_ITEM_GET_ID_CO(iID);
	iAction = MENU_ITEM_GET_ID_AC(iID);

	stSCHEDULER_TEST.stADC_SETTING.iReading_IN1 = 0;

	uiMASK = 0;
	if (iNumber > 0)
	{
		uiMASK = 1 << (iNumber - 1);
	}


	iINSPOS = 0;
	switch (iClass)
	{
	case MENU_ITEM_CS_ACTIONS_OUTS: // OUT
		{
			if (eEvt == MENU_ITEM_EVENT_BTN_ENTER)
			{
//				if ((stSCHEDULER_TEST.uiOUT_OPEN == 0) && (stSCHEDULER_TEST.uiOUT_CLOSE == 0))
				if (
						(stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaApertertura == 0)
							&&
						(stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaChiusura == 0))
				{
					if (iAction)
					{
//						stSCHEDULER_TEST.uiOUT_OPEN |= uiMASK;
						stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaApertertura |= uiMASK;
					}
					else
					{
//						stSCHEDULER_TEST.uiOUT_CLOSE |= uiMASK;
						stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaChiusura |= uiMASK;
					}
					taskSchedulerSignalAction();
				}
			}


			strDISPLAY[iINSPOS] = 'O';
			iINSPOS++;

			//			if (stSCHEDULER_TEST.bLOADING)
			if (stSCHEDULER_CTX.stCOIL_STATUS.bLOADING)
			{
				strDISPLAY[iINSPOS] = '.';
				iINSPOS++;
			}
			strDISPLAY[iINSPOS] = '0' + iNumber;
			iINSPOS++;
//			if (stSCHEDULER_TEST.bDISCHARGE)
			if (stSCHEDULER_CTX.stCOIL_STATUS.bDISCHARGE)
			{
				strDISPLAY[iINSPOS] = '.';
				iINSPOS++;
			}
			strDISPLAY[iINSPOS] = iAction ? 'O' : 'C';
			iINSPOS++;
			strDISPLAY[iINSPOS] = 0;
		}
		break;
	case MENU_ITEM_CS_ACTIONS_INS: // IN
		{

			if (eEvt == MENU_ITEM_EVENT_BTN_ENTER)
			{
				if (stSCHEDULER_TEST.uiIN_PULLUP & uiMASK)
				{
					stSCHEDULER_TEST.uiIN_PULLUP &= ~uiMASK;
				}
				else
				{
					stSCHEDULER_TEST.uiIN_PULLUP |= uiMASK;
				}
			}

			strDISPLAY[iINSPOS] = 'i';
			iINSPOS++;
			strDISPLAY[iINSPOS] = '0' + iNumber;
			iINSPOS++;
			if (stSCHEDULER_TEST.uiIN_PULLUP & uiMASK)
			{
				strDISPLAY[iINSPOS] = '.';
				iINSPOS++;
			}

			if (stSCHEDULER_TEST.uiIN_VALUES & uiMASK)
			{
				strDISPLAY[iINSPOS] = 'H';
			}
			else
			{
				strDISPLAY[iINSPOS] = 'L';
			}

			iINSPOS++;

			strDISPLAY[iINSPOS] = 0;
		}
		break;
	case MENU_ITEM_CS_ACTIONS_ADC: // ADC
		{
			int iValue;
			int iDPos;
			iDPos = -1;
			switch (iNumber)
			{
			default:
			case MENU_ITEM_CO_ACTIONS_ADC_VBAT:
				{
					iValue =stSCHEDULER_TEST.stADC_VALUES.iVBatt;
					iDPos = 3;
				}
				break;
			case MENU_ITEM_CO_ACTIONS_ADC_10V_1:
				{
					stSCHEDULER_TEST.stADC_SETTING.iReading_IN1 = 1;
					iValue =stSCHEDULER_TEST.stADC_VALUES.i10V;
					iDPos = 3;
				}
				break;
			case MENU_ITEM_CO_ACTIONS_ADC_10V_2:
				{
					stSCHEDULER_TEST.stADC_SETTING.iReading_IN1 = 1;
					iValue =stSCHEDULER_TEST.stADC_VALUES.i10V;
					iDPos = 3;
				}
				break;
			case MENU_ITEM_CO_ACTIONS_ADC_10V_3:
				{
					stSCHEDULER_TEST.stADC_SETTING.iReading_IN1 = 1;
					iValue =stSCHEDULER_TEST.stADC_VALUES.i10V;
					iDPos = 3;
				}
				break;
			case MENU_ITEM_CO_ACTIONS_ADC_4_20_mA:
				{
					stSCHEDULER_TEST.stADC_SETTING.iReading_IN1 = 2;
					iValue =stSCHEDULER_TEST.stADC_VALUES.i4_20mA;
					iDPos = 3;
				}
				break;
			case MENU_ITEM_CO_ACTIONS_ADC_CURR:
				{
					iValue =stSCHEDULER_TEST.stADC_VALUES.iCurr;
				}
				break;
			}

			menuPrintNumber_dec(strDISPLAY,iValue,iDPos,-1,puiDisplayOptions);
		}
		break;
	}

	return true;
}

