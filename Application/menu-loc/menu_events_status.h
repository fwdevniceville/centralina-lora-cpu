/*
 * menu_events_status.h
 *
 *  Created on: 15 ott 2018
 *      Author: daniele_parise
 */

#ifndef MENU_LOC_MENU_EVENTS_STATUS_H_
#define MENU_LOC_MENU_EVENTS_STATUS_H_

typedef struct tagMENU_STATUS{
	char	strSTATUS_LORA[10];
	char	strSTATUS_BUFFER[10];
	int		iCurrentDisplay;
	int		iLORA_RX_RSSI;
	int		iLORA_RX_SNR;
	char	strLORA_RX_RSSI_TMP[10];
	char	strLORA_RX_SNR_TMP[10];
	int		iLORA_TX_DATARATE;
	int		iLORA_TX_POWER;
	int		iLORA_RESET;
	int		iSYSTEM_RESET;
}MENU_STATUS;

extern MENU_STATUS	stMENU_STATUS;

void menuStatusSetLoRaWANStatus(const char* strStatus);

typedef enum etagEMENSTS_LORA_LINK_STATUS{
	MENSTS_LORA_LINK_OK,
	MENSTS_LORA_LINK_DUTYVIOLATION,
	MENSTS_LORA_LINK_TXBUSY
}EMENSTS_LORA_LINK_STATUS;

void menuStatusSetLinkStatus(EMENSTS_LORA_LINK_STATUS eSTS, int iRSSI, int iSNR);

#endif /* MENU_LOC_MENU_EVENTS_STATUS_H_ */
