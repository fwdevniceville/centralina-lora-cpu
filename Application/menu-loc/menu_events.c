/*
 * menu_events.c
 *
 *  Created on: 29 apr 2018
 *      Author: daniele_parise
 */

#include <stdbool.h>
#include <stdint.h>
#include "../menu/menu_func.h"
#include "menu_vars.h"
#include "menu_def.h"
#include "menu_def_lora.h"
#include "menu_def_tst.h"
#include "menu_def_sts.h"
#include "../Tasks/taskComunication.h"
#include "../Tasks/taskSupervisor.h"
#include "../Storage/confdata.h"

bool menuItemEventTst(EMENU_ITEM_EVENT eEvt,int iID,void* pValuePtr,uint8_t* puiDisplayOptions);

bool menuItemEvent(EMENU_ITEM_EVENT eEvt,int iID,void* pValuePtr,uint8_t* puiDisplayOptions)
{
	switch (MENU_ITEM_GET_ID_CM(iID))
	{
	case MENU_ITEM_CM_STATUS:
		{
			switch (MENU_ITEM_GET_ID_CS(iID))
			{
			default:
			case MENU_ITEM_CS_STATUS_OUTS:
			case MENU_ITEM_CS_STATUS_INS:
			case MENU_ITEM_CS_STATUS_VBAT:
			case MENU_ITEM_CS_STATUS_CNTR:
				{
					menuItemEventStatus(eEvt,iID,pValuePtr, puiDisplayOptions);
				}
				break;
			case MENU_ITEM_CS_STATUS_LORA:
				{
					menuItemEventStatusLoRaWan(eEvt,iID,pValuePtr, puiDisplayOptions);
				}
				break;
			}
		}
		break;
	case MENU_ITEM_CM_ACTIONS:
		{
			menuItemEventTst(eEvt,iID,pValuePtr, puiDisplayOptions);
		}
		break;
	default:
		{

			switch (eEvt)
			{
			default:
				break;
			case MENU_ITEM_EVENT_REFRESH:
				{

				}
				break;
			case MENU_ITEM_EVENT_BTN_ENTER:
				{

				}
				break;
			case MENU_ITEM_EVENT_ENTER_FROM_SUB:
				{
					switch (iID)
					{
					case MENU_DEF_ITEM_ID_BASE_LORA:
						{
							taskComunicationOnProcessNewLoraParams();
						}
						break;
					}
				}
				break;
			}




		}
		break;
	}

	return true;
}

bool menuChangingEditValue(int iID, void* pValuePtr)
{

	return true;
}

bool menuChangedEditValue(int iID, void* pValuePtr)
{
	switch (MENU_ITEM_GET_ID_CM(iID))
	{
	case MENU_ITEM_CM_STATUS:
		{
			switch (MENU_ITEM_GET_ID_CS(iID))
			{
			case MENU_ITEM_CS_STATUS_LORA:
				{
					menuChangedEditValueStatusLoRaWan(iID,pValuePtr);
				}
				break;
			case MENU_ITEM_CS_STATUS_SYSTEM:
				{
					menuChangedEditValueStatusSystem(iID,pValuePtr);
				}
				break;
			}

		}
		break;
	case MENU_ITEM_CM_CONFIGS:
		{
			switch (MENU_ITEM_GET_ID_CS(iID))
			{
			case MENU_ITEM_CS_CONFIGS_LORA:
				{


					taskSupervisorSaveLoraCfgSimple(&stMENU_VARS.stLORA_CFG);
				}
				break;
			}
		}
		break;
	}
	return true;
}

/*
 *
 * PAYLOAD LORA EU868
 *
 * DataRate 	M 		N
 * 0 			59 		51
 * 1 			59 		51
 * 2 			59 		51
 * 3 			123 	115
 * 4 			250 	242
 * 5 			250 	242
 * 6 			250 	242
 * 7 			250 	242
 * 8:15 Not defined
 */
