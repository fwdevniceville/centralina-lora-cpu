/*
 * menu_def.h
 *
 *  Created on: 02 mag 2018
 *      Author: daniele_parise
 */

#ifndef MENU_LOC_MENU_DEF_TST_H_
#define MENU_LOC_MENU_DEF_TST_H_

extern const MENU_ITEM aMENU_ITEMS_BASE_TEST[];

#define	MENU_ITEM_CS_ACTIONS_OUTS	(1)
#define	MENU_ITEM_CS_ACTIONS_INS	(2)
#define	MENU_ITEM_CS_ACTIONS_ADC	(3)

#define	MENU_ITEM_CO_ACTIONS_ADC_VBAT		(1)
#define	MENU_ITEM_CO_ACTIONS_ADC_10V_1		(2)
#define	MENU_ITEM_CO_ACTIONS_ADC_10V_2		(3)
#define	MENU_ITEM_CO_ACTIONS_ADC_10V_3		(4)
#define	MENU_ITEM_CO_ACTIONS_ADC_4_20_mA	(5)
#define	MENU_ITEM_CO_ACTIONS_ADC_CURR		(6)

#define	MENU_ITEM_CS_CONFIGS_OUTS	(5)

#endif /* MENU_LOC_MENU_DEF_H_ */
