/*
 * menu_def.h
 *
 *  Created on: 02 mag 2018
 *      Author: daniele_parise
 */

#ifndef MENU_LOC_MENU_DEF_H_
#define MENU_LOC_MENU_DEF_H_

#define MENU_ITEM_CM_ACTIONS	(1)
#define MENU_ITEM_CM_CONFIGS	(2)
#define MENU_ITEM_CM_STATUS		(3)


extern const MENU_ITEM aMENU_ITEMS_BASE[];


#endif /* MENU_LOC_MENU_DEF_H_ */
