/*
 * configurations.h
 *
 *  Created on: 26 feb 2018
 *      Author: daniele_parise
 */

#ifndef STORAGE_CONFIGURATIONS_H_
#define STORAGE_CONFIGURATIONS_H_


#define CFG_SCHEDULER_ROW_START_YEAR_MASK			(0x7F)
#define CFG_SCHEDULER_ROW_START_YEAR_AUTO_MASK		(0x80)
#define CFG_SCHEDULER_ROW_START_MONTH_MASK			(0x0F)
#define CFG_SCHEDULER_ROW_START_MONTH_AUTO_MASK		(0x80)
#define CFG_SCHEDULER_ROW_START_MONTH_DAY_MASK		(0x3F)
#define CFG_SCHEDULER_ROW_START_MONTH_DAY_AUTO_MASK	(0x80)



#define CFG_SCHEDULER_ROW_TYPE_INVALID		(0)
#define CFG_SCHEDULER_ROW_TYPE_DAYYEAR		(1)
#define CFG_SCHEDULER_ROW_TYPE_WEEKDAY		(2)
#define CFG_SCHEDULER_ROW_TYPE_DAYPERIOD	(3)
typedef struct tagCFG_SCHEDULER_ROW{
	uint8_t		uiAge;
	uint8_t		uiORD;
	uint8_t		uiTYPE;
	uint8_t		uiStartYear;

	uint8_t		uiStartMonth;
	uint8_t		uiStartMonthDay;
	uint16_t	uiPeriodDay;

	uint8_t		uiStart_hour;
	uint8_t		uiStart_min;
	uint16_t	uiRuntime_min;

	uint8_t		uiOUT_MASK;
	uint8_t		uiCRC8_MSG;
	uint16_t	uiCRC16_ROW;
}CFG_SCHEDULER_ROW;	// 16

#define CFG_SCHEDULER_MAX_ROW			(64)
typedef struct tagCFG_SCHEDULER{
	CFG_SCHEDULER_ROW	aROWS[CFG_SCHEDULER_MAX_ROW];	// 16 * 64 = 1024
}CFG_SCHEDULER;	// 1616

#include "confdata.h"


typedef struct tagCFG_GEN_SLOT{
	uint16_t				uiCRC16;
	uint8_t					uiSLOT_CODE;
	uint8_t					uiSPARE;
	UCFG_GEN_SLOT_DATA		uDATA;
}CFG_GEN_SLOT;

typedef struct tagCFG_GEN{
	CFG_GEN_SLOT	aSLOTS[__CONFIG_SLOT_MAX__];
}CFG_GEN;	// 1616





typedef struct tagSTORAGE_IN_MEMORY{
	CFG_GEN				stCFG_GEN;
	CFG_SCHEDULER		stCFG_SCHEDULER;
	int					iNEXT_CFG_BASE_FREE;
	int					iNEXT_SCHEDULER_ROW_FREE_ADDRESS;
	int					iFIRST_SCHEDULER_ROW_WROTE_ADDRESS;
	unsigned			bREADY	: 1;
}STORAGE_IN_MEMORY;


extern STORAGE_IN_MEMORY stSTORAGE_IN_MEMORY;


void configurationsLoad(void);
//void configurationsDefaultCfgBase(void);
int configGetSchedulerTable(CFG_SCHEDULER_ROW** paRows);
void configAdjustNewSchedule(CFG_SCHEDULER_ROW* pNEW_ROW);
bool configSaveNewSchedule(const CFG_SCHEDULER_ROW* pNEW_ROW);
bool configEraseSchedule(unsigned uiSchedule);
bool configEnableSchedule(unsigned uiSchedule,bool bEnable);
//int configSaveNewConfig(const CFG_BASE* pNEW_CFG);
bool configGetSchedule(unsigned uiSchedule,CFG_SCHEDULER_ROW* pRow);

#define configurationIsReady()			((stSTORAGE_IN_MEMORY.bREADY))

const UCFG_GEN_SLOT_DATA* configConfigGetRO(ECONFIG_SLOT_CODE eCODE);
void configConfigGet(ECONFIG_SLOT_CODE eCODE, UCFG_GEN_SLOT_DATA* uDATA);
void configConfigSet(ECONFIG_SLOT_CODE eCODE, const UCFG_GEN_SLOT_DATA* uDATA);
void configConfigDefaultSet(ECONFIG_SLOT_CODE eCODE, UCFG_GEN_SLOT_DATA* uDATA);



#endif /* STORAGE_CONFIGURATIONS_H_ */

