/*
 * configurations.c
 *
 *  Created on: 27 feb 2018
 *      Author: sa2025
 */

#include <stdint.h>
#include <stdbool.h>
#include "configurations.h"
#include "confutils.h"
#include "../Utils/crc16.h"
#include "hal_inc.h"
#include <string.h>
#include "../SystemFunctions.h"

void configurationsLoadSchedules(void);
void configurationsLoadCfgBase(void);
bool bResetFlash = false;

void configurationsLoad(void)
{

	configurationsLoadCfgBase();

	configurationsLoadSchedules();
}

STORAGE_IN_MEMORY stSTORAGE_IN_MEMORY;



int configGetSchedulerTable(CFG_SCHEDULER_ROW** paRows)
{
	*paRows = &stSTORAGE_IN_MEMORY.stCFG_SCHEDULER.aROWS[0];
	return CFG_SCHEDULER_MAX_ROW;
}

bool configGetSchedule(unsigned uiSchedule,CFG_SCHEDULER_ROW* pRow)
{
	if (uiSchedule < CFG_SCHEDULER_MAX_ROW)
	{
		*pRow = stSTORAGE_IN_MEMORY.stCFG_SCHEDULER.aROWS[uiSchedule];
		return true;
	}
	return false;
}

#define CFG_STORAGE_SECTOR_SIZE			(0x400)
#define CFG_SCHEDULER_SIZE				(CFG_SCHEDULER_MAX_ROW * sizeof(CFG_SCHEDULER_ROW))
#define CFG_SCHEDULER_FADDRESS_BASE		(DATA_EEPROM_BASE)
#define CFG_SCHEDULER_FADDRESS_END		(DATA_EEPROM_BASE + CFG_SCHEDULER_SIZE)
#define CFG_SCHEDULER_ROLBACK_BASE		(CFG_SCHEDULER_FADDRESS_END)
#define CFG_SCHEDULER_ROLBACK_END		(CFG_SCHEDULER_ROLBACK_BASE + (16 * sizeof(CFG_SCHEDULER_ROW)))
#define CFG_BASE_ADDR_START_0			(DATA_EEPROM_BASE + (CFG_STORAGE_SECTOR_SIZE * 2))
#define CFG_BASE_SIZE_MAX				(0x200)
#define CFG_BASE_ADDR_START_1			(CFG_BASE_ADDR_START_0 + CFG_BASE_SIZE_MAX)
#define CFG_BASE_ADDR_END				(CFG_BASE_ADDR_START_1 + CFG_BASE_SIZE_MAX)
struct tag_CFG_SCHEDULER_SIZE_TEST{uint8_t	aCFG_SCHEDULER_SIZE_TEST[(CFG_STORAGE_SECTOR_SIZE == CFG_SCHEDULER_SIZE) ? 1 : -1];};


bool configSaveNewScheduleInternal(unsigned uiSchedule,const CFG_SCHEDULER_ROW* pNEW_ROW);

bool configEraseSchedule(unsigned uiSchedule)
{
	return configSaveNewScheduleInternal(uiSchedule,NULL);
}

bool configEnableSchedule(unsigned uiSchedule,bool bEnable)
{
	bool	bSuccess;
	bSuccess = false;
	if (
			(uiSchedule < CFG_SCHEDULER_MAX_ROW)
				||
			(uiSchedule == 255)
		)
	{
		CFG_SCHEDULER_ROW	stTEMP_ROW;
		int					iFrom;
		int					iTo;

		iFrom 	= uiSchedule;
		iTo 	= uiSchedule;
		if (uiSchedule == 255)
		{
			iFrom 	= 0;
			iTo 	= (CFG_SCHEDULER_MAX_ROW - 1);
		}

		while (iFrom <= iTo)
		{
			stTEMP_ROW = stSTORAGE_IN_MEMORY.stCFG_SCHEDULER.aROWS[iFrom];
			if (stTEMP_ROW.uiTYPE != 0)
			{

				if (bEnable)
				{
					if (stTEMP_ROW.uiTYPE & 0x80)
					{
						configAdjustNewSchedule(&stTEMP_ROW);
					}
					stTEMP_ROW.uiTYPE &= ~0x80;
				}
				else
				{
					stTEMP_ROW.uiTYPE |= 0x80;
				}

				if (configSaveNewScheduleInternal(iFrom,&stTEMP_ROW))
				{
					bSuccess = true;
				}
			}
			iFrom++;
		}
	}
	return bSuccess;
}

void configAdjustNewSchedule(CFG_SCHEDULER_ROW* pNEW_ROW)
{
	DateTime			dtCurrDate;
	systemGetCurrentDateTime(&dtCurrDate);

	if ((pNEW_ROW->uiStartYear == 0) || (pNEW_ROW->uiStartYear & CFG_SCHEDULER_ROW_START_YEAR_AUTO_MASK))
	{
		pNEW_ROW->uiStartYear = (dtCurrDate.tm_year % 100) | CFG_SCHEDULER_ROW_START_YEAR_AUTO_MASK;
	}
	if ((pNEW_ROW->uiStartMonth == 0) || (pNEW_ROW->uiStartMonth & CFG_SCHEDULER_ROW_START_MONTH_AUTO_MASK))
	{
		pNEW_ROW->uiStartMonth = dtCurrDate.tm_mon | CFG_SCHEDULER_ROW_START_MONTH_AUTO_MASK;
	}
	if ((pNEW_ROW->uiStartMonthDay == 0) || (pNEW_ROW->uiStartMonthDay & CFG_SCHEDULER_ROW_START_MONTH_DAY_AUTO_MASK))
	{
		pNEW_ROW->uiStartMonthDay = dtCurrDate.tm_mday | CFG_SCHEDULER_ROW_START_MONTH_DAY_AUTO_MASK;
	}
}

bool configSaveNewSchedule(const CFG_SCHEDULER_ROW* pNEW_ROW)
{
	return configSaveNewScheduleInternal(pNEW_ROW->uiORD,pNEW_ROW);
}

bool configSaveNewScheduleInternal(unsigned uiSchedule,const CFG_SCHEDULER_ROW* pNEW_ROW)
{
	CFG_SCHEDULER_ROW	stTEMP_ROW;
	uint32_t	uiFLASH_ADDR;
	if (uiSchedule < CFG_SCHEDULER_MAX_ROW)
	{
		memset(&stTEMP_ROW,0,sizeof(stTEMP_ROW));
		if (pNEW_ROW)
		{
			stTEMP_ROW = *pNEW_ROW;
			stTEMP_ROW.uiAge 		= 1;
			stTEMP_ROW.uiCRC16_ROW  = calcCRC16(0xFFFF,(uint8_t*)&stTEMP_ROW,sizeof(CFG_SCHEDULER_ROW) - sizeof(uint16_t));
		}
		uiFLASH_ADDR = CFG_SCHEDULER_FADDRESS_BASE;
		uiFLASH_ADDR += sizeof(CFG_SCHEDULER_ROW) * uiSchedule;
		confutilsWriteToFlash(uiFLASH_ADDR,sizeof(CFG_SCHEDULER_ROW),&stTEMP_ROW);
		stSTORAGE_IN_MEMORY.stCFG_SCHEDULER.aROWS[uiSchedule] = stTEMP_ROW;
		return true;
	}
	return false;
}


void configurationsLoadSchedules(void)
{
	CFG_SCHEDULER_ROW	stTEMP_ROW;
	int			iSchedule;
	uint32_t	uiFLASH_ADDR;
	uint16_t	uiCRC16;
	memset(&stSTORAGE_IN_MEMORY.stCFG_SCHEDULER,0,sizeof(stSTORAGE_IN_MEMORY.stCFG_SCHEDULER));

	uiFLASH_ADDR = CFG_SCHEDULER_FADDRESS_BASE;
	iSchedule = 0;
	while (iSchedule < CFG_SCHEDULER_MAX_ROW)
	{
		confutilsReadFromFlash(uiFLASH_ADDR,sizeof(CFG_SCHEDULER_ROW),&stTEMP_ROW);

		uiCRC16 = calcCRC16(0xFFFF,(uint8_t*)&stTEMP_ROW,sizeof(CFG_SCHEDULER_ROW) - sizeof(uint16_t));
		if (uiCRC16 == stTEMP_ROW.uiCRC16_ROW)
		{
			stSTORAGE_IN_MEMORY.stCFG_SCHEDULER.aROWS[iSchedule] = stTEMP_ROW;
		}
		else
		{

		}

		uiFLASH_ADDR+= sizeof(CFG_SCHEDULER_ROW);
		iSchedule++;
	}
}


void configurationsLoadCfgBase(void)
{
	CFG_GEN_SLOT*	pCURR_SLOT;
	ECONFIG_SLOT_CODE eCODE;
	int iSCNR;
	uint16_t		uiCRC16;
	confutilsReadFromFlash(CFG_BASE_ADDR_START_0, sizeof(stSTORAGE_IN_MEMORY.stCFG_GEN),&stSTORAGE_IN_MEMORY.stCFG_GEN);
	iSCNR = 0;
	while (iSCNR < __CONFIG_SLOT_MAX__)
	{
		pCURR_SLOT = &stSTORAGE_IN_MEMORY.stCFG_GEN.aSLOTS[iSCNR];

		uiCRC16 = calcCRC16(0xFFFF,&((uint8_t*)pCURR_SLOT)[sizeof(uint16_t)],sizeof(CFG_GEN_SLOT) - sizeof(uint16_t));
		if (
				(uiCRC16 != pCURR_SLOT->uiCRC16)
				|| (pCURR_SLOT->uiSLOT_CODE != iSCNR)  //!! TBD!!
				)
		{
			eCODE = iSCNR;
			memset(pCURR_SLOT,0,sizeof(CFG_GEN_SLOT));
			configConfigDefaultSet(eCODE,&pCURR_SLOT->uDATA);
			pCURR_SLOT->uiSLOT_CODE = (uint8_t)eCODE;
		}

		iSCNR++;
	}
}

const UCFG_GEN_SLOT_DATA* configConfigGetRO(ECONFIG_SLOT_CODE eCODE)
{
	if (eCODE < __CONFIG_SLOT_MAX__)
	{
		return &stSTORAGE_IN_MEMORY.stCFG_GEN.aSLOTS[eCODE].uDATA;
	}
	return NULL;
}

void configConfigGet(ECONFIG_SLOT_CODE eCODE, UCFG_GEN_SLOT_DATA* uDATA)
{
	memset(uDATA,0,sizeof(UCFG_GEN_SLOT_DATA));
	if (eCODE < __CONFIG_SLOT_MAX__)
	{
		memcpy(uDATA,&stSTORAGE_IN_MEMORY.stCFG_GEN.aSLOTS[eCODE].uDATA,sizeof(UCFG_GEN_SLOT_DATA));
	}
}

void configConfigSet(ECONFIG_SLOT_CODE eCODE, const UCFG_GEN_SLOT_DATA* uDATA)
{
	if (eCODE < __CONFIG_SLOT_MAX__)
	{
		CFG_GEN_SLOT	stCURR_SLOT;
		uint32_t		uiFLASH_ADDR;
		stCURR_SLOT	= stSTORAGE_IN_MEMORY.stCFG_GEN.aSLOTS[eCODE];

		stCURR_SLOT.uDATA = *uDATA;

		stCURR_SLOT.uiCRC16 = calcCRC16(0xFFFF,&((uint8_t*)&stCURR_SLOT)[sizeof(uint16_t)],sizeof(CFG_GEN_SLOT) - sizeof(uint16_t));

		uiFLASH_ADDR = CFG_BASE_ADDR_START_0;
		uiFLASH_ADDR += sizeof(CFG_GEN_SLOT) * eCODE;

		confutilsWriteToFlash(uiFLASH_ADDR,sizeof(CFG_GEN_SLOT),&stCURR_SLOT);

		stSTORAGE_IN_MEMORY.stCFG_GEN.aSLOTS[eCODE] = stCURR_SLOT;
	}

}

#if 0


#define CFG_BASE_FADDRESS_BASE_0		(DATA_EEPROM_BASE + (CFG_STORAGE_SECTOR_SIZE * 2))
#define CFG_BASE_FADDRESS_BASE_1		(0x3000)

#define CFG_SCHEDULER_MAX_ROW_IN_FLASH	((0x2000) / (sizeof(CFG_SCHEDULER_ROW)))

bool configurationsCheckFree(const void* bmem, size_t size);

void configurationsLoadSchedules(void)
{
	uint32_t	uiFLASH_ADDR;
	uint16_t	uiCRC16;
	int 		iSCNR_FROWS;
	int 		iPTR_FREE_ROW_ADDRESS;
	int 		iPTR_FIRST_WRITE_ROW_ADDRESS;
	int 		iPTR_GROWS;

	CFG_SCHEDULER_ROW* pMEM_ROW;

	memset(&stSTORAGE_IN_MEMORY.stCFG_SCHEDULER,0,sizeof(stSTORAGE_IN_MEMORY.stCFG_SCHEDULER));


	iPTR_FIRST_WRITE_ROW_ADDRESS = -1;
	iPTR_FREE_ROW_ADDRESS = -1;
	iSCNR_FROWS = 0;
	uiFLASH_ADDR = 0;
	while (iSCNR_FROWS < CFG_SCHEDULER_MAX_ROW_IN_FLASH)
	{
		confutilsReadFromFlash(uiFLASH_ADDR,sizeof(CFG_SCHEDULER_ROW),&stTEMP_ROW);

		uiCRC16 = calcCRC16(0xFFFF,(uint8_t*)&stTEMP_ROW,sizeof(CFG_SCHEDULER_ROW) - sizeof(uint16_t));
		if (uiCRC16 != stTEMP_ROW.uiCRC16_ROW)
		{
			if (configurationsCheckFree(&stTEMP_ROW,sizeof(CFG_SCHEDULER_ROW)))
			{
				if (iPTR_FREE_ROW_ADDRESS < 0)
				{
					iPTR_FREE_ROW_ADDRESS = (int)uiFLASH_ADDR;
				}
			}
			else
			{
				if (iPTR_FIRST_WRITE_ROW_ADDRESS < 0)
				{
					iPTR_FIRST_WRITE_ROW_ADDRESS = (int)uiFLASH_ADDR;
				}
			}
		}
		else
		{
			if (iPTR_FIRST_WRITE_ROW_ADDRESS < 0)
			{
				iPTR_FIRST_WRITE_ROW_ADDRESS = (int)uiFLASH_ADDR;
			}
			iPTR_FREE_ROW_ADDRESS = -1;
			if (stTEMP_ROW.uiAge > 0)
			{
				if (stTEMP_ROW.uiORD < CFG_SCHEDULER_MAX_ROW)
				{
					iPTR_GROWS = stTEMP_ROW.uiORD;
					pMEM_ROW = &stSTORAGE_IN_MEMORY.stCFG_SCHEDULER.aROWS[iPTR_GROWS];

					uiCRC16 = calcCRC16(0xFFFF,(uint8_t*)pMEM_ROW,sizeof(CFG_SCHEDULER_ROW) - sizeof(uint16_t));
					if ((uiCRC16 != pMEM_ROW->uiCRC16_ROW) || (pMEM_ROW->uiAge < stTEMP_ROW.uiAge))
					{
						*pMEM_ROW = stTEMP_ROW;
					}
				}
			}

		}
		uiFLASH_ADDR+=sizeof(CFG_SCHEDULER_ROW);
		iSCNR_FROWS++;
	}
	stSTORAGE_IN_MEMORY.iFIRST_SCHEDULER_ROW_WROTE_ADDRESS	= iPTR_FIRST_WRITE_ROW_ADDRESS;
	stSTORAGE_IN_MEMORY.iNEXT_SCHEDULER_ROW_FREE_ADDRESS 	= iPTR_FREE_ROW_ADDRESS;
}


void configWriteSchedule(CFG_SCHEDULER_ROW* pNEW_ROW);
void configWriteSchedule(CFG_SCHEDULER_ROW* pNEW_ROW)
{
	pNEW_ROW->uiAge++;
	if (pNEW_ROW->uiAge == 0)
	{
		pNEW_ROW->uiAge = 1;
	}

	pNEW_ROW->uiCRC16_ROW = calcCRC16(0xFFFF,(uint8_t*)pNEW_ROW,sizeof(CFG_SCHEDULER_ROW) - sizeof(uint16_t));
	confutilsWriteToFlash(stSTORAGE_IN_MEMORY.iNEXT_SCHEDULER_ROW_FREE_ADDRESS,sizeof(CFG_SCHEDULER_ROW),pNEW_ROW);
	stSTORAGE_IN_MEMORY.iNEXT_SCHEDULER_ROW_FREE_ADDRESS+=sizeof(CFG_SCHEDULER_ROW);
}

int configSaveNewSchedule(const CFG_SCHEDULER_ROW* pNEW_ROW)
{
	if (pNEW_ROW->uiORD < CFG_SCHEDULER_MAX_ROW)
	{
		CFG_SCHEDULER_ROW* pMEM_ROW;
		unsigned	uiCURR_AGE;
		pMEM_ROW = &stSTORAGE_IN_MEMORY.stCFG_SCHEDULER.aROWS[pNEW_ROW->uiORD];
		uiCURR_AGE = pMEM_ROW->uiAge;
		*pMEM_ROW = *pNEW_ROW;
		pMEM_ROW->uiAge=uiCURR_AGE;
		BSP_QSPI_Init();

		configWriteSchedule(pMEM_ROW);

		BSP_QSPI_DeInit();
		return 0;
	}
	return -1;
}


bool configurationsCheckFree(const void* bmem, size_t size)
{
	while (size)
	{
		if (*(((const uint8_t*)bmem++)) != 0xFF)
		{
			return false;
		}
		size--;
	}
	return true;
}

bool configurationsLoadCfgBaseSingle(unsigned uiITEM,CFG_BASE_BLOCK* pBLOCK, unsigned uiMAX_SIZE);
bool configurationsLoadCfgBaseSingle(unsigned uiITEM,CFG_BASE_BLOCK* pBLOCK, unsigned uiMAX_SIZE)
{
	if (uiITEM < 2)
	{
		uint32_t	uiFLASH_ADDR;
		uint16_t	uiCRC16;
		uint16_t	uiSIZE;
		uiFLASH_ADDR = CFG_BASE_FADDRESS_BASE_0 + (CFG_STORAGE_SECTOR_SIZE * uiITEM);
		memset(pBLOCK,0,sizeof(CFG_BASE_BLOCK));
		confutilsReadFromFlash(uiFLASH_ADDR,sizeof(CFG_BASE_HDR),&pBLOCK->stHDR);
		uiCRC16 = calcCRC16(0xFFFF,(uint8_t*)&pBLOCK->stHDR.uiSIZE,sizeof(CFG_BASE_HDR) - sizeof(uint16_t));
		if ((uiCRC16 == pBLOCK->stHDR.uiCRC_16_SZ) && (pBLOCK->stHDR.uiSIZE <= (uiMAX_SIZE - sizeof(CFG_BASE_HDR))))
		{
			uiSIZE = pBLOCK->stHDR.uiSIZE;
			confutilsReadFromFlash(uiFLASH_ADDR + sizeof(CFG_BASE_HDR),uiSIZE,&pBLOCK->stBASE);
			uiCRC16 = calcCRC16(0xFFFF,(uint8_t*)&pBLOCK->stBASE.iAGE,uiSIZE - sizeof(uint16_t));
			if (uiCRC16 == pBLOCK->stBASE.uiCRC_16)
			{
				return true;
			}
		}
	}
	return false;
}

CFG_BASE_BLOCK stBLOCK_TMP;
uint8_t	auiTMP_BUFFER[sizeof(CFG_BASE_BLOCK) + 32];

void configurationsLoadCfgBase(void)
{
	CFG_BASE_BLOCK* pBLOCK;
	int				iSCNR;
	int				iOLD_AGE;
	int				iNEXT_FREE;
	pBLOCK = (CFG_BASE_BLOCK*)&auiTMP_BUFFER[0];

	iOLD_AGE = -1;
	iNEXT_FREE = 0;
	iSCNR = 0;
	while (iSCNR < 2)
	{
		if (configurationsLoadCfgBaseSingle(iSCNR,pBLOCK,sizeof(auiTMP_BUFFER)))
		{
			if ((iOLD_AGE > 0xC0) && (pBLOCK->stBASE.iAGE < 0x20))
			{
				// wrap around
				iOLD_AGE -= 0x100;
			}
			if ((pBLOCK->stBASE.iAGE > iOLD_AGE))
			{
				stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER = *pBLOCK;
				iOLD_AGE = pBLOCK->stBASE.iAGE;
				iNEXT_FREE = iSCNR + 1;
			}
		}
		iSCNR++;
	}
	if (iNEXT_FREE >= 2)
	{
		iNEXT_FREE = 0;
	}
	stSTORAGE_IN_MEMORY.iNEXT_CFG_BASE_FREE = iNEXT_FREE;
	if (iOLD_AGE < 0)
	{
		configurationsDefaultCfgBase();
	}

	stSTORAGE_IN_MEMORY.stCFG_BASE = stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stBASE.stBASE;
}

__weak void configurationsDefaultCfgBase(void)
{
	memset(&stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER,0,sizeof(stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER));
}


int configSaveNewConfig(const CFG_BASE* pNEW_CFG)
{
	unsigned uiSIZE;
	stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stBASE.stBASE = *pNEW_CFG;
	stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stBASE.iAGE++;
	stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stBASE.iAGE &= 0xFF;
	if (stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stBASE.iAGE <= 0)
	{
		stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stBASE.iAGE = 1;
	}
	uiSIZE = sizeof(stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stBASE);
	stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stBASE.uiCRC_16 =
			calcCRC16(
					0xFFFF,
					(uint8_t*)&stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stBASE.iAGE,
					uiSIZE - sizeof(uint16_t)
					);
	stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stHDR.uiSIZE = uiSIZE;
	stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stHDR.uiCRC_16_SZ =
			calcCRC16(
					0xFFFF,
					(uint8_t*)&stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stHDR.uiSIZE,
					sizeof(stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER.stHDR.uiSIZE)
					);
	{
		uint32_t	uiFreeFlash;
		uint8_t		uiRet;

		uiFreeFlash = CFG_BASE_FADDRESS_BASE_0 + (CFG_STORAGE_SECTOR_SIZE * stSTORAGE_IN_MEMORY.iNEXT_CFG_BASE_FREE);

		stSTORAGE_IN_MEMORY.iNEXT_CFG_BASE_FREE++;
		if (stSTORAGE_IN_MEMORY.iNEXT_CFG_BASE_FREE >= 2)
		{
			stSTORAGE_IN_MEMORY.iNEXT_CFG_BASE_FREE = 0;
		}

		BSP_QSPI_Init();


		uiRet = BSP_QSPI_Erase_Block(uiFreeFlash);

		if (uiRet == QSPI_OK)
		{
			uiRet =  BSP_QSPI_Write(
					(uint8_t*)&stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER,
					uiFreeFlash,
					sizeof(stSTORAGE_IN_MEMORY.stCFG_BASE_SAVER));
		}

		BSP_QSPI_DeInit();
	}
	return 0;
}

#endif
