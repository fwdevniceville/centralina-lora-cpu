/*
 * confdata.h
 *
 *  Created on: 26 mar 2018
 *      Author: daniele_parise
 */

#ifndef STORAGE_CONFDATA_H_
#define STORAGE_CONFDATA_H_

typedef enum etagECONFIG_SLOT_CODE{
	CONFIG_SLOT_LORAWAN,
	CONFIG_SLOT_SYSTEM_PARAMETERS,
	_CONFIG_SLOT_FREE_02,
	_CONFIG_SLOT_FREE_03,
	_CONFIG_SLOT_FREE_04,
	_CONFIG_SLOT_FREE_05,
	_CONFIG_SLOT_FREE_06,
	_CONFIG_SLOT_FREE_07,
	__CONFIG_SLOT_MAX__
}ECONFIG_SLOT_CODE;


typedef struct tagCFG_COM_SCHEDULE_ITEM{
	uint16_t	uiMMStart;
	uint16_t	uiDsecPeriodStatus;
	uint8_t		uiDsecPeriodFastCmd;
	uint8_t		uiDsecPersistenceFastCmd;
}CFG_COM_SCHEDULE_ITEM; // 16

typedef struct tagCFG_SYSTEM_PARAMETERS{
	uint16_t	uiWorkPeriodsMask;	// bit0...bit7 3 ore per bit

	uint16_t	uiSecsPeriodWork;
	uint16_t	uiSecsPeriodFastUplinkWork;
	uint16_t	uiSecsTimeFastUplinkWork;

	uint16_t	uiSecsPeriodRest;
	uint16_t	uiSecsPeriodFastUplinkRest;
	uint16_t	uiSecsTimeFastUplinkRest;

	uint16_t	uiMinutesForReJoin;

	uint16_t	uiBatteryMin_mV;
	uint16_t	uiBatteryMax_mV;

	uint16_t	uiOutType;		// 0 mono , 1 bistabile
	uint16_t	uiOutLoad_mV;
	uint16_t	uiOutLoad_TLoad;
	uint16_t	uiOutLoad_TDischarge;
	uint16_t	uiNoDownlinkShutdown;

	uint16_t	uiIN0_TYPE;
	uint16_t	uiIN0_AR_sec;
	uint16_t	uiIN0_RAW_TH_HI;
	uint16_t	uiIN0_RAW_TH_LO;
	uint16_t	uiIN0_RAW_TH_DELTA;

	uint16_t	uiIN1_TYPE;
	uint16_t	uiIN1_AR_sec;
	uint16_t	uiIN1_RAW_TH_HI;
	uint16_t	uiIN1_RAW_TH_LO;
	uint16_t	uiIN1_RAW_TH_DELTA;

	uint16_t	uiIN2_COUNTER_ENABLE;
	uint16_t	uiIN2_COUNTER_AR_msec;
	uint16_t	uiIN2_COUNTER_AR2_msec;

}CFG_SYSTEM_PARAMETERS; // 16

typedef struct tagCFG_LORAWAN{
	uint8_t		uiRegion;
	uint8_t		uiADR_ENABLE;
	uint8_t		uiDEFAULT_DR;
	uint8_t		uiTxPower;

	uint8_t		uiSPARE04;
	uint8_t		uiSPARE05;
	uint8_t		uiSPARE06;
	uint8_t		uiABP;

	uint8_t		auiABP_DEV_ADDRESS[4];
	uint8_t		auiNWK_ID[4];

	uint8_t		auiDEV_EUI[8];
	uint8_t		auiJOIN_EUI[8];

	uint8_t		auiAPP_KEY__ABP_S_KEY[16];
	uint8_t		auiNWK_KEY__ABP_S_KEY[16];

	uint8_t		auiABP_S_NWK_S_INT_KEY[16];
	uint8_t		auiABP_F_NWK_S_INT_KEY[16];
}CFG_LORAWAN; // 16


typedef union tagUCFG_GEN_SLOT_DATA{
	uint8_t					aRAW[124];
	CFG_LORAWAN				stLORAWAN;
	CFG_SYSTEM_PARAMETERS	stSYSTEM_PARAMETERS;
}UCFG_GEN_SLOT_DATA;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
typedef struct tagCHECK_UCFG_GEN_SLOT_DATA_SIZE{int aiSize[(124 - sizeof(UCFG_GEN_SLOT_DATA))];}CHECK_UCFG_GEN_SLOT_DATA_SIZE;
#pragma GCC diagnostic pop



#endif /* STORAGE_CONFDATA_H_ */
