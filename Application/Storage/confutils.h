/*
 * confutils.h
 *
 *  Created on: 27 feb 2018
 *      Author: daniele_parise
 */

#ifndef STORAGE_CONFUTILS_H_
#define STORAGE_CONFUTILS_H_

void confutilsInitFlash(void);
void confutilsClearFlashErrors(void);
int confutilsReadFromFlash(uint32_t uiFlashAddress,uint32_t uiSize,void* pRamDest);
int confutilsWriteToFlash(uint32_t uiFlashAddress,uint32_t uiSize,const void* pRamSrc);


#endif /* STORAGE_CONFUTILS_H_ */
