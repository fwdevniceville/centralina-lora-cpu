/*
 * confdata.c
 *
 *  Created on: 26 mar 2018
 *      Author: daniele_parise
 */

#include <stdint.h>
#include <stdbool.h>
#include "configurations.h"
#include <string.h>



//const uint8_t	auiCON_GEN_SLOT_CODE_
void configConfigDefaultSet(ECONFIG_SLOT_CODE eCODE, UCFG_GEN_SLOT_DATA* puDATA)
{
	switch (eCODE)
	{
	default:
		break;
	case CONFIG_SLOT_LORAWAN:
		{
			const uint8_t	aAppKey[16] = { 0x47, 0xba, 0xc6, 0x58, 0x60, 0x54, 0x73, 0x29, 0xef, 0x63, 0x97, 0xc1, 0x04, 0x02, 0xde, 0x97 };
			puDATA->stLORAWAN.uiADR_ENABLE = 0x01;
			puDATA->stLORAWAN.uiDEFAULT_DR = 0x00;

			memcpy(puDATA->stLORAWAN.auiAPP_KEY__ABP_S_KEY,aAppKey,16);
			memcpy(puDATA->stLORAWAN.auiNWK_KEY__ABP_S_KEY,aAppKey,16);
		}
		break;
	case CONFIG_SLOT_SYSTEM_PARAMETERS:
		{
			puDATA->stSYSTEM_PARAMETERS.uiWorkPeriodsMask = 0x00;

			puDATA->stSYSTEM_PARAMETERS.uiSecsPeriodWork			= 60;
			puDATA->stSYSTEM_PARAMETERS.uiSecsPeriodFastUplinkWork	= 10;
			puDATA->stSYSTEM_PARAMETERS.uiSecsTimeFastUplinkWork	= 60;

			puDATA->stSYSTEM_PARAMETERS.uiSecsPeriodRest			= 60;
			puDATA->stSYSTEM_PARAMETERS.uiSecsPeriodFastUplinkRest	= 10;
			puDATA->stSYSTEM_PARAMETERS.uiSecsTimeFastUplinkRest	= 60;

			puDATA->stSYSTEM_PARAMETERS.uiMinutesForReJoin			= 0;

			puDATA->stSYSTEM_PARAMETERS.uiOutType				= 1;		// 0 mono , 1 bistabile
			puDATA->stSYSTEM_PARAMETERS.uiOutLoad_mV			= 9000;
			puDATA->stSYSTEM_PARAMETERS.uiOutLoad_TLoad			= 4000;
			puDATA->stSYSTEM_PARAMETERS.uiOutLoad_TDischarge	= 200;
			puDATA->stSYSTEM_PARAMETERS.uiNoDownlinkShutdown	= 0;

			puDATA->stSYSTEM_PARAMETERS.uiBatteryMax_mV			= 4500;
			puDATA->stSYSTEM_PARAMETERS.uiBatteryMin_mV			= 3100;

			puDATA->stSYSTEM_PARAMETERS.uiIN0_TYPE				= 2;
			puDATA->stSYSTEM_PARAMETERS.uiIN0_AR_sec			= 10;
			puDATA->stSYSTEM_PARAMETERS.uiIN0_RAW_TH_HI			= 1024;
			puDATA->stSYSTEM_PARAMETERS.uiIN0_RAW_TH_LO			= 100;
			puDATA->stSYSTEM_PARAMETERS.uiIN0_RAW_TH_DELTA		= 50;

			puDATA->stSYSTEM_PARAMETERS.uiIN1_TYPE				= 2;
			puDATA->stSYSTEM_PARAMETERS.uiIN1_AR_sec			= 10;
			puDATA->stSYSTEM_PARAMETERS.uiIN1_RAW_TH_HI			= 1024;
			puDATA->stSYSTEM_PARAMETERS.uiIN1_RAW_TH_LO			= 100;
			puDATA->stSYSTEM_PARAMETERS.uiIN1_RAW_TH_DELTA		= 50;

			puDATA->stSYSTEM_PARAMETERS.uiIN2_COUNTER_ENABLE	= 1;
			puDATA->stSYSTEM_PARAMETERS.uiIN2_COUNTER_AR_msec	= 100;
		}
		break;
	}
}
