/*
 * confutils.c
 *
 *  Created on: 27 feb 2018
 *      Author: daniele_parise
 */

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "confutils.h"
#include "configurations.h"
#include "hal_inc.h"
#include <string.h>

void confutilsInitFlash(void)
{
	confutilsClearFlashErrors();
}

void confutilsClearFlashErrors(void)
{
	__HAL_FLASH_CLEAR_FLAG(
			FLASH_FLAG_WRPERR	|
			FLASH_FLAG_PGAERR	|
			FLASH_FLAG_SIZERR	|
			FLASH_FLAG_OPTVERR	|
			FLASH_FLAG_RDERR	|
			FLASH_FLAG_FWWERR	|
			FLASH_FLAG_NOTZEROERR
		);
}

int confutilsReadFromFlash(uint32_t uiFlashAddress,uint32_t uiSize,void* pRamDest)
{
	if (((uiFlashAddress & 3) == 0) &&  ((uiSize & 3) == 0))
	{
		if ((uiFlashAddress >= DATA_EEPROM_BASE) && (uiSize > 0) && ((uiFlashAddress + uiSize) <= (DATA_EEPROM_BANK1_END + 1)))
		{
			memcpy(pRamDest,(void*)uiFlashAddress,uiSize);
			return 0;
		}
	}
	return -1;
}

int confutilsWriteToFlash(uint32_t uiFlashAddress,uint32_t uiSize,const void* pRamSrc)
{
	confutilsClearFlashErrors();
	if (((uiFlashAddress & 3) == 0) &&  ((uiSize & 3) == 0))
	{
		if ((uiFlashAddress >= DATA_EEPROM_BASE) && (uiSize > 0) && ((uiFlashAddress + uiSize) < (DATA_EEPROM_BANK1_END)))
		{
			uint32_t*	aData;
			aData = (uint32_t*)(pRamSrc);
			HAL_FLASHEx_DATAEEPROM_Unlock();
			while (uiSize > 0)
			{
				if ((*(uint32_t*)uiFlashAddress) != *aData)
				{
					HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD,uiFlashAddress,*aData);
				}
				aData++;
				uiFlashAddress+=4;
				uiSize-=4;
			}
			HAL_FLASHEx_DATAEEPROM_Lock();
			return 0;
		}
	}
	return -1;
}

