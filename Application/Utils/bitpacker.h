/*
 * bitpacker.h
 *
 *  Created on: 26 mar 2018
 *      Author: daniele_parise
 */

#ifndef UTILS_BITPACKER_H_
#define UTILS_BITPACKER_H_


typedef struct tagBITPACKER
{
	const	uint8_t*		pBufferPop;
	uint8_t*				pBufferPush;
	uint16_t				uiArraySize;
	uint16_t				uiArrayPos;
	int16_t					iCurrBit;
	int16_t					iMaxBit;
}BITPACKER;

void bitpackerInitPop (BITPACKER* pBitPacker, const uint8_t* pBuffer, uint16_t uiSize);
void bitpackerInitPush (BITPACKER* pBitPacker, uint8_t* pBuffer, uint16_t uiSize);
bool bitpackerPushTestFreeBits(BITPACKER* pBitPacker,uint16_t uiBitSize);

void bitpackerPushIntValue (BITPACKER* pBitPacker,int iVal, uint16_t uiBitSize);
void bitpackerPushUIntValue (BITPACKER* pBitPacker,unsigned uiVal, uint16_t uiBitSize);
unsigned bitpackerGetPushedBytes (BITPACKER* pBitPacker);

void bitpackerPopIntValue (BITPACKER* pBitPacker,int* piVal, uint16_t uiBitSize);
void bitpackerPopUIntValue (BITPACKER* pBitPacker,unsigned* puiVal, uint16_t uiBitSize);


#endif /* UTILS_BITPACKER_H_ */
