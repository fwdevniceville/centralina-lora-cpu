/*
 * crc16.c
 *
 *  Created on: 27 feb 2018
 *      Author: daniele_parise
 */
#include <stdint.h>
#include <stddef.h>
#include "crc16.h"

uint16_t calcCRC16(uint16_t wCONTINUE_FROM, const uint8_t* pbyBuffer, unsigned uiSize)
{
	uint16_t	wRetCRC;
	size_t		uiScanner;
	uint8_t		byShiftCounter;

	wRetCRC = wCONTINUE_FROM;
	uiScanner = 0;

	while (uiScanner < uiSize)
	{
		wRetCRC = (wRetCRC & 0xFF00) | (((uint8_t)(wRetCRC) ^ pbyBuffer[uiScanner]) & 0x00FF);

		byShiftCounter = 0;
		while (byShiftCounter < 8 )
		{
			byShiftCounter++;
			if (wRetCRC & 0x0001)
			{
				wRetCRC >>= 1;
				wRetCRC ^= 0xA001;
			}
			else
			{
				wRetCRC >>= 1;
			}
		}
		uiScanner++;
	}

	return wRetCRC;
}
