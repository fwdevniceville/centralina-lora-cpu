/*
 * utilities.h
 *
 *  Created on: 26 mar 2018
 *      Author: daniele_parise
 */

#ifndef UTILS_UTILITIES_H_
#define UTILS_UTILITIES_H_
#include "stm32l4xx_hal.h"

typedef uint64_t TimerTime_t;
#define DBG( x ) do{  } while(0)
#define DBG_PRINTF(...)
#define DBG_PRINTF_CRITICAL(...)

#define BACKUP_PRIMASK()  uint32_t primask_bit= __get_PRIMASK()
#define DISABLE_IRQ() __disable_irq()
#define ENABLE_IRQ() __enable_irq()
#define RESTORE_PRIMASK() __set_PRIMASK(primask_bit)

#endif /* UTILS_UTILITIES_H_ */
