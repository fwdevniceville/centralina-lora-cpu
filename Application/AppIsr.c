/*
 * AppIsr.c
 *
 *  Created on: 11 feb 2018
 *      Author: daniele_parise
 */


#include <stdbool.h>
#include "cmsis_os.h"
#include "../Application/Tasks/taskInterface.h"
#include "../Application/Tasks/taskScheduler.h"
#include "../Application/menu-loc/menu_vars.h"

/*
void EXTI_IRQHandler(unsigned uiFROM,unsigned uiTO);
void EXTI_IRQHandler(unsigned uiFROM,unsigned uiTO)
{
	unsigned uiSTART;
	uiSTART = GPIO_PIN_0;
	while (uiSTART <= GPIO_PIN_1)
	{
		if(__HAL_GPIO_EXTI_GET_IT(uiSTART))
		{
			__HAL_GPIO_EXTI_CLEAR_IT(uiSTART);
			HAL_GPIO_EXTI_Callback(uiSTART);
		}
		uiSTART <<=1;
	}

}

void EXTI0_1_IRQHandler(void)
{
	EXTI_IRQHandler(GPIO_PIN_0,GPIO_PIN_1);
}

void EXTI2_3_IRQHandler(void)
{
	EXTI_IRQHandler(GPIO_PIN_2,GPIO_PIN_3);
}


void EXTI4_15_IRQHandler(void)
{
	EXTI_IRQHandler(GPIO_PIN_4,GPIO_PIN_15);
}

*/

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin & (GPIO_PIN_5))
	{
		taskSchedulerSignalFluxCNTR_ISR();
	}
	if (GPIO_Pin & (GPIO_PIN_0 | GPIO_PIN_6 | GPIO_PIN_13))
	{
		taskInterfacePostEvent(TASKINTERFACE_EVT_BTN);
	}
}
