/*
 * osintcom.h
 *
 *  Created on: 15 mar 2018
 *      Author: daniele_parise
 */

#ifndef FEERTOS_OSINTCOM_H_
#define FEERTOS_OSINTCOM_H_

void osintcomInit(void);
void osintcomFree(void);

#define	OSINTCOM_TX_PAYLOAD_MAX			(256)
typedef struct tagOSINTCOM_TX{
	uint8_t			uiCODE;
	uint8_t			uiTransferReplies;
	uint16_t 		uiPayLoadSize;
	uint8_t 		aPayLoad[OSINTCOM_TX_PAYLOAD_MAX];
}OSINTCOM_TX;

#define	OSINTCOM_RX_PAYLOAD_MAX			(256)

typedef struct tagOSINTCOM_RX{
	uint8_t		uiResult;
	uint8_t		uiTransferRepliesUsed;
	uint16_t 	uiPayLoadFilled;
	uint8_t 	aPayLoad[OSINTCOM_RX_PAYLOAD_MAX];
}OSINTCOM_RX;

int osintcomExchangeData(const OSINTCOM_TX* pTX,OSINTCOM_RX* pRX,unsigned uiTransferTimeOut_ms);


#endif /* FEERTOS_OSINTCOM_H_ */
