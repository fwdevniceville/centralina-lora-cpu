/*
 * FRT_BoardFunctions.c
 *
 *  Created on: 15 feb 2018
 *      Author: daniele_parise
 */
#include "FreeRTOS.h"
#include "FRT_BoardFunctions.h"
#include "task.h"
#include "cmsis_os.h"
#include "../SystemFunctions.h"
#include "../intcom_hw/intcom_hw.h"
#include "../intcom/intcom.h"
typedef enum etagESLEEP_MODE{
	SLEEP_MODE_NONE,
	SLEEP_MODE_IDLE_NO_TIMEOUT,
	SLEEP_MODE_STOP2_NO_TIMEOUT,
	SLEEP_MODE_STOP2
}ESLEEP_MODE;

typedef struct tagFRT_CUST_SUPPORT{
	ESLEEP_MODE	eCURRENT_SLEEP_MODE;
#ifdef DEBUG
	DateTime	dateEnter;
	DateTime	dateExit;
#endif
	uint64_t	uiEnterTime_ms;
	uint64_t	uiExitTime_ms;
	uint32_t	uiRunningRqstMask;
	uint32_t	xExpectedIdleTime;
	uint32_t	xNextTaskUnblockTime;
	uint32_t	xTickCount;
}FRT_CUST_SUPPORT;

FRT_CUST_SUPPORT	stFRT_CUST_SUPPORT;

#define traceLOW_POWER_IDLE_BEGIN()	systemSleepSetTimerTicksValues(xExpectedIdleTime,xNextTaskUnblockTime,xTickCount);

void FRT_START_SleepSupportTimer(unsigned uiTimeout_ms);
unsigned FRT_STOP_SleepSupportTimer(void);

void systemSleepSetTimerTicksValues(uint32_t xExpectedIdleTime, uint32_t xNextTaskUnblockTime, uint32_t xTickCount)
{
	stFRT_CUST_SUPPORT.xExpectedIdleTime = xExpectedIdleTime;
	stFRT_CUST_SUPPORT.xNextTaskUnblockTime = xNextTaskUnblockTime;
	stFRT_CUST_SUPPORT.xTickCount = xTickCount;
}

void FRT_SetRunningRQST(EFRT_TASK_ORDER_ID eTaskOrderId,bool bRunning)
{
	if (bRunning)
	{
		stFRT_CUST_SUPPORT.uiRunningRqstMask |= (1 << (int)eTaskOrderId);
	}
	else
	{
		stFRT_CUST_SUPPORT.uiRunningRqstMask &= ~(1 << (int)eTaskOrderId);
	}
}

void FRT_SleepEnterInStop(void);
void FRT_SleepRestoreFromStop(void);


void PreSleepProcessing(uint32_t *ulExpectedIdleTime)
{
	eSleepModeStatus	eSleepStatus;

	stFRT_CUST_SUPPORT.eCURRENT_SLEEP_MODE = SLEEP_MODE_NONE;
	eSleepStatus = eTaskConfirmSleepModeStatus();
	if (eSleepStatus == eNoTasksWaitingTimeout)
	{
		stFRT_CUST_SUPPORT.eCURRENT_SLEEP_MODE = SLEEP_MODE_IDLE_NO_TIMEOUT;
	}

	if (
			(stFRT_CUST_SUPPORT.uiRunningRqstMask == 0)
			&&
			(*ulExpectedIdleTime > 20)
		)
	{
		if (stFRT_CUST_SUPPORT.eCURRENT_SLEEP_MODE != SLEEP_MODE_IDLE_NO_TIMEOUT)
		{
			FRT_START_SleepSupportTimer(stFRT_CUST_SUPPORT.xExpectedIdleTime);

			stFRT_CUST_SUPPORT.eCURRENT_SLEEP_MODE = SLEEP_MODE_STOP2;
		}
		else
		{
			stFRT_CUST_SUPPORT.eCURRENT_SLEEP_MODE = SLEEP_MODE_STOP2_NO_TIMEOUT;
		}

	}

	if (stFRT_CUST_SUPPORT.eCURRENT_SLEEP_MODE != SLEEP_MODE_NONE)
	{
		CLEAR_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);
		HAL_SuspendTick();
		__HAL_RCC_TIM2_CLK_DISABLE();

#ifdef DEBUG
		stFRT_CUST_SUPPORT.uiEnterTime_ms = systemGetCurrentDateTime_ms(&stFRT_CUST_SUPPORT.dateEnter);
#else
		stFRT_CUST_SUPPORT.uiEnterTime_ms = systemGetCurrentDateTime_ms(NULL);
#endif


		switch (stFRT_CUST_SUPPORT.eCURRENT_SLEEP_MODE)
		{
		default:
			break;
		case SLEEP_MODE_STOP2:
		case SLEEP_MODE_STOP2_NO_TIMEOUT:

			FRT_SleepEnterInStop();

			*ulExpectedIdleTime = 0; // WFI gia' eseguito


			intcom_process_lpm_rx();

			break;
		}



	}
}

unsigned	uiRetTimer;
void PostSleepProcessing(uint32_t *ulExpectedIdleTime)
{
	switch (stFRT_CUST_SUPPORT.eCURRENT_SLEEP_MODE)
	{
	default:
		break;
	case SLEEP_MODE_STOP2:

	case SLEEP_MODE_STOP2_NO_TIMEOUT:
		{
			/*
			__asm volatile( "cpsie i" );
			// facciamo schattere l'interrupt pendente
			__asm volatile( "cpsid i" );
			*/
			FRT_SleepRestoreFromStop();
		}
	case SLEEP_MODE_IDLE_NO_TIMEOUT:
		{
			int64_t	timeDelta;
			uint32_t	uiMaxDelta;
#ifdef DEBUG
			stFRT_CUST_SUPPORT.uiExitTime_ms = systemGetCurrentDateTime_ms(&stFRT_CUST_SUPPORT.dateExit);
#else
			stFRT_CUST_SUPPORT.uiExitTime_ms = systemGetCurrentDateTime_ms(NULL);
#endif
			timeDelta = stFRT_CUST_SUPPORT.uiExitTime_ms - stFRT_CUST_SUPPORT.uiEnterTime_ms;

			uiMaxDelta = stFRT_CUST_SUPPORT.xNextTaskUnblockTime - stFRT_CUST_SUPPORT.xTickCount;

			uiRetTimer = FRT_STOP_SleepSupportTimer();

			if (uiMaxDelta > 0)
			{
				uiMaxDelta--; // mal nol fa!!!
			}
//			stFRT_CUST_SUPPORT.xExpectedIdleTime = xExpectedIdleTime;
//			stFRT_CUST_SUPPORT.xNextTaskUnblockTime = xNextTaskUnblockTime;
//			stFRT_CUST_SUPPORT.xTickCount = xTickCount;
/*
			if (timeDelta > *ulExpectedIdleTime)
			{
				timeDelta = *ulExpectedIdleTime;
			}
*/
			if (timeDelta > uiMaxDelta)
			{
				timeDelta = uiMaxDelta;
			}
			if (timeDelta < 0)
			{
				timeDelta = 0;
			}

			vTaskStepTick( (TickType_t)timeDelta );

//			WRITE_REG(SysTick->VAL,0);
//			*ulExpectedIdleTime = 0;

			__HAL_RCC_TIM2_CLK_ENABLE();
			HAL_ResumeTick();
//			SET_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);
		}
		break;
	}
	stFRT_CUST_SUPPORT.eCURRENT_SLEEP_MODE = SLEEP_MODE_NONE;
}

void FRT_SleepEnterInStop(void)
{
#if (PREFETCH_ENABLE != 0)
	//  Disable Prefetch Buffer
	// Forse togliamo il clock dalla flash
	__HAL_FLASH_PREFETCH_BUFFER_DISABLE();
#endif

//	DBGMCU_CR
	  intcom_hwEnterStopMode();

//!! NUOVO SPEGNAMO I CLOCK DELLe PERIFERICE _NON USATE
	  __HAL_RCC_GPIOE_CLK_DISABLE();
	  __HAL_RCC_GPIOC_CLK_DISABLE();
	  __HAL_RCC_GPIOA_CLK_DISABLE();
	  __HAL_RCC_GPIOB_CLK_DISABLE();
	  __HAL_RCC_GPIOD_CLK_DISABLE();


	// Confermiamo che il clock di  sistema duramte e dopo lo Sleep sia HSI
	__HAL_RCC_WAKEUPSTOP_CLK_CONFIG(RCC_STOP_WAKEUPCLOCK_HSI);

	// Dobbiamo armeggiare con i resistri PWR
	__HAL_RCC_PWR_CLK_ENABLE();
	// Per entrare in Stop2 non dobbiamo essere in RUN LP
	HAL_PWREx_DisableLowPowerRunMode();
	SET_BIT(PWR->CR, PWR_CR_CWUF);
	HAL_PWR_EnterSTOPMode(PWR_MAINREGULATOR_ON,PWR_STOPENTRY_WFI);
}

void SystemClock_RestoreConfig(void);

void FRT_SleepRestoreFromStop(void)
{
#if (PREFETCH_ENABLE != 0)
  	// Riabilitiamo il prefetch dalla flash
	__HAL_FLASH_PREFETCH_BUFFER_ENABLE();
#endif

	SystemClock_RestoreConfig();

	intcom_hwExitStopMode();

	//!! NUOVO ACCENDIAMO I CLOCK DELLe PERIFERICE _NON USATE
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();


	// Riconfiguramo Clock e PLL
}

void SystemClock_RestoreConfig( void)
{
  /* After wake-up from STOP reconfigure the system clock */
  /* Enable HSI */
  __HAL_RCC_HSI_ENABLE();

  /* Wait till HSI is ready */
  while( __HAL_RCC_GET_FLAG(RCC_FLAG_HSIRDY) == RESET ) {}
#if 1
  __HAL_RCC_SYSCLK_CONFIG ( RCC_SYSCLKSOURCE_HSI );
#else
  /* Enable PLL */
  __HAL_RCC_PLL_ENABLE();
  /* Wait till PLL is ready */
  while( __HAL_RCC_GET_FLAG( RCC_FLAG_PLLRDY ) == RESET ) {}

  /* Select PLL as system clock source */
  __HAL_RCC_SYSCLK_CONFIG ( RCC_SYSCLKSOURCE_PLLCLK );

  /* Wait till PLL is used as system clock source */
  while( __HAL_RCC_GET_SYSCLK_SOURCE( ) != RCC_SYSCLKSOURCE_STATUS_PLLCLK ) {}
#endif
}

uint64_t	uiStartTime4Stats;
uint64_t	uiStartTime4StatsDelta;

void configureTimerForRunTimeStats(void)
{
	uiStartTime4Stats = systemGetCurrentDateTime_ms(NULL);
}

unsigned long getRunTimeCounterValue(void)
{
	unsigned long	ulDelta;
	unsigned long	ulNewTime;


	ulNewTime = systemGetCurrentDateTime_ms(NULL);
	if (uiStartTime4Stats == 0)
	{
		uiStartTime4Stats = ulNewTime;
		uiStartTime4StatsDelta = ulNewTime;
	}

	ulDelta = ulNewTime - uiStartTime4StatsDelta;
	uiStartTime4StatsDelta = ulNewTime;
	if (ulDelta > 60000)
	{
		ulDelta = 0;
	}
	else
	{
		ulDelta = ulNewTime - uiStartTime4Stats;
	}
	return ulDelta;
}


/* USER CODE END PREPOSTSLEEP */

void SystemPeripheralClockDisable(void)
{
}

//LPTIM1->ISR;      /*!< LPTIM Interrupt and Status register,             Address offset: 0x00 */
//LPTIM1->ICR;      /*!< LPTIM Interrupt Clear register,                  Address offset: 0x04 */
//LPTIM1->IER;      /*!< LPTIM Interrupt Enable register,                 Address offset: 0x08 */
//LPTIM1->CFGR;     /*!< LPTIM Configuration register,                    Address offset: 0x0C */
//LPTIM1->CR;       /*!< LPTIM Control register,                          Address offset: 0x10 */
//LPTIM1->CMP;      /*!< LPTIM Compare register,                          Address offset: 0x14 */
//LPTIM1->ARR;      /*!< LPTIM Autoreload register,                       Address offset: 0x18 */
//LPTIM1->CNT;      /*!< LPTIM Counter register,                          Address offset: 0x1C */


#define LPTIM_PRESCALER_DIV32                   ((uint32_t)(LPTIM_CFGR_PRESC_0 | LPTIM_CFGR_PRESC_2))

void FRT_START_SleepSupportTimer(unsigned uiTimeout_ms)
{
    MODIFY_REG(RCC->CCIPR,RCC_CCIPR_LPTIM1SEL,RCC_LPTIM1CLKSOURCE_LSE);

//  __HAL_RCC_LPTIM1_FORCE_RESET();
//    __HAL_RCC_LPTIM1_RELEASE_RESET();

    __HAL_RCC_LPTIM1_CLK_ENABLE();
    __HAL_RCC_LPTIM1_CLK_SLEEP_ENABLE();


    WRITE_REG(LPTIM1->CR, 0);
    WRITE_REG(LPTIM1->CFGR, LPTIM_PRESCALER_DIV32);
    WRITE_REG(LPTIM1->CR, LPTIM_CR_ENABLE);
    if (uiTimeout_ms > 60000)
    {
    	uiTimeout_ms = 60000;
    }
    uiTimeout_ms *=1024;
    uiTimeout_ms /=1000;
    WRITE_REG(LPTIM1->ARR, 65535);
    WRITE_REG(LPTIM1->CMP, uiTimeout_ms);
    WRITE_REG(LPTIM1->CNT, 0);

    WRITE_REG(LPTIM1->IER, LPTIM_IER_CMPMIE);

	HAL_NVIC_SetPriority(LPTIM1_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(LPTIM1_IRQn);

    WRITE_REG(LPTIM1->CR, LPTIM_CR_ENABLE | LPTIM_CR_SNGSTRT);

}

unsigned FRT_STOP_SleepSupportTimer(void)
{
	unsigned uiTimeElapsM;

	uiTimeElapsM =  READ_REG(LPTIM1->CNT);

	WRITE_REG(LPTIM1->CR, 0);


//    __HAL_RCC_LPTIM1_FORCE_RESET();
//    __HAL_RCC_LPTIM1_CLK_SLEEP_DISABLE();
//	__HAL_RCC_LPTIM1_CLK_DISABLE();

	uiTimeElapsM *=1000;
	uiTimeElapsM /=1024;

	return uiTimeElapsM;
}

void LPTIM1_IRQHandler(void)
{
    WRITE_REG(LPTIM1->ICR,
    		LPTIM_ICR_CMPMCF |
			LPTIM_ICR_ARRMCF |
			LPTIM_ICR_EXTTRIGCF |
			LPTIM_ICR_CMPOKCF |
			LPTIM_ICR_ARROKCF |
			LPTIM_ICR_UPCF |
			LPTIM_ICR_DOWNCF);

}
