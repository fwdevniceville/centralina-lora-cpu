/*
 * hw_uart_brd.c
 *
 *  Created on: 11 mar 2018
 *      Author: daniele_parise
 */

#include <stdbool.h>
#include <stdint.h>
#include "hw_bridge.h"
#include "../intcom/intcom.h"
#include <string.h>


uint16_t	uiPORTA_CKL_IMAGE;
uint16_t	uiPORTD_CKL_IMAGE;

void hw_bridge_gpio_clock_set(GPIO_TypeDef * uiPORT,unsigned uiPIN,bool bSet)
{
	uint16_t*	puiPORT_CKL_IMAGE;
	uint16_t	uiNULL_PORT;
	uint16_t	uiMASK;
	puiPORT_CKL_IMAGE = &uiNULL_PORT;
	switch ((uintptr_t)uiPORT)
	{
	case GPIOA_BASE:
		{
			puiPORT_CKL_IMAGE = &uiPORTA_CKL_IMAGE;
		}
		break;
	case GPIOD_BASE:
		{
			puiPORT_CKL_IMAGE = &uiPORTD_CKL_IMAGE;
		}
		break;
	}

//	uiMASK = 1<<uiPIN;
	uiMASK = uiPIN;

	if (bSet)
	{
		*puiPORT_CKL_IMAGE |= uiMASK;
	}
	else
	{
		*puiPORT_CKL_IMAGE &= ~uiMASK;
	}
	switch ((uintptr_t)uiPORT)
	{
	case (uintptr_t)GPIOA_BASE:
		{
			if (uiPORTA_CKL_IMAGE)
			{
				__GPIOA_CLK_ENABLE();
			}
			else
			{
				__GPIOA_CLK_DISABLE();
			}
		}
		break;
	case (uintptr_t)GPIOD_BASE:
		{
			if (uiPORTD_CKL_IMAGE)
			{
				__GPIOD_CLK_ENABLE();
			}
			else
			{
				__GPIOD_CLK_DISABLE();
			}
		}
		break;
	}
}

void hw_bridge_gpio_clock_enable(GPIO_TypeDef * uiPORT,unsigned uiPIN)
{
	hw_bridge_gpio_clock_set(uiPORT,uiPIN,true);
}




void hw_bridge_gpio_clock_disable(GPIO_TypeDef * uiPORT,unsigned uiPIN)
{
	hw_bridge_gpio_clock_set(uiPORT,uiPIN,false);
}






/**
  * @brief UART MSP Initialization
  *        This function configures the hardware resources used in this example:
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration
  *           - NVIC configuration for UART interrupt request enable
  * @param huart: UART handle pointer
  * @retval None
  */
/*
void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART2)
	{
	    __HAL_RCC_USART2_CLK_ENABLE();
		intcom_IoInit( );
	}
}

void HAL_UART_MspDeInit(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART2)
	{
		__USART2_CLK_ENABLE();
		intcom_IoDeInit( );
	}
}

*/

void USART2_IRQHandler( void )
{
   intcom_IRQHandler( );
}


#define         ID1                                 ( 0x1FF80050 )
#define         ID2                                 ( 0x1FF80054 )
#define         ID3                                 ( 0x1FF80064 )


void hwGetDeviceID(DEVICE_HW_ID * pID)
{
	memset(pID,0,sizeof(DEVICE_HW_ID));
	pID->aDevEUI[7] = ( ( *( uint32_t* )ID1 )+ ( *( uint32_t* )ID3 ) ) >> 24;
	pID->aDevEUI[6] = ( ( *( uint32_t* )ID1 )+ ( *( uint32_t* )ID3 ) ) >> 16;
	pID->aDevEUI[5] = ( ( *( uint32_t* )ID1 )+ ( *( uint32_t* )ID3 ) ) >> 8;
	pID->aDevEUI[4] = ( ( *( uint32_t* )ID1 )+ ( *( uint32_t* )ID3 ) );
	pID->aDevEUI[3] = ( ( *( uint32_t* )ID2 ) ) >> 24;
	pID->aDevEUI[2] = ( ( *( uint32_t* )ID2 ) ) >> 16;
	pID->aDevEUI[1] = ( ( *( uint32_t* )ID2 ) ) >> 8;
	pID->aDevEUI[0] = ( ( *( uint32_t* )ID2 ) );

	*((uint32_t*)&pID->aRAND_SEED[0]) = ( ( *( uint32_t* )ID1 ) ^ ( *( uint32_t* )ID2 ) ^ ( *( uint32_t* )ID3 ) );
	*((uint32_t*)&pID->aSERIAL[0]) = *( uint32_t* )ID1;
	*((uint32_t*)&pID->aSERIAL[4]) = *( uint32_t* )ID2;
	*((uint32_t*)&pID->aSERIAL[8]) = *( uint32_t* )ID3;
}

uint32_t HW_GetRandomSeed( void )
{
  return ( ( *( uint32_t* )ID1 ) ^ ( *( uint32_t* )ID2 ) ^ ( *( uint32_t* )ID3 ) );
}

void HW_GetUniqueId( uint8_t *id )
{
    id[7] = ( ( *( uint32_t* )ID1 )+ ( *( uint32_t* )ID3 ) ) >> 24;
    id[6] = ( ( *( uint32_t* )ID1 )+ ( *( uint32_t* )ID3 ) ) >> 16;
    id[5] = ( ( *( uint32_t* )ID1 )+ ( *( uint32_t* )ID3 ) ) >> 8;
    id[4] = ( ( *( uint32_t* )ID1 )+ ( *( uint32_t* )ID3 ) );
    id[3] = ( ( *( uint32_t* )ID2 ) ) >> 24;
    id[2] = ( ( *( uint32_t* )ID2 ) ) >> 16;
    id[1] = ( ( *( uint32_t* )ID2 ) ) >> 8;
    id[0] = ( ( *( uint32_t* )ID2 ) );
}

