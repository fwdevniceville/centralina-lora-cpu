/*
 * hwbuttons.h
 *
 *  Created on: 22 apr 2019
 *      Author: daniele_parise
 */

#ifndef HWBRD_HWBUTTONS_H_
#define HWBRD_HWBUTTONS_H_

#define hwbuttonsTestBTN1()		(READ_BIT(GPIOA->IDR,GPIO_PIN_0))
#define hwbuttonsTestBTN2()		(READ_BIT(GPIOC->IDR,GPIO_PIN_13))
#define hwbuttonsTestBTN3()		(READ_BIT(GPIOE->IDR,GPIO_PIN_6))


#endif /* HWBRD_HWBUTTONS_H_ */
