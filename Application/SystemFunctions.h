/*
 * SystemFunctions.h
 *
 *  Created on: 21 feb 2018
 *      Author: daniele_parise
 */

#ifndef SYSTEMFUNCTIONS_H_
#define SYSTEMFUNCTIONS_H_

void SystemSettings(void);

void SystemClock_ConfigNORM(void);
void SystemClock_ConfigLP(void);

typedef struct tagDateTime
{
	int	tm_msec;
	int	tm_sec;
	int	tm_min;
	int	tm_hour;
	int	tm_mday;
	int	tm_mon;
	int	tm_year;
	int	tm_wday;
	int	tm_yday;
}DateTime;

#define  SECONDS_IN_1DAY   (uint32_t) 86400
#define  SECONDS_IN_1HOUR   (uint32_t) 3600
#define  SECONDS_IN_1MINUTE   (uint32_t) 60
#define  MINUTES_IN_1HOUR    (uint32_t) 60
#define  HOURS_IN_1DAY      (uint32_t) 24

uint64_t systemGetCurrentDateTime(DateTime* t);
uint64_t systemDateTime2secondsFrom2K(DateTime* t);
uint32_t systemYear2DaysFrom2K(int iYear);
uint64_t systemYearYDay2secondsFrom2K(int iYear,int iYday);
uint64_t systemYearMonthDay2secondsFrom2K(int iYear, int iMonth, int iMday);

uint64_t systemGetCurrentDateTime_ms(DateTime* t);

typedef struct tagSYS_DATETIME_SET_GET{
	unsigned	Year;
	unsigned	Month;
	unsigned	MDay;
	unsigned	WDay;
	unsigned	Hour;
	unsigned	Minutes;
}SYS_DATETIME_SET_GET;

bool systemDateTimeSet(const SYS_DATETIME_SET_GET* pSET);
void systemDateTimeGet(SYS_DATETIME_SET_GET* pSET);

#define SYS_TEST_IN(va,mi,ma)	(((va) >= (mi)) && ((va) <= (ma)))

void systemBuckBoostEnable(bool bEnable);

#endif /* SYSTEMFUNCTIONS_H_ */
