/*
 * taskInterface.h
 *
 *  Created on: 11 feb 2018
 *      Author: daniele_parise
 */

#ifndef _TASKINTERFACE_H_
#define _TASKINTERFACE_H_
typedef enum etagETASKINTERFACE_EVENT{
	TASKINTERFACE_EVT_NONE		= 0,
	TASKINTERFACE_EVT_BTN		= 1,
	TASKINTERFACE_EVT_TIME		= 2,
	TASKINTERFACE_EVT_DISPLAY	= 3
}ETASKINTERFACE_EVENT;
void taskInterfacePostEvent(ETASKINTERFACE_EVENT eBTN_EVT);
void taskInterfacePostDisplayAlwaysOn(bool bDisplayAlwaysOn);

#endif /* APPLICATION_TASKS_TASKINTERFACE_H_ */
