/*
 * taskMENU.c
 *
 *  Created on: 07 feb 2018
 *      Author: daniele_parise
 */
#include "cmsis_os.h"
#include <stdbool.h>
#include "../SystemFunctions.h"
#include "../Storage/configurations.h"
#include <string.h>
#include "taskSupervisor.h"
#include "../menu-loc/menu_vars.h"
#include "../Tasks/taskComunication.h"
#include "../Storage/confutils.h"

extern osThreadId tastSupervisorHandle;
#define TASK_SUPERVISOR_SAVR_CFG_LORAWAN				(0x0001)
#define TASK_SUPERVISOR_SAVR_CFG_SYSTEM_PARAMETERS		(0x0002)
#define TASK_SUPERVISOR_REBOOT_SYS						(0x0100)

typedef struct tagTASKSUPERVISOR_CTX{
	CFG_LORAWAN				stLORAWAN_CFG;
	CFG_SYSTEM_PARAMETERS	stCFG_SYSTEM_PARAMETERS;
}TASKSUPERVISOR_CTX;

TASKSUPERVISOR_CTX	stTASKSUPERVISOR_CTX;

void taskSupervisorSaveLoraCfg(const struct tagCFG_LORAWAN* pLoRa)
{
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG = *pLoRa;
	xTaskNotify(
			tastSupervisorHandle,
			TASK_SUPERVISOR_SAVR_CFG_LORAWAN,
			eSetBits);
}

void taskSupervisorSaveSysParamsCfg(const struct tagCFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS)
{
	stTASKSUPERVISOR_CTX.stCFG_SYSTEM_PARAMETERS = *pCFG_SYSTEM_PARAMETERS;
	xTaskNotify(
			tastSupervisorHandle,
			TASK_SUPERVISOR_SAVR_CFG_SYSTEM_PARAMETERS,
			eSetBits);
}


void taskSupervisorSaveLoraCfgSimple(const struct tagMENU_VARS_LORA_CFG* pLoRa)
{

	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.uiRegion			= 0x00;
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.uiDEFAULT_DR 	= pLoRa->iLORA_DATA_RATE;
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.uiADR_ENABLE 	= pLoRa->iLORA_ADR;
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.uiTxPower 		= pLoRa->iLORA_TX_POWER;

	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.uiABP = 0;

	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[7] = pLoRa->iNID1;		// NN_LO
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[6] = pLoRa->iNID1 >> 8;	// NN_HI
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[5] = pLoRa->iCID1;	// DD_LO
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[4] = pLoRa->iCID1 >> 8;	// DD_LO
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[3] = pLoRa->iCID2;	// CC_LO
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[2] = pLoRa->iCID2 >> 8;	// CC_HI
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[1] = 0x00;	// RR
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[0] = 0x00;	// VV

	xTaskNotify(
			tastSupervisorHandle,
			TASK_SUPERVISOR_SAVR_CFG_LORAWAN,
			eSetBits);
}

void taskSupervisorDysplayLoraCfg(void)
{
	stMENU_VARS.stLORA_CFG.iNID1 			= (int)stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[7] | ((int)stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[6] << 8);
	stMENU_VARS.stLORA_CFG.iCID1 			= (int)stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[5] | ((int)stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[4] << 8);
	stMENU_VARS.stLORA_CFG.iCID2 			= (int)stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[3] | ((int)stTASKSUPERVISOR_CTX.stLORAWAN_CFG.auiDEV_EUI[2] << 8);
	stMENU_VARS.stLORA_CFG.iLORA_DATA_RATE 	= stTASKSUPERVISOR_CTX.stLORAWAN_CFG.uiDEFAULT_DR;
	stMENU_VARS.stLORA_CFG.iLORA_ADR 		= stTASKSUPERVISOR_CTX.stLORAWAN_CFG.uiADR_ENABLE;
	stMENU_VARS.stLORA_CFG.iLORA_TX_POWER 	= stTASKSUPERVISOR_CTX.stLORAWAN_CFG.uiTxPower;
}


void LoadConfigurations(void);

void taskSupervisorReboot(void)
{
	xTaskNotify(
			tastSupervisorHandle,
			TASK_SUPERVISOR_REBOOT_SYS,
			eSetBits);
}

void StartSupervisorTask(void const * argument)
{
	UCFG_GEN_SLOT_DATA uDATA;
	confutilsInitFlash();
	LoadConfigurations();
	SystemSettings();

	configConfigGet(CONFIG_SLOT_LORAWAN,&uDATA);
	stTASKSUPERVISOR_CTX.stLORAWAN_CFG = uDATA.stLORAWAN;

	taskSupervisorDysplayLoraCfg();


	stSTORAGE_IN_MEMORY.bREADY = true;

	 while (true)
	 {
		 bool 		bNotified;
		 uint32_t	uiNotyMask;
		 bNotified = xTaskNotifyWait((uint32_t)-1, (uint32_t)-1, &uiNotyMask, osWaitForever);
		 if (bNotified)
		 {
			memset(&uDATA,0,sizeof(uDATA));
			 if (uiNotyMask & TASK_SUPERVISOR_SAVR_CFG_LORAWAN)
			 {
				uDATA.stLORAWAN = stTASKSUPERVISOR_CTX.stLORAWAN_CFG;
				configConfigSet(CONFIG_SLOT_LORAWAN,&uDATA);

				taskComunicationOnNewLoraParams();
//				 stSTORAGE_IN_MEMORY
			 }
			 if (uiNotyMask & TASK_SUPERVISOR_SAVR_CFG_SYSTEM_PARAMETERS)
			 {
				uDATA.stSYSTEM_PARAMETERS = stTASKSUPERVISOR_CTX.stCFG_SYSTEM_PARAMETERS;
				configConfigSet(CONFIG_SLOT_SYSTEM_PARAMETERS,&uDATA);
			 }

			 if (uiNotyMask & TASK_SUPERVISOR_REBOOT_SYS)
			 {
				NVIC_SystemReset();
			 }
		 }
	 }
}


void LoadConfigurations(void)
{


	configurationsLoad();



}

