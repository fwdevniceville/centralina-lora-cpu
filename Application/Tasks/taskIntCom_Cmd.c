/*
 * taskIntCom_Cmd.c
 *
 *  Created on: 20 mar 2018
 *      Author: daniele_parise
 */

#include "cmsis_os.h"
#include <stdbool.h>
#include <stdint.h>

#include "taskIntCom.h"
#include "../FeeRTOS/FRT_BoardFunctions.h"
#include "../intcom/intcom.h"
#include "../gencom/gencomhl.h"
#include <string.h>
#include "taskComunication.h"
#include "../menu-loc/menu_vars.h"
#include "../msgcom/msgcom.h"
#include "../Storage/configurations.h"
#include "taskSupervisor.h"
#include "taskScheduler.h"
#include "../hwbrd/hw_bridge.h"

LoraRxInfos	stLoraRxInfos;
uint8_t	auiBufferAnsw[512];

bool taskIntComRecvCallback(GENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer)
{
	CFG_SCHEDULER_ROW	stROW;
	unsigned uiAnswSize;
	bool bCompleted;
	bCompleted = true;
	uiAnswSize = 2;
	memset(auiBufferAnsw,0,sizeof(auiBufferAnsw));
	auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_UNK;
	auiBufferAnsw[1] = 0;
	switch (uiCmd)
	{
	case MODINTCOM_CMD_SEND_EVENT:
		{
			auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_OK;
			switch (abyBuffer[0])
			{
			case LORA_USER_EVENT_REBOOT:
				taskComunicationRecvLoRaRebooted();
				break;
			case LORA_USER_EVENT_JOINED:
				taskComunicationRecvLoRaJoined();
				break;
			case LORA_USER_EVENT_CLASS_CONFIRMED:
				break;
			}
		}
		break;
	case MODINTCOM_CMD_SEND_DOWNLINK:
		{

			stLoraRxInfos.uiDownLinkCounter =  abyBuffer[0];
			stLoraRxInfos.uiDownLinkCounter<<=8;
			stLoraRxInfos.uiDownLinkCounter |= abyBuffer[1];
			stLoraRxInfos.uiDownLinkCounter<<=8;
			stLoraRxInfos.uiDownLinkCounter |= abyBuffer[2];
			stLoraRxInfos.uiDownLinkCounter<<=8;
			stLoraRxInfos.uiDownLinkCounter |= abyBuffer[3];

			stLoraRxInfos.Rssi 				=  abyBuffer[4];
			stLoraRxInfos.Rssi<<=8;
			stLoraRxInfos.Rssi 				|= abyBuffer[5];

			stLoraRxInfos.Snr 				=  abyBuffer[6];
			stLoraRxInfos.bFramePending 	=  abyBuffer[7];

			stMENU_VARS.iPOWER_LEVEL = stLoraRxInfos.Rssi;

			taskComunicationRecvLoRaDownlink(&stLoraRxInfos,abyBuffer[8],abyBuffer[9],abyBuffer[10],&abyBuffer[11]);

			auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_OK;
		}
		break;
	case MODEXTCOM_CPU_READ_SCHEDULE:
		{
			int iSchedule;
			iSchedule = abyBuffer[0];
			auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_INVALID_PARAMS;
			if (configGetSchedule(iSchedule,&stROW))
			{
				auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_OK;

				auiBufferAnsw[1]	= abyBuffer[1]; // Meglio, altrimenti tiriamo su anche porcheria
				auiBufferAnsw[2]	= stROW.uiTYPE;
				auiBufferAnsw[3]	= stROW.uiStartYear;
				auiBufferAnsw[4]	= stROW.uiStartMonth;
				auiBufferAnsw[5]	= stROW.uiStartMonthDay;
				auiBufferAnsw[6]	= stROW.uiPeriodDay;
				auiBufferAnsw[7]	= stROW.uiStart_hour;
				auiBufferAnsw[8]	= stROW.uiStart_min;
				auiBufferAnsw[9]	= stROW.uiRuntime_min >> 8;
				auiBufferAnsw[10]	= stROW.uiRuntime_min;
				auiBufferAnsw[11]	= stROW.uiOUT_MASK;
				auiBufferAnsw[12]	= stROW.uiCRC8_MSG;
				uiAnswSize = 13;
			}
		}
		break;
	case MODEXTCOM_CPU_MODIFY_SCHEDULE:
		{
			memset(&stROW,0,sizeof(stROW));
			stROW.uiORD 			= abyBuffer[1];
			stROW.uiTYPE 			= abyBuffer[2];
			stROW.uiStartYear 		= abyBuffer[3] & CFG_SCHEDULER_ROW_START_YEAR_MASK;
			stROW.uiStartMonth 		= abyBuffer[4] & CFG_SCHEDULER_ROW_START_MONTH_MASK;
			stROW.uiStartMonthDay 	= abyBuffer[5] & CFG_SCHEDULER_ROW_START_MONTH_DAY_MASK;
			stROW.uiPeriodDay 		= abyBuffer[6];
			stROW.uiStart_hour 		= abyBuffer[7];
			stROW.uiStart_min 		= abyBuffer[8];
			stROW.uiRuntime_min 	= ((uint16_t)abyBuffer[9] << 8) | abyBuffer[10];
			stROW.uiOUT_MASK 		= abyBuffer[11];
			stROW.uiCRC8_MSG 		= abyBuffer[12];

			configAdjustNewSchedule(&stROW);
			auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_INVALID_PARAMS;
			if (configSaveNewSchedule(&stROW))
			{
				auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_OK;
			}
		}
		break;
	case MODEXTCOM_CPU_READ_LORA_PARAMS:
		{
			const UCFG_GEN_SLOT_DATA*	puDATA;
			puDATA = configConfigGetRO(CONFIG_SLOT_LORAWAN);
			auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_OK;
			auiBufferAnsw[1] = 0;
			memcpy(&auiBufferAnsw[2],&puDATA->stLORAWAN,sizeof(puDATA->stLORAWAN));

			uiAnswSize = 2 + sizeof(puDATA->stLORAWAN);

		}
		break;
	case MODEXTCOM_CPU_WRITE_LORA_PARAMS:
		{
			taskSupervisorSaveLoraCfg((CFG_LORAWAN*)&abyBuffer[2]);
			auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_OK;
			auiBufferAnsw[1] = 0;
		}
		break;
	case MODEXTCOM_CPU_READ_HW_KEYS:
		{
			DEVICE_HW_ID	stDEVICE_HW_ID;

			auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_OK;
			auiBufferAnsw[1] = 0;
			// facciamo questo per evitere errori di allineamento
			hwGetDeviceID(&stDEVICE_HW_ID);
			*(DEVICE_HW_ID*)&auiBufferAnsw[2] = stDEVICE_HW_ID;
			uiAnswSize = 2 + sizeof(DEVICE_HW_ID);
		}
		break;
	case MODEXTCOM_CPU_BOARD_TEST_CMD:
		{
			size_t	sizRetData;
			sizRetData = 0;
			taskSchedulerExecBoardTestCmd(abyBuffer,uiSize,&auiBufferAnsw[1],&auiBufferAnsw[2],sizeof(auiBufferAnsw) - 2,&sizRetData);
			auiBufferAnsw[0] = GENCOMHL_ANSW_CMD_OK;
//			auiBufferAnsw[1] = 0;
			uiAnswSize = 2 + sizRetData;
		}
		break;
	case MODEXTCOM_CPU_PING:
		{
			memcpy(&auiBufferAnsw[0],abyBuffer,uiSize);
			uiAnswSize = uiSize;
		}
		break;
	}

	gencomhlPostRecvAnswer(&stINTCOMHL_CTX,uiCmd,uiAnswSize,uiProgr,auiBufferAnsw);

	return bCompleted;

}
