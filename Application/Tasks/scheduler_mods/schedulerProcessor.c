/*
 * schedulerProcessor.c
 *
 *  Created on: 21 nov 2018
 *      Author: daniele_parise
 */


#include "cmsis_os.h"
#include <stdbool.h>
#include <stdint.h>
#include "../taskScheduler.h"
#include "../../Storage/configurations.h"
#include "../../SystemFunctions.h"
#include "schedulerProcessor.h"

bool schedulerProcessRow(const CFG_SCHEDULER_ROW* pRow, uint64_t uiCurrSecondsFrom2K, const DateTime* pDateTime, uint32_t* pOutMask)
{
	uint64_t	uiSecondsFrom;
	uint64_t	uiSecondsFromPrevDay;
	uint64_t	uiSecondsTo;
	unsigned	uiStartMonth;
	unsigned	uiStartMonthDay;
	unsigned	uiStartYear;
	bool 		bRunning;

	uiSecondsFrom 			= 0;
	uiSecondsFromPrevDay	= 0;
	uiSecondsTo				= 0;

	uiStartYear = pRow->uiStartYear & CFG_SCHEDULER_ROW_START_YEAR_MASK;
	uiStartMonth = pRow->uiStartMonth & CFG_SCHEDULER_ROW_START_MONTH_MASK;
	uiStartMonthDay = pRow->uiStartMonthDay & CFG_SCHEDULER_ROW_START_MONTH_DAY_MASK;

	bRunning = false;

	switch (pRow->uiTYPE)
	{
	case CFG_SCHEDULER_ROW_TYPE_DAYYEAR:
		{
			uiSecondsFrom = systemYearMonthDay2secondsFrom2K(pDateTime->tm_year,uiStartMonth ,uiStartMonthDay);
			uiSecondsFromPrevDay = uiSecondsFrom - SECONDS_IN_1DAY;
			// problemi se la schedulazione e' pianificata il 31/12 a cavallo con il giorno successivo
			//il 31/12/2018 diventa 31/12/2019 e la fascia oraria avanza di 364 giorni
			// per ora non lo consideriamo per il casotto dell'anno bisestile
		}
		break;
	case CFG_SCHEDULER_ROW_TYPE_WEEKDAY:
		{
			uint16_t 	uiDayMask;
			int16_t 	iDayBit;
			iDayBit = pDateTime->tm_wday - 1;
			uiDayMask = 1 << iDayBit;
			if (pRow->uiPeriodDay & uiDayMask)
			{
				uiSecondsFrom = systemYearYDay2secondsFrom2K(pDateTime->tm_year,pDateTime->tm_yday);
			}
			// controlliamo il giorno precedente se era schedulabile per il discorso della schedulazione a cavallo di 2 giorni
			{
				iDayBit--;
				if (iDayBit < 0)
				{
					iDayBit = 6;
				}
				if (pRow->uiPeriodDay & uiDayMask)
				{
					uiSecondsFromPrevDay = uiSecondsFrom - SECONDS_IN_1DAY;
				}
			}

		}
		break;
	case CFG_SCHEDULER_ROW_TYPE_DAYPERIOD:
		{
			uint64_t	uiStartDay;
			uiSecondsFrom = systemYearMonthDay2secondsFrom2K(2000 + uiStartYear, uiStartMonth,uiStartMonthDay);

			if (pRow->uiPeriodDay == 0)
			{
				// con il periodo 0 e' il giorno esatto
				uiSecondsFromPrevDay = uiSecondsFrom - SECONDS_IN_1DAY;
			}
			else
			{
				uiSecondsTo = systemYearYDay2secondsFrom2K(pDateTime->tm_year,pDateTime->tm_yday);

				if (uiSecondsTo > uiSecondsFrom)
				{
					uiStartDay = uiSecondsTo - uiSecondsFrom;
				}
				else
				{
					uiStartDay = uiSecondsFrom - uiSecondsTo;
				}

				// cosi' abbiamo la differenza di giorni
//				uiStartDay /= SECONDS_IN_1DAY; obsoleto stiamo lavorando a multipli di SECONDS_IN_1DAY
				uiSecondsFrom = uiStartDay % pRow->uiPeriodDay;

				// vediamo se ieri era un giorno del periodo
				uiStartDay -= SECONDS_IN_1DAY;
				uiSecondsFromPrevDay =uiStartDay % pRow->uiPeriodDay;

//				if (uiSecondsFrom < SECONDS_IN_1DAY) // test paraculissimo
				if (uiSecondsFrom == 0)
				{
					// Siamo in una divisione perfetta del periodo, quindi e' il giorno giusto
					uiSecondsFrom = uiSecondsTo; //uiSecondsTo, per risparmio stack, sono i secondi che rappresentano l'oggi all'inizio
				}
				else
				{
					uiSecondsFrom = 0;
				}
				if (uiSecondsFromPrevDay == 0)
				{
					// Siamo in una divisione perfetta del periodo, quindi e' il giorno giusto
					// e abbiamo gia' apputato che non siamo al primo gennaio
					uiSecondsFromPrevDay = uiSecondsTo - SECONDS_IN_1DAY; //uiSecondsTo, per risparmio stack, sono i secondi che rappresentano l'oggi all'inizio e il risultato e' ieri
				}
				else
				{
					uiSecondsFromPrevDay = 0;
				}
			}
		}
		break;
	}

	// per ora teniamole doppie poi semplifichiamo

	// vediamo se ieri era iniziata una schedulazione che si e' protratta fino ad adesso
	if (uiSecondsFromPrevDay > 0)
	{
		uiSecondsFromPrevDay+=(((pRow->uiStart_hour * MINUTES_IN_1HOUR) + pRow->uiStart_min) * SECONDS_IN_1MINUTE);
		uiSecondsTo = uiSecondsFromPrevDay + ((pRow->uiRuntime_min) * SECONDS_IN_1MINUTE);

		if (uiCurrSecondsFrom2K >= uiSecondsFromPrevDay)
		{
			if (uiCurrSecondsFrom2K < uiSecondsTo)
			{
				*pOutMask |= pRow->uiOUT_MASK;
				bRunning = true;
			}
		}
	}

	// vediamo se siamo nel mezzo di una schedulazione
	if (uiSecondsFrom > 0)
	{
		uiSecondsFrom+=(((pRow->uiStart_hour * MINUTES_IN_1HOUR) + pRow->uiStart_min) * SECONDS_IN_1MINUTE);
		uiSecondsTo = uiSecondsFrom + ((pRow->uiRuntime_min) * SECONDS_IN_1MINUTE);

		if (uiCurrSecondsFrom2K >= uiSecondsFrom)
		{
			if (uiCurrSecondsFrom2K < uiSecondsTo)
			{
				*pOutMask |= pRow->uiOUT_MASK;
				bRunning = true;
			}
		}
	}

	return bRunning;
}
