/*
 * schedulerADCReaders.h
 *
 *  Created on: 21 nov 2018
 *      Author: daniele_parise
 */

#ifndef TASKS_SCHEDULER_MODS_SCHEDULERADCREADERS_H_
#define TASKS_SCHEDULER_MODS_SCHEDULERADCREADERS_H_


bool taskSchedulerReanAndEvalIN(uint64_t uiCurrSecondsFrom2K,ADCIN_EVAL* pEVAL);
#ifdef ADCREADER_READ_VALUES_IN_PARALLEL
#else
bool taskSchedulerReadVBatt(bool bUnderLoad);
#endif


#ifdef ADCREADER_READ_VALUES_IN_PARALLEL
bool taskSchedulerReanAndEvalINS(bool bReadBatt, bool bReadTemp);
#else
bool taskSchedulerReanAndEvalINS(void);
#endif

#endif /* TASKS_SCHEDULER_MODS_SCHEDULERADCREADERS_H_ */
