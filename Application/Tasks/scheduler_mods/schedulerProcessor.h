/*
 * schedulerProcessor.h
 *
 *  Created on: 21 nov 2018
 *      Author: daniele_parise
 */

#ifndef TASKS_SCHEDULER_MODS_SCHEDULERPROCESSOR_H_
#define TASKS_SCHEDULER_MODS_SCHEDULERPROCESSOR_H_

bool schedulerProcessRow(const CFG_SCHEDULER_ROW* pRow, uint64_t uiCurrSecondsFrom2K, const DateTime* pDateTime, uint32_t* pOutMask);


#endif /* TASKS_SCHEDULER_MODS_SCHEDULERPROCESSOR_H_ */
