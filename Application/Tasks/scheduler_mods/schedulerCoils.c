/*
 * schedulerCoils.c
 *
 *  Created on: 21 nov 2018
 *      Author: daniele_parise
 */
#include "cmsis_os.h"
#include <stdbool.h>
#include <stdint.h>
#include "../taskScheduler.h"
#include "schedulerCoils.h"
#include "schedulerADCReaders.h"
#include "../../inout/coildrv.h"
#include "../../inout/adcreader.h"
#include "../../SystemFunctions.h"


bool schedulerCoilActionLatch(unsigned uiOpenPulseMask, unsigned uiClosePulseMask)
{
	if (uiOpenPulseMask || uiClosePulseMask)
	{
		unsigned	uiMASK_DRV_OPEN;
		unsigned	uiMASK_DRV_CLOSE;
		unsigned	uiOUT_SCNR;
		int			iRemainingTime;
		bool		bFirst;

		bFirst = true;
		coildrvPowerLoad(0);

		coildrv3V3_PWR_VLV_OFF();
		coildrv3V3_PWR_COM_OFF();
		coildrvPWR_LO();

		uiOUT_SCNR = 0x01;
		while (uiOUT_SCNR <= 0x08)
		{
			stSCHEDULER_CTX.stCOIL_STATUS.uiOutMask = uiOUT_SCNR;
			uiMASK_DRV_OPEN = 0;

			uiMASK_DRV_CLOSE = uiClosePulseMask & uiOUT_SCNR;
			if (uiMASK_DRV_CLOSE == 0)
			{
				uiMASK_DRV_OPEN = uiOpenPulseMask & uiOUT_SCNR;
			}
			if (uiMASK_DRV_OPEN || uiMASK_DRV_CLOSE)
			{
				stSCHEDULER_CTX.stCOIL_STATUS.bLOADING = true;
				iRemainingTime = stSCHEDULER_CTX.stCOIL_SETTINGS.uiTLOAD;
				coildrvPowerLoad(stSCHEDULER_CTX.stCOIL_SETTINGS.uimVPULSE);

				stSCHEDULER_CTX.stCOIL_STATUS.iDriveCounter++;

				stSCHEDULER_CTX.stCOIL_STATUS.iCurrentStart = adcreaderReadCurrent(20,0,NULL);
				iRemainingTime -= 20;

				if (iRemainingTime > 0)
				{
					osDelay(iRemainingTime);
				}
				coildrvPowerLoad(0);
				stSCHEDULER_CTX.stCOIL_STATUS.bLOADING = false;
				stSCHEDULER_CTX.stCOIL_STATUS.bDISCHARGE = false;


				if (uiMASK_DRV_OPEN != 0)
				{
					coildrvOUT_POL(uiMASK_DRV_OPEN);
					coildrvCOMM_LO();
				}
				else
				{
					coildrvOUT_POL(~uiMASK_DRV_CLOSE);
					coildrvCOMM_HI();
				}

				osDelay(2);
				coildrv3V3_PWR_VLV_ON();
				coildrv3V3_PWR_COM_ON();
				osDelay(2);

				stSCHEDULER_CTX.stCOIL_STATUS.bDISCHARGE = true;
				coildrvPWR_HI();
#ifndef ADCREADER_SKIP
				if (1)
				{
#ifdef DEBUG
					volatile uint64_t	uiStartMeas_ms;
					volatile uint64_t	uiEndMeas_ms;
					volatile uint64_t	uiDeltaMeas_ms;
					uiStartMeas_ms = systemGetCurrentDateTime_ms(NULL);
#endif
					iRemainingTime = stSCHEDULER_CTX.stCOIL_SETTINGS.uiTPULSE;

					stSCHEDULER_CTX.stCOIL_STATUS.iCurrentMean = adcreaderReadCurrent(20,5,&stSCHEDULER_CTX.stCOIL_STATUS.iCurrentPeak);
					iRemainingTime -= 20;


#ifdef DEBUG
					uiEndMeas_ms = systemGetCurrentDateTime_ms(NULL);
					uiDeltaMeas_ms = uiEndMeas_ms - uiStartMeas_ms;
#endif

					if (iRemainingTime > 0)
					{
						osDelay(iRemainingTime);
					}

				}
				else
#endif
				{
					osDelay(stSCHEDULER_CTX.stCOIL_SETTINGS.uiTPULSE);
				}
				coildrvPWR_LO();
				stSCHEDULER_CTX.stCOIL_STATUS.bDISCHARGE = false;

				osDelay(2);
				coildrv3V3_PWR_VLV_OFF();
				coildrv3V3_PWR_COM_OFF();

				if (stSCHEDULER_CTX.stCOIL_STATUS.bCURRENT_SENSOR_ERROR)
				{
					stSCHEDULER_CTX.stVALVOLE.uiValvoleMalfunzionantiOC = 0xF;
					stSCHEDULER_CTX.stVALVOLE.uiValvoleMalFunzionantiSC = 0xF;
				}
				else
				{
					int iTempCorrente;
					uint16_t	uiValvoleMalfunzionantiOC;
					uint16_t	uiValvoleMalFunzionantiSC;

					uiValvoleMalfunzionantiOC = stSCHEDULER_CTX.stVALVOLE.uiValvoleMalfunzionantiOC & ~ uiOUT_SCNR;
					uiValvoleMalFunzionantiSC = stSCHEDULER_CTX.stVALVOLE.uiValvoleMalFunzionantiSC & ~ uiOUT_SCNR;

					stSCHEDULER_CTX.stVALVOLE.uiValvoleMalFunzionantiSC &= ~ uiOUT_SCNR;
					iTempCorrente = stSCHEDULER_CTX.stCOIL_STATUS.iCurrentMean - stSCHEDULER_CTX.stCOIL_STATUS.iCurrentStart;

					stSCHEDULER_CTX.stCOIL_STATUS.iCurrentDischarge_mA = iTempCorrente;

					iTempCorrente = stSCHEDULER_CTX.stCOIL_STATUS.iCurrentPeak - stSCHEDULER_CTX.stCOIL_STATUS.iCurrentStart;

					stSCHEDULER_CTX.stCOIL_STATUS.iCurrentDischargePeak_mA = iTempCorrente;

					if (stSCHEDULER_CTX.stCOIL_STATUS.iCurrentDischargePeak_mA > 300)
					{
						if (stSCHEDULER_CTX.stCOIL_STATUS.iCurrentDischarge_mA > 300)
						{
						}
						else
						{
							uiValvoleMalFunzionantiSC |= uiOUT_SCNR;
						}
					}
					else
					{
						uiValvoleMalfunzionantiOC |= uiOUT_SCNR;
					}
					stSCHEDULER_CTX.stVALVOLE.uiValvoleMalFunzionantiSC = uiValvoleMalFunzionantiSC;
					stSCHEDULER_CTX.stVALVOLE.uiValvoleMalfunzionantiOC = uiValvoleMalfunzionantiOC;
				}


				stSCHEDULER_CTX.stVALVOLE.uiValvoleChiuse &= ~uiMASK_DRV_OPEN;
				stSCHEDULER_CTX.stVALVOLE.uiValvoleChiuse |= uiMASK_DRV_CLOSE;

				stSCHEDULER_CTX.stVALVOLE.uiValvoleAperte &= ~uiMASK_DRV_CLOSE;
				stSCHEDULER_CTX.stVALVOLE.uiValvoleAperte |= uiMASK_DRV_OPEN;

				uiMASK_DRV_OPEN |= uiMASK_DRV_CLOSE;
				uiOpenPulseMask &= ~uiMASK_DRV_OPEN;
				uiClosePulseMask &= ~uiMASK_DRV_OPEN;
			}
			if (stSCHEDULER_CTX.eMODE != stSCHEDULER_CTX.eMODE_NEW)
			{
				break;
			}
			uiOUT_SCNR<<=1;
		}

	}
	return true;
}



bool schedulerCoilActionAC(unsigned uiKeepOpenMask)
{

	if (uiKeepOpenMask != 0)
	{
		int iDiscTime;
		coildrvPWR_LO();
		coildrvPWR2_EXT_HI();
		stSCHEDULER_CTX.stCOIL_STATUS.bLOADING = true;
		coildrvPowerLoad(21000);
		osDelay(stSCHEDULER_CTX.stCOIL_SETTINGS.uiTLOAD);
		coildrvPowerLoad(0);
		stSCHEDULER_CTX.stCOIL_STATUS.bLOADING = false;

		stSCHEDULER_CTX.stCOIL_STATUS.iDriveCounter++;

		coildrv3V3_PWR_VLV_OFF();
		osDelay(2);
		coildrvOUT_POL(uiKeepOpenMask);
		osDelay(2);
#ifdef BOARD_PROTOTYPE
		coildrv3V3_PWR_VLV_ON();
#else
		coildrv3V3_PWR_COM_ON();
#endif
		osDelay(2);
		stSCHEDULER_CTX.stCOIL_STATUS.bDISCHARGE = true;
		coildrvPWR_HI();

		iDiscTime = stSCHEDULER_CTX.stCOIL_SETTINGS.uiTPULSE;

#ifndef ADCREADER_SKIP
		stSCHEDULER_CTX.stCOIL_STATUS.iCurrentMean = adcreaderReadCurrent(20,5,&stSCHEDULER_CTX.stCOIL_STATUS.iCurrentPeak);

		iDiscTime-=20;
#endif

		osDelay(iDiscTime);
		coildrvPWR_LO();
		stSCHEDULER_CTX.stCOIL_STATUS.bDISCHARGE = false;
	}
	else
	{
		coildrv3V3_PWR_VLV_OFF();
		osDelay(2);
		coildrv3V3_PWR_COM_OFF();
		osDelay(2);
		coildrvOUT_POL(0);
		coildrvPWR2_EXT_LO();
		return true;
	}
	return false;
}

void schedulerCoilAction(unsigned uiKeepOpenMask ,unsigned uiOpenPulseMask, unsigned uiClosePulseMask)
{
	bool	bFreeHW;
	coildrvInit();
#ifndef ADCREADER_SKIP

	if (!stSCHEDULER_CTX.stCOIL_STATUS.bCURRENT_SENSOR_ERROR)
	{
		adcreaderReadCurrentEnable(true);
		osDelay(2);
		if (!adcreaderIsReadCurrentEnabled())
		{
			adcreaderReadCurrentEnable(false);
			stSCHEDULER_CTX.stCOIL_STATUS.bCURRENT_SENSOR_ERROR =true;
		}

		stSCHEDULER_CTX.stCOIL_STATUS.iCurrentBase = adcreaderReadCurrent(20,0,NULL);
	}
#endif

	if ((coildrvIsLatchingEV() && !stSCHEDULER_CTX.stCOIL_SETTINGS.bForceMonostabile) || stSCHEDULER_CTX.stCOIL_SETTINGS.bForceLatch)
	{
		bFreeHW = schedulerCoilActionLatch(uiOpenPulseMask,uiClosePulseMask);
	}
	else
	{

		bFreeHW = schedulerCoilActionAC(uiKeepOpenMask);
		stSCHEDULER_CTX.stVALVOLE.uiValvoleChiuse = ~ uiKeepOpenMask;
		stSCHEDULER_CTX.stVALVOLE.uiValvoleChiuse &= 0x0F;
	}

#ifndef ADCREADER_SKIP
	adcreaderReadCurrentEnable(false);
#endif

	if (bFreeHW)
	{
		coildrv3V3_PWR_VLV_OFF();
		coildrv3V3_PWR_COM_OFF();
		coildrvFree();
	}
}
