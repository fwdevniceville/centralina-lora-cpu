/*
 * schedulerADCReaders.c
 *
 *  Created on: 21 nov 2018
 *      Author: daniele_parise
 */

#include "cmsis_os.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "../taskScheduler.h"
#include "schedulerADCReaders.h"
#include "../../inout/adcreader.h"
#include "../../SystemFunctions.h"

#include "../taskComunication.h"

ADCREADER_CHANNEL_CTX	aADCREADER_CHANNEL_CTX[4];



#ifdef ADCREADER_READ_VALUES_IN_PARALLEL
bool taskSchedulerReanAndEvalINS(bool bReadBatt, bool bReadTemp)
#else
bool taskSchedulerReanAndEvalINS(void)
#endif
{
	uint64_t			uiCurrSecondsFrom2K;
	int32_t				iConverter;
	EADCREADER_IN0_MODE eMODE_IN0;
	uint8_t				uiPULLUPS;
	bool				bAdvise;
	bool				bVExt;

	memset(&aADCREADER_CHANNEL_CTX[0],0,sizeof(aADCREADER_CHANNEL_CTX));
	uiCurrSecondsFrom2K = systemGetCurrentDateTime(NULL);

	aADCREADER_CHANNEL_CTX[0].stSettings.eCH = ADCREADER_IN0;
	aADCREADER_CHANNEL_CTX[0].stSettings.bVREFIN_CORRECTION = true;
	aADCREADER_CHANNEL_CTX[0].stSettings.uiDropSams = 0;
	aADCREADER_CHANNEL_CTX[0].stSettings.uiSam_ms = 10;
	uiPULLUPS = 0;
	eMODE_IN0 = ADCREADER_IN0_MODE_ADC_5V;
	switch (stSCHEDULER_CTX.stADCIN_STATUS.stIN0.stCFG.uiIN_TYPE)
	{
	case ADCIN_INTYPE_DISABLED:
		aADCREADER_CHANNEL_CTX[0].stSettings.eCH = ADCREADER_NULL;
		break;
	case ADCIN_INTYPE_0_5V_PULLUP:
		uiPULLUPS |= 0x01;
	case ADCIN_INTYPE_0_5V:
		eMODE_IN0 = ADCREADER_IN0_MODE_ADC_5V;
		break;
	case ADCIN_INTYPE_0_10V:
		eMODE_IN0 = ADCREADER_IN0_MODE_ADC_10V;
		break;
	case ADCIN_INTYPE_4_20mA:
		eMODE_IN0 = ADCREADER_IN0_MODE_ADC_4_20mA;
		break;
	}

	aADCREADER_CHANNEL_CTX[1].stSettings.eCH = ADCREADER_IN4;
	aADCREADER_CHANNEL_CTX[1].stSettings.bVREFIN_CORRECTION = true;
	aADCREADER_CHANNEL_CTX[1].stSettings.uiDropSams = 0;
	aADCREADER_CHANNEL_CTX[1].stSettings.uiSam_ms = 10;
	switch (stSCHEDULER_CTX.stADCIN_STATUS.stIN4.stCFG.uiIN_TYPE)
	{
	case ADCIN_INTYPE_DISABLED:
		aADCREADER_CHANNEL_CTX[1].stSettings.eCH = ADCREADER_NULL;
		break;
	case ADCIN_INTYPE_0_5V_PULLUP:
		uiPULLUPS |= 0x02;
		break;
	case ADCIN_INTYPE_0_5V:
		break;
	case ADCIN_INTYPE_0_10V:
		break;
	case ADCIN_INTYPE_4_20mA:
		break;
	}

#ifdef ADCREADER_READ_VALUES_IN_PARALLEL
	aADCREADER_CHANNEL_CTX[2].stSettings.eCH = ADCREADER_NULL;
	if (bReadBatt)
	{
		aADCREADER_CHANNEL_CTX[2].stSettings.eCH = ADCREADER_VBATT;
		aADCREADER_CHANNEL_CTX[2].stSettings.bVREFIN_CORRECTION = true;
		aADCREADER_CHANNEL_CTX[2].stSettings.uiDropSams = 1;
		aADCREADER_CHANNEL_CTX[2].stSettings.uiSam_ms = 10;
		adcreaderReadVBattEnable(true);

	}

	aADCREADER_CHANNEL_CTX[3].stSettings.eCH = ADCREADER_NULL;
	if (bReadTemp)
	{
	}

	adcreaderReadIN0Select(eMODE_IN0);

	adcreaderEnablePullUps(uiPULLUPS);

	osDelay(1);



	adcreaderReadChannelsMean(&aADCREADER_CHANNEL_CTX[0],4);

	stSCHEDULER_CTX.stADCIN_STATUS.stIN0.iValueRaw = aADCREADER_CHANNEL_CTX[0].stResult.uiMean;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN4.iValueRaw = aADCREADER_CHANNEL_CTX[1].stResult.uiMean;

	if (bReadBatt)
	{
		adcreaderReadVBattEnable(false);

		bVExt = adcreaderIsVEXTPresent();

		stSCHEDULER_CTX.stBATTERY.bLowBattPowerOff = taskComunicatioSetVBatRAW(aADCREADER_CHANNEL_CTX[2].stResult.uiMean,stSCHEDULER_CTX.stBATTERY.ui_mV_min,stSCHEDULER_CTX.stBATTERY.ui_mV_max,bVExt);
	}
	if (bReadTemp)
	{
	}
	adcreaderEnablePullUps(0);
	adcreaderReadIN0Select(ADCREADER_IN0_MODE_IN);

#else


	adcreaderReadIN0Select(eMODE_IN0);

	adcreaderEnablePullUps(uiPULLUPS);

	osDelay(1);

	stSCHEDULER_CTX.stADCIN_STATUS.stIN4.iValueRaw = 0;
	if (stSCHEDULER_CTX.stADCIN_STATUS.stIN4.stCFG.uiIN_TYPE != ADCIN_INTYPE_DISABLED)
	{
		stSCHEDULER_CTX.stADCIN_STATUS.stIN4.iValueRaw = adcreaderReadChannelMean(ADCREADER_IN4,10,0,NULL,true);
	}


	stSCHEDULER_CTX.stADCIN_STATUS.stIN0.iValueRaw = 0;
	if (stSCHEDULER_CTX.stADCIN_STATUS.stIN0.stCFG.uiIN_TYPE != ADCIN_INTYPE_DISABLED)
	{
		stSCHEDULER_CTX.stADCIN_STATUS.stIN0.iValueRaw = adcreaderReadChannelMean(ADCREADER_IN0,10,0,NULL,true);
	}

#endif
	// 4 : 4.37V = 2203
	// 0 : 4.37V = 1906
	// 0 : 19.6mA = 3003
	iConverter = stSCHEDULER_CTX.stADCIN_STATUS.stIN4.iValueRaw;
	iConverter *= 437;
	iConverter /= 2203;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN4.uiValue_cV = iConverter;
	iConverter = stSCHEDULER_CTX.stADCIN_STATUS.stIN0.iValueRaw;
	if (0) // 4..20mA
	{
		iConverter *= 1960;
		iConverter /= 3003;
	}
	else
	{
		iConverter *= 437;
		iConverter /= 1906;
	}
	stSCHEDULER_CTX.stADCIN_STATUS.stIN0.uiValue_cV = iConverter;

//	adcreaderReadIN0Select(true);

	bAdvise = taskSchedulerReanAndEvalIN(uiCurrSecondsFrom2K,&stSCHEDULER_CTX.stADCIN_STATUS.stIN0);
	if (taskSchedulerReanAndEvalIN(uiCurrSecondsFrom2K,&stSCHEDULER_CTX.stADCIN_STATUS.stIN4))
	{
		bAdvise = true;
	}

	if (bAdvise)
	{
		taskComunicationSignalUpLinkRequest(true);
	}
	return bAdvise;
}


bool taskSchedulerReanAndEvalIN(uint64_t uiCurrSecondsFrom2K,ADCIN_EVAL* pEVAL)
{
	int64_t		iDeltaT;
	int16_t		iDeltaValue;
	bool		bAdvise;

	int16_t		iValueExport;
	int16_t		iValue_cV;
	uint8_t		uiZone;

	iValue_cV = pEVAL->uiValue_cV;

	if (iValue_cV > 0x3FF)
	{
		iValue_cV = 0x3FF;
	}

	iValueExport = pEVAL->iValue_cVLast;
	iDeltaT = 0;
	if (pEVAL->uiLastTimeChange != 0)
	{
		iDeltaT = (int64_t)uiCurrSecondsFrom2K - (int64_t)pEVAL->uiLastTimeChange;
	}
	if (iDeltaT >= 0)
	{
		iDeltaValue = iValue_cV - pEVAL->iValue_cVLast;
		if (iDeltaValue < 0)
		{
			iDeltaValue = -iDeltaValue;
		}
		pEVAL->iTimeDelayNoiseCurr += iDeltaT;
		if (iDeltaValue <= pEVAL->stCFG.iValueRaw_Delta)
		{
			pEVAL->iTimeDelayAdviseCurr = 0;
			iValueExport = iValue_cV;
		}
		else
		{
			pEVAL->iTimeDelayAdviseCurr += iDeltaT;
			if (pEVAL->iTimeDelayAdviseCurr > pEVAL->stCFG.iTimeDelayAdviseMax)
			{
				pEVAL->iTimeDelayNoiseCurr = 0;
				iValueExport = iValue_cV;
			}
		}
		if (pEVAL->iTimeDelayNoiseCurr > pEVAL->stCFG.iTimeDelayNoiseMax)
		{

		}
	}

	pEVAL->iValue_cVLast = iValueExport;
	pEVAL->uiLastTimeChange 	= uiCurrSecondsFrom2K;

	uiZone = pEVAL->uiLastZoneTemp;
	switch (uiZone)
	{
	case 0:
		{
			if (iValueExport > pEVAL->stCFG.iValueRaw_LO_HI)
			{
				uiZone = 1;
			}
		}
		break;
	case 1:
	case 2:
		{
			if (iValueExport > pEVAL->stCFG.iValueRaw_HI_HI)
			{
				uiZone = 3;
			}
			else
			if (iValueExport < pEVAL->stCFG.iValueRaw_LO_LO)
			{
				uiZone = 0;
			}
			else
			if (pEVAL->stCFG.iValueRaw_HI_HI >= 1023)
			{
				uiZone = 1;
			}
		}
		break;
	case 3:
		{
			if (iValueExport < pEVAL->stCFG.iValueRaw_HI_LO)
			{
				uiZone = 2;
			}
		}
		break;
	}

	bAdvise = false;
	if (pEVAL->uiLastZoneTemp != uiZone)
	{
		pEVAL->uiLastZoneTemp 		= uiZone;
		pEVAL->uiLastZone	 		= uiZone;
		bAdvise = true;
	}

	pEVAL->uiValueExport 		= iValueExport;
	pEVAL->uiValueExportWOpt 	= pEVAL->uiValueExport;

	pEVAL->uiIN_TYPE = pEVAL->stCFG.uiIN_TYPE;
	pEVAL->uiValueExportWOpt |= (pEVAL->uiIN_TYPE & 0x7) << 13;

	pEVAL->uiValueExportWOpt |= (pEVAL->uiLastZone & 0x7) << 10;
	return bAdvise;
}

#ifdef ADCREADER_READ_VALUES_IN_PARALLEL

#else
bool taskSchedulerReadVBatt(bool bUnderLoad)
{
	int iSamples;
	int iValueRaw;

	adcreaderReadVBattEnable(true);


	iValueRaw = adcreaderReadVBatt(10,1, NULL);

	adcreaderReadVBattEnable(false);

	return taskComunicatioSetVBatRAW(iValueRaw,stSCHEDULER_CTX.stBATTERY.ui_mV_min,stSCHEDULER_CTX.stBATTERY.ui_mV_max,bUnderLoad);
}
#endif
