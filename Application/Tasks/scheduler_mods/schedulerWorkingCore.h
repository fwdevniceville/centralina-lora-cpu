/*
 * schedulerWorkingCore.h
 *
 *  Created on: 27 dic 2018
 *      Author: daniele_parise
 */

#ifndef TASKS_SCHEDULER_MODS_SCHEDULERWORKINGCORE_H_
#define TASKS_SCHEDULER_MODS_SCHEDULERWORKINGCORE_H_

void schedulerWorkingCoreInit(TickType_t uiOsTimerTicks);
void schedulerWorkingCoreFree(void);
TickType_t schedulerWorkingCore(TickType_t uiOsTimerTicks,bool bForceAction);

#endif /* TASKS_SCHEDULER_MODS_SCHEDULERWORKINGCORE_H_ */
