/*
 * schedulerCoils.h
 *
 *  Created on: 21 nov 2018
 *      Author: daniele_parise
 */

#ifndef TASKS_SCHEDULER_MODS_SCHEDULERCOILS_H_
#define TASKS_SCHEDULER_MODS_SCHEDULERCOILS_H_

void schedulerCoilAction(unsigned uiKeepOpenMask, unsigned uiOpenMask, unsigned uiCloseMask);
bool schedulerCoilActionLatch(unsigned uiOpenPulseMask, unsigned uiClosePulseMask);
bool schedulerCoilActionAC(unsigned uiKeepOpenMask);


#endif /* TASKS_SCHEDULER_MODS_SCHEDULERCOILS_H_ */
