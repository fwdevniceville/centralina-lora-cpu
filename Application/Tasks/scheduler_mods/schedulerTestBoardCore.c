/*
 * schedulerTestBoardCore.c
 *
 *  Created on: 27 dic 2018
 *      Author: daniele_parise
 */

#include "cmsis_os.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "../../SystemFunctions.h"
#include "../taskScheduler.h"
#include "../taskInterface.h"
#include "../../Storage/configurations.h"
#include "../../FeeRTOS/FRT_BoardFunctions.h"
#include "schedulerTestBoardCore.h"
#include "schedulerADCReaders.h"
#include "schedulerCoils.h"
#include "schedulerProcessor.h"
//#define ADCREADER_SKIP
#ifndef ADCREADER_SKIP
#include "../../inout/adcreader.h"

#endif
#include "../../inout/coildrv.h"
#include "../../Display/display.h"
#include "../../hwbrd/hwbuttons.h"

#define LORABOARD_TEST_CMD_ACTIVATE			(0x01)
#define LORABOARD_TEST_CMD_DEACTIVATE		(0x02)
#define LORABOARD_TEST_CMD_BASE_ACTIVE		(0x10)
#define LORABOARD_TEST_CMD_ACTIONS			(0x10)
#define LORABOARD_TEST_CMD_GET_DATA			(0x20)

#define LORABOARD_TEST_RET_OK			(0x00)
#define LORABOARD_TEST_UNSUPPORTED		(0x81)
#define LORABOARD_TEST_INVALID			(0x82)
#define LORABOARD_TEST_SYS_INIT			(0x83)
#define LORABOARD_TEST_NO_MODE			(0x84)
#define LORABOARD_TEST_BUSY				(0x85)

#define TEST_BOARD_CMD_OUT_OPTIONS_VPWR_ON					(0x01)
#define TEST_BOARD_CMD_OUT_OPTIONS_VEXT_ON					(0x02)
#define TEST_BOARD_CMD_OUT_OPTIONS_LOSIDE_DISCONNECTED		(0x04)
#define TEST_BOARD_CMD_OUT_POWER_BUCKBOOST_ON				(0x10)

#define TEST_BOARD_CMD_IN_MODE_NO_PULLUP		(0x00)
#define TEST_BOARD_CMD_IN_MODE_PULLUP			(0x01)
#define TEST_BOARD_CMD_IN_MODE_ADC_0_10V		(0x02)
#define TEST_BOARD_CMD_IN_MODE_ADC_4_20mA		(0x03)

#define TEST_BOARD_CMD_ADC_OPTIONS_CURR_RD		(0x01)
#define TEST_BOARD_CMD_ADC_OPTIONS_VBATT_RD		(0x02)

typedef struct tagTEST_BOARD_CMD {
	int8_t	iOUT_DRIVE_NUM;
	uint8_t	uiOUT_DRIVE_dVDrive;
	uint8_t uiOUT_DRIVE_dSLoad;
	uint8_t uiOUT_DRIVE_OPTIONS;
	uint8_t uiMODE_IN0;
	uint8_t uiMODE_IN1;
	uint8_t uiADC_OPTIONS;
	uint8_t uiDISPLAY_OVERRIDE;
	uint8_t	auiDISPLAY[8];
}TEST_BOARD_CMD;

#define TEST_BOARD_RET_INT_SGN_READ_CURR_ENABLED	(0x0001)
#define TEST_BOARD_RET_INT_SGN_READ_VBAT_ENABLED	(0x0002)
#define TEST_BOARD_RET_INT_SGN_VEXT_PRESENT			(0x0100)

typedef struct tagTEST_BOARD_RET {
	int8_t		iOUT_DRIVING;
	uint8_t		uiOUT_OPTIONS;

	uint8_t		uimAM_OUT_DRIVE_HI;
	uint8_t		uimAM_OUT_DRIVE_LO;
	uint8_t		uimAP_OUT_DRIVE_HI;
	uint8_t		uimAP_OUT_DRIVE_LO;
	uint8_t		uimAC_OUT_DRIVE_HI;
	uint8_t		uimAC_OUT_DRIVE_LO;

	uint8_t		uiMODE_IN0;
	uint8_t		uiMODE_IN1;

	uint8_t		uiBUTTONS;
	uint8_t		uiADC_OPTIONS;

	uint8_t		uiADC_IN0_HI;
	uint8_t		uiADC_IN0_LO;

	uint8_t		uiADC_IN1_HI;
	uint8_t		uiADC_IN1_LO;

	uint8_t		uiADC_VBATT_HI;
	uint8_t		uiADC_VBATT_LO;

	uint8_t		uiCNTR_HI;
	uint8_t		uiCNTR_LO;
	uint8_t		uiINT_SGN_HI;
	uint8_t		uiINT_SGN_LO;
}TEST_BOARD_RET;

typedef struct tagTEST_BOARD_RET_CPLX {
	int8_t		iOUT_DRIVING;
	uint8_t		uiOUT_OPTIONS;
	uint8_t		uiOUT_DRIVE_dVDrive;

	int16_t		imA_OUT_DRIVE_5ms_peak;
	int16_t		imA_OUT_DRIVE_20ms_mean;
	int16_t		imA_OUT_continuos;

	uint8_t		uiMODE_IN0;
	uint8_t		uiMODE_IN1;

	uint8_t		uiBUTTONS;
	uint8_t		uiADC_OPTIONS;

	uint16_t	uiADC_VBATT;
	uint16_t	uiADC_IN0;
	uint16_t	uiADC_IN1;
	uint16_t	uiCNTR;
	uint16_t	uiINT_SGN;
}TEST_BOARD_RET_CPLX;

typedef struct tagSCHEDULER_TEST_BOARD{
	TEST_BOARD_CMD		stCMD_NEW;
	TEST_BOARD_CMD		stCMD;
	TEST_BOARD_RET_CPLX	stSTATUS_TMP;
	TEST_BOARD_RET_CPLX	stSTATUS_NEW;
}SCHEDULER_TEST_BOARD;

SCHEDULER_TEST_BOARD	stSCHEDULER_TEST_BOARD;

void taskSchedulerExecBoardTestCmd(const uint8_t* abyBuffer, size_t uiBufferSize,uint8_t* uiResult,uint8_t* abyBufferRet,size_t sizBufferRetSizeMax,size_t* psizBufferRetSize)
{
	uint8_t	uiCMD;
	uiCMD = abyBuffer[0];
	*uiResult = LORABOARD_TEST_SYS_INIT;
	if (stSCHEDULER_CTX.eMODE > SCHEDULER_MODE_INIT)
	{
		*uiResult =LORABOARD_TEST_RET_OK;
		switch (uiCMD)
		{
		case LORABOARD_TEST_CMD_ACTIVATE:
			taskSchedulerChangeMode(SCHEDULER_MODE_BOARD_TEST);
			if (stSCHEDULER_CTX.eMODE != SCHEDULER_MODE_BOARD_TEST)
			{
				*uiResult =LORABOARD_TEST_NO_MODE;
			}
			break;
		case LORABOARD_TEST_CMD_DEACTIVATE:
			taskSchedulerChangeMode(SCHEDULER_MODE_WORKING);
			break;
		}

		if (uiCMD >= LORABOARD_TEST_CMD_BASE_ACTIVE)
		{
			if (stSCHEDULER_CTX.eMODE != SCHEDULER_MODE_BOARD_TEST)
			{
				*uiResult =LORABOARD_TEST_NO_MODE;
			}
			else
			{
				int iCMD_SIZE;
				switch (uiCMD)
				{
				default:
					*uiResult =LORABOARD_TEST_UNSUPPORTED;
					break;
				case LORABOARD_TEST_CMD_ACTIONS:
					{

						iCMD_SIZE = uiBufferSize;
						iCMD_SIZE-=2;
						if (iCMD_SIZE > sizeof(stSCHEDULER_TEST_BOARD.stCMD))
						{
							iCMD_SIZE = sizeof(stSCHEDULER_TEST_BOARD.stCMD);
						}
						memset(&stSCHEDULER_TEST_BOARD.stCMD_NEW,0,sizeof(stSCHEDULER_TEST_BOARD.stCMD));
						memcpy(&stSCHEDULER_TEST_BOARD.stCMD_NEW,&abyBuffer[2],iCMD_SIZE);
					}
				case LORABOARD_TEST_CMD_GET_DATA:
					{
						TEST_BOARD_RET	stANSW;

						stANSW.iOUT_DRIVING 		= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.iOUT_DRIVING;
						stANSW.uiOUT_OPTIONS 		= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiOUT_OPTIONS;
						stANSW.uimAM_OUT_DRIVE_HI 	= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.imA_OUT_DRIVE_20ms_mean >> 8;
						stANSW.uimAM_OUT_DRIVE_LO 	= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.imA_OUT_DRIVE_20ms_mean;
						stANSW.uimAP_OUT_DRIVE_HI 	= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.imA_OUT_DRIVE_5ms_peak >> 8;
						stANSW.uimAP_OUT_DRIVE_LO 	= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.imA_OUT_DRIVE_5ms_peak;
						stANSW.uimAC_OUT_DRIVE_HI 	= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.imA_OUT_continuos >> 8;
						stANSW.uimAC_OUT_DRIVE_LO 	= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.imA_OUT_continuos;

						stANSW.uiMODE_IN0 			= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiMODE_IN0;
						stANSW.uiMODE_IN1 			= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiMODE_IN1;
						stANSW.uiBUTTONS 			= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiBUTTONS;
						stANSW.uiADC_OPTIONS 		= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiADC_OPTIONS;

						stANSW.uiADC_IN0_HI 		= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiADC_IN0 >> 8;
						stANSW.uiADC_IN0_LO 		= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiADC_IN0;
						stANSW.uiADC_IN1_HI 		= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiADC_IN1 >> 8;
						stANSW.uiADC_IN1_LO 		= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiADC_IN1;
						stANSW.uiADC_VBATT_HI 		= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiADC_VBATT >> 8;
						stANSW.uiADC_VBATT_LO 		= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiADC_VBATT;
						stANSW.uiCNTR_HI 			= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiCNTR >> 8;
						stANSW.uiCNTR_LO 			= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiCNTR;
						stANSW.uiINT_SGN_HI 		= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiINT_SGN >> 8;
						stANSW.uiINT_SGN_LO 		= stSCHEDULER_TEST_BOARD.stSTATUS_NEW.uiINT_SGN;


						iCMD_SIZE = sizBufferRetSizeMax;
						if (iCMD_SIZE > sizeof(TEST_BOARD_RET))
						{
							iCMD_SIZE = sizeof(TEST_BOARD_RET);
						}
						memcpy(&abyBufferRet[0],&stANSW,iCMD_SIZE);
						*psizBufferRetSize = iCMD_SIZE;
					}
					break;
				}
				taskSchedulerSignalAction();
			}
		}
	}
}


void schedulerTestBoardCoreInit(TickType_t uiOsTimerTicks)
{
	memset(&stSCHEDULER_TEST_BOARD.stCMD,0,sizeof(stSCHEDULER_TEST_BOARD.stCMD));
	FRT_SetRunningRQST(FRT_TASK_ORDER_SCHEDULER,true);

	coildrvInit();

	taskInterfacePostDisplayAlwaysOn(true);
}

void schedulerTestBoardCoreFree(void)
{
	taskInterfacePostDisplayAlwaysOn(false);

	FRT_SetRunningRQST(FRT_TASK_ORDER_SCHEDULER,false);
	coildrvFree();
	displayOutOverride(false,NULL);
}


void schedulerTestBoardCoreDriveOut(const TEST_BOARD_CMD* pCMD_OUT);

TickType_t schedulerTestBoardCore(TickType_t uiOsTimerTicks,bool bForceAction)
{
	static const uint8_t	auiDISPLAY[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
	const uint8_t			*auiDISPLAY_CURRENT;
	// copiamo il comando per evitare race conditions
	stSCHEDULER_TEST_BOARD.stCMD = stSCHEDULER_TEST_BOARD.stCMD_NEW;

	auiDISPLAY_CURRENT = &auiDISPLAY[0];
	if (stSCHEDULER_TEST_BOARD.stCMD.uiDISPLAY_OVERRIDE)
	{
		auiDISPLAY_CURRENT = &stSCHEDULER_TEST_BOARD.stCMD.auiDISPLAY[0];
	}

	displayOutOverride(1,auiDISPLAY_CURRENT);

	if (stSCHEDULER_TEST_BOARD.stCMD.uiOUT_DRIVE_OPTIONS & TEST_BOARD_CMD_OUT_POWER_BUCKBOOST_ON)
	{
		systemBuckBoostEnable(true);

	}
	else
	{
		systemBuckBoostEnable(false);
	}

	adcreaderInit(ADCREADER_IN0_MODE_ADC_5V);
	// OUTS
	{
		if (
				(stSCHEDULER_TEST_BOARD.stSTATUS_TMP.iOUT_DRIVING != stSCHEDULER_TEST_BOARD.stCMD.iOUT_DRIVE_NUM)
					||
				(stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiOUT_OPTIONS != stSCHEDULER_TEST_BOARD.stCMD.uiOUT_DRIVE_OPTIONS)
					||
				(stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiOUT_DRIVE_dVDrive != stSCHEDULER_TEST_BOARD.stCMD.uiOUT_DRIVE_dVDrive)
			)
		{


			schedulerTestBoardCoreDriveOut(&stSCHEDULER_TEST_BOARD.stCMD);
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.iOUT_DRIVING = stSCHEDULER_TEST_BOARD.stCMD.iOUT_DRIVE_NUM;
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiOUT_OPTIONS = stSCHEDULER_TEST_BOARD.stCMD.uiOUT_DRIVE_OPTIONS;
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiOUT_DRIVE_dVDrive = stSCHEDULER_TEST_BOARD.stCMD.uiOUT_DRIVE_dVDrive;
		}
	}
	{
		EADCREADER_IN0_MODE 	eIN0_MODE;
		unsigned			uiPULLUP_MASK;
		uiPULLUP_MASK = 0x00;

		switch (stSCHEDULER_TEST_BOARD.stCMD.uiMODE_IN0)
		{
		default:
		case TEST_BOARD_CMD_IN_MODE_NO_PULLUP:
			eIN0_MODE = ADCREADER_IN0_MODE_ADC_5V;
			break;
		case TEST_BOARD_CMD_IN_MODE_PULLUP:
			eIN0_MODE = ADCREADER_IN0_MODE_ADC_5V;
			uiPULLUP_MASK |= 0x01;
			break;
		case TEST_BOARD_CMD_IN_MODE_ADC_0_10V:
			eIN0_MODE = ADCREADER_IN0_MODE_ADC_10V;
			break;
		case TEST_BOARD_CMD_IN_MODE_ADC_4_20mA:
			eIN0_MODE = ADCREADER_IN0_MODE_ADC_4_20mA;
			break;
		}

		switch (stSCHEDULER_TEST_BOARD.stCMD.uiMODE_IN1)
		{
		default:
		case TEST_BOARD_CMD_IN_MODE_NO_PULLUP:
			break;
		case TEST_BOARD_CMD_IN_MODE_PULLUP:
			uiPULLUP_MASK |= 0x02;
			break;
		case TEST_BOARD_CMD_IN_MODE_ADC_0_10V:
			break;
		case TEST_BOARD_CMD_IN_MODE_ADC_4_20mA:
			break;
		}

		adcreaderReadIN0Select(eIN0_MODE);
		adcreaderEnablePullUps(uiPULLUP_MASK);

		adcreaderReadCurrentEnable(stSCHEDULER_TEST_BOARD.stCMD.uiADC_OPTIONS & TEST_BOARD_CMD_ADC_OPTIONS_CURR_RD ? true : false);
		adcreaderReadVBattEnable(stSCHEDULER_TEST_BOARD.stCMD.uiADC_OPTIONS & TEST_BOARD_CMD_ADC_OPTIONS_VBATT_RD ? true : false);



		stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiMODE_IN0 		= stSCHEDULER_TEST_BOARD.stCMD.uiMODE_IN0;
		stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiMODE_IN1 		= stSCHEDULER_TEST_BOARD.stCMD.uiMODE_IN1;
		stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiADC_OPTIONS 	= stSCHEDULER_TEST_BOARD.stCMD.uiADC_OPTIONS;

		stSCHEDULER_TEST_BOARD.stSTATUS_TMP.imA_OUT_continuos 	= adcreaderReadCurrent(10,0,NULL);
		stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiADC_VBATT 		= adcreaderReadChannelMean(ADCREADER_VBATT,10,0,NULL,true);
		stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiADC_IN0 			= adcreaderReadChannelMean(ADCREADER_IN0,10,0,NULL,true);
		stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiADC_IN1 			= adcreaderReadChannelMean(ADCREADER_IN4,10,0,NULL,true);


		stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiBUTTONS = 0;
		if (hwbuttonsTestBTN1())
		{
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiBUTTONS |= 0x01;
		}
		if (hwbuttonsTestBTN2())
		{
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiBUTTONS |= 0x02;
		}
		if (hwbuttonsTestBTN3())
		{
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiBUTTONS |= 0x04;
		}

		if (coildrvIsLatchingEV())
		{
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiBUTTONS |= 0x10;
		}
		if (coildrvIsBoostEV())
		{
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiBUTTONS |= 0x20;
		}


		taskSchedulerGetFluxCNTR(&stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiCNTR);
		stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiINT_SGN = 0;
		if (adcreaderIsReadCurrentEnabled())
		{
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiINT_SGN |= TEST_BOARD_RET_INT_SGN_READ_CURR_ENABLED;
		}
		if (adcreaderIsReadVBattEnabled())
		{
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiINT_SGN |= TEST_BOARD_RET_INT_SGN_READ_VBAT_ENABLED;
		}
		if (adcreaderIsVEXTPresent())
		{
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.uiINT_SGN |= TEST_BOARD_RET_INT_SGN_VEXT_PRESENT;
		}


#define TEST_BOARD_RET_INT_SGN_READ_CURR_ENABLED	(0x0001)
#define TEST_BOARD_RET_INT_SGN_READ_BATT_ENABLED	(0x0002)
#define TEST_BOARD_RET_INT_SGN_VEXT_PRESENT			(0x0100)

		adcreaderReadCurrentEnable(false);
		adcreaderFree();

		// copiamo lo stato per evitare race conditions
		stSCHEDULER_TEST_BOARD.stSTATUS_NEW = stSCHEDULER_TEST_BOARD.stSTATUS_TMP;
	}
	return 100;
}


void schedulerTestBoardCoreDriveOut(const TEST_BOARD_CMD* pCMD_OUT)
{
	int iOUT;
	iOUT = pCMD_OUT->iOUT_DRIVE_NUM;
	if (iOUT == 0)
	{
		coildrvPowerLoad(0);
	}
	coildrvPWR_LO();
	coildrvPWR2_EXT_LO();
	osDelay(2);
	coildrv3V3_PWR_VLV_OFF();
	coildrv3V3_PWR_COM_OFF();
	if (iOUT != 0)
	{
		unsigned	uiOUT_MASK;
		bool 		bInverted;
		bool 		bOnlyHISide;
		bOnlyHISide = false;
		if (pCMD_OUT->uiOUT_DRIVE_OPTIONS & TEST_BOARD_CMD_OUT_OPTIONS_LOSIDE_DISCONNECTED)
		{
			bOnlyHISide = true;
		}
		bInverted = false;
		if (iOUT < 0)
		{
			iOUT = -iOUT;
			bInverted = true;
		}
		iOUT--;
		uiOUT_MASK = 1 << iOUT;


		coildrvPowerLoad((int)pCMD_OUT->uiOUT_DRIVE_dVDrive * 100);
		osDelay((int)pCMD_OUT->uiOUT_DRIVE_dSLoad * 100);

		if (bInverted)
		{
			coildrvCOMM_HI();
			if (bOnlyHISide)
			{
				// TUTTO ALTO
				coildrvOUT_POL(0x0F);
			}
			else
			{
				coildrvOUT_POL(~uiOUT_MASK);
			}
		}
		else
		{
			coildrvOUT_POL(uiOUT_MASK);
			if (bOnlyHISide)
			{
				// TUTTO ALTO
				// COM ALTO E SOLO UNA USCITA ALTA
				coildrvCOMM_HI();
			}
			else
			{
				coildrvCOMM_LO();
			}
		}
		osDelay(2);
		if (bOnlyHISide)
		{
			// PER NON SBAGLIARE NON ALIMENTIAMO
			// LA PARE BASSA DEL PONTE
		}
		else
		{
			coildrv3V3_PWR_VLV_ON();
			coildrv3V3_PWR_COM_ON();
		}
		osDelay(2);
		if (pCMD_OUT->uiOUT_DRIVE_OPTIONS & TEST_BOARD_CMD_OUT_OPTIONS_VEXT_ON)
		{
			coildrvPWR2_EXT_HI();
		}
		if (pCMD_OUT->uiOUT_DRIVE_OPTIONS & TEST_BOARD_CMD_OUT_OPTIONS_VPWR_ON)
		{
			coildrvPWR_HI();
		}
		{
			int	iMean;
			int	iPulse;
			adcreaderReadCurrentEnable(true);
			iMean = adcreaderReadCurrent(20,5,&iPulse);
			adcreaderReadCurrentEnable(false);
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.imA_OUT_DRIVE_5ms_peak = iPulse;
			stSCHEDULER_TEST_BOARD.stSTATUS_TMP.imA_OUT_DRIVE_20ms_mean = iMean;
		}
		osDelay(100);
	}
}

