/*
 * schedulerWorkingCore.c
 *
 *  Created on: 27 dic 2018
 *      Author: daniele_parise
 */

#include "cmsis_os.h"
#include <stdbool.h>
#include <stdint.h>
#include "../../SystemFunctions.h"
#include "../taskScheduler.h"
#include "../taskComunication.h"
#include "../../Storage/configurations.h"
#include "../../FeeRTOS/FRT_BoardFunctions.h"
#include "schedulerWorkingCore.h"
#include "schedulerADCReaders.h"
#include "schedulerCoils.h"
#include "schedulerProcessor.h"

//#define ADCREADER_SKIP
#ifndef ADCREADER_SKIP
#include "../../inout/adcreader.h"

#endif
#include "../../inout/coildrv.h"


typedef struct tagSCHEDULER_INT_CTX{
	CFG_SCHEDULER_ROW	aTEMP_SCHED[4];
	DateTime			dtCurrDate;
	TickType_t 			uiOsTimerTicksLast;
	TickType_t 			uiVBattCheckCountDown;
#ifdef DEBUG
	int 				iTestOpen;
#endif
	unsigned			bPROCESS_IN_CFG : 1;
}SCHEDULER_INT_CTX;

SCHEDULER_INT_CTX	stSCHEDULER_INT_CTX;

void schedulerLoadConfigurations(void);

void schedulerWorkingCoreInit(TickType_t uiOsTimerTicks)
{
	stSCHEDULER_INT_CTX.bPROCESS_IN_CFG			= true;
	coildrvFree();
	stSCHEDULER_INT_CTX.uiOsTimerTicksLast			= uiOsTimerTicks;

}

void schedulerWorkingCoreFree(void)
{
}


TickType_t schedulerWorkingCore(TickType_t uiOsTimerTicks,bool bForceAction)
{
	int			iCurrentSchedule;
	bool		bLowBattPowerOff;
	bool		bPushUpLink;
	TickType_t 	uiOsTimerTicksDelta;
	iCurrentSchedule = -1;
#ifdef ADCREADER_READ_VALUES_IN_PARALLEL
	bool bReadBatt;
	bool bReadTemp;
	bReadBatt = false;
	bReadTemp = false;
#endif
#ifdef DEBUG
	if (stSCHEDULER_INT_CTX.iTestOpen > 0)
	{
		taskSchedulerOpenValve(stSCHEDULER_INT_CTX.iTestOpen,1);
		stSCHEDULER_INT_CTX.iTestOpen = 0;
	}
#endif

	uiOsTimerTicksDelta = uiOsTimerTicks - stSCHEDULER_INT_CTX.uiOsTimerTicksLast;


	if (
			(uiOsTimerTicksDelta >= 10000)
				||
			bForceAction
				||
			stSCHEDULER_INT_CTX.bPROCESS_IN_CFG
		)
	{
		uint64_t			uiCurrSecondsFrom2K;
		uint32_t			uiOutMask;
		uint32_t			uiOutMaskPulseClose;
		uint32_t			uiOutMaskPulseOpen;
		uint32_t			uiOutKeepOnMask;
		CFG_SCHEDULER_ROW* 	aRows;
		CFG_SCHEDULER_ROW* 	pRow;
		int					iRowsCount;
		int					iRowsCounter;

		bPushUpLink = false;

		schedulerLoadConfigurations();
		if (stSCHEDULER_INT_CTX.bPROCESS_IN_CFG)
		{
			stSCHEDULER_INT_CTX.bPROCESS_IN_CFG = false;
#ifndef ADCREADER_SKIP
			adcreaderInit(ADCREADER_IN0_MODE_IN);
#ifdef ADCREADER_READ_VALUES_IN_PARALLEL
			taskSchedulerReanAndEvalINS(true,true);
#else
			taskSchedulerReadVBatt(false);
#endif
			adcreaderFree();
#endif
		}

		stSCHEDULER_INT_CTX.uiOsTimerTicksLast = uiOsTimerTicks;

		uiCurrSecondsFrom2K = systemGetCurrentDateTime(&stSCHEDULER_INT_CTX.dtCurrDate);

		iRowsCount =  configGetSchedulerTable(&aRows);

		uiOutMask	= 0;
		iRowsCounter = 0;
		while (iRowsCounter < iRowsCount)
		{
			pRow =&aRows[iRowsCounter];
			if (schedulerProcessRow(pRow,uiCurrSecondsFrom2K,&stSCHEDULER_INT_CTX.dtCurrDate,&uiOutMask))
			{
				iCurrentSchedule = pRow->uiORD;
			}

			iRowsCounter++;

		}

		iRowsCounter = 0;
		while (iRowsCounter < (sizeof(stSCHEDULER_INT_CTX.aTEMP_SCHED) / sizeof(stSCHEDULER_INT_CTX.aTEMP_SCHED[0])))
		{
			pRow =&stSCHEDULER_INT_CTX.aTEMP_SCHED[iRowsCounter];
			if (schedulerProcessRow(pRow,uiCurrSecondsFrom2K,&stSCHEDULER_INT_CTX.dtCurrDate,&uiOutMask))
			{
				iCurrentSchedule = 0x80 | iRowsCounter;
			}

			iRowsCounter++;

		}
		stSCHEDULER_CTX.iCurrentSchedule 	= iCurrentSchedule;
		stSCHEDULER_CTX.stVALVOLE.uiValvoleSchedulate = uiOutMask;

		stSCHEDULER_CTX.stCOIL_STATUS.iDriveCounter = 0;

		stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateAperte |= stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaApertertura;
		stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateChiuse |= stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaChiusura;
		stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateAperte &= ~stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaChiusura;
		stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateChiuse &= ~stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaApertertura;

		stSCHEDULER_CTX.stVALVOLE.uiValvoleAperte &= ~ stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaApertertura;
		stSCHEDULER_CTX.stVALVOLE.uiValvoleChiuse &= ~ stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaChiusura;

		uiOutMask |= stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateAperte;

		uiOutMask &= ~stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateChiuse;
		uiOutMask &= ~stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateChiuseLowBatt;


		uiOutMaskPulseClose = ((~uiOutMask) ^ stSCHEDULER_CTX.stVALVOLE.uiValvoleChiuse) & ~uiOutMask;
		uiOutMaskPulseClose &= 0xF;
		uiOutMaskPulseOpen  = (( uiOutMask) ^ stSCHEDULER_CTX.stVALVOLE.uiValvoleAperte) & uiOutMask;
		uiOutMaskPulseOpen &= 0xF;

		uiOutKeepOnMask = uiOutMask;

#ifndef ADCREADER_SKIP
		adcreaderInit(ADCREADER_IN0_MODE_ADC_10V);
#endif

		if (
				(uiOutMaskPulseClose != 0)
				||
				(uiOutMaskPulseOpen != 0)
			)
		{
			FRT_SetRunningRQST(FRT_TASK_ORDER_SCHEDULER,true);


			systemBuckBoostEnable(true);

			schedulerCoilAction(uiOutKeepOnMask,uiOutMaskPulseOpen,uiOutMaskPulseClose);

			systemBuckBoostEnable(false);

			FRT_SetRunningRQST(FRT_TASK_ORDER_SCHEDULER,false);
		}
		else
		{
#ifndef ADCREADER_SKIP
			if (!stSCHEDULER_CTX.stCOIL_STATUS.bCURRENT_SENSOR_CHECKED)
			{
				stSCHEDULER_CTX.stCOIL_STATUS.bCURRENT_SENSOR_CHECKED = true;
				if (!stSCHEDULER_CTX.stCOIL_STATUS.bCURRENT_SENSOR_ERROR)
				{
					adcreaderReadCurrentEnable(true);
					osDelay(2);
					if (!adcreaderIsReadCurrentEnabled())
					{
						adcreaderReadCurrentEnable(false);
						stSCHEDULER_CTX.stCOIL_STATUS.bCURRENT_SENSOR_ERROR =true;
					}

					stSCHEDULER_CTX.stCOIL_STATUS.iCurrentBase = adcreaderReadCurrent(20,0,NULL);

				}
				adcreaderReadCurrentEnable(false);
			}
#endif
		}

		stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaApertertura = 0;
		stSCHEDULER_CTX.stVALVOLE.uiValvoleForzaChiusura = 0;

#ifndef ADCREADER_SKIP
		if (stSCHEDULER_CTX.stCOIL_STATUS.iDriveCounter > 0)
		{
			if (stSCHEDULER_INT_CTX.uiVBattCheckCountDown > 10000)
			{
				stSCHEDULER_INT_CTX.uiVBattCheckCountDown = 10000;
			}
		}

		{
			if (stSCHEDULER_INT_CTX.uiVBattCheckCountDown < uiOsTimerTicksDelta)
			{
				stSCHEDULER_INT_CTX.uiVBattCheckCountDown = 0;
			}
			else
			{
				stSCHEDULER_INT_CTX.uiVBattCheckCountDown-= uiOsTimerTicksDelta;
			}
			if (stSCHEDULER_INT_CTX.uiVBattCheckCountDown == 0)
			{
				stSCHEDULER_INT_CTX.uiVBattCheckCountDown = 60000;

#ifdef ADCREADER_READ_VALUES_IN_PARALLEL
				bReadBatt = true;
				bReadTemp = true;
#else
				bLowBattPowerOff = taskSchedulerReadVBatt(false);
/*
				bLowBattPowerOff = taskSchedulerReadVBatt(
								stSCHEDULER_CTX.stCOIL_STATUS.iDriveCounter > 0 ? true : false
								);
*/
				if (bLowBattPowerOff)
				{
					stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateChiuseLowBatt = 0xFFFF;
				}
				else
				{
					stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateChiuseLowBatt = 0;
				}
#endif
			}
		}
#endif

#ifndef ADCREADER_SKIP

#ifdef ADCREADER_READ_VALUES_IN_PARALLEL

		bPushUpLink = taskSchedulerReanAndEvalINS(bReadBatt,bReadTemp);
		if (bReadBatt)
		{
			if (stSCHEDULER_CTX.stBATTERY.bLowBattPowerOff)
			{
				stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateChiuseLowBatt = 0xFFFF;
			}
			else
			{
				stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateChiuseLowBatt = 0;
			}
		}
#else
		bPushUpLink = taskSchedulerReanAndEvalINS();
#endif

		adcreaderFree();
#endif
/*
		stSCHEDULER_CTX.stVALVOLE.uiValvoleChiuse &= ~uiOutMaskPulseOpen;
		stSCHEDULER_CTX.stVALVOLE.uiValvoleChiuse |= uiOutMaskPulseClose;

		stSCHEDULER_CTX.stVALVOLE.uiValvoleAperte &= ~uiOutMaskPulseClose;
		stSCHEDULER_CTX.stVALVOLE.uiValvoleAperte |= uiOutMaskPulseOpen;
*/
		if (bPushUpLink)
		{
			taskComunicationSignalUpLinkRequest(true);
		}

	}
	else
	if (uiOsTimerTicksDelta >= 10000)
	{
		uiOsTimerTicksDelta = 0;
	}
	else
	{
		uiOsTimerTicksDelta = 10000 - uiOsTimerTicksDelta;
	}
	return uiOsTimerTicksDelta;
}



void schedulerLoadConfigurations(void)
{
	const UCFG_GEN_SLOT_DATA* puDATA = configConfigGetRO(CONFIG_SLOT_SYSTEM_PARAMETERS);
	int		iTemp;

	stSCHEDULER_CTX.stCOIL_SETTINGS.bForceLatch = false;
	if (puDATA->stSYSTEM_PARAMETERS.uiOutType)
	{
		stSCHEDULER_CTX.stCOIL_SETTINGS.bForceLatch = true;
	}
	stSCHEDULER_CTX.stCOIL_SETTINGS.uimVPULSE 	= puDATA->stSYSTEM_PARAMETERS.uiOutLoad_mV;
	stSCHEDULER_CTX.stCOIL_SETTINGS.uiTLOAD 	= puDATA->stSYSTEM_PARAMETERS.uiOutLoad_TLoad;
	stSCHEDULER_CTX.stCOIL_SETTINGS.uiTPULSE 	= puDATA->stSYSTEM_PARAMETERS.uiOutLoad_TDischarge;


	stSCHEDULER_CTX.stADCIN_STATUS.stIN0.stCFG.uiIN_TYPE 				= puDATA->stSYSTEM_PARAMETERS.uiIN0_TYPE;
	iTemp = puDATA->stSYSTEM_PARAMETERS.uiIN0_RAW_TH_DELTA / 2;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN0.stCFG.iValueRaw_LO_LO 			= puDATA->stSYSTEM_PARAMETERS.uiIN0_RAW_TH_LO - iTemp;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN0.stCFG.iValueRaw_LO_HI 			= puDATA->stSYSTEM_PARAMETERS.uiIN0_RAW_TH_LO + iTemp;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN0.stCFG.iValueRaw_HI_LO 			= puDATA->stSYSTEM_PARAMETERS.uiIN1_RAW_TH_HI - iTemp;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN0.stCFG.iValueRaw_HI_HI 			= puDATA->stSYSTEM_PARAMETERS.uiIN1_RAW_TH_HI + iTemp;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN0.stCFG.iValueRaw_Delta 			= puDATA->stSYSTEM_PARAMETERS.uiIN0_RAW_TH_DELTA;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN0.stCFG.iTimeDelayAdviseMax 		= puDATA->stSYSTEM_PARAMETERS.uiIN0_AR_sec;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN0.stCFG.iTimeDelayNoiseMax 		= 0;


	stSCHEDULER_CTX.stADCIN_STATUS.stIN4.stCFG.uiIN_TYPE 				= puDATA->stSYSTEM_PARAMETERS.uiIN1_TYPE;
	iTemp = puDATA->stSYSTEM_PARAMETERS.uiIN1_RAW_TH_DELTA / 2;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN4.stCFG.iValueRaw_LO_LO 			= puDATA->stSYSTEM_PARAMETERS.uiIN1_RAW_TH_LO - iTemp;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN4.stCFG.iValueRaw_LO_HI 			= puDATA->stSYSTEM_PARAMETERS.uiIN1_RAW_TH_LO + iTemp;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN4.stCFG.iValueRaw_HI_LO 			= puDATA->stSYSTEM_PARAMETERS.uiIN1_RAW_TH_HI - iTemp;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN4.stCFG.iValueRaw_HI_HI 			= puDATA->stSYSTEM_PARAMETERS.uiIN1_RAW_TH_HI + iTemp;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN4.stCFG.iValueRaw_Delta 			= puDATA->stSYSTEM_PARAMETERS.uiIN1_RAW_TH_DELTA;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN4.stCFG.iTimeDelayAdviseMax 		= puDATA->stSYSTEM_PARAMETERS.uiIN1_AR_sec;
	stSCHEDULER_CTX.stADCIN_STATUS.stIN4.stCFG.iTimeDelayNoiseMax 		= 0;


	stSCHEDULER_CTX.stBATTERY.ui_mV_min 			= puDATA->stSYSTEM_PARAMETERS.uiBatteryMin_mV;
	stSCHEDULER_CTX.stBATTERY.ui_mV_max 			= puDATA->stSYSTEM_PARAMETERS.uiBatteryMax_mV;

	stSCHEDULER_CTX.stFLUX_STATUS.uiAntiBunch_ms 	= puDATA->stSYSTEM_PARAMETERS.uiIN2_COUNTER_AR_msec;

}

bool taskSchedulerOpenValve(int iNum,int iMinutes)
{
	if ((iNum > 0) && (iNum <= 4))
	{
		CFG_SCHEDULER_ROW*	pROW;
		iNum--;

		pROW = &stSCHEDULER_INT_CTX.aTEMP_SCHED[iNum];
		pROW->uiTYPE = CFG_SCHEDULER_ROW_TYPE_INVALID;

		if (iMinutes > 0)
		{
			DateTime			dtCurrDate;
			systemGetCurrentDateTime(&dtCurrDate);

			pROW->uiStartYear 		= dtCurrDate.tm_year % 100;
			pROW->uiStartMonth		= dtCurrDate.tm_mon;
			pROW->uiStartMonthDay	= dtCurrDate.tm_mday;
			pROW->uiPeriodDay	= 0;
			pROW->uiStart_hour	= dtCurrDate.tm_hour;
			pROW->uiStart_min	= dtCurrDate.tm_min;
			pROW->uiRuntime_min	= iMinutes;
			pROW->uiOUT_MASK	= 1 << iNum;
			pROW->uiTYPE = CFG_SCHEDULER_ROW_TYPE_DAYPERIOD;

			taskSchedulerSignalAction();

		}
		return true;
	}
	return false;
}
