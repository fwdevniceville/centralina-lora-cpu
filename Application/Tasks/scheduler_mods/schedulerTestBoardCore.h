/*
 * schedulerTestBoardCore.h
 *
 *  Created on: 27 dic 2018
 *      Author: daniele_parise
 */

#ifndef TASKS_SCHEDULER_MODS_SCHEDULERTESTBOARDCORE_H_
#define TASKS_SCHEDULER_MODS_SCHEDULERTESTBOARDCORE_H_

void schedulerTestBoardCoreInit(TickType_t uiOsTimerTicks);
void schedulerTestBoardCoreFree(void);
TickType_t schedulerTestBoardCore(TickType_t uiOsTimerTicks,bool bForceAction);

#endif /* TASKS_SCHEDULER_MODS_SCHEDULERTESTBOARDCORE_H_ */
