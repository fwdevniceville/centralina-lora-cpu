/*
 * taskScheduler.h
 *
 *  Created on: 27 feb 2018
 *      Author: daniele_parise
 */

#ifndef TASKS_TASKSCHEDULER_H_
#define TASKS_TASKSCHEDULER_H_

#define ADCIN_INTYPE_DISABLED		(0)
#define ADCIN_INTYPE_0_5V_PULLUP	(1)
#define ADCIN_INTYPE_0_5V			(2)
#define ADCIN_INTYPE_0_10V			(3)
#define ADCIN_INTYPE_4_20mA			(4)

typedef struct tagADCIN_EVAL{
	struct {
		int32_t		iTimeDelayAdviseMax;
		int32_t		iTimeDelayNoiseMax;

		int16_t		iValueRaw_LO_LO;
		int16_t		iValueRaw_LO_HI;

		int16_t		iValueRaw_HI_LO;
		int16_t		iValueRaw_HI_HI;

		int16_t		iValueRaw_Delta;

		uint8_t		uiIN_TYPE;
	}stCFG;
	uint64_t	uiLastTimeChange;
	int32_t		iTimeDelayAdviseCurr;
	int32_t		iTimeDelayNoiseCurr;
	uint16_t	uiValue_cV;
	int16_t		iValueRaw;
	int16_t		iValue_cVLast;

	uint16_t	uiValueExport; 		// 1023 = 10V = 1000 cV
	uint16_t	uiValueExportWOpt;
	uint8_t		uiLastZoneTemp;
	uint8_t		uiLastZone;
	uint8_t		uiIN_TYPE;
	uint8_t		uiFlagsExport;
}ADCIN_EVAL;

typedef enum etagESCHEDULER_MODE{
	SCHEDULER_MODE_INIT,
	SCHEDULER_MODE_WORKING,
	SCHEDULER_MODE_BOARD_TEST
}ESCHEDULER_MODE;

typedef struct tagSCHEDULER_CTX{
	ESCHEDULER_MODE	eMODE;
	ESCHEDULER_MODE	eMODE_NEW;
	int			iRebootIdleCounter;
	int			iCurrentSchedule;
	bool		bTaskRunning;
	struct{
		uint16_t	uiValvoleSchedulate;
		uint16_t	uiValvoleAperte;
		uint16_t	uiValvoleChiuse;
		uint16_t	uiValvoleMalfunzionantiOC;
		uint16_t	uiValvoleMalFunzionantiSC;

		uint16_t	uiValvoleForzateAperte;
		uint16_t	uiValvoleForzateChiuse;
		uint16_t	uiValvoleForzateChiuseLowBatt;

		uint16_t	uiValvoleForzaApertertura;
		uint16_t	uiValvoleForzaChiusura;
	}stVALVOLE;
	struct {
		unsigned	uimVPULSE;
		unsigned	uiTLOAD;
		unsigned	uiTPULSE;
		bool		bForceLatch;
		bool		bForceMonostabile;
	}stCOIL_SETTINGS;
	struct {
		uint16_t	uiOutMask;
		int 		iCurrentBase;
		int 		iCurrentStart;
		int 		iCurrentPeak;
		int 		iCurrentMean;
		int 		iCurrentDischarge_mA;
		int 		iCurrentDischargePeak_mA;
		int			iDriveCounter;
		bool		bLOADING;
		bool		bDISCHARGE;
		bool		bCURRENT_SENSOR_ERROR;
		bool		bCURRENT_SENSOR_CHECKED;
	}stCOIL_STATUS;
	struct {
		ADCIN_EVAL	stIN0;
		ADCIN_EVAL	stIN4;
	}stADCIN_STATUS;
	struct {
		uint32_t	uiLastTimerTick;
		unsigned 	uiFLUX_CNTR;
		unsigned 	uiAntiBunch_ms;
		uint8_t		uiFLAGS;
	}stFLUX_STATUS;
	struct {
		unsigned 	ui_mV_min;
		unsigned 	ui_mV_max;
#ifdef ADCREADER_READ_VALUES_IN_PARALLEL
		bool		bLowBattPowerOff;
#else
#endif
	}stBATTERY;
}SCHEDULER_CTX;

void taskSchedulerOneMinuteEvent(void);
void taskSchedulerSignalAction(void);

bool taskSchedulerOpenValve(int iNum,int iMinutes);

void taskSchedulerExecBoardTestCmd(const uint8_t* abyBuffer, size_t uiBufferSize,uint8_t* uiResult,uint8_t* abyBufferRet,size_t sizBufferRetSizeMax,size_t* psizBufferRetSize);
void taskSchedulerChangeMode(ESCHEDULER_MODE eMODE);


#define taskSchedulerGetOpenValves()	(stSCHEDULER_CTX.stVALVOLE.uiValvoleAperte)

#define taskSchedulerGetValueIN1WOpt()	(stSCHEDULER_CTX.stADCIN_STATUS.stIN0.uiValueExportWOpt)
#define taskSchedulerGetValueIN2WOpt()	(stSCHEDULER_CTX.stADCIN_STATUS.stIN4.uiValueExportWOpt)
#define taskSchedulerGetFluxCNTROpt()	(stSCHEDULER_CTX.stFLUX_STATUS.uiFLUX_CNTR)

uint8_t taskSchedulerGetValueIN1(uint16_t* puiValue);
uint8_t taskSchedulerGetValueIN2(uint16_t* puiValue);
uint8_t taskSchedulerGetFluxCNTR(uint16_t* puiValue);
void taskSchedulerSignalFluxCNTR_ISR(void);

extern SCHEDULER_CTX	stSCHEDULER_CTX;

#endif /* TASKS_TASKSCHEDULER_H_ */
