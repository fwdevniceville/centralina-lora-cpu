/*
 * lora_payload.h
 *
 *  Created on: 30 mag 2018
 *      Author: daniele_parise
 */

#ifndef TASKS_LORA_PAYLOAD_H_
#define TASKS_LORA_PAYLOAD_H_



#define CENTRALINA_LORAWAN_RESULT_OK				(0)
#define CENTRALINA_LORAWAN_INVALID_DATA				(1)
#define CENTRALINA_LORAWAN_RET_UPLINK_TOO_LONG		(2)



#define CENTRALINA_LORAWAN_PORT_CMD			(10)
#define CENTRALINA_LORAWAN_PORT_CONFIG		(20)

// GENERAL DOWNLINK PAYLOAD FOR PORT 10
// PROGRESSIVE MESSAGE ID + COMMAND_1 + COMMAND_2 + ... + COMMAND_END (until payload end)
// CMD FORMAT:
// COMAND_CODE + related payload

// RELATIVE UPLINK
// PROGRESSIVE MESSAGE ID(same ad command) + RETURN_COMMAND_1 + RETURN_COMMAND_2 + ... + RETURN_COMMAND_END
// RETURN_CMD FORMAT:
// RETURN_COMAND_CODE + RESULT + related return payload  (RESULT == 0 ==> SUCCESS)


// Set board datetime
// COMAND_CODE(0x01) + YY_MM_DD_WD_HH_MM (6 BYTES)
// size 1 + 6 	Year(0..99)_Month(1..12)_Day(1..31)_WeekWay(1..7 ; 1 = Monday)_Hour(0..23)_Minutes(0..59)
// example numeric
// PROGRESSIVE MESSAGE ID		: 1
// COMAND_CODE					: 0x01
// Year(0..99)					: 17
// Month(1..12)					: 7
// Day(1..31)					: 23
// WeekWay(1..7 ; 1 = Monday)	: 1
// Hour(0..23)					: 5
// Minutes(0..59)				: 23



#define CENTRALINA_LORAWAN_CMD_DATETIME			(0x01)

// get board datetime
// COMAND_CODE (0x41)
// size 1
#define CENTRALINA_LORAWAN_CMD_GET_BOARDTIME	(0x41)


// get board linux timestamp 1970
// COMAND_CODE (0x42)
// size 1
#define CENTRALINA_LORAWAN_CMD_GET_TIMESTAMP	(0x42)

// reboot
// COMAND_CODE (0x03)
// size 1

#define CENTRALINA_LORAWAN_CMD_REBOOT			(0x03)

//*********************************************************************

// Set scheduler's packed slot
// COMAND_CODE(0x04) + PACKED_SLOT + crc8(PACKED_SLOT)
// PACKED_SLOT: BYTE[0..7] from BYTE[0]BIT[7] to BYTE[7]BIT[0] : MSB byte order
// slot number 				: 7 bits 	valid (0..63)	reserved (64..127)
// mode						: 3 bits 	valid (0..3)	reserved (4..7) 0 == disabled, 1 == exact day, 2 == week's day, 3 = day's period
// year (without century)	: 7 bits 	valid (0..99)	0 if mode == 2
// month					: 4 bits 	valid (1..12) 	0 if mode == 2
// month's day				: 5 bits 	valid (1..31) 	0 if mode == 2
// period (days units)		: 8 bits 	valid (1..255)	bit mask if mode == 2 (one bits for week's day bit0 == monday)
// start hour				: 5 bits 	valid (0..23)
// start minutes			: 6 bits 	valid (0..59)
// runtime(minutes)			: 11 bits 	valid (1..1440)
// out_mask					: 8 bits	valid (bit mask)


// example:
// slot number				: BYTE[0]BIT[7..1]
// mode						: BYTE[0]BIT[0] + BYTE[1]BIT[7..6]
// year						: BYTE[1]BIT[5..0] + BYTE[2]BIT[7]
// mont						: BYTE[2]BIT[6..3]
// day						: BYTE[2]BIT[2..0] + BYTE[3]BIT[7..6]
// period					: BYTE[3]BIT[5..0] + BYTE[4]BIT[7..6]
// start hour				: BYTE[4]BIT[5..1]
// start minute				: BYTE[4]BIT[0] + BYTE[5]BIT[7..3]
// runtime					: BYTE[5]BIT[2..0] + BYTE[6]BIT[7..0]
// out_mask					: BYTE[7]

// example numeric
// PROGRESSIVE MESSAGE ID	: 2
// COMAND_CODE				: 0x04
// slot number				: 1
// mode						: 1
// year						: 18
// mont						: 5
// day						: 23
// period					: 0
// start hour				: 11
// start minute				: 34
// runtime					: 60
// out_mask					: 3
// CRC8						: 0x63
//
// HEX PAYLOAD				: 0x02,0x04,0x02,0x49,0x2D,0xC0,0x16,0x58,0x3C,0x03,0x63
// base64 PAYLOAD			: AgQCSS3AFlg8A2M=
// MQTT STRING
// application/1/node/0000000500040124/tx
// {"reference" : "jdi9je333sl29", "confirmed" : true, "fPort" : 10, "data" : "AgQCSS3AFlg8A2M=" }
//



// size 1 + 8 + 1
#define CENTRALINA_LORAWAN_CMD_PROGRAM_PACKED		(0x04)





// read seduler's packed slot
// COMAND_CODE(0x45) + slot number
// size 1 + 1
#define CENTRALINA_LORAWAN_CMD_GET_PROGRAM_PACKED	(0x44)

// Set scheduler's slot
// COMAND_CODE(0x04) + SLOT + crc8(SLOT)
// SLOT: BYTE[0..10] : MSB byte order
// slot number 				: uint8_t 	valid (0..63)	reserved (64..127)
// mode						: uint8_t 	valid (0..3)	reserved (4..7) 0 == disabled, 1 == exact day, 2 == week's day, 3 = day's period
// year (without century)	: uint8_t 	valid (0..99)	0 if mode == 2
// month					: uint8_t 	valid (1..12) 	0 if mode == 2
// month's day				: uint8_t 	valid (1..31) 	0 if mode == 2
// period (days units)		: uint8_t 	valid (1..255)	bit mask if mode == 2 (one bits for week's day bit0 == monday)
// start hour				: uint8_t 	valid (0..23)
// start minutes			: uint8_t 	valid (0..59)
// runtime(minutes)			: uint16_t 	valid (1..1440)  (MSB byte order)
// out_mask					: uint8_t	valid (bit mask)


// example numeric
// PROGRESSIVE MESSAGE ID	: 2
// COMAND_CODE				: 0x05
// slot number				: 1
// mode						: 1
// year						: 18
// mont						: 5
// day						: 23
// period					: 0
// start hour				: 11
// start minute				: 34
// runtime					: 60
// out_mask					: 3
// CRC8						: NA = 0
//
// HEX PAYLOAD				: 0x02,0x04,0x01,0x01,0x12,0x05,0x17,0x00,0x0B,0x22,0x00,0x3C,0x03,0x00
// base64 PAYLOAD			: AgUBARIFFwALIgA8AwA=
// MQTT STRING
// application/1/node/0000000500040124/tx
// {"reference" : "jdi9je333sl29", "confirmed" : true, "fPort" : 10, "data" : "AgUBARIFFwALIgA8AwA=" }
//

// size 1 + 11 + 1
#define CENTRALINA_LORAWAN_CMD_PROGRAM				(0x05)

// read seduler's slot
// COMAND_CODE(0x45) + slot number
// size 1 + 1
#define CENTRALINA_LORAWAN_CMD_GET_PROGRAM			(0x45)

// erase scheduler's slot
// COMAND_CODE(0x06) + slot number
// size 1 + 1
#define CENTRALINA_LORAWAN_CMD_ERASE_PROGRAM		(0x06)

// enable/disable scheduler's slot
// COMAND_CODE(0x07) + slot number
// size 1 + 1 + 1

// slot number:
//	0..63 : select exact slot number
//	255 : select all slots

// example numeric
// PROGRESSIVE MESSAGE ID	: 2
// COMAND_CODE				: 0x07
// slot number				: 1
// active (0,1)				: 1

#define CENTRALINA_LORAWAN_CMD_UPDATE_PROGRAM		(0x07)

// drive specific out
// COMAND_CODE(0x11) + out number + timeout for close (minutes)
// size 1 + 1 + 1
#define CENTRALINA_LORAWAN_CMD_MANUAL_OUT		(0x11)
// example numeric
// PROGRESSIVE MESSAGE ID		: 2
// COMAND_CODE					: 0x11
// out number					: 1			(out num. 1)
// timeout for close (minutes)	: 10		(10 minutes)



// Control out status: force always on or alwais off
// COMAND_CODE(0x12) + out mask always on + out mask always off
// size 1 + 1 + 1
#define CENTRALINA_LORAWAN_CMD_FILTER_OUT		(0x12)
// example numeric
// PROGRESSIVE MESSAGE ID		: 2
// COMAND_CODE					: 0x12
// out mask always on			: 0x03		(out num. 1 , 2 always ON)
// out mask always off			: 0x0C		(out num. 3 , 4 always OFF)


#define CENTRALINA_LORAWAN_CMD_CONFIG_ALL				(0x20)
#define CENTRALINA_LORAWAN_CMD_CONFIG_COMUNICATION		(0x21)
#define CENTRALINA_LORAWAN_CMD_CONFIG_BATTERY			(0x22)
#define CENTRALINA_LORAWAN_CMD_CONFIG_OUT_DRIVE			(0x23)
#define CENTRALINA_LORAWAN_CMD_CONFIG_COUNTER			(0x26)
#define CENTRALINA_LORAWAN_CMD_CONFIG_INPUT_0			(0x28)
#define CENTRALINA_LORAWAN_CMD_CONFIG_INPUT_1			(0x29)
#define CENTRALINA_LORAWAN_CMD_CONFIG_UINT8_PAIR		(0x30)
#define CENTRALINA_LORAWAN_CMD_CONFIG_UINT16_PAIR		(0x31)



// CMD_RET + DATA UPLINK
// PROGRESSIVE MESSAGE ID(same ad command) + RETURN_COMMAND_1 + RETURN_COMMAND_2 + ... + RETURN_COMMAND_END
// RETURN_CMD FORMAT:
// RETURN_COMAND_CODE + RESULT + related return payload  (RESULT == 0 ==> SUCCESS)
// RETURN_COMAND_CODE = COMAND_CODE | 0x80
// RESULT = error code, see below


#define CENTRALINA_LORAWAN_PORT_CMD_RET_DATA		(11)
#define CENTRALINA_LORAWAN_PORT_CONFIGRET_DATA		(21)

// return value posted in uplink payload
// return code = relative command code masked in or with 0x80
#define CENTRALINA_LORAWAN_RET_MASK				(0x80)

// Set board datetime return
// COMAND_RET_CODE(0x81) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_DATETIME			(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_DATETIME)

// Get board datetime return
// COMAND_RET_CODE(0xC1) + RESULT + YY_MM_DD_WD_HH_MM (6 BYTES)
// size 1 + 6 	Year(0..99)_Month(1..12)_Day(1..31)_WeekWay(1..7 ; 1 = Monday)_Hour(0..23)_Minutes(0..59)
#define CENTRALINA_LORAWAN_RET_GET_BOARDTIME	(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_GET_BOARDTIME)

// Get board datetime return
// COMAND_RET_CODE(0xC1) + RESULT + TIMESTAMP (7 BYTES)
// size 1 + 7
#define CENTRALINA_LORAWAN_RET_GET_TIMESTAMP	(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_GET_TIMESTAMP)


// Set scheduler's packed slot return
// COMAND_RET_CODE(0x84) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_PROGRAM_PACKED			(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_PROGRAM_PACKED)

// Get scheduler's packed slot return
// COMAND_RET_CODE(0xC4) + RESULT + PACKED_SLOT + crc8(PACKED_SLOT)
// PACKED_SLOT: same as for CENTRALINA_LORAWAN_CMD_PROGRAM
// crc8(PACKED_SLOT): same as for CENTRALINA_LORAWAN_CMD_PROGRAM
// size 1 + 1 + 8 + 1
#define CENTRALINA_LORAWAN_RET_GET_PROGRAM_PACKED		(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_GET_PROGRAM_PACKED)

// Set scheduler's packed slot return
// COMAND_RET_CODE(0x84) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_PROGRAM				(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_PROGRAM)

// Get scheduler's slot return
// COMAND_RET_CODE(0xC4) + RESULT + PACKED_SLOT + crc8(PACKED_SLOT)
// PACKED_SLOT: same as for CENTRALINA_LORAWAN_CMD_PROGRAM
// crc8(PACKED_SLOT): same as for CENTRALINA_LORAWAN_CMD_PROGRAM
// size 1 + 1 + 8 + 1
#define CENTRALINA_LORAWAN_RET_GET_PROGRAM			(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_GET_PROGRAM)

// Erase scheduler's slot return
// COMAND_RET_CODE(0x86) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_ERASE_PROGRAM	(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_ERASE_PROGRAM)

// Erase scheduler's slot return
// COMAND_RET_CODE(0x87) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_UPDATE_PROGRAM	(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_UPDATE_PROGRAM)

// drive specific out return
// COMAND_RET_CODE(0x91) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_MANUAL_OUT		(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_MANUAL_OUT)

// drive specific out return
// COMAND_RET_CODE(0x92) + RESULT
// size 1 + 1 + 0
#define CENTRALINA_LORAWAN_RET_FILTER_OUT		(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_FILTER_OUT)


#define CENTRALINA_LORAWAN_RET_CONFIG_ALL		(CENTRALINA_LORAWAN_RET_MASK | CENTRALINA_LORAWAN_CMD_CONFIG_ALL)



#define CENTRALINA_LORAWAN_PORT_INFOS		(12)

#define CENTRALINA_LORAWAN_INFOS_STATUS_SYS		(1) // size 1 + 2
#define CENTRALINA_LORAWAN_INFOS_STATUS_OUTS	(2) // size 1 + 2
#define CENTRALINA_LORAWAN_INFOS_CHECK_SCHED	(3) // size 1 + 2
#define CENTRALINA_LORAWAN_INFOS_STATUS_INS		(4) // size 1 + 1
#define CENTRALINA_LORAWAN_INFOS_STATUS_TIME	(5) // size 1 + ?
#define CENTRALINA_LORAWAN_INFOS_VALUE_IN1		(11) // size 1 + 2
#define CENTRALINA_LORAWAN_INFOS_VALUE_IN2		(12) // size 1 + 2
#define CENTRALINA_LORAWAN_INFOS_VALUE_CNTR		(20) // size 1 + 2
#define CENTRALINA_LORAWAN_INFOS_ALL			(100) // size 1 + 2


#endif /* TASKS_LORA_PAYLOAD_H_ */
