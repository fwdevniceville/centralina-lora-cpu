/*
 * taskComunicationRecv.c
 *
 *  Created on: 05 giu 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include <string.h>
#include "cmsis_os.h"
#include "hal_inc.h"
#include "../menu-loc/menu_vars.h"
#include "../menu-loc/menu_events_status.h"
#include "../Utils/bitpacker.h"
#include "../FeeRTOS/FRT_BoardFunctions.h"
#include "../Storage/configurations.h"

#include "../Tasks/taskIntCom.h"
#include "../Tasks/taskScheduler.h"
#include "../Tasks/taskComunication.h"
#include "../Tasks/taskSupervisor.h"

#include "../Utils/crc8.h"
#include "../SystemFunctions.h"
#include "lora_payload.h"


void taskComunicationPushRetData(TASKCOMUNICATION_UPLINK* pUPLINK_RET_CMD,uint8_t uiRCode,uint8_t uiRRes,uint8_t* auiRetData,uint8_t uiRetDataSize, bool bDataSizeEncoded);
void taskComunicationPushRetData(TASKCOMUNICATION_UPLINK* pUPLINK_RET_CMD,uint8_t uiRCode,uint8_t uiRRes,uint8_t* auiRetData,uint8_t uiRetDataSize, bool bDataSizeEncoded)
{
	int iDiffSpace;
	int	iHdrSize;
	iDiffSpace = TASKCOMUNICATION_UPLINK_PAYLOAD_MAX - pUPLINK_RET_CMD->uiSize;
	iHdrSize = 2;
	if (bDataSizeEncoded)
	{
		iHdrSize = 3;
	}
	if (iDiffSpace >= iHdrSize)
	{
		uint8_t*	puiCurrBuffPos;
		puiCurrBuffPos = &pUPLINK_RET_CMD->auiPayload[pUPLINK_RET_CMD->uiSize];
		puiCurrBuffPos[0] = uiRCode | CENTRALINA_LORAWAN_RET_MASK;
		puiCurrBuffPos[1] = CENTRALINA_LORAWAN_RET_UPLINK_TOO_LONG;
		if (bDataSizeEncoded)
		{
			puiCurrBuffPos[2] = uiRetDataSize;
		}
		if (iDiffSpace >= (iHdrSize + uiRetDataSize))
		{
			puiCurrBuffPos[1] = uiRRes;
			memcpy(&puiCurrBuffPos[iHdrSize] , auiRetData,uiRetDataSize);
		}
		pUPLINK_RET_CMD->uiSize += (iHdrSize + uiRetDataSize);
	}
}

bool taskComunicationRecvLoRaDownlink_USER(const LoraRxInfos* pLoraRxInfos, unsigned uiPort,bool bConfirmNeeded,unsigned uiSize,const uint8_t* abyBuffer);

void taskComunicationRecvLoRaDownlink(const LoraRxInfos* pLoraRxInfos, unsigned uiPort,bool bConfirmNeeded,unsigned uiSize,const uint8_t* abyBuffer)
{
	bool	bUplinkNeeded;
	bUplinkNeeded = false;
	int iSNR;

	stTASKCOMUNICATION_CTX.tTimefromLastDownLink = 0;
	pLoraRxInfos->uiDownLinkCounter;
	pLoraRxInfos->Rssi;
	pLoraRxInfos->Snr;
	pLoraRxInfos->bFramePending;

	iSNR = (((int)pLoraRxInfos->Snr) * 10) / 4; // il valore arriva dai registri del modem e va diviso per 4;
	menuStatusSetLinkStatus(MENSTS_LORA_LINK_OK,pLoraRxInfos->Rssi,iSNR);

	if ((uiPort > 0) && (uiPort < 224))
	{
		bUplinkNeeded = taskComunicationRecvLoRaDownlink_USER(pLoraRxInfos, uiPort, bConfirmNeeded, uiSize,  abyBuffer);
		stTASKCOMUNICATION_CTX.bRECV_USER_DOWNLINK = true;
	}
	else
	{
		stTASKCOMUNICATION_CTX.bRECV_SYSTEM_DOWNLINK = true;
	}

	if (bUplinkNeeded || pLoraRxInfos->bFramePending)
	{
		stTASKCOMUNICATION_CTX.iDownLinkPendingUpTries =  0;
		if (pLoraRxInfos->bFramePending)
		{
			stTASKCOMUNICATION_CTX.iDownLinkPendingUpTries =  3;
		}
		taskComunicationSignalUpLinkRequest(false);
	}
}

void taskComunicationRecvLoRaDownlink_USER_CMD(TASKCOMUNICATION_UPLINK* pUPLINK_RET_CMD,unsigned uiSize,const uint8_t* abyBuffer);
void taskComunicationRecvLoRaDownlink_USER_CONFIG(TASKCOMUNICATION_UPLINK* pUPLINK_RET_CMD,unsigned uiSize,const uint8_t* abyBuffer);

bool taskComunicationRecvLoRaDownlink_USER(const LoraRxInfos* pLoraRxInfos, unsigned uiPort,bool bConfirmNeeded,unsigned uiSize,const uint8_t* abyBuffer)
{
	TASKCOMUNICATION_UPLINK*	pUPLINK_RET_CMD;
	uint8_t						uiRET_PORT;

	pUPLINK_RET_CMD = &stTASKCOMUNICATION_CTX.stUPLINK_RET_CMD;
	pUPLINK_RET_CMD->uiPort = 0;
	pUPLINK_RET_CMD->uiSize = 0;


	switch (uiPort)
	{
	case CENTRALINA_LORAWAN_PORT_CMD:
		{
			taskComunicationRecvLoRaDownlink_USER_CMD(pUPLINK_RET_CMD,uiSize,abyBuffer);
			uiRET_PORT = CENTRALINA_LORAWAN_PORT_CMD_RET_DATA;
		}
		break;
	case CENTRALINA_LORAWAN_PORT_CONFIG:
		{
			taskComunicationRecvLoRaDownlink_USER_CONFIG(pUPLINK_RET_CMD,uiSize,abyBuffer);
			uiRET_PORT = CENTRALINA_LORAWAN_PORT_CONFIGRET_DATA;
		}
		break;
	}
	if (pUPLINK_RET_CMD->uiSize > 0)
	{
		pUPLINK_RET_CMD->uiPort = uiRET_PORT;
		return true;
	}
	return false;
}

void taskComunicationRecvLoRaDownlink_USER_CMD(TASKCOMUNICATION_UPLINK* pUPLINK_RET_CMD,unsigned uiSize,const uint8_t* abyBuffer)
{
	BITPACKER					stBP;
	CFG_SCHEDULER_ROW			stROW;
	unsigned					uiProgrRequest;
	unsigned					uiSCNR;
	unsigned					uiPSZ;
	int							iResponseBufferSize;
	unsigned					uiTMP;
	const uint8_t*				aBLOCK;
	uint8_t*					auiResponseBuffer;
	uint8_t						uiResult;
	uiProgrRequest = abyBuffer[0];
	auiResponseBuffer = &stTASKCOMUNICATION_CTX.auiSVCResponseBuffer[0];

	uiSCNR = 0;
	pUPLINK_RET_CMD->uiSize = 1;
	pUPLINK_RET_CMD->auiPayload[0] = uiProgrRequest;
	uiSCNR = 1;
	while (uiSCNR < uiSize)
	{
		SYS_DATETIME_SET_GET	stDATETIME;
		uiPSZ = uiSize - uiSCNR;
		uiResult = CENTRALINA_LORAWAN_INVALID_DATA;
		iResponseBufferSize = -1;

		aBLOCK = &abyBuffer[uiSCNR];

		switch (aBLOCK[0])
		{
		case CENTRALINA_LORAWAN_CMD_DATETIME:
			{
				if (uiPSZ >= 7)
				{
					iResponseBufferSize = 0;

					stDATETIME.Year 	= aBLOCK[1];
					stDATETIME.Month 	= aBLOCK[2];
					stDATETIME.MDay 	= aBLOCK[3];
					stDATETIME.WDay 	= aBLOCK[4];
					stDATETIME.Hour 	= aBLOCK[5];
					stDATETIME.Minutes 	= aBLOCK[6];

					if (systemDateTimeSet(&stDATETIME))
					{
						uiResult = CENTRALINA_LORAWAN_RESULT_OK;
					}

					uiPSZ = 7;
				}

			}
			break;
		case CENTRALINA_LORAWAN_CMD_GET_BOARDTIME:
			{
				if (uiPSZ >= 1)
				{
					iResponseBufferSize = 6;

					systemDateTimeGet(&stDATETIME);
					auiResponseBuffer[0] = stDATETIME.Year;
					auiResponseBuffer[1] = stDATETIME.Month;
					auiResponseBuffer[2] = stDATETIME.MDay;
					auiResponseBuffer[3] = stDATETIME.WDay;
					auiResponseBuffer[4] = stDATETIME.Hour;
					auiResponseBuffer[5] = stDATETIME.Minutes;

					uiResult = CENTRALINA_LORAWAN_RESULT_OK;


					uiPSZ = 1;

				}
			}
			break;
		case CENTRALINA_LORAWAN_CMD_GET_TIMESTAMP:
			{
				if (uiPSZ >= 1)
				{
					uint64_t	uiSTAMP_LINUX;
					uiPSZ = 1;
					iResponseBufferSize = 8;


					uiSTAMP_LINUX = systemGetCurrentDateTime(NULL);

					uiSTAMP_LINUX+= 946684800;

					auiResponseBuffer[0] = uiSTAMP_LINUX >> 56;
					auiResponseBuffer[1] = uiSTAMP_LINUX >> 48;
					auiResponseBuffer[2] = uiSTAMP_LINUX >> 40;
					auiResponseBuffer[3] = uiSTAMP_LINUX >> 32;
					auiResponseBuffer[4] = uiSTAMP_LINUX >> 24;
					auiResponseBuffer[5] = uiSTAMP_LINUX >> 16;
					auiResponseBuffer[6] = uiSTAMP_LINUX >>  8;
					auiResponseBuffer[7] = uiSTAMP_LINUX >>  0;

					uiResult = CENTRALINA_LORAWAN_RESULT_OK;


				}
			}
			break;
		case CENTRALINA_LORAWAN_CMD_REBOOT:
			{
				if (uiPSZ >= 1)
				{
					uiPSZ = 1;
					taskSupervisorReboot();
					uiResult = CENTRALINA_LORAWAN_RESULT_OK;
					iResponseBufferSize = 0;
				}
			}
			break;
		case CENTRALINA_LORAWAN_CMD_PROGRAM_PACKED:
			if (uiPSZ >= 10)
			{
				iResponseBufferSize = 0;
//						uiTMP = calcCRC8(0xFF,&aBLOCK[1],8);// crc8
//						if (uiTMP == aBLOCK[9])
				{
					memset(&stROW,0,sizeof(stROW));

					bitpackerInitPop(&stBP,&aBLOCK[1],8);
					bitpackerPopUIntValue(&stBP,&uiTMP, 7); 	// uiSchedulazione
					stROW.uiORD 			= uiTMP;
					bitpackerPopUIntValue(&stBP,&uiTMP, 3); 			// uiModo
					stROW.uiTYPE 			= uiTMP;
					bitpackerPopUIntValue(&stBP,&uiTMP, 7);			// uiAnno
					stROW.uiStartYear 		= uiTMP;
					bitpackerPopUIntValue(&stBP,&uiTMP, 4);			// uiMese
					stROW.uiStartMonth 		= uiTMP;
					bitpackerPopUIntValue(&stBP,&uiTMP, 5);			// uiGiorno
					stROW.uiStartMonthDay 	= uiTMP;
					bitpackerPopUIntValue(&stBP,&uiTMP, 8);			// uiPeriodo
					stROW.uiPeriodDay 		= uiTMP;
					bitpackerPopUIntValue(&stBP,&uiTMP, 5);		// uiStartOra
					stROW.uiStart_hour 		= uiTMP;
					bitpackerPopUIntValue(&stBP,&uiTMP, 6);		// uiStartMinuti
					stROW.uiStart_min 		= uiTMP;
					bitpackerPopUIntValue(&stBP,&uiTMP, 11);	// uiDurataMinuti
					stROW.uiRuntime_min 	=uiTMP;
					bitpackerPopUIntValue(&stBP,&uiTMP, 8);	// uiValvoleAccese
					stROW.uiOUT_MASK 		=uiTMP;
					stROW.uiCRC8_MSG 		=aBLOCK[9];

					configAdjustNewSchedule(&stROW);

					if (configSaveNewSchedule(&stROW))
					{
						uiResult = CENTRALINA_LORAWAN_RESULT_OK;
						stTASKCOMUNICATION_CTX.iSendScheduleInfo		= stROW.uiORD;
						stTASKCOMUNICATION_CTX.iSendScheduleInfoProgr 	= -1;
					}

				}
				uiPSZ = 10;
			}
			break;
		case CENTRALINA_LORAWAN_CMD_PROGRAM:
			if (uiPSZ >= 13)
			{
				iResponseBufferSize = 0;
//						uiTMP = calcCRC8(0xFF,&aBLOCK[1],11);// crc8
//						if (uiTMP == aBLOCK[12])
				{
					memset(&stROW,0,sizeof(stROW));
					stROW.uiORD 			= aBLOCK[1];
					stROW.uiTYPE 			= aBLOCK[2];
					stROW.uiStartYear 		= aBLOCK[3] & CFG_SCHEDULER_ROW_START_YEAR_MASK;
					stROW.uiStartMonth 		= aBLOCK[4] & CFG_SCHEDULER_ROW_START_MONTH_MASK;
					stROW.uiStartMonthDay 	= aBLOCK[5] & CFG_SCHEDULER_ROW_START_MONTH_DAY_MASK;
					stROW.uiPeriodDay 		= aBLOCK[6];
					stROW.uiStart_hour 		= aBLOCK[7];
					stROW.uiStart_min 		= aBLOCK[8];
					stROW.uiRuntime_min 	= ((uint16_t)aBLOCK[9] << 8) | aBLOCK[10];
					stROW.uiOUT_MASK 		= aBLOCK[11];
					stROW.uiCRC8_MSG 		= aBLOCK[12];

					configAdjustNewSchedule(&stROW);

					if (configSaveNewSchedule(&stROW))
					{
						uiResult = CENTRALINA_LORAWAN_RESULT_OK;
						stTASKCOMUNICATION_CTX.iSendScheduleInfo		= stROW.uiORD;
						stTASKCOMUNICATION_CTX.iSendScheduleInfoProgr 	= -1;
					}

				}
				uiPSZ = 13;
			}
			break;
		case CENTRALINA_LORAWAN_CMD_ERASE_PROGRAM:
			{
				if (uiPSZ >= 2)
				{
					iResponseBufferSize = 0;
					if (configEraseSchedule(aBLOCK[1]))
					{
						uiResult = CENTRALINA_LORAWAN_RESULT_OK;
					}

				}
			}
			break;
		case CENTRALINA_LORAWAN_CMD_GET_PROGRAM:
		case CENTRALINA_LORAWAN_CMD_GET_PROGRAM_PACKED:
			{
				if (uiPSZ >= 2)
				{
					iResponseBufferSize = 0;
					if (configGetSchedule(aBLOCK[1],&stROW))
					{
						switch (aBLOCK[0])
						{
						case CENTRALINA_LORAWAN_CMD_GET_PROGRAM:
							{
//										auiResponseBuffer[0]		= stROW.uiORD;
								auiResponseBuffer[0]		= aBLOCK[1]; // Meglio, altrimenti tiriamo su anche porcheria
								auiResponseBuffer[1]		= stROW.uiTYPE;
								auiResponseBuffer[2]		= stROW.uiStartYear;
								auiResponseBuffer[3]		= stROW.uiStartMonth;
								auiResponseBuffer[4]		= stROW.uiStartMonthDay;
								auiResponseBuffer[5]		= stROW.uiPeriodDay;
								auiResponseBuffer[6]		= stROW.uiStart_hour;
								auiResponseBuffer[7]		= stROW.uiStart_min;
								auiResponseBuffer[8]		= stROW.uiRuntime_min >> 8;
								auiResponseBuffer[9]		= stROW.uiRuntime_min;
								auiResponseBuffer[10]	= stROW.uiOUT_MASK;
								auiResponseBuffer[11]	= stROW.uiCRC8_MSG;
								iResponseBufferSize = 12;
							}
							break;
						default:
						case CENTRALINA_LORAWAN_CMD_GET_PROGRAM_PACKED:
							{
								bitpackerInitPush(&stBP,&auiResponseBuffer[0],9);
								bitpackerPushUIntValue(&stBP,stROW.uiORD					,7);
								bitpackerPushUIntValue(&stBP,stROW.uiTYPE					,3);
								bitpackerPushUIntValue(&stBP, (stROW.uiStartYear 		& CFG_SCHEDULER_ROW_START_YEAR_AUTO_MASK) 		? 0x00 : (stROW.uiStartYear 	& CFG_SCHEDULER_ROW_START_YEAR_MASK)		,7);
								bitpackerPushUIntValue(&stBP, (stROW.uiStartMonth 		& CFG_SCHEDULER_ROW_START_MONTH_AUTO_MASK) 		? 0x00 : (stROW.uiStartMonth	& CFG_SCHEDULER_ROW_START_MONTH_MASK)		,4);
								bitpackerPushUIntValue(&stBP, (stROW.uiStartMonthDay 	& CFG_SCHEDULER_ROW_START_MONTH_DAY_AUTO_MASK) 	? 0x00 : (stROW.uiStartMonthDay & CFG_SCHEDULER_ROW_START_MONTH_DAY_MASK)	,5);
								bitpackerPushUIntValue(&stBP,stROW.uiPeriodDay				,8);
								bitpackerPushUIntValue(&stBP,stROW.uiStart_hour				,5);
								bitpackerPushUIntValue(&stBP,stROW.uiStart_min				,6);
								bitpackerPushUIntValue(&stBP,stROW.uiRuntime_min			,11);
								bitpackerPushUIntValue(&stBP,stROW.uiOUT_MASK				,8);
								bitpackerPushUIntValue(&stBP,stROW.uiCRC8_MSG				,8);
								iResponseBufferSize = 9;
							}
							break;
						}
						uiResult = CENTRALINA_LORAWAN_RESULT_OK;
					}
					uiPSZ = 2;
				}

			}
			break;

		case CENTRALINA_LORAWAN_CMD_UPDATE_PROGRAM:
			{
				if (uiPSZ >= 3)
				{
					iResponseBufferSize = 0;
					if (configEnableSchedule(aBLOCK[1],aBLOCK[2]))
					{
						uiResult = CENTRALINA_LORAWAN_RESULT_OK;
					}
				}
			}
			break;

		case CENTRALINA_LORAWAN_CMD_MANUAL_OUT:
			{
				if (uiPSZ >= 3)
				{
					int iVNUM;
					int iMINOPEN;
					iResponseBufferSize = 0;

					iVNUM 		= aBLOCK[1];
					iMINOPEN 	= aBLOCK[2];
					if (taskSchedulerOpenValve(iVNUM , iMINOPEN))
					{
						uiResult = CENTRALINA_LORAWAN_RESULT_OK;
					}
					uiPSZ = 3;
				}
			}

			break;

		case CENTRALINA_LORAWAN_CMD_FILTER_OUT:
			{
				if (uiPSZ >= 3)
				{
					iResponseBufferSize = 0;

					stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateAperte 	= aBLOCK[1];
					stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateChiuse 	= aBLOCK[2];

					uiResult = CENTRALINA_LORAWAN_RESULT_OK;

					uiPSZ = 3;
				}
			}
			break;
		}

		if (iResponseBufferSize >= 0)
		{
			taskComunicationPushRetData(pUPLINK_RET_CMD,aBLOCK[0],uiResult,&auiResponseBuffer[0],iResponseBufferSize,false);
		}

		uiSCNR+=uiPSZ;
	}
}

#include "parammaps/parammaps.h"
#include "config_maps.h"
PARAMSMAPDEFINE_EXTERN(CFG_SYSTEM_PARAMETERS);

uint8_t taskComunicationRecvSysParamsComunication(CFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS,const uint8_t* pbyBuffer,int iBuffersize);
uint8_t taskComunicationRecvSysParamsBattery(CFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS,const uint8_t* pbyBuffer,int iBuffersize);
uint8_t taskComunicationRecvSysParamsOutDriver(CFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS,const uint8_t* pbyBuffer,int iBuffersize);
uint8_t taskComunicationRecvSysParamsInput(CFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS,int iNumber,const uint8_t* pbyBuffer,int iBuffersize);
uint8_t taskComunicationRecvSysParamsCounter(CFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS,const uint8_t* pbyBuffer,int iBuffersize);

void taskComunicationRecvLoRaDownlink_USER_CONFIG(TASKCOMUNICATION_UPLINK* pUPLINK_RET_CMD,unsigned uiSize,const uint8_t* abyBuffer)
{
	CFG_SYSTEM_PARAMETERS		stCFG_SYSTEM_PARAMETERS;
	CFG_SYSTEM_PARAMETERS		stCFG_SYSTEM_PARAMETERS_CPY;
	unsigned					uiProgrRequest;
	unsigned					uiSCNR;
	unsigned					uiPSZ;
	int							iResponseBufferSize;
	const uint8_t*				aBLOCK;
	const UCFG_GEN_SLOT_DATA*	puDATA;
	uint8_t*					auiResponseBuffer;
	uint8_t*					auiRecvBlockBuffer;
	uint8_t						uiSizeConfigBlock;
	uint8_t						uiResult;
	uint8_t						uiConfigStatus;
	bool						bParamsSysModified;

	bParamsSysModified = false;

	puDATA = configConfigGetRO(CONFIG_SLOT_SYSTEM_PARAMETERS);

	stCFG_SYSTEM_PARAMETERS 	= puDATA->stSYSTEM_PARAMETERS;
	stCFG_SYSTEM_PARAMETERS_CPY = puDATA->stSYSTEM_PARAMETERS;

	uiProgrRequest = abyBuffer[0];
	auiResponseBuffer = &stTASKCOMUNICATION_CTX.auiSVCResponseBuffer[0];
	auiRecvBlockBuffer = &stTASKCOMUNICATION_CTX.auiRecvBlockBuffer[0];
	uiSCNR = 0;
	pUPLINK_RET_CMD->uiSize = 1;
	pUPLINK_RET_CMD->auiPayload[0] = uiProgrRequest;
	uiSCNR = 1;

	while (uiSCNR < uiSize)
	{
		uiPSZ = uiSize - uiSCNR;
		uiResult = CENTRALINA_LORAWAN_INVALID_DATA;
		iResponseBufferSize = -1;

		aBLOCK = &abyBuffer[uiSCNR];

		uiSizeConfigBlock = aBLOCK[1];
		if (uiPSZ >= 2)
		{
			if (uiPSZ >= (2 + uiSizeConfigBlock))
			{
				uiPSZ = (2 + uiSizeConfigBlock);
				if (uiSizeConfigBlock > sizeof(stTASKCOMUNICATION_CTX.auiRecvBlockBuffer))
				{
					uiSizeConfigBlock = sizeof(stTASKCOMUNICATION_CTX.auiRecvBlockBuffer);
				}

				uiConfigStatus = 0xFF;
				memset(&stTASKCOMUNICATION_CTX.auiRecvBlockBuffer[0],0,sizeof(stTASKCOMUNICATION_CTX.auiRecvBlockBuffer));
				memcpy(&stTASKCOMUNICATION_CTX.auiRecvBlockBuffer[0],&aBLOCK[2],uiSizeConfigBlock);

				switch (aBLOCK[0])
				{
				case CENTRALINA_LORAWAN_CMD_CONFIG_ALL:
					{

						iResponseBufferSize = 0;

						// comunication 7 bytes
						uiConfigStatus = taskComunicationRecvSysParamsComunication(&stCFG_SYSTEM_PARAMETERS, &auiRecvBlockBuffer[0],7);

						if (uiConfigStatus == CENTRALINA_LORAWAN_RESULT_OK)
						{
							// battery check 2 bytes
							uiConfigStatus = taskComunicationRecvSysParamsBattery(&stCFG_SYSTEM_PARAMETERS, &auiRecvBlockBuffer[7],2);
						}

						if (uiConfigStatus == CENTRALINA_LORAWAN_RESULT_OK)
						{
							// out drive parameters 5 bytes
							uiConfigStatus = taskComunicationRecvSysParamsOutDriver(&stCFG_SYSTEM_PARAMETERS, &auiRecvBlockBuffer[7 + 2],5);
						}

						if (uiConfigStatus == CENTRALINA_LORAWAN_RESULT_OK)
						{
							// input 1 parameters 8 bytes
							uiConfigStatus = taskComunicationRecvSysParamsInput(&stCFG_SYSTEM_PARAMETERS, 0,&auiRecvBlockBuffer[7 + 2 + 5],8);
						}

						if (uiConfigStatus == CENTRALINA_LORAWAN_RESULT_OK)
						{
							// input 2 parameters 8 bytes
							uiConfigStatus = taskComunicationRecvSysParamsInput(&stCFG_SYSTEM_PARAMETERS, 1,&auiRecvBlockBuffer[7 + 2 + 5 + 8],8);
						}

						if (uiConfigStatus == CENTRALINA_LORAWAN_RESULT_OK)
						{
							// counter 1 parameters 2 bytes
							uiConfigStatus = taskComunicationRecvSysParamsCounter(&stCFG_SYSTEM_PARAMETERS, &auiRecvBlockBuffer[7 + 2 + 5 + 8 + 8],2);
						}

						uiResult = uiConfigStatus;
					}

					break;
				case CENTRALINA_LORAWAN_CMD_CONFIG_COMUNICATION:
					{
						iResponseBufferSize = 0;
						// comunication 7 bytes
						uiConfigStatus = taskComunicationRecvSysParamsComunication(&stCFG_SYSTEM_PARAMETERS, &auiRecvBlockBuffer[0],uiSizeConfigBlock);
						uiResult = uiConfigStatus;
					}

					break;
				case CENTRALINA_LORAWAN_CMD_CONFIG_BATTERY:
					{
						iResponseBufferSize = 0;
						// battery check 2 bytes
						uiConfigStatus = taskComunicationRecvSysParamsBattery(&stCFG_SYSTEM_PARAMETERS, &auiRecvBlockBuffer[0],uiSizeConfigBlock);
						uiResult = uiConfigStatus;
					}

					break;
				case CENTRALINA_LORAWAN_CMD_CONFIG_OUT_DRIVE:
					{
						iResponseBufferSize = 0;
						// out drive parameters 5 bytes
						uiConfigStatus = taskComunicationRecvSysParamsOutDriver(&stCFG_SYSTEM_PARAMETERS, &auiRecvBlockBuffer[0],uiSizeConfigBlock);
						uiResult = uiConfigStatus;
					}

					break;
				case CENTRALINA_LORAWAN_CMD_CONFIG_COUNTER:
					{
						iResponseBufferSize = 0;
						// counter 1 parameters 2 bytes
						uiConfigStatus = taskComunicationRecvSysParamsCounter(&stCFG_SYSTEM_PARAMETERS, &auiRecvBlockBuffer[0],uiSizeConfigBlock);
						uiResult = uiConfigStatus;
					}

					break;
				case CENTRALINA_LORAWAN_CMD_CONFIG_INPUT_0:
					{
						iResponseBufferSize = 0;
						// input 1 parameters 8 bytes
						uiConfigStatus = taskComunicationRecvSysParamsInput(&stCFG_SYSTEM_PARAMETERS, 0,&auiRecvBlockBuffer[0],uiSizeConfigBlock);
						uiResult = uiConfigStatus;
					}

					break;
				case CENTRALINA_LORAWAN_CMD_CONFIG_INPUT_1:
					{
						iResponseBufferSize = 0;
						// input 2 parameters 8 bytes
						uiConfigStatus = taskComunicationRecvSysParamsInput(&stCFG_SYSTEM_PARAMETERS, 1,&auiRecvBlockBuffer[0],uiSizeConfigBlock);
						uiResult = uiConfigStatus;
					}

					break;
				case CENTRALINA_LORAWAN_CMD_CONFIG_UINT8_PAIR:
					{
						iResponseBufferSize = 0;
						if ((uiSizeConfigBlock % 2) == 0)
						{
							unsigned uiParamsScanner;
							int		 iVAL;
							uiResult = CENTRALINA_LORAWAN_RESULT_OK;
							uiParamsScanner = 0;
							while (uiParamsScanner < uiSizeConfigBlock)
							{
								iVAL = auiRecvBlockBuffer[uiParamsScanner + 1];
								uiConfigStatus = parammapAccessItem(
										auiRecvBlockBuffer[uiParamsScanner + 0],
										&iVAL,
										true,
										true,
										(uint8_t*)&stCFG_SYSTEM_PARAMETERS,
										PARAMSMAPGET_PTR(CFG_SYSTEM_PARAMETERS));
								if (uiConfigStatus != PARAMMAP_RETVAL_SUCCESS)
								{
									uiResult = CENTRALINA_LORAWAN_INVALID_DATA;
									break;
								}
								uiParamsScanner+=2;
							}
						}
					}

					break;
				case CENTRALINA_LORAWAN_CMD_CONFIG_UINT16_PAIR:
					{
						iResponseBufferSize = 0;
						if ((uiSizeConfigBlock % 4) == 0)
						{
							unsigned uiParamsScanner;
							uiResult = CENTRALINA_LORAWAN_RESULT_OK;
							uiParamsScanner = 0;
							while (uiParamsScanner < uiSizeConfigBlock)
							{
								uint16_t	uiID;
								int			iVAL;
								uiID = auiRecvBlockBuffer[uiParamsScanner++];
								uiID <<= 8;
								uiID |= auiRecvBlockBuffer[uiParamsScanner++];

								iVAL = auiRecvBlockBuffer[uiParamsScanner++];
								iVAL <<= 8;
								iVAL |= auiRecvBlockBuffer[uiParamsScanner++];
								uiConfigStatus = parammapAccessItem(
										uiID,
										&iVAL,
										false,
										true,
										(uint8_t*)&stCFG_SYSTEM_PARAMETERS,
										PARAMSMAPGET_PTR(CFG_SYSTEM_PARAMETERS));
								if (uiConfigStatus != PARAMMAP_RETVAL_SUCCESS)
								{
									uiResult = CENTRALINA_LORAWAN_INVALID_DATA;
									break;
								}
							}
						}
					}

					break;
				}

				if (uiConfigStatus == 0)
				{
					if (memcmp(&stCFG_SYSTEM_PARAMETERS_CPY,&stCFG_SYSTEM_PARAMETERS,sizeof(stCFG_SYSTEM_PARAMETERS_CPY)) != 0)
					{
						bParamsSysModified = true;
					}
					stCFG_SYSTEM_PARAMETERS_CPY = stCFG_SYSTEM_PARAMETERS;
				}
				else
				{
					stCFG_SYSTEM_PARAMETERS 	= stCFG_SYSTEM_PARAMETERS_CPY;
				}

			}
		}


		if (iResponseBufferSize >= 0)
		{
			taskComunicationPushRetData(pUPLINK_RET_CMD,aBLOCK[0],uiResult,&auiResponseBuffer[0],iResponseBufferSize,true);
		}

		uiSCNR+=uiPSZ;
	}
	if (bParamsSysModified)
	{
		taskSupervisorSaveSysParamsCfg(&stCFG_SYSTEM_PARAMETERS);
	}

}

#define RECVSYSPARAM_TYPE_UINT8			(0x1)
#define RECVSYSPARAM_TYPE_UINT16		(0x2)
#define RECVSYSPARAM_TYPE_NO_ADAPT		(0x80)
uint8_t taskComunicationRecvSysParamsByID(CFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS,const uint8_t* pbyBuffer,int iBuffersize,unsigned uiTypeIn, unsigned uiIDFrom,unsigned uiParamsNum)
{
	unsigned 	uiPARAMS_TO;
	int			iValueIn;
	int			iValueInSize;
	int			iValueInSizeS;
	uint8_t		uiRESULT;

	iValueInSize = uiTypeIn & 0x0F;
	iValueIn = 0;
	iValueInSizeS = 0;
	uiPARAMS_TO = uiIDFrom + uiParamsNum;
	while (uiIDFrom < uiPARAMS_TO)
	{
		if (iBuffersize > 0)
		{
			iValueIn |= *pbyBuffer;
			iValueInSizeS++;
			pbyBuffer++;
		}

		if (iValueInSizeS < iValueInSize)
		{
			iValueIn <<= 8;
		}
		else
		{
			uiRESULT = parammapAccessItem(
					uiIDFrom,
					&iValueIn,
					(uiTypeIn & RECVSYSPARAM_TYPE_NO_ADAPT) ? false : true,
					true,
					(uint8_t*)pCFG_SYSTEM_PARAMETERS,PARAMSMAPGET_PTR(CFG_SYSTEM_PARAMETERS));
			if (uiRESULT != PARAMMAP_RETVAL_SUCCESS)
			{
				return CENTRALINA_LORAWAN_INVALID_DATA;
			}

			uiIDFrom++;

			iValueInSizeS 	= 0;
			iValueIn		= 0;
		}


	}

	return CENTRALINA_LORAWAN_RESULT_OK;

}

uint8_t taskComunicationRecvSysParamsComunication(CFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS,const uint8_t* pbyBuffer,int iBuffersize)
{
	return taskComunicationRecvSysParamsByID(pCFG_SYSTEM_PARAMETERS,pbyBuffer,iBuffersize,RECVSYSPARAM_TYPE_UINT8,100,7);
	/*

	pCFG_SYSTEM_PARAMETERS->uiWorkPeriodsMask			=	pbyBuffer[ 0] * 1;	// bit0...bit7 3 ore per bit

	pCFG_SYSTEM_PARAMETERS->uiSecsPeriodWork			=	pbyBuffer[ 1] * 10;
	pCFG_SYSTEM_PARAMETERS->uiSecsPeriodFastUplinkWork	=	pbyBuffer[ 2] * 10;
	pCFG_SYSTEM_PARAMETERS->uiSecsTimeFastUplinkWork	=	pbyBuffer[ 3] * 10;

	pCFG_SYSTEM_PARAMETERS->uiSecsPeriodRest			=	pbyBuffer[ 4] * 10;
	pCFG_SYSTEM_PARAMETERS->uiSecsPeriodFastUplinkRest	=	pbyBuffer[ 5] * 10;
	pCFG_SYSTEM_PARAMETERS->uiSecsTimeFastUplinkRest	=	pbyBuffer[ 6] * 10;
	return CENTRALINA_LORAWAN_RESULT_OK;
	*/
}

uint8_t taskComunicationRecvSysParamsBattery(CFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS,const uint8_t* pbyBuffer,int iBuffersize)
{
	return taskComunicationRecvSysParamsByID(pCFG_SYSTEM_PARAMETERS,pbyBuffer,iBuffersize,RECVSYSPARAM_TYPE_UINT8,110,2);
	/*
	pCFG_SYSTEM_PARAMETERS->uiBatteryMax_mV				=	pbyBuffer[0] * 20;
	pCFG_SYSTEM_PARAMETERS->uiBatteryMin_mV				=	pbyBuffer[1] * 20;
	return CENTRALINA_LORAWAN_RESULT_OK;
	*/
}

uint8_t taskComunicationRecvSysParamsOutDriver(CFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS,const uint8_t* pbyBuffer,int iBuffersize)
{
	return taskComunicationRecvSysParamsByID(pCFG_SYSTEM_PARAMETERS,pbyBuffer,iBuffersize,RECVSYSPARAM_TYPE_UINT8,120,5);
	/*
	pCFG_SYSTEM_PARAMETERS->uiOutType					=	pbyBuffer[0] * 1;		// 0 mono , 1 bistabile
	pCFG_SYSTEM_PARAMETERS->uiOutLoad_mV				=	pbyBuffer[1] * 100;
	pCFG_SYSTEM_PARAMETERS->uiOutLoad_TLoad				=	pbyBuffer[2] * 100;
	pCFG_SYSTEM_PARAMETERS->uiOutLoad_TDischarge		=	pbyBuffer[3] * 10;
	pCFG_SYSTEM_PARAMETERS->uiNoDownlinkShutdown		=	pbyBuffer[4] * 10;
	return CENTRALINA_LORAWAN_RESULT_OK;
	*/
}

uint8_t taskComunicationRecvSysParamsInput(CFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS,int iNumber,const uint8_t* pbyBuffer,int iBuffersize)
{
#if 1
	unsigned uiIDBASE;
	unsigned uiIDBASE_V;
	unsigned uiRESULT;
	uiIDBASE = 0;
	uiIDBASE_V = 0;
	switch (iNumber)
	{
	case 0:
		uiIDBASE = 150;
		uiIDBASE_V = 152;
		break;
	case 1:
		uiIDBASE = 160;
		uiIDBASE_V = 162;
		break;
	}
	uiRESULT = CENTRALINA_LORAWAN_INVALID_DATA;
	if (uiIDBASE > 0)
	{
		uiRESULT = taskComunicationRecvSysParamsByID(pCFG_SYSTEM_PARAMETERS,&pbyBuffer[0],iBuffersize,RECVSYSPARAM_TYPE_UINT8,uiIDBASE,2);
		if (uiRESULT == CENTRALINA_LORAWAN_RESULT_OK)
		{
			uiRESULT = taskComunicationRecvSysParamsByID(pCFG_SYSTEM_PARAMETERS,&pbyBuffer[2],iBuffersize,RECVSYSPARAM_TYPE_UINT16 | RECVSYSPARAM_TYPE_NO_ADAPT,uiIDBASE_V,3);
		}
	}
	return uiRESULT;
#else
	switch (iNumber)
	{
	case 0:
		pCFG_SYSTEM_PARAMETERS->uiIN0_TYPE					=	pbyBuffer[0] * 1;
		pCFG_SYSTEM_PARAMETERS->uiIN0_AR_sec				=	pbyBuffer[1] * 1;
		pCFG_SYSTEM_PARAMETERS->uiIN0_RAW_TH_HI				=	((uint16_t)pbyBuffer[2]  << 8) | (uint16_t)pbyBuffer[3];
		pCFG_SYSTEM_PARAMETERS->uiIN0_RAW_TH_LO				=	((uint16_t)pbyBuffer[4]  << 8) | (uint16_t)pbyBuffer[5];
		pCFG_SYSTEM_PARAMETERS->uiIN0_RAW_TH_DELTA			=	((uint16_t)pbyBuffer[6]  << 8) | (uint16_t)pbyBuffer[7];
		break;
	case 1:
		pCFG_SYSTEM_PARAMETERS->uiIN1_TYPE					=	pbyBuffer[0] * 1;
		pCFG_SYSTEM_PARAMETERS->uiIN1_AR_sec				=	pbyBuffer[1] * 1;
		pCFG_SYSTEM_PARAMETERS->uiIN1_RAW_TH_HI				=	((uint16_t)pbyBuffer[2]  << 8) | (uint16_t)pbyBuffer[3];
		pCFG_SYSTEM_PARAMETERS->uiIN1_RAW_TH_LO				=	((uint16_t)pbyBuffer[4]  << 8) | (uint16_t)pbyBuffer[5];
		pCFG_SYSTEM_PARAMETERS->uiIN1_RAW_TH_DELTA			=	((uint16_t)pbyBuffer[6]  << 8) | (uint16_t)pbyBuffer[7];
		break;
	}
	return CENTRALINA_LORAWAN_RESULT_OK;
#endif
}

uint8_t taskComunicationRecvSysParamsCounter(CFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS,const uint8_t* pbyBuffer,int iBuffersize)
{
	return taskComunicationRecvSysParamsByID(pCFG_SYSTEM_PARAMETERS,pbyBuffer,iBuffersize,RECVSYSPARAM_TYPE_UINT8,180,2);
	/*
	pCFG_SYSTEM_PARAMETERS->uiIN2_COUNTER_ENABLE		=	pbyBuffer[0] * 50;
	pCFG_SYSTEM_PARAMETERS->uiIN2_COUNTER_AR_msec		=	pbyBuffer[1] * 50;
	return CENTRALINA_LORAWAN_RESULT_OK;
	*/
}





//							stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateAperte 	= aBLOCK[1];
//							stSCHEDULER_CTX.stVALVOLE.uiValvoleForzateChiuse 	= aBLOCK[2];
