/*
 * config_maps.c
 *
 *  Created on: 10 feb 2019
 *      Author: daniele_parise
 */

#include <stdbool.h>
#include <stdint.h>
#include "parammaps/parammaps.h"

PARAMSMAPDEFINE_BEGIN(CFG_SYSTEM_PARAMETERS)
PARAMSMAPDEFINE_ENTRY("uiWorkPeriodsMask"			,100,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	1,		1,	0,	255)

PARAMSMAPDEFINE_ENTRY("uiSecsPeriodWork"			,101,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	10,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiSecsPeriodFastUplinkWork"	,102,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	10,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiSecsTimeFastUplinkWork"	,103,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	10,		1,	0,	255)

PARAMSMAPDEFINE_ENTRY("uiSecsPeriodRest"			,104,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	10,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiSecsPeriodFastUplinkRest"	,105,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	10,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiSecsTimeFastUplinkRest"	,106,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	10,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiMinutesForReJoin"			,107,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	60,		1,	0,	255)

PARAMSMAPDEFINE_ENTRY("uiBatteryMax_mV"				,110,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	20,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiBatteryMin_mV"				,111,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	20,		1,	0,	255)

PARAMSMAPDEFINE_ENTRY("uiOutType"					,120,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	1,		1,	0,	1)
PARAMSMAPDEFINE_ENTRY("uiOutLoad_mV"				,121,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	100,	1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiOutLoad_TLoad"				,122,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	100,	1,	0,	100)
PARAMSMAPDEFINE_ENTRY("uiOutLoad_TDischarge"		,123,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	10,		1,	0,	100)
PARAMSMAPDEFINE_ENTRY("uiNoDownlinkShutdown"		,124,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	10,		1,	0,	255)

PARAMSMAPDEFINE_ENTRY("uiIN0_TYPE"					,150,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	1,		1,	0,	4)
PARAMSMAPDEFINE_ENTRY("uiIN0_AR_sec"				,151,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	1,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiIN0_RAW_TH_HI"				,152,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	4,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiIN0_RAW_TH_LO"				,153,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	4,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiIN0_RAW_TH_DELTA"			,154,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	4,		1,	0,	255)

PARAMSMAPDEFINE_ENTRY("uiIN1_TYPE"					,160,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	1,		1,	0,	2)
PARAMSMAPDEFINE_ENTRY("uiIN1_AR_sec"				,161,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	1,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiIN1_RAW_TH_HI"				,162,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	4,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiIN1_RAW_TH_LO"				,163,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	4,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiIN1_RAW_TH_DELTA"			,164,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	4,		1,	0,	255)

PARAMSMAPDEFINE_ENTRY("uiIN2_COUNTER_ENABLE"		,180,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	1,		1,	0,	1)
PARAMSMAPDEFINE_ENTRY("uiIN2_COUNTER_AR_msec"		,181,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	50,		1,	0,	255)
PARAMSMAPDEFINE_ENTRY("uiIN2_COUNTER_AR2_msec"		,182,PARAMSMAP_TYPE_UINT8,PARAMSMAP_TYPE_UINT16,	50,		1,	0,	255)

PARAMSMAPDEFINE_END()




