/*
 * taskComunication.h
 *
 *  Created on: 27 feb 2018
 *      Author: daniele_parise
 */

#ifndef TASKS_TASKCOMUNICATION_H_
#define TASKS_TASKCOMUNICATION_H_




typedef enum etagETASKCOMUNICATION_STATUS{

	TASKCOMUNICATION_INIT,
	TASKCOMUNICATION_SET_PARAMS,
	TASKCOMUNICATION_WAIT_PARAMS,
	TASKCOMUNICATION_JOIN,
	TASKCOMUNICATION_WAIT_JOIN,
	TASKCOMUNICATION_EXCHANGE,
	TASKCOMUNICATION_WAIT_RX
}ETASKCOMUNICATION_STATUS;

#define TASKCOMUNICATION_UPLINK_PAYLOAD_MAX	(128)
typedef struct tagTASKCOMUNICATION_UPLINK{
		uint8_t	auiPayload[TASKCOMUNICATION_UPLINK_PAYLOAD_MAX];
		uint8_t	uiSize;
		uint8_t	uiPort;
}TASKCOMUNICATION_UPLINK;

typedef struct tagTASKCOMUNICATION_CTX{
	ETASKCOMUNICATION_STATUS	eSTATUS;
	TASKCOMUNICATION_UPLINK		stUPLINK;
	TASKCOMUNICATION_UPLINK		stUPLINK_RET_CMD;
	struct {
		int		iBaseDataRate;
		int		iJointReties;
	}stCOM_STATUS;
#ifdef DEBUG
	int							iCNTR_UPLINK_ERRORS;
#endif
	uint8_t						auiSVCResponseBuffer[128];
	uint8_t						auiRecvBlockBuffer[128];

	int64_t						iLastUplinkDownLinkPendingUpTime;
	int64_t						iLastUplinkFastComTime;
	int64_t						iStartUplinkFastComTime;

	int64_t						iLastUplinkTime;
	int64_t						iNextUplinkTimeMinimum;
#ifdef DEBUG
	uint32_t					uiTimeDelayForDutyTx;
#endif

	int32_t						tTimefromLastDownLink;
	int16_t						iSendScheduleInfo;
	int16_t						iSendScheduleInfoProgr;
	int8_t 						iSendScheduleID;
	int8_t 						iSendScheduleCount;
	int8_t 						iDownLinkPendingUpTries;
	uint8_t						uiLoRaVBatt_0_254;
	int16_t						iMeasVBatt_mV;

	unsigned		bREINIT 				: 1;
	unsigned		bLORA_JOINED	 		: 1;
	unsigned		bPARAMS_MODIFIED 		: 1;
	unsigned		bSEND_STATUS_SYS 		: 1;
	unsigned		bSEND_STATUS_OUT 		: 1;
	unsigned		bSEND_STATUS_INS 		: 1;
	unsigned		bRECV_USER_DOWNLINK	 	: 1;
	unsigned		bRECV_SYSTEM_DOWNLINK	: 1;

	unsigned		bSEND_FORCED_STATUS_UPLINK 	: 1;
	unsigned		bSEND_CONFIRMED_UPLINK 	: 1;

	unsigned		bLowBattWarning 		: 1;
	unsigned		bLowBattShutdown 		: 1;

}TASKCOMUNICATION_CTX;

extern TASKCOMUNICATION_CTX	stTASKCOMUNICATION_CTX;

void taskComunicationSignalUpLinkRequest(bool bForceStatusUplink);

typedef struct tagLoraRxInfos{
	uint32_t uiDownLinkCounter;
	int16_t Rssi;
	int8_t Snr;
	uint8_t	bFramePending;
}LoraRxInfos;

void taskComunicationRecvLoRaDownlink(const LoraRxInfos* pLoraRxInfos, unsigned uiPort,bool bConfirmNeeded,unsigned uiSize,const uint8_t* abyBuffer);
void taskComunicationRecvLoRaJoined(void);
void taskComunicationRecvLoRaRebooted(void);

void taskComunicationOnNewLoraParams(void);

void taskComunicationOnProcessNewLoraParams(void);
void taskComunicationLoRaWanReset(void);
void taskComunicationLoRaWanPulseConfirmedUplink(void);

bool taskComunicatioSetVBatRAW(int iVBattRAW,unsigned ui_mV_min,unsigned ui_mV_max,bool bVExtPresent);

#endif /* TASKS_TASKCOMUNICATION_H_ */
