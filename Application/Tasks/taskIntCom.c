/*
 * taskIntCom.c
 *
 *  Created on: 15 mar 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include <stdint.h>
#include "cmsis_os.h"
#include "taskIntCom.h"
#include "../FeeRTOS/FRT_BoardFunctions.h"
#include <string.h>
#include "../intcom/intcom.h"
#include "../gencom/gencom.h"
#include "../msgcom/msgcom.h"

extern osMutexId mutexIntComFunctionsCallHandle;
extern osMutexId mutexIntComHandle;
extern osThreadId taskIntComHandle;


#define TASKINCOM_EVENT_FUNCTION_COMPLETED_SEND	(0x01)
EventGroupHandle_t evtgrpIntComFunctionsCompletedHandle;
StaticEventGroup_t evtgrpIntComFunctionsCompletedCreated;


bool taskIntComOnBusRecvCallback(GENCOM_CTX* pGENCOM_CTX,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer);
bool taskIntComOnBusSentCallback(GENCOM_CTX* pGENCOM_CTX,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer);



bool taskIntComRecvCallback(GENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer);
void taskIntCom_SendCmd_Clbk(GENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,EGENCOMHL_STATUS_RET eStautsRet,const struct tagGENCOMHL_MESSAGE_REQUEST* pRQST, const struct tagGENCOMHL_MESSAGE_ANSWER* pANSW);

GENCOMHL_CTX stINTCOMHL_CTX;

void StartIntComTask(void const * argument)
{
	int			iNextDeltaT_ms;
	uint32_t	lastTime;
	uint32_t	nowTime;
	evtgrpIntComFunctionsCompletedHandle = xEventGroupCreateStatic( &evtgrpIntComFunctionsCompletedCreated );
	intcom_Init(taskIntComOnBusRecvCallback,taskIntComOnBusSentCallback);
//	intcomhlInit(taskIntComRecvCallback,taskIntCom_SendCmd_Clbk,NULL,NULL);

//	intcom_Init(taskIntComOnBusRecvCallback,taskIntComOnBusSentCallback);
	gencomhlInit(&stINTCOMHL_CTX,&stINTCOM_CTX,taskIntComRecvCallback,taskIntCom_SendCmd_Clbk,NULL,NULL);

	iNextDeltaT_ms = -1;
	nowTime = osKernelSysTick();
	while (1)
	{
		BaseType_t	bNotified;
		uint32_t	uiEventMask;
		unsigned	uiTimeOut;
		bool		bSytemRunning;
		uint32_t	nowDelta;

		uiTimeOut =portMAX_DELAY;

		bSytemRunning = false;


		if (iNextDeltaT_ms >= 0)
		{
			uiTimeOut = iNextDeltaT_ms;
		}

		lastTime = nowTime;

		bNotified = xTaskNotifyWait(
									0x0000,
									0xFFFF,
									&uiEventMask,
									uiTimeOut);


		nowTime = osKernelSysTick();

		nowDelta = nowTime - lastTime;

		osMutexWait(mutexIntComHandle, portMAX_DELAY);
		iNextDeltaT_ms = -1;

		iNextDeltaT_ms = gencomhlExecComunicationSlot(&stINTCOMHL_CTX,nowDelta ,uiEventMask,&bSytemRunning);


		osMutexRelease(mutexIntComHandle);

		FRT_SetRunningRQST(FRT_TASK_ORDER_INTCOM,bSytemRunning ? true : false);

	}
}

void taskIntCom_SendCmd_Clbk(GENCOMHL_CTX* pGENCOMHL_CTX,void* pctx,EGENCOMHL_STATUS_RET eStautsRet,const struct tagGENCOMHL_MESSAGE_REQUEST* pRQST, const struct tagGENCOMHL_MESSAGE_ANSWER* pANSW)
{
	xEventGroupSetBits(evtgrpIntComFunctionsCompletedHandle,TASKINCOM_EVENT_FUNCTION_COMPLETED_SEND);
}


EGENCOMHL_STATUS_RET taskIntCom_SendCmd(
		uint8_t uiCmd,
		unsigned uiSizeHdr,const uint8_t* abyBufferHdr,
		unsigned uiSize,const uint8_t* abyBuffer,
		uint32_t uiTimeOut,
		uint8_t* puiRetStatus,
		unsigned* puiRetSize,uint8_t* abyRetBuffer)
{

	EGENCOMHL_STATUS_RET eRet;
	eRet = GENCOMHL_STATUS_INVAL;

	if (uiSize <= GENCOMHL_REQUEST_MAX)
	{
		osMutexWait(mutexIntComFunctionsCallHandle, osWaitForever);

		osMutexWait(mutexIntComHandle, osWaitForever);


		eRet = gencomhlPostSendRequest(&stINTCOMHL_CTX,0,uiCmd,uiSizeHdr,abyBufferHdr,uiSize,abyBuffer,uiTimeOut,2);
		if (eRet == GENCOMHL_STATUS_QUEUED)
		{
			eRet = GENCOMHL_STATUS_SUCCESS;
		}
		if (eRet == GENCOMHL_STATUS_SUCCESS)
		{
			xEventGroupClearBits(evtgrpIntComFunctionsCompletedHandle,TASKINCOM_EVENT_FUNCTION_COMPLETED_SEND);
			xTaskNotify(taskIntComHandle,GENCOMHL_EVENT_RQST_POST,eSetBits);
		}

		osMutexRelease(mutexIntComHandle);

		if (eRet == GENCOMHL_STATUS_SUCCESS)
		{
			EventBits_t	uiRetEvt;
			bool	bCancelled;
			eRet = GENCOMHL_STATUS_TIMEOUT;
			uiRetEvt =  xEventGroupWaitBits(evtgrpIntComFunctionsCompletedHandle,
								   TASKINCOM_EVENT_FUNCTION_COMPLETED_SEND,
								   TASKINCOM_EVENT_FUNCTION_COMPLETED_SEND,
								   pdFALSE,
								   (uiTimeOut * 3) + 1000);
			osMutexWait(mutexIntComHandle, portMAX_DELAY);

			bCancelled = true;

			if (uiRetEvt & TASKINCOM_EVENT_FUNCTION_COMPLETED_SEND)
			{
				bCancelled = false;
			}

			eRet = gencomhlCompleteSendRequest(&stINTCOMHL_CTX,puiRetStatus,puiRetSize,abyRetBuffer,bCancelled);


			osMutexRelease(mutexIntComHandle);

		}
		osMutexRelease(mutexIntComFunctionsCallHandle);
	}
	return eRet;
}

bool taskIntComOnBusRecvCallback(GENCOM_CTX* pGENCOM_CTX,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer)
{
	EventBits_t uxBitsToSet;
	bool		bValid;
	bool		bIsAck;
	uxBitsToSet = 0;
	bIsAck = false;
	bValid = gencomhlCheckAndStoreRecvFromBus(&stINTCOMHL_CTX,uiCmd,uiSize,uiProgr,abyBuffer,&bIsAck);
	if (bValid)
	{
		if (bIsAck)
		{
			uxBitsToSet = GENCOMHL_EVENT_ANSW_RECV;

		}
		else
		{
			uxBitsToSet = GENCOMHL_EVENT_RQST_RECV;

		}

	}

	if (uxBitsToSet != 0)
	{
		xTaskNotifyFromISR(taskIntComHandle,uxBitsToSet,eSetBits,NULL);
		return true;
	}
	return false;
}

bool taskIntComOnBusSentCallback(GENCOM_CTX* pGENCOM_CTX,uint8_t uiCmd, unsigned uiSize,unsigned uiProgr,const uint8_t* abyBuffer)
{
	EventBits_t uxBitsToSet;
	if (uiCmd & GENCOMHL_CMD_ANSWER_MASK)
	{
		uxBitsToSet = GENCOMHL_EVENT_ANSW_SENT;
	}
	else
	{
		uxBitsToSet = GENCOMHL_EVENT_RQST_SENT;
	}
	xTaskNotifyFromISR(taskIntComHandle,uxBitsToSet,eSetBits,NULL);
	return true;
}


EGENCOMHL_STATUS_RET taskIntCom_LoRaWANConfig(const LORAWAN_PARAMS* pLoRaWAN,uint8_t* puiRetStatus)
{

	return taskIntCom_SendCmd(MODINTCOM_CMD_SET_LORA_PARAMS,sizeof(LORAWAN_PARAMS),(uint8_t*)pLoRaWAN,0,NULL,100,puiRetStatus,NULL,NULL);

}


EGENCOMHL_STATUS_RET taskIntCom_LoRaWANEnable(bool bEnable,uint8_t uiBaseDR,uint8_t* puiRetStatus,TASKINTCOM_LORATX_RET_INFOS* pRetInfos)
{
	EGENCOMHL_STATUS_RET	eRET;
	unsigned uiRetSize;
	uint8_t	auiRetBuffer[20];
	uint8_t	aHRD[2];
	aHRD[0] = bEnable ? 1 : 0;
	aHRD[1] = uiBaseDR;

	uiRetSize = sizeof(auiRetBuffer);

	eRET = taskIntCom_SendCmd(MODINTCOM_CMD_ENABLE_LORA,2,&aHRD[0],0,NULL,100,puiRetStatus,&uiRetSize,&auiRetBuffer[0]);
	if (eRET == GENCOMHL_STATUS_SUCCESS)
	{
		if (pRetInfos)
		{
			memset(pRetInfos,0,sizeof(TASKINTCOM_LORATX_RET_INFOS));
			if (uiRetSize >= 6)
			{
				pRetInfos->timeNextTxDelay = auiRetBuffer[2];
				pRetInfos->timeNextTxDelay<<=8;
				pRetInfos->timeNextTxDelay |= auiRetBuffer[3];
				pRetInfos->timeNextTxDelay<<=8;
				pRetInfos->timeNextTxDelay |= auiRetBuffer[4];
				pRetInfos->timeNextTxDelay<<=8;
				pRetInfos->timeNextTxDelay |= auiRetBuffer[5];
				if (uiRetSize >= 8)
				{
					pRetInfos->uiDataRateUsed	= auiRetBuffer[6]; // datarate used
					pRetInfos->uiTxPowerUsed	= auiRetBuffer[7]; // txPower used
				}
				if (uiRetSize >= 10)
				{
					pRetInfos->uiUplinkCounter	= auiRetBuffer[8]; // uplink counter
					pRetInfos->uiUplinkCounter<<=8;
					pRetInfos->uiUplinkCounter	|= auiRetBuffer[9]; // uplink counter
				}
				if (uiRetSize >= 12)
				{
					pRetInfos->timeOnAirTx	= auiRetBuffer[10]; // uplink counter
					pRetInfos->timeOnAirTx<<=8;
					pRetInfos->timeOnAirTx	|= auiRetBuffer[11]; // uplink counter
				}

			}
		}

	}

	return eRET;
}

EGENCOMHL_STATUS_RET taskIntCom_SendUplink(const INTCOMHL_LORA_DEV_STATUS* pDSTS,uint8_t uiPort, bool bConfirm, unsigned uiSize,const uint8_t* abyBuffer,uint8_t* puiRetStatus,TASKINTCOM_LORATX_RET_INFOS* pRetInfos )
{
	EGENCOMHL_STATUS_RET	eRET;
	unsigned uiRetSize;
	unsigned uiHdrSize;
	uint8_t		auiRetBuffer[20];
	uint8_t		aHdr[10];
	uiRetSize = sizeof(auiRetBuffer);
	uiHdrSize = 3;
	aHdr[0] =uiPort;
	aHdr[1] = 0;
	if (pDSTS)
	{
		uiHdrSize = 3 + 4;
		aHdr[1] = 4;
	}
	if (bConfirm)
	{
		aHdr[1] |= 0x80;
	}
	if (pDSTS)
	{
		aHdr[2 + 0] =pDSTS->uiBattery;
		aHdr[2 + 1] =pDSTS->uiTemperature;
		aHdr[2 + 2] =0;
		aHdr[2 + 3] =0;
		aHdr[2 + 4] =uiSize;
	}
	else
	{
		aHdr[2] =uiSize;
	}




	eRET = taskIntCom_SendCmd(MODINTCOM_CMD_SEND_UPLINK,uiHdrSize,aHdr,uiSize,abyBuffer,500,puiRetStatus,&uiRetSize,&auiRetBuffer[0]);
	if (eRET == GENCOMHL_STATUS_SUCCESS)
	{
		if (pRetInfos)
		{
			memset(pRetInfos,0,sizeof(TASKINTCOM_LORATX_RET_INFOS));
			if (uiRetSize >= 6)
			{
				pRetInfos->timeNextTxDelay = auiRetBuffer[2];
				pRetInfos->timeNextTxDelay<<=8;
				pRetInfos->timeNextTxDelay |= auiRetBuffer[3];
				pRetInfos->timeNextTxDelay<<=8;
				pRetInfos->timeNextTxDelay |= auiRetBuffer[4];
				pRetInfos->timeNextTxDelay<<=8;
				pRetInfos->timeNextTxDelay |= auiRetBuffer[5];
				if (uiRetSize >= 8)
				{
					pRetInfos->uiDataRateUsed	= auiRetBuffer[6]; // datarate used
					pRetInfos->uiTxPowerUsed	= auiRetBuffer[7]; // txPower used
				}
				if (uiRetSize >= 10)
				{
					pRetInfos->uiUplinkCounter	= auiRetBuffer[8]; // uplink counter
					pRetInfos->uiUplinkCounter<<=8;
					pRetInfos->uiUplinkCounter	|= auiRetBuffer[9]; // uplink counter
				}
				if (uiRetSize >= 12)
				{
					pRetInfos->timeOnAirTx	= auiRetBuffer[10]; // uplink counter
					pRetInfos->timeOnAirTx<<=8;
					pRetInfos->timeOnAirTx	|= auiRetBuffer[11]; // uplink counter
				}

			}
		}

	}

	return eRET;
}

void taskIntCom_testCmd(void)
{
	EGENCOMHL_STATUS_RET	eRet;
	uint8_t				aBuff[10];
	uint8_t				aBuffRet[10];
	unsigned			uiRetSize;
	uiRetSize = 10;

	eRet = taskIntCom_SendCmd(MODINTCOM_CMD_SEND_UPLINK,10,aBuff,0,NULL,500,NULL,&uiRetSize,aBuffRet);
}
