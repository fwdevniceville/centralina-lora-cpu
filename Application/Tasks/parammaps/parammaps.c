/*
 * parammaps.c
 *
 *  Created on: 10 feb 2019
 *      Author: daniele_parise
 */

#include <stdbool.h>
#include <stdint.h>
#include "parammaps.h"

uint8_t parammapAccessItem(unsigned uiID, int* piValueInOut, bool bAdapt,bool bWrite, uint8_t* pMemory, const PARAMS_MAP aMAP[])
{
	const PARAMS_MAP*	pPARAMS_MAP;
	unsigned uiSCNR;
	unsigned uiSCNR_MEM;
	unsigned uiMEM_SIZE;
	int iValueMem;
	uiSCNR_MEM = 0;
	uiSCNR = 0;
	while (uiSCNR < 100)
	{
		pPARAMS_MAP = &aMAP[uiSCNR];
		if (pPARAMS_MAP->uiPID == 0)
		{
			return PARAMMAP_RETVAL_NOMAP;
		}
		uiMEM_SIZE = 0;
		switch (PARAMSMAP_GET_TYPE_MEM(pPARAMS_MAP->uiTYPES))
		{
		case PARAMSMAP_TYPE_UINT8:
			uiMEM_SIZE = sizeof(uint8_t);
			break;
		case PARAMSMAP_TYPE_UINT16:
			uiMEM_SIZE = sizeof(uint16_t);
			break;
		}

		if (uiMEM_SIZE == 0)
		{
			return PARAMMAP_RETVAL_BADMAP;
		}

		if (pPARAMS_MAP->uiPID == uiID)
		{
			iValueMem = *piValueInOut;
			if (bAdapt)
			{
				iValueMem*=pPARAMS_MAP->uiMULT;
				iValueMem/=pPARAMS_MAP->uiDIV;
			}
			switch (PARAMSMAP_GET_TYPE_MEM(pPARAMS_MAP->uiTYPES))
			{
			case PARAMSMAP_TYPE_UINT8:
				pMemory[uiSCNR_MEM + 0] = (iValueMem >> 0) & 0xFF;
				break;
			case PARAMSMAP_TYPE_UINT16:
				pMemory[uiSCNR_MEM + 0] = (iValueMem >> 0) & 0xFF;
				pMemory[uiSCNR_MEM + 1] = (iValueMem >> 8) & 0xFF;
				break;
			}


			return PARAMMAP_RETVAL_SUCCESS;
		}

		uiSCNR_MEM+=uiMEM_SIZE;
		uiSCNR++;
	}
	return PARAMMAP_RETVAL_INVALMAP;
}
