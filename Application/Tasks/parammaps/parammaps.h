/*
 * parammaps.h
 *
 *  Created on: 10 feb 2019
 *      Author: daniele_parise
 */

#ifndef TASKS_PARAMMAPS_PARAMMAPS_H_
#define TASKS_PARAMMAPS_PARAMMAPS_H_

typedef struct tagPARAMS_MAP{
	uint8_t	uiPID;
	uint8_t	uiTYPES;
	uint8_t	uiMULT;
	uint8_t	uiDIV;
}PARAMS_MAP;

#define PARAMSMAP_TYPE_UINT8			(0x01)
#define PARAMSMAP_TYPE_UINT16			(0x02)

#define PARAMSMAP_GET_TYPE_MEM(type)	(((type) >> 0) & 0xF)
#define PARAMSMAP_GET_TYPE_IN(type)		(((type) >> 4) & 0xF)

#define PARAMSMAPDEFINE_BEGIN(name)		const PARAMS_MAP	aPARAMS_MAP_##name[] = {
#define PARAMSMAPDEFINE_ENTRY(label,id,type_in,type_mem,in2mem_mult,in2mem_div,val_min,val_max) {id,(type_in << 4) | (type_mem & 0xF),in2mem_mult,in2mem_div},
#define PARAMSMAPDEFINE_END()	{0}};

#define PARAMSMAPDEFINE_EXTERN(name)	extern const PARAMS_MAP	aPARAMS_MAP_##name[]
#define PARAMSMAPGET_PTR(name)			&aPARAMS_MAP_##name[0]

#define PARAMMAP_RETVAL_SUCCESS		(0x00)
#define PARAMMAP_RETVAL_ORANGE		(0x41)
#define PARAMMAP_RETVAL_INVALMAP	(0x81)
#define PARAMMAP_RETVAL_BADMAP		(0x82)
#define PARAMMAP_RETVAL_NOMAP		(0x83)
uint8_t parammapAccessItem(unsigned uiID, int* piValueIn, bool bAdapt,bool bWrite, uint8_t* pMemory, const PARAMS_MAP aMAP[]);

#endif /* TASKS_PARAMMAPS_PARAMMAPS_H_ */
