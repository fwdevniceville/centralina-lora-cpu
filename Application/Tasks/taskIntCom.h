/*
 * taskIntCom.h
 *
 *  Created on: 15 mar 2018
 *      Author: daniele_parise
 */

#ifndef TASKS_TASKINTCOM_H_
#define TASKS_TASKINTCOM_H_

//#include "../ge/intcom.h"
#include "../gencom/gencomhl.h"

EGENCOMHL_STATUS_RET taskIntCom_SendCmd(
		uint8_t uiCmd,
		unsigned uiSizeHdr,const uint8_t* abyBufferHdr,
		unsigned uiSize,const uint8_t* abyBuffer,
		uint32_t uiTimeOut,
		uint8_t* puiRetStatus,
		unsigned* puiRetSize,uint8_t* abyRetBuffer);

typedef struct tagLORAWAN_PARAMS{
	uint8_t		uiEnableAdaptiveDataRate;
	int8_t		iTransmissionDataRate;
	uint8_t		uiEnablePublicNetwork;
	uint8_t		uiJointRequestTrials;


	uint8_t		uiABP;

	uint8_t		auiABP_DEV_ADDRESS[4];
	uint8_t		auiABP_NWK_ID[4];

	uint8_t		auiDEV_EUI[8];
	uint8_t		auiJOIN_EUI[8];

	uint8_t		auiAPP_KEY__ABP_S_KEY[16];
	uint8_t		auiNWK_KEY__ABP_S_KEY[16];

	uint8_t		auiABP_S_NWK_S_INT_KEY[16];
	uint8_t		auiABP_F_NWK_S_INT_KEY[16];
}LORAWAN_PARAMS;

//*puiRetStatus LORA values
#define LORAWAN_STATUS_OK							(0x00)
#define LORAWAN_STATUS_NJOIN						(0x40)
#define LORAWAN_STATUS_NINIT						(0x41)
#define LORAWAN_STATUS_NENABLE						(0x42)

#define LORAWAN_STATUS_INVAL						(0x81)
#define LORAWAN_STATUS_GENERIC_LORAMAC_FAULT		(0x90)
#define LORAWAN_MAC_STATUS_DUTY_VIOLATION			(0x91)
#define LORAWAN_MAC_STATUS_SERVICE_UNKNOWN			(0x92)
#define LORAWAN_MAC_STATUS_PARAMETER_INVALID		(0x93)
#define LORAWAN_MAC_STATUS_FREQUENCY_INVALID		(0x94)
#define LORAWAN_MAC_STATUS_DATARATE_INVALID			(0x95)
#define LORAWAN_MAC_STATUS_FREQ_AND_DR_INVALID		(0x96)
#define LORAWAN_MAC_STATUS_NO_NETWORK_JOINED		(0x97)
#define LORAWAN_MAC_STATUS_LENGTH_ERROR				(0x98)
#define LORAWAN_MAC_STATUS_DEVICE_OFF				(0x99)
#define LORAWAN_MAC_STATUS_REGION_NOT_SUPPORTED		(0x9A)
#define LORAWAN_MAC_STATUS_CERTIFICATION_RUNNING	(0x9B)
#define LORAWAN_MAC_STATUS_NO_CHANNEL_FOUND			(0x9C)
#define LORAWAN_MAC_STATUS_TX_SYS_BUSY				(0x9D)

typedef struct tagTASKINTCOM_LORATX_RET_INFOS{
	uint32_t	timeNextTxDelay;
	uint16_t	uiUplinkCounter;
	uint16_t	timeOnAirTx;
	uint8_t		uiDataRateUsed;
	uint8_t		uiTxPowerUsed;
}TASKINTCOM_LORATX_RET_INFOS;

// LORAWAN_STATUS_OK
// LORAWAN_STATUS_INVAL
EGENCOMHL_STATUS_RET taskIntCom_LoRaWANConfig(const LORAWAN_PARAMS* pLoRaWAN,uint8_t* puiRetStatus);

// LORAWAN_STATUS_OK
// LORAWAN_STATUS_INVAL
// LORAWAN_STATUS_NINIT
EGENCOMHL_STATUS_RET taskIntCom_LoRaWANEnable(bool bEnable,uint8_t uiBaseDR,uint8_t* puiRetStatus,TASKINTCOM_LORATX_RET_INFOS* pRetInfos);
// LORAWAN_STATUS_OK
// LORAWAN_STATUS_INVAL
// LORAWAN_STATUS_NINIT
// LORAWAN_STATUS_NENABLE
// LORAWAN_STATUS_SEND_FAIL
typedef struct tagINTCOMHL_LORA_DEV_STATUS{
	uint8_t	uiBattery;
	uint8_t	uiTemperature;
}INTCOMHL_LORA_DEV_STATUS;

EGENCOMHL_STATUS_RET taskIntCom_SendUplink(const INTCOMHL_LORA_DEV_STATUS* pDSTS,uint8_t uiPort, bool bConfirm, unsigned uiSize,const uint8_t* abyBuffer,uint8_t* puiRetStatus,TASKINTCOM_LORATX_RET_INFOS* pRetInfos);

EGENCOMHL_STATUS_RET taskIntCom_SendDeviceStatus(uint8_t uiBatteryLoRa,uint8_t uiTemperatureLoRa);

void taskIntCom_testCmd(void);

extern GENCOMHL_CTX stINTCOMHL_CTX;

#endif /* TASKS_TASKINTCOM_H_ */
