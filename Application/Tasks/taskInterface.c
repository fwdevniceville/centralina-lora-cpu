/*
 * taskLCD.c
 *
 *  Created on: 07 feb 2018
 *      Author: daniele_parise
 */
#include "cmsis_os.h"
#include <string.h>
#include <stdbool.h>
#include "taskInterface.h"
#include "rtc.h"
#include "lcd.h"
#include "../SystemFunctions.h"
#include "../Display/display.h"

#include "../FeeRTOS/FRT_BoardFunctions.h"
#include "../Tasks/taskIntCom.h"
#include "../Menu/menu_func.h"
#include "../menu-loc/menu_def.h"
#include "../menu-loc/menu_vars.h"
#include "../hwbrd/hwbuttons.h"

typedef struct tagTASK_INTERFACE_CTX{
	osMessageQId qmsgEvents;
	uint32_t	uiLastSysTickOnEventButton;
	uint32_t	uiTimeoutMenu;
	uint32_t	uiTimeoutMenuDisp;
	char		strDISPLAY[20];
	unsigned	bPOWER_DOWN	: 1;
	unsigned	bTaskRunning	: 1;
	unsigned	bDisplayAlwaysOn	: 1;
}TASK_INTERFACE_CTX;

TASK_INTERFACE_CTX	stTASK_INTERFACE_CTX;
uint8_t	qmsgEvents_Buffer[1 * sizeof(uint16_t)];
osStaticMessageQDef_t   qmsgEventsControlblock;     ///< control block to hold queue's data for static allocation; NULL for dynamic allocation

//osMessageQDef(qmsgEventsDef, 1, uint16_t);
osMessageQStaticDef(qmsgEventsDef, 1, uint16_t,&qmsgEvents_Buffer[0],&qmsgEventsControlblock);

void taskInterfacePostDisplayAlwaysOn(bool bDisplayAlwaysOn)
{
	stTASK_INTERFACE_CTX.bDisplayAlwaysOn		= bDisplayAlwaysOn;
	taskInterfacePostEvent(TASKINTERFACE_EVT_DISPLAY);
}

void taskInterfaceInit(void)
{
	stTASK_INTERFACE_CTX.qmsgEvents 	= osMessageCreate(osMessageQ(qmsgEventsDef), NULL);
	stTASK_INTERFACE_CTX.bPOWER_DOWN 		= true;
	stTASK_INTERFACE_CTX.uiTimeoutMenu		= 10000;
	stTASK_INTERFACE_CTX.uiTimeoutMenuDisp	= 60000;
#ifdef DEBUG
	stTASK_INTERFACE_CTX.uiTimeoutMenuDisp	= 600000;
#endif
	stTASK_INTERFACE_CTX.bTaskRunning		= true;
}

void taskInterfacePostEvent(ETASKINTERFACE_EVENT eBTN_EVT)
{
	if (stTASK_INTERFACE_CTX.bTaskRunning)
	{
		osMessagePut(stTASK_INTERFACE_CTX.qmsgEvents,(uint32_t)eBTN_EVT,0);
	}
}


EMENU_EVENTS taskInterfaceGetButtonState(void)
{
	uint16_t 				uiTST1;
	uint16_t 				uiTST2;
	uint16_t 				uiTST3;
	EMENU_EVENTS	eBTN;
	eBTN = MENU_EVENT_BTN_NONE;
	uiTST1 = hwbuttonsTestBTN1(); // TST1
	uiTST2 = hwbuttonsTestBTN2(); // TST2
	uiTST3 = hwbuttonsTestBTN3(); // TST3

	if (uiTST2 && uiTST3) // uiTST1 && uiTST3
	{
		eBTN = MENU_EVENT_BTN_CONFIRM;
	}
	else
	if (uiTST2) // uiTST1
	{
		eBTN = MENU_EVENT_BTN_NEXT;
	}
	else
	if (uiTST3) // uiTST3
	{
		eBTN = MENU_EVENT_BTN_ENTER;
	}
	else
	if (uiTST1) // uiTST2
	{
		eBTN = MENU_EVENT_BTN_EXIT;
	}
	return eBTN;

}

void StartInterfaceTask(void const * argument)
{
	uint32_t 	ulNewSysTick;
	uint32_t 	ulDeltaSysTickButton;
	uint32_t 	ulOldSysTickImmediate;
	uint32_t 	ulDeltaSysTickImmediate;
	uint32_t 	ulPressionTime;
	uint32_t	uiTimeoutMenu;


	EMENU_EVENTS	eButtonNew;
	EMENU_EVENTS	eButtonLast;

	unsigned	uiDelay;
	bool		bPressedState;
	bPressedState 	= false;
	ulPressionTime 	= 0;

	taskInterfaceInit();

	eButtonNew = eButtonLast = MENU_EVENT_BTN_NONE;


	ulOldSysTickImmediate = osKernelSysTick();
	stTASK_INTERFACE_CTX.uiLastSysTickOnEventButton 	= ulOldSysTickImmediate;
	stTASK_INTERFACE_CTX.bPOWER_DOWN = false;
	MX_LCD_Init();

	while (1)
	{
		ETASKINTERFACE_EVENT	eEvent;
		osEvent event;

		uiDelay = 500;
		if (stTASK_INTERFACE_CTX.bPOWER_DOWN)
		{
			uiDelay = osWaitForever;
//			FRT_SetRunningRQST(FRT_TASK_ORDER_INTERFACE,false);
		}
		else
		{
//			FRT_SetRunningRQST(FRT_TASK_ORDER_INTERFACE,true);
		}
		if (bPressedState)
		{
			uiDelay = 100;
		}

		event = osMessageGet(stTASK_INTERFACE_CTX.qmsgEvents, uiDelay);
		eEvent = TASKINTERFACE_EVT_NONE;
		ulNewSysTick = osKernelSysTick();

		ulDeltaSysTickImmediate = ulNewSysTick - ulOldSysTickImmediate;
		ulOldSysTickImmediate 	= ulNewSysTick;

		if(event.status == osEventMessage)
		{
			eEvent =  (ETASKINTERFACE_EVENT) (event.value.v);
			switch (eEvent)
			{
			case TASKINTERFACE_EVT_BTN:
				{
					stTASK_INTERFACE_CTX.uiLastSysTickOnEventButton = ulNewSysTick;
				}
				break;
			}
		}

		eButtonNew = taskInterfaceGetButtonState();
		if (eButtonLast != eButtonNew)
		{
			ulPressionTime = 0;
			eButtonLast = eButtonNew;
		}
		else
		if (eButtonNew != MENU_EVENT_BTN_NONE)
		{
			ulPressionTime+= ulDeltaSysTickImmediate;
		}
		bPressedState 	= (eButtonNew != MENU_EVENT_BTN_NONE) ? true : false;

		ulDeltaSysTickButton = ulNewSysTick - stTASK_INTERFACE_CTX.uiLastSysTickOnEventButton;

		uiTimeoutMenu = stTASK_INTERFACE_CTX.uiTimeoutMenu;
		if (menuIsDisplaingValues())
		{
			uiTimeoutMenu = stTASK_INTERFACE_CTX.uiTimeoutMenuDisp;
		}
		if (
				(uiTimeoutMenu > 0)
					&&
				(ulDeltaSysTickButton > uiTimeoutMenu)
					&&
				(!stTASK_INTERFACE_CTX.bDisplayAlwaysOn)
			)
		{
			HAL_LCD_DeInit(&hlcd);

			menuDeinit();
			stTASK_INTERFACE_CTX.bPOWER_DOWN = true;
		}
		else
		if (stTASK_INTERFACE_CTX.bPOWER_DOWN)
		{
//			if (eEvent != TASKINTERFACE_EVT_NONE)
			if (
					(ulPressionTime > 100)
						||
					(stTASK_INTERFACE_CTX.bDisplayAlwaysOn)
				)
			{
				MX_LCD_Init();


				/**
				  * @brief  Configure the LCD Blink mode and Blink frequency.
				  * @param  BlinkMode: specifies the LCD blink mode.
				  *   This parameter can be one of the following values:
				  *     @arg LCD_BLINKMODE_OFF:           Blink disabled
				  *     @arg LCD_BLINKMODE_SEG0_COM0:     Blink enabled on SEG[0], COM[0] (1 pixel)
				  *     @arg LCD_BLINKMODE_SEG0_ALLCOM:   Blink enabled on SEG[0], all COM (up to 8
				  *                                       pixels according to the programmed duty)
				  *     @arg LCD_BLINKMODE_ALLSEG_ALLCOM: Blink enabled on all SEG and all COM
				  *                                       (all pixels)
				  * @param  BlinkFrequency: specifies the LCD blink frequency.
				  *     @arg LCD_BLINKFREQUENCY_DIV8:    The Blink frequency = fLcd/8
				  *     @arg LCD_BLINKFREQUENCY_DIV16:   The Blink frequency = fLcd/16
				  *     @arg LCD_BLINKFREQUENCY_DIV32:   The Blink frequency = fLcd/32
				  *     @arg LCD_BLINKFREQUENCY_DIV64:   The Blink frequency = fLcd/64
				  *     @arg LCD_BLINKFREQUENCY_DIV128:  The Blink frequency = fLcd/128
				  *     @arg LCD_BLINKFREQUENCY_DIV256:  The Blink frequency = fLcd/256
				  *     @arg LCD_BLINKFREQUENCY_DIV512:  The Blink frequency = fLcd/512
				  *     @arg LCD_BLINKFREQUENCY_DIV1024: The Blink frequency = fLcd/1024
				  * @retval None
				  */

//				BSP_LCD_GLASS_BlinkConfig(LCD_BLINKMODE_ALLSEG_ALLCOM,LCD_BLINKFREQUENCY_DIV512);

				menuInit();

				stTASK_INTERFACE_CTX.bPOWER_DOWN = false;
			}
		}
		else
		{
			uint8_t uiDisplayOptions;

			uiDisplayOptions = 0;

			menuManage(&aMENU_ITEMS_BASE[0],eButtonNew,ulDeltaSysTickImmediate,ulPressionTime,stTASK_INTERFACE_CTX.strDISPLAY,&uiDisplayOptions);

			displayOut(stTASK_INTERFACE_CTX.strDISPLAY,uiDisplayOptions);

		}

	}
	HAL_LCD_DeInit(&hlcd);

}

/*
 *
 * PAYLOAD LORA EU868
 *
 * DataRate 	M 		N
 * 0 			59 		51
 * 1 			59 		51
 * 2 			59 		51
 * 3 			123 	115
 * 4 			250 	242
 * 5 			250 	242
 * 6 			250 	242
 * 7 			250 	242
 * 8:15 Not defined
 */
