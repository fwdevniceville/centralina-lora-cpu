/*
 * taskMENU.c
 *
 *  Created on: 07 feb 2018
 *      Author: daniele_parise
 */
#include <stdbool.h>
#include <string.h>
#include "cmsis_os.h"
#include "hal_inc.h"
#include "../menu-loc/menu_vars.h"
#include "../menu-loc/menu_events_status.h"
#include "../Utils/bitpacker.h"
#include "../FeeRTOS/FRT_BoardFunctions.h"
#include "../Storage/configurations.h"

#include "../Tasks/taskIntCom.h"
#include "../Tasks/taskScheduler.h"
#include "../Tasks/taskComunication.h"
#include "../Tasks/taskSupervisor.h"

#include "../Utils/crc8.h"
#include "../SystemFunctions.h"

extern osThreadId taskComunicatioHandle;


#define TASK_COMUNICATION_EVT_MASK_UPRQST			(0x0001)
#define TASK_COMUNICATION_EVT_MASK_NEW_PARAMS		(0x0002)
#define TASK_COMUNICATION_EVT_MASK_LORA_JOINED		(0x0004)
#define TASK_COMUNICATION_EVT_MASK_LORA_REBOOTED	(0x0008)
#define TASK_COMUNICATION_EVT_MASK_CONFUPLINK		(0x0010)

void taskComunicationSignalUpLinkRequest(bool bForceStatusUplink)
{
	if (bForceStatusUplink)
	{
		stTASKCOMUNICATION_CTX.bSEND_FORCED_STATUS_UPLINK = true;
	}
	osSignalSet(taskComunicatioHandle,TASK_COMUNICATION_EVT_MASK_UPRQST);
}

void taskComunicationOnNewLoraParams(void)
{
	stTASKCOMUNICATION_CTX.bPARAMS_MODIFIED = true;
}

// no acq delay (enable to read)
#define VBAT_RAW_5V0_F	(2539)
#define VBAT_RAW_4V5_F	(2298)
#define VBAT_RAW_4V0_F	(2054)
#define VBAT_RAW_3V5_F	(1814)
#define VBAT_RAW_3V0_F	(1567)
#define VBAT_RAW_2V5_F	(1319)

//  acq delay (enable to read)
#define VBAT_RAW_5V0	(2903)
#define VBAT_RAW_4V5	(2618)
#define VBAT_RAW_4V0	(2374)
#define VBAT_RAW_3V5	(2051)
#define VBAT_RAW_3V0	(1772)
#define VBAT_RAW_2V5	(1484)


typedef struct tagVBAT_CONV_ATOM{
	int	iRAW;
	int	imV;
}VBAT_CONV_ATOM;

#ifdef CENTRALINA_PRTOTIPO_LAST
const VBAT_CONV_ATOM aVBAT_CONV_TABLE[] =
{
		{1484,2500},
		{1772,3000},
		{2051,3500},
		{2374,4000},
		{2618,4500},
		{2903,5000}
};

#else

const VBAT_CONV_ATOM aVBAT_CONV_TABLE[] =
{
		{1342,2500},
		{1613,3000},
		{1880,3500},
		{2164,4000},
		{2450,4500},
		{2723,5000}
};

#endif

#define VBAT_CONV_TABLE_MAX (sizeof(aVBAT_CONV_TABLE) / sizeof(VBAT_CONV_ATOM))

int adcreaderVBatt_adc2mV(int iadc);
int adcreaderVBatt_adc2mV(int iadc)
{
	int iret;
	iret = iadc - VBAT_RAW_2V5;
	iret *= (5000 - 2500);
	iret /= (VBAT_RAW_5V0 - VBAT_RAW_2V5);
	iret+=2500;
	return iret;
}

int adcreaderVBatt_adc2mVprec(int iadc);
int adcreaderVBatt_adc2mVprec(int iadc)
{
	int	iP_MIN;
	int	iP_MAX;
	int iret;
	int	iscnr;
	iscnr = 0;
	iP_MIN = 0;
	iP_MAX = VBAT_CONV_TABLE_MAX - 1;

	while (iscnr < VBAT_CONV_TABLE_MAX)
	{
		if (iadc < aVBAT_CONV_TABLE[iscnr].iRAW)
		{
			iP_MAX = iscnr;
			break;
		}
		iP_MIN = iscnr;
		iscnr++;
	}
	if (iscnr >= VBAT_CONV_TABLE_MAX)
	{
		iP_MIN = VBAT_CONV_TABLE_MAX - 2;
		iP_MAX = VBAT_CONV_TABLE_MAX - 1;
	}
	else if (iscnr <= 0)
	{
		iP_MIN = 0;
		iP_MAX = 1;
	}

	iret = iadc - aVBAT_CONV_TABLE[iP_MIN].iRAW;
	iret *= (aVBAT_CONV_TABLE[iP_MAX].imV - aVBAT_CONV_TABLE[iP_MIN].imV);
	iret /= (aVBAT_CONV_TABLE[iP_MAX].iRAW - aVBAT_CONV_TABLE[iP_MIN].iRAW);
	iret+=aVBAT_CONV_TABLE[iP_MIN].imV;
	return iret;
}





int adcreaderVBatt_mV2LoraCharge(int imV_batt,int i_mV_min,int i_mV_max);
int adcreaderVBatt_mV2LoraCharge(int imV_batt,int i_mV_min,int i_mV_max)
{
	int iret;
	iret = imV_batt - i_mV_min;
	iret *= (254);
	iret /= (i_mV_max - i_mV_min);
	return iret;
}


bool taskComunicatioSetVBatRAW(int iVBattRAW,unsigned ui_mV_min,unsigned ui_mV_max,bool bVExtPresent)
{
	int	iVBatt_mV;
	int	iVBatt_LoraWan;

	iVBatt_mV = adcreaderVBatt_adc2mVprec(iVBattRAW);
	stTASKCOMUNICATION_CTX.iMeasVBatt_mV = (int16_t)iVBatt_mV;

	if (bVExtPresent)
	{
		stTASKCOMUNICATION_CTX.bLowBattShutdown = false;
		stTASKCOMUNICATION_CTX.bLowBattWarning 	= false;

		stTASKCOMUNICATION_CTX.uiLoRaVBatt_0_254 = 0;

	}
	else
	{

		iVBatt_LoraWan = adcreaderVBatt_mV2LoraCharge(iVBatt_mV,ui_mV_min,ui_mV_max);

		if (iVBatt_LoraWan > (256 / 20)) // 10%
		{
			stTASKCOMUNICATION_CTX.bLowBattShutdown = false;
			stTASKCOMUNICATION_CTX.bLowBattWarning 	= false;
		}
		if (iVBatt_LoraWan <= (256 / 10)) // 10%
		{
			stTASKCOMUNICATION_CTX.bLowBattWarning = true;
		}
		if (iVBatt_LoraWan <= 0)
		{
			stTASKCOMUNICATION_CTX.bLowBattShutdown = true;
			stTASKCOMUNICATION_CTX.bLowBattWarning = true;
		}
		if (iVBatt_LoraWan > 254)
		{
			iVBatt_LoraWan = 254;
		}
		if (iVBatt_LoraWan < 1)
		{
			iVBatt_LoraWan = 1;
		}
		stTASKCOMUNICATION_CTX.uiLoRaVBatt_0_254 = (uint8_t)iVBatt_LoraWan;
	}
	return stTASKCOMUNICATION_CTX.bLowBattShutdown ? true : false;
}

void taskComunicationLoRaWanReset(void)
{
	stTASKCOMUNICATION_CTX.bREINIT = true;
	osSignalSet(taskComunicatioHandle,TASK_COMUNICATION_EVT_MASK_NEW_PARAMS);
}

void taskComunicationOnProcessNewLoraParams(void)
{
	if (stTASKCOMUNICATION_CTX.bPARAMS_MODIFIED)
	{
		stTASKCOMUNICATION_CTX.bPARAMS_MODIFIED = false;
		taskComunicationLoRaWanReset();
	}

}

void taskComunicationLoRaWanPulseConfirmedUplink(void)
{
	stTASKCOMUNICATION_CTX.bSEND_CONFIRMED_UPLINK = true;
	osSignalSet(taskComunicatioHandle,TASK_COMUNICATION_EVT_MASK_CONFUPLINK);
}

void taskComunicationRecvLoRaRebooted(void)
{
	stTASKCOMUNICATION_CTX.bREINIT	= true;
	osSignalSet(taskComunicatioHandle,TASK_COMUNICATION_EVT_MASK_LORA_REBOOTED);
}

void taskComunicationRecvLoRaJoined(void)
{
	stTASKCOMUNICATION_CTX.bLORA_JOINED	= true;
	osSignalSet(taskComunicatioHandle,TASK_COMUNICATION_EVT_MASK_LORA_JOINED);
}

int taskcomunicationFillUplink(TASKCOMUNICATION_UPLINK* pUPLINK);
int taskComunicationEvalDeltaTUplink(bool* pbSendUpLink,bool* pbLoadStatusUpLink,int64_t* piCurrentTime);

TASKCOMUNICATION_CTX	stTASKCOMUNICATION_CTX;


#ifdef DEBUG
typedef struct tagTSKSCHED_MANCFG{
	CFG_SCHEDULER_ROW			stROW;
	bool						bSave;
}TSKSCHED_MANCFG;

TSKSCHED_MANCFG	stTSKSCHED_MANCFG;
#endif


bool taskcomunicationIsLoraWanParamsValid(const CFG_LORAWAN* pParams);
bool taskcomunicationIsLoraWanParamsValid(const CFG_LORAWAN* pParams)
{

	return true;
}

void StartComunicationTask(void const * argument)
{
	BaseType_t	bNotified;
	uint32_t	uiNotyMask;
	TickType_t	tLastTimerTicks;
	TickType_t	tNewTimerTicks;
	TickType_t	tDeltaTimerTicks;
	TASKINTCOM_LORATX_RET_INFOS	stLoraTxRetInfos;

	EGENCOMHL_STATUS_RET	eComRet;
	uint8_t			uiResult;

	stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_INIT;


	while (true)
	{
#ifdef DEBUG
		if (stTSKSCHED_MANCFG.bSave)
		{
			stTSKSCHED_MANCFG.bSave = false;
			configSaveNewSchedule(&stTSKSCHED_MANCFG.stROW);
		}
#endif



		if (stTASKCOMUNICATION_CTX.bREINIT)
		{
			stTASKCOMUNICATION_CTX.bREINIT = false;
			stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_INIT;
		}
		switch (stTASKCOMUNICATION_CTX.eSTATUS)
		{

		case TASKCOMUNICATION_INIT:
			{
				if (stSTORAGE_IN_MEMORY.bREADY)
				{
					menuStatusSetLoRaWANStatus("ini");
					stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_SET_PARAMS;
				}
				else
				{
					menuStatusSetLoRaWANStatus("VCF");
					osDelay(1000);
				}

			}
			break;
		case TASKCOMUNICATION_SET_PARAMS:
			{
				const UCFG_GEN_SLOT_DATA* puDATA;

				menuStatusSetLoRaWANStatus("InIt");
				puDATA = configConfigGetRO(CONFIG_SLOT_LORAWAN);

				if (taskcomunicationIsLoraWanParamsValid(&puDATA->stLORAWAN))
				{
					LORAWAN_PARAMS	stLoRaWAN;

					stTASKCOMUNICATION_CTX.stCOM_STATUS.iBaseDataRate = puDATA->stLORAWAN.uiDEFAULT_DR;
					stTASKCOMUNICATION_CTX.stCOM_STATUS.iJointReties = 0;

					memset(&stLoRaWAN,0,sizeof(LORAWAN_PARAMS));

					stLoRaWAN.uiEnableAdaptiveDataRate 	= puDATA->stLORAWAN.uiADR_ENABLE;
					stLoRaWAN.iTransmissionDataRate		= puDATA->stLORAWAN.uiDEFAULT_DR;
					stLoRaWAN.uiEnablePublicNetwork		= 1;
					stLoRaWAN.uiJointRequestTrials		= 3;

					stLoRaWAN.uiABP	= puDATA->stLORAWAN.uiABP;

					memcpy(stLoRaWAN.auiABP_DEV_ADDRESS		, puDATA->stLORAWAN.auiABP_DEV_ADDRESS		,  4);
					memcpy(stLoRaWAN.auiABP_NWK_ID			, puDATA->stLORAWAN.auiNWK_ID				,  4);

					memcpy(stLoRaWAN.auiDEV_EUI				, puDATA->stLORAWAN.auiDEV_EUI				,  8);
					memcpy(stLoRaWAN.auiJOIN_EUI			, puDATA->stLORAWAN.auiJOIN_EUI				,  8);
					memcpy(stLoRaWAN.auiAPP_KEY__ABP_S_KEY	, puDATA->stLORAWAN.auiAPP_KEY__ABP_S_KEY	, 16);
					memcpy(stLoRaWAN.auiNWK_KEY__ABP_S_KEY	, puDATA->stLORAWAN.auiNWK_KEY__ABP_S_KEY	, 16);
					memcpy(stLoRaWAN.auiABP_S_NWK_S_INT_KEY	, puDATA->stLORAWAN.auiABP_S_NWK_S_INT_KEY	, 16);
					memcpy(stLoRaWAN.auiABP_F_NWK_S_INT_KEY	, puDATA->stLORAWAN.auiABP_F_NWK_S_INT_KEY	, 16);


					eComRet =taskIntCom_LoRaWANConfig(&stLoRaWAN,&uiResult);

					if (eComRet == GENCOMHL_STATUS_SUCCESS)
					{
						switch (uiResult)
						{
						case LORAWAN_STATUS_OK:
							stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_JOIN;
							break;
						case LORAWAN_STATUS_NJOIN:
							break;
						case LORAWAN_STATUS_NINIT:
							break;
						case LORAWAN_STATUS_NENABLE:
							break;
						default:
						case LORAWAN_STATUS_INVAL:
							break;
						}

					}
					if (stTASKCOMUNICATION_CTX.eSTATUS == TASKCOMUNICATION_SET_PARAMS)
					{
						menuStatusSetLoRaWANStatus("nOC");
						bNotified = xTaskNotifyWait((uint32_t)-1, (uint32_t)-1, &uiNotyMask, 30000);
					}
				}
				else
				{
					stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_WAIT_PARAMS;
				}
			}
			break;

		case TASKCOMUNICATION_WAIT_PARAMS:
			{
				menuStatusSetLoRaWANStatus("vPA");
				bNotified = xTaskNotifyWait((uint32_t)-1, (uint32_t)-1, &uiNotyMask, 60000);

				stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_INIT;

			}
			break;



		case TASKCOMUNICATION_JOIN:
			{
				stTASKCOMUNICATION_CTX.stCOM_STATUS.iJointReties++;
				if (stTASKCOMUNICATION_CTX.stCOM_STATUS.iJointReties > 3)
				{
					stTASKCOMUNICATION_CTX.stCOM_STATUS.iJointReties = 0;
					stTASKCOMUNICATION_CTX.stCOM_STATUS.iBaseDataRate--;
					if (stTASKCOMUNICATION_CTX.stCOM_STATUS.iBaseDataRate < 0)
					{
						stTASKCOMUNICATION_CTX.stCOM_STATUS.iBaseDataRate = 0;
					}
				}
				stTASKCOMUNICATION_CTX.bLORA_JOINED = false;
				eComRet =taskIntCom_LoRaWANEnable(true,stTASKCOMUNICATION_CTX.stCOM_STATUS.iBaseDataRate,&uiResult,&stLoraTxRetInfos);
#ifdef DEBUG
				stTASKCOMUNICATION_CTX.uiTimeDelayForDutyTx = 0;
#endif
				if (eComRet == GENCOMHL_STATUS_SUCCESS)
				{
					int iTPause;
					iTPause = 0;
					switch (uiResult)
					{
					case LORAWAN_STATUS_OK:
						stTASKCOMUNICATION_CTX.iLastUplinkTime = systemGetCurrentDateTime_ms(NULL);
						stTASKCOMUNICATION_CTX.iNextUplinkTimeMinimum = stTASKCOMUNICATION_CTX.iLastUplinkTime + 120000;
#ifdef DEBUG
						stTASKCOMUNICATION_CTX.uiTimeDelayForDutyTx = 1000;
#endif

						stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_WAIT_JOIN;
						break;
					case LORAWAN_STATUS_NJOIN:
					case LORAWAN_MAC_STATUS_DUTY_VIOLATION:
#ifdef DEBUG
						stTASKCOMUNICATION_CTX.uiTimeDelayForDutyTx = stLoraTxRetInfos.timeNextTxDelay;
#endif
						iTPause = stLoraTxRetInfos.timeNextTxDelay;
						iTPause += 1000;
						break;
					case LORAWAN_MAC_STATUS_CERTIFICATION_RUNNING:
					case LORAWAN_MAC_STATUS_NO_CHANNEL_FOUND:
					case LORAWAN_MAC_STATUS_TX_SYS_BUSY:
						iTPause = 30000;
						break;

					case LORAWAN_STATUS_NINIT:
					case LORAWAN_STATUS_NENABLE:
					case LORAWAN_STATUS_INVAL:
					default:
						iTPause = 30000;
						stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_INIT;
						break;
					}
					if (iTPause > 0)
					{
						bNotified = xTaskNotifyWait((uint32_t)-1, (uint32_t)-1, &uiNotyMask, iTPause);
					}
					menuStatusSetLoRaWANStatus("JoR");
				}
			}
			break;
		case TASKCOMUNICATION_WAIT_JOIN:
			{
				bNotified = xTaskNotifyWait((uint32_t)-1, (uint32_t)-1, &uiNotyMask,  60000);
				if (stTASKCOMUNICATION_CTX.bLORA_JOINED)
				{
					stTASKCOMUNICATION_CTX.iLastUplinkTime = 0;
					stTASKCOMUNICATION_CTX.iNextUplinkTimeMinimum = 0;
					stTASKCOMUNICATION_CTX.tTimefromLastDownLink = 0;

					stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_EXCHANGE;
				}
				else
				{
					int64_t	iCurrentTime;
					iCurrentTime = systemGetCurrentDateTime_ms(NULL);
					if (iCurrentTime > stTASKCOMUNICATION_CTX.iNextUplinkTimeMinimum)
					{
//						stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_ENABLE;

						stTASKCOMUNICATION_CTX.iLastUplinkTime = 0;
						stTASKCOMUNICATION_CTX.iNextUplinkTimeMinimum = 0;

						stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_EXCHANGE;
					}
				}
			}
			break;
		case TASKCOMUNICATION_EXCHANGE:
			{
				TASKCOMUNICATION_UPLINK*	pUPLINK;
				int64_t		iCurrentTime;
				int			iDeltaTUplink;
				bool		bSendUpLink;
				bool		bLoadStatusUpLink;
				bool		bConfirmed;
				stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_EXCHANGE;

				iDeltaTUplink = taskComunicationEvalDeltaTUplink(&bSendUpLink,&bLoadStatusUpLink,&iCurrentTime);

				// siamo sicuri, ma per sicurezza
				if (iDeltaTUplink < 0)
				{
					iDeltaTUplink = 0;
				}


				bNotified = xTaskNotifyWait((uint32_t)-1, (uint32_t)-1, &uiNotyMask, iDeltaTUplink);

				iDeltaTUplink = taskComunicationEvalDeltaTUplink(&bSendUpLink,&bLoadStatusUpLink,&iCurrentTime);

				bConfirmed = false;
				if (stTASKCOMUNICATION_CTX.bSEND_CONFIRMED_UPLINK)
				{
					bConfirmed = true;
					bSendUpLink = true;
				}
				pUPLINK = NULL;

				if (stTASKCOMUNICATION_CTX.stUPLINK_RET_CMD.uiPort > 0)
				{
					pUPLINK = &stTASKCOMUNICATION_CTX.stUPLINK_RET_CMD;
					bSendUpLink = true;
				}

				tNewTimerTicks = xTaskGetTickCount();
				if (stTASKCOMUNICATION_CTX.tTimefromLastDownLink == 0)
				{
					stTASKCOMUNICATION_CTX.tTimefromLastDownLink = 1;
					tLastTimerTicks = tNewTimerTicks;
				}
				{
					const UCFG_GEN_SLOT_DATA*		puDATA;
					puDATA = configConfigGetRO(CONFIG_SLOT_SYSTEM_PARAMETERS);

					tDeltaTimerTicks = tNewTimerTicks - tLastTimerTicks;
					tLastTimerTicks = tNewTimerTicks;
					stTASKCOMUNICATION_CTX.tTimefromLastDownLink += tDeltaTimerTicks;

					tDeltaTimerTicks = puDATA->stSYSTEM_PARAMETERS.uiMinutesForReJoin;
					if (tDeltaTimerTicks <= 0)
					{
						tDeltaTimerTicks = (24 * 60 * 60000);
					}
					else
					if (tDeltaTimerTicks <= (30 * 60000))
					{
						tDeltaTimerTicks = (30 * 60000);
					}
					else
					if (tDeltaTimerTicks > (1000000000L / 60000L))
					{
						// Evitiamo overflow pericolosi e ci accontentiamo di un miliardo di millisecondi
						tDeltaTimerTicks = 1000000000L;
					}
					else
					{
						tDeltaTimerTicks *= 60000;
					}

					if (stTASKCOMUNICATION_CTX.tTimefromLastDownLink > tDeltaTimerTicks)
					{
						stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_INIT;
						bSendUpLink = false;
					}
				}
				if (bSendUpLink)
				{
					if (pUPLINK == NULL)
					{
						pUPLINK = &stTASKCOMUNICATION_CTX.stUPLINK;
						stTASKCOMUNICATION_CTX.stUPLINK.uiSize = 0;
						if (bLoadStatusUpLink)
						{
							taskcomunicationFillUplink(&stTASKCOMUNICATION_CTX.stUPLINK);

							stTASKCOMUNICATION_CTX.bSEND_FORCED_STATUS_UPLINK = false;
						}
					}
					{
						INTCOMHL_LORA_DEV_STATUS	stDevStatus = {0};
						stDevStatus.uiBattery = stTASKCOMUNICATION_CTX.uiLoRaVBatt_0_254;
						eComRet = taskIntCom_SendUplink(
								&stDevStatus,
								pUPLINK->uiPort,
								bConfirmed,
								pUPLINK->uiSize,
								pUPLINK->auiPayload,
								&uiResult,
								&stLoraTxRetInfos);

#ifdef DEBUG
						stTASKCOMUNICATION_CTX.uiTimeDelayForDutyTx 	= 0;
#endif
						stTASKCOMUNICATION_CTX.iNextUplinkTimeMinimum	= 0;
						switch (eComRet)
						{
						case GENCOMHL_STATUS_SUCCESS:
							{
#ifdef DEBUG
								if (uiResult != 0)
								{
									stTASKCOMUNICATION_CTX.iCNTR_UPLINK_ERRORS++;
								}
#endif
								switch (uiResult)
								{
								case LORAWAN_STATUS_OK:
									{
										stTASKCOMUNICATION_CTX.stCOM_STATUS.iJointReties = 0;

										if (pUPLINK == &stTASKCOMUNICATION_CTX.stUPLINK_RET_CMD)
										{
											stTASKCOMUNICATION_CTX.stUPLINK_RET_CMD.uiPort = 0;
										}
										else
										{
											if (stTASKCOMUNICATION_CTX.stUPLINK.uiSize > 0)
											{
												stTASKCOMUNICATION_CTX.iLastUplinkTime = iCurrentTime;
											}

										}
										stTASKCOMUNICATION_CTX.iLastUplinkFastComTime 			= iCurrentTime;
										stTASKCOMUNICATION_CTX.iLastUplinkDownLinkPendingUpTime = iCurrentTime;

										if (stTASKCOMUNICATION_CTX.iDownLinkPendingUpTries > 0)
										{
											stTASKCOMUNICATION_CTX.iDownLinkPendingUpTries--;
										}

										stMENU_STATUS.iLORA_TX_DATARATE	= stLoraTxRetInfos.uiDataRateUsed;
										stMENU_STATUS.iLORA_TX_POWER 	= stLoraTxRetInfos.uiTxPowerUsed;

										if (bConfirmed)
										{
											stTASKCOMUNICATION_CTX.bSEND_CONFIRMED_UPLINK = false;
										}

										menuStatusSetLoRaWANStatus("Jnd");
			//							FRT_SetRunningRQST(FRT_TASK_ORDER_COMUNICATION,true);
										stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_WAIT_RX;
									}
									break;
								case LORAWAN_STATUS_NJOIN:
									//!! correggere non rimettere in join se e' gia settato per fare il join
									stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_JOIN;
									break;
								case LORAWAN_STATUS_NINIT:
									stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_INIT;
									break;
								case LORAWAN_STATUS_NENABLE:
									break;
								case LORAWAN_MAC_STATUS_DUTY_VIOLATION:
									{
										// la comunicazione era stata reinviata causa violazione del duty cycle
#ifdef DEBUG
										stTASKCOMUNICATION_CTX.uiTimeDelayForDutyTx = stLoraTxRetInfos.timeNextTxDelay;
#endif
										stTASKCOMUNICATION_CTX.iNextUplinkTimeMinimum = iCurrentTime + stLoraTxRetInfos.timeNextTxDelay;
										// aggiungiamo un secondo per evitare di ricadere nella violazione
										// causa errori del clock (se uno conta piu' veloce dell'altro pensa che sia ora ma non lo e')
										stTASKCOMUNICATION_CTX.iNextUplinkTimeMinimum += 1000;

										menuStatusSetLinkStatus(MENSTS_LORA_LINK_DUTYVIOLATION,0,0);

									}
									break;
								case LORAWAN_MAC_STATUS_CERTIFICATION_RUNNING:
								case LORAWAN_MAC_STATUS_NO_CHANNEL_FOUND:
								case LORAWAN_MAC_STATUS_TX_SYS_BUSY:
									{
//										ritentare dopo 10 o 20 secondi
										// la comunicazione � fallita causa problemi interni
										// canali saturati o systema impegnato a comunicare per conto suo
										unsigned	uiDelay_ms;
										uiDelay_ms = 20000;// 20
#ifdef DEBUG
										stTASKCOMUNICATION_CTX.uiTimeDelayForDutyTx = uiDelay_ms;
#endif
										stTASKCOMUNICATION_CTX.iNextUplinkTimeMinimum = iCurrentTime + uiDelay_ms;

										menuStatusSetLinkStatus(MENSTS_LORA_LINK_TXBUSY,0,0);
									}
									break;
								default:
								case LORAWAN_STATUS_INVAL:
									// se entriamo qua c'e' qualcosa di oscuro che avviene
									osDelay(10000);
									break;
								}
							}
							break;
						default:
							{
								osDelay(1000);
							}
							break;
						}

					}
				}
			}
			break;
		case TASKCOMUNICATION_WAIT_RX:
			{
				osDelay(3000);
//				osDelay(10000);
//				FRT_SetRunningRQST(FRT_TASK_ORDER_COMUNICATION,false);

				stTASKCOMUNICATION_CTX.eSTATUS = TASKCOMUNICATION_EXCHANGE;
			}
			break;
		}
	 }
}

#include "lora_payload.h"
#include "rtc.h"


int taskcomunicationFillUplink(TASKCOMUNICATION_UPLINK* pUPLINK)
{
	int 		iNextSend;
//	BITPACKER	stBP;
	int			iPOS_CUR;
	int			iPOS_MAX;
	unsigned 	uiStato;
	uint8_t*	abyCurPayBuffer;
	uint16_t	uiTEMP_VAL;
	uint8_t		uiValFlags;


	iNextSend = 0;

	abyCurPayBuffer =&stTASKCOMUNICATION_CTX.stUPLINK.auiPayload[0];
	iPOS_CUR = 0;
	iPOS_MAX = 11;

	stTASKCOMUNICATION_CTX.stUPLINK.uiPort = CENTRALINA_LORAWAN_PORT_INFOS;

	uiStato = 0x00;
	if (stSCHEDULER_CTX.iRebootIdleCounter > 0)
	{
		stSCHEDULER_CTX.iRebootIdleCounter--;
		uiStato |= 0x80;
	}

	if (rtcIsSetted())
	{
//		stSCHEDULER_CTX.iRebootIdleCounter = 0;
	}
	else
	{
		uiStato |= 0x40;
	}

	if (stTASKCOMUNICATION_CTX.bLowBattWarning)
	{
		uiStato |= 0x20;
	}
	if (stTASKCOMUNICATION_CTX.bLowBattShutdown)
	{
		uiStato |= 0x30;
	}

	if (stSCHEDULER_CTX.iCurrentSchedule >= 0)
	{
		uiStato |= 0x08;
	}

	if (false)
	{
		abyCurPayBuffer[ 0]  = CENTRALINA_LORAWAN_INFOS_ALL;
		abyCurPayBuffer[ 1]  = uiStato;
		abyCurPayBuffer[ 2]  = (uint8_t)stSCHEDULER_CTX.iCurrentSchedule;
		abyCurPayBuffer[ 3]  = (uint8_t)stSCHEDULER_CTX.stVALVOLE.uiValvoleSchedulate;
		abyCurPayBuffer[ 4]  = stSCHEDULER_CTX.stVALVOLE.uiValvoleAperte;
		abyCurPayBuffer[ 5]  = stSCHEDULER_CTX.stVALVOLE.uiValvoleMalfunzionantiOC;
		abyCurPayBuffer[ 6]  = (stSCHEDULER_CTX.stVALVOLE.uiValvoleMalFunzionantiSC << 4);

		abyCurPayBuffer[ 7]  = taskSchedulerGetValueIN1(&uiTEMP_VAL);;
		abyCurPayBuffer[ 8]  = (uiTEMP_VAL >> 8);
		abyCurPayBuffer[ 9]  = (uiTEMP_VAL >> 0);

		abyCurPayBuffer[10]  = taskSchedulerGetValueIN2(&uiTEMP_VAL);
		abyCurPayBuffer[11]  = (uiTEMP_VAL >> 8);
		abyCurPayBuffer[12]  = (uiTEMP_VAL >> 0);

		abyCurPayBuffer[13]  = taskSchedulerGetFluxCNTR(&uiTEMP_VAL);
		abyCurPayBuffer[14]  = (uiTEMP_VAL >> 8);
		abyCurPayBuffer[15]  = (uiTEMP_VAL >> 0);

		abyCurPayBuffer+=16;
		iPOS_CUR+=16;

	}
	else
	{

	//	if (stTASKCOMUNICATION_CTX.bSEND_STATUS_SYS)
	{
// SIAMO OK NON SERVE TESTARE
//		if ((iPOS_MAX - iPOS_CUR) >= 3)
		{
			// bitmask stato
			abyCurPayBuffer[0] =CENTRALINA_LORAWAN_INFOS_STATUS_SYS;

			abyCurPayBuffer[1] = uiStato;
			abyCurPayBuffer[2] = (uint8_t)stSCHEDULER_CTX.iCurrentSchedule;
			abyCurPayBuffer[3] = (uint8_t)stSCHEDULER_CTX.stVALVOLE.uiValvoleSchedulate;

			abyCurPayBuffer+=4;
			iPOS_CUR+=4;

			stTASKCOMUNICATION_CTX.bSEND_STATUS_SYS = false;
		}

	}


//	if (stTASKCOMUNICATION_CTX.bSEND_STATUS_OUT)
	{
		// SIAMO OK NON SERVE TESTARE
//		if ((iPOS_MAX - iPOS_CUR) >= 3)
		{
			// bitmask stato

			abyCurPayBuffer[0] = CENTRALINA_LORAWAN_INFOS_STATUS_OUTS;
			abyCurPayBuffer[1] = stSCHEDULER_CTX.stVALVOLE.uiValvoleAperte;
			abyCurPayBuffer[2] = stSCHEDULER_CTX.stVALVOLE.uiValvoleMalfunzionantiOC;
			abyCurPayBuffer[2] |= (stSCHEDULER_CTX.stVALVOLE.uiValvoleMalFunzionantiSC << 4);


			abyCurPayBuffer+=3;
			iPOS_CUR+=3;

			stTASKCOMUNICATION_CTX.bSEND_STATUS_OUT = false;
		}

	}
	{
//		uiValFlags = taskSchedulerGetValueIN1(&uiTEMP_VAL);
		uiTEMP_VAL = taskSchedulerGetValueIN1WOpt();
		abyCurPayBuffer[0] = CENTRALINA_LORAWAN_INFOS_VALUE_IN1;
		abyCurPayBuffer[1] = (uiTEMP_VAL >> 8);
		abyCurPayBuffer[2] = (uiTEMP_VAL >> 0);
		abyCurPayBuffer+=3;
		iPOS_CUR+=3;
	}
	{
//		uiValFlags = taskSchedulerGetValueIN2(&uiTEMP_VAL);
		uiTEMP_VAL = taskSchedulerGetValueIN2WOpt();
		abyCurPayBuffer[0] = CENTRALINA_LORAWAN_INFOS_VALUE_IN2;
		abyCurPayBuffer[1] = (uiTEMP_VAL >> 8);
		abyCurPayBuffer[2] = (uiTEMP_VAL >> 0);
		abyCurPayBuffer+=3;
		iPOS_CUR+=3;
	}
	{
//		uiValFlags = taskSchedulerGetFluxCNTR(&uiTEMP_VAL);
		uiTEMP_VAL = taskSchedulerGetFluxCNTROpt();
		abyCurPayBuffer[0] = CENTRALINA_LORAWAN_INFOS_VALUE_CNTR;
		abyCurPayBuffer[1] = (uiTEMP_VAL >> 8);
		abyCurPayBuffer[2] = (uiTEMP_VAL >> 0);
		abyCurPayBuffer+=3;
		iPOS_CUR+=3;
	}
/*
	{
		CFG_SCHEDULER_ROW	stRow;
		if (configGetSchedule(stTASKCOMUNICATION_CTX.iSendScheduleInfo,&stRow))
		{
			// SIAMO OK NON SERVE TESTARE
	//		if ((iPOS_MAX - iPOS_CUR) >= 3)
			{
				abyCurPayBuffer[0] = CENTRALINA_LORAWAN_INFOS_CHECK_SCHED;
				abyCurPayBuffer[1] = (uint8_t)stTASKCOMUNICATION_CTX.iSendScheduleInfo;
				abyCurPayBuffer[2] = stRow.uiCRC8_MSG;
				if (stRow.uiAge == 0)
				{
					abyCurPayBuffer[1] |= 0x80;
				}

				abyCurPayBuffer+=3;
				iPOS_CUR+=3;
			}
		}
		else
		{
			stTASKCOMUNICATION_CTX.iSendScheduleInfo		=-1;
			stTASKCOMUNICATION_CTX.iSendScheduleInfoProgr 	= 2;
		}

		stTASKCOMUNICATION_CTX.iSendScheduleInfoProgr++;
		if (stTASKCOMUNICATION_CTX.iSendScheduleInfoProgr > 2)
		{
			stTASKCOMUNICATION_CTX.iSendScheduleInfoProgr = 0;
			stTASKCOMUNICATION_CTX.iSendScheduleInfo++;
		}
	}
*/
	if (stTASKCOMUNICATION_CTX.bSEND_STATUS_INS)
	{

	}
/*
	bitpackerInitPush(&stBP,&stTASKCOMUNICATION_CTX.stUPLINK.auiPayload[0],sizeof(stTASKCOMUNICATION_CTX.stUPLINK.auiPayload));
	int8_t 						iSendScheduleID;
	int8_t 						iSendScheduleCount;

	while (stTASKCOMUNICATION_CTX.iSendScheduleFrom < stTASKCOMUNICATION_CTX.iSendScheduleTo)
	{
		if ((iPOS_MAX - iPOS_CUR) >= 9)
		{
			pCurPayBuffer[0] =

		}
		if (bitpackerPushTestFreeBits(&stBP,8 *8 ))
		{
			unsigned uiSchedulazione;
			unsigned uiModo;
			unsigned uiGiorno;
			unsigned uiPeriodo;
			unsigned uiStartMinuti;
			unsigned uiDurataMinuti;
			unsigned uiValvoleAccese;

			uiSchedulazione = stTASKCOMUNICATION_CTX.iSendScheduleFrom;

			uiModo 				= 1;
			uiGiorno 			=123;
			uiPeriodo 			= 0;
			uiStartMinuti		= 720;
			uiDurataMinuti		= 60;
			uiValvoleAccese		= 0x01;

			bitpackerPushUIntValue(&stBP, 6, 8);
			bitpackerPushUIntValue(&stBP, uiSchedulazione, 6);
			bitpackerPushUIntValue(&stBP, uiSchedulazione, 7);
			bitpackerPushUIntValue(&stBP, uiModo, 3);
			bitpackerPushUIntValue(&stBP, uiGiorno, 9);
			bitpackerPushUIntValue(&stBP, uiPeriodo, 7);
			bitpackerPushUIntValue(&stBP, uiStartMinuti, 11);
			bitpackerPushUIntValue(&stBP, uiDurataMinuti, 11);
			bitpackerPushUIntValue(&stBP, uiValvoleAccese, 8);
			stTASKCOMUNICATION_CTX.iSendScheduleFrom++;
		}
		else
		{
			break;
		}
	}
*/
	}
	stTASKCOMUNICATION_CTX.stUPLINK.uiSize =iPOS_CUR;
	return iNextSend;
}


int taskComunicationGetCurrentDeltaTUplink(DateTime* ptCurrentTime,int* iDeltaTUplink4DownLink,int* piDeltaTUplink4FastComm,int* piTimeUplink4FastComm);

int taskComunicationEvalDeltaTUplink(bool* pbSendUpLink,bool* pbLoadStatusUpLink,int64_t* piCurrentTime)
{
	DateTime	tCurrentTime;
	int64_t		iDeltaTimeLastStatusUplink;
	int64_t		iDeltaTimeLastUplinkFastComTime;
	int64_t		iDeltaTimeStartUplinkFastComTime;
	int64_t		iDeltaTimeLastUplinkDownLinkPendingUpTime;
	int64_t		iDeltaTUplink;
	int64_t		iDeltaTFastComComparison;
	int			iDeltaTUplink4DownLink;
	int			iDeltaTUplink4FastComm;
	int			iTimeUplink4FastComm;

	*pbSendUpLink = false;
	*pbLoadStatusUpLink = false;
	*piCurrentTime = systemGetCurrentDateTime_ms(&tCurrentTime);

	iDeltaTUplink = taskComunicationGetCurrentDeltaTUplink(&tCurrentTime,&iDeltaTUplink4DownLink,&iDeltaTUplink4FastComm,&iTimeUplink4FastComm);

	if (stTASKCOMUNICATION_CTX.iLastUplinkTime > *piCurrentTime)
	{
		// e' successo qualcosa all'orologio, ci risincroniziamo
		stTASKCOMUNICATION_CTX.iNextUplinkTimeMinimum 	= 0;
		stTASKCOMUNICATION_CTX.iLastUplinkTime 			= *piCurrentTime;
	}
	if (stTASKCOMUNICATION_CTX.iLastUplinkFastComTime > *piCurrentTime)
	{
		stTASKCOMUNICATION_CTX.iLastUplinkFastComTime 	= *piCurrentTime;
	}
	if (stTASKCOMUNICATION_CTX.iLastUplinkDownLinkPendingUpTime > *piCurrentTime)
	{
		stTASKCOMUNICATION_CTX.iLastUplinkDownLinkPendingUpTime = *piCurrentTime;
	}
	if ((stTASKCOMUNICATION_CTX.iStartUplinkFastComTime > *piCurrentTime) || stTASKCOMUNICATION_CTX.bRECV_USER_DOWNLINK)
	{
		stTASKCOMUNICATION_CTX.bRECV_USER_DOWNLINK 		= false;
		stTASKCOMUNICATION_CTX.iStartUplinkFastComTime = *piCurrentTime;
	}

	iDeltaTimeLastStatusUplink					= *piCurrentTime - stTASKCOMUNICATION_CTX.iLastUplinkTime;
	iDeltaTimeLastUplinkFastComTime				= *piCurrentTime - stTASKCOMUNICATION_CTX.iLastUplinkFastComTime;
	iDeltaTimeStartUplinkFastComTime			= *piCurrentTime - stTASKCOMUNICATION_CTX.iStartUplinkFastComTime;
	iDeltaTimeLastUplinkDownLinkPendingUpTime	= *piCurrentTime - stTASKCOMUNICATION_CTX.iLastUplinkDownLinkPendingUpTime;

	iDeltaTUplink -= iDeltaTimeLastStatusUplink;

	if (stTASKCOMUNICATION_CTX.bSEND_FORCED_STATUS_UPLINK)
	{
		iDeltaTUplink = 0;
	}

	if (iDeltaTUplink <= 0)
	{
		iDeltaTUplink = 0;
		*pbSendUpLink = true;
		*pbLoadStatusUpLink = true;
	}
	else
	{
		if (stTASKCOMUNICATION_CTX.iDownLinkPendingUpTries > 0)
		{
			iDeltaTUplink = (int64_t)iDeltaTUplink4DownLink - iDeltaTimeLastUplinkDownLinkPendingUpTime;
			if (iDeltaTUplink <= 0)
			{
				iDeltaTUplink = 0;
				*pbSendUpLink = true;
			}
		}
		else if (stTASKCOMUNICATION_CTX.iStartUplinkFastComTime > 0)
		{
			iDeltaTFastComComparison = (int64_t)iTimeUplink4FastComm - iDeltaTimeStartUplinkFastComTime;
			if (iDeltaTFastComComparison > 0)
			{
				// siamo nel periodo temporade di comunicazione veloce
				iDeltaTUplink = (int64_t)iDeltaTUplink4FastComm - iDeltaTimeLastUplinkFastComTime;
				if (iDeltaTUplink <= 0)
				{
					iDeltaTUplink = 0;
					*pbSendUpLink = true;
				}
			}
			else
			{
				// spegniamo il tutto
				stTASKCOMUNICATION_CTX.iStartUplinkFastComTime = -1;
			}
		}
	}


	if (stTASKCOMUNICATION_CTX.iNextUplinkTimeMinimum > 0)
	{
		// la comunicazione era stata reinviata causa violazione del duty cycle
		iDeltaTimeLastStatusUplink = stTASKCOMUNICATION_CTX.iNextUplinkTimeMinimum - *piCurrentTime;
		if (iDeltaTimeLastStatusUplink > iDeltaTUplink)
		{
			iDeltaTUplink = iDeltaTimeLastStatusUplink;
		}
		if (iDeltaTUplink > 0)
		{
			*pbSendUpLink = false;
		}
	}
	return iDeltaTUplink;
}

int taskComunicationGetCurrentDeltaTUplink(DateTime* ptCurrentTime,int* piDeltaTUplink4DownLink,int* piDeltaTUplink4FastComm,int* piTimeUplink4FastComm)
{
	const UCFG_GEN_SLOT_DATA*		puDATA;
	const CFG_COM_SCHEDULE_ITEM*	pItem;
	int						iMinDayTime;
	int						iDeltaUp;
	int						iDeltaUp4Down;
	int						iDeltaFastUp;
	int						iTimeFastUp;
	int						iScnr;
	int						iMask;

	iDeltaUp 					= 60;
	iDeltaUp4Down 				= 10;
	iDeltaFastUp 				= 20;
	iTimeFastUp 				= 5  * 60;

	puDATA = configConfigGetRO(CONFIG_SLOT_SYSTEM_PARAMETERS);
	iMinDayTime = (ptCurrentTime->tm_hour * 60) + ptCurrentTime->tm_min;
	{
		iScnr = iMinDayTime / (3 * 60);
		iMask = 1 << iScnr;
		if (puDATA->stSYSTEM_PARAMETERS.uiWorkPeriodsMask & iMask)
		{
			iDeltaUp4Down	= 10;
			iDeltaUp 		= puDATA->stSYSTEM_PARAMETERS.uiSecsPeriodWork;
			iDeltaFastUp 	= puDATA->stSYSTEM_PARAMETERS.uiSecsPeriodFastUplinkWork;
			iTimeFastUp 	= puDATA->stSYSTEM_PARAMETERS.uiSecsTimeFastUplinkWork;
		}
		else
		{
			iDeltaUp4Down	= 10;
			iDeltaUp 		= puDATA->stSYSTEM_PARAMETERS.uiSecsPeriodRest;
			iDeltaFastUp 	= puDATA->stSYSTEM_PARAMETERS.uiSecsPeriodFastUplinkRest;
			iTimeFastUp 	= puDATA->stSYSTEM_PARAMETERS.uiSecsTimeFastUplinkRest;
		}


	}
	if (iDeltaUp4Down < 10)
	{
		iDeltaUp4Down = 10;
	}
	if (iDeltaUp < 10)
	{
		iDeltaUp = 120;
	}
	if (iDeltaFastUp >= iDeltaUp)
	{
		iDeltaFastUp = 0;
		iTimeFastUp = 0;
	}
	*piDeltaTUplink4DownLink 	= iDeltaUp4Down * 1000;
	*piDeltaTUplink4FastComm 	= iDeltaFastUp * 1000;
	*piTimeUplink4FastComm 		= iTimeFastUp *1000;
	iDeltaUp *= 1000;
	return iDeltaUp;
}


