/*
 * taskScheduler.c
 *
 *  Created on: 27 feb 2018
 *      Author: sa2025
 */

#include "cmsis_os.h"
#include <stdbool.h>
#include <stdint.h>
#include "taskScheduler.h"
#include "../Storage/configurations.h"
#include "../SystemFunctions.h"
#include "../menu-loc/menu_vars.h"
#include "../Tasks/taskComunication.h"

#include "scheduler_mods/schedulerWorkingCore.h"
#include "scheduler_mods/schedulerTestBoardCore.h"

extern osThreadId taskSchedulerHandle;



SCHEDULER_CTX	stSCHEDULER_CTX;

#define TASK_SCHEDULER_EVENT_ACTION				(0x01)
#define TASK_SCHEDULER_EVENT_CHANGE_MODE		(0x04)
#define TASK_SCHEDULER_EVENT_FLUX_CNTR			(0x02)

void taskSchedulerSignalAction(void)
{
	xTaskNotify(taskSchedulerHandle,TASK_SCHEDULER_EVENT_ACTION,eSetBits);
}

void taskSchedulerChangeMode(ESCHEDULER_MODE eMODE)
{
	if (stSCHEDULER_CTX.eMODE_NEW != eMODE)
	{
		stSCHEDULER_CTX.eMODE_NEW = eMODE;
		xTaskNotify(taskSchedulerHandle,TASK_SCHEDULER_EVENT_CHANGE_MODE,eSetBits);
	}

}

void taskSchedulerSignalFluxCNTR_ISR(void)
{
	if (stSCHEDULER_CTX.bTaskRunning)
	{
		uint32_t	uiTimerTick;
		uint32_t	uiTimerTickDelta;
		uiTimerTick = osKernelSysTick();
		uiTimerTickDelta = uiTimerTick - stSCHEDULER_CTX.stFLUX_STATUS.uiLastTimerTick;
		stSCHEDULER_CTX.stFLUX_STATUS.uiLastTimerTick = uiTimerTick;

		if (uiTimerTickDelta > stSCHEDULER_CTX.stFLUX_STATUS.uiAntiBunch_ms)
		{
			stSCHEDULER_CTX.stFLUX_STATUS.uiFLUX_CNTR++;
			stMENU_VARS.uiFLUX_CNTR++;
		}

		xTaskNotify(taskSchedulerHandle,TASK_SCHEDULER_EVENT_FLUX_CNTR,eSetBits);
	}
}

uint8_t taskSchedulerGetValueIN1(uint16_t* puiValue)
{
	*puiValue = stSCHEDULER_CTX.stADCIN_STATUS.stIN0.uiValueExport;
	return stSCHEDULER_CTX.stADCIN_STATUS.stIN0.uiFlagsExport;
}

uint8_t taskSchedulerGetValueIN2(uint16_t* puiValue)
{
	*puiValue = stSCHEDULER_CTX.stADCIN_STATUS.stIN4.uiValueExport;
	return stSCHEDULER_CTX.stADCIN_STATUS.stIN4.uiFlagsExport;
}

uint8_t taskSchedulerGetFluxCNTR(uint16_t* puiValue)
{
	*puiValue = stSCHEDULER_CTX.stFLUX_STATUS.uiFLUX_CNTR;
	return stSCHEDULER_CTX.stFLUX_STATUS.uiFLAGS;
}


void StartSchedulerTask(void const * argument)
{
	TickType_t	tmDeltaTRQST;
	TickType_t	tmDeltaTCurrent;
	BaseType_t	bNotified;
	uint32_t	uiNotyMask;
	uint32_t	uiOsTimerNew;
	bool		bForceAction;

	stSCHEDULER_CTX.eMODE 		= SCHEDULER_MODE_INIT;
	stSCHEDULER_CTX.eMODE_NEW 	= SCHEDULER_MODE_INIT;
	stSCHEDULER_CTX.stVALVOLE.uiValvoleAperte = 0;
	stSCHEDULER_CTX.stVALVOLE.uiValvoleChiuse = 0;
	stSCHEDULER_CTX.iRebootIdleCounter = 10;

	uiOsTimerNew = osKernelSysTick();

	schedulerWorkingCoreInit(uiOsTimerNew);
	systemBuckBoostEnable(false);

	stSCHEDULER_CTX.stCOIL_SETTINGS.bForceLatch	= true;

	stSCHEDULER_CTX.bTaskRunning = true;

	tmDeltaTCurrent = 1000;


	while (true)
	{

		bNotified = xTaskNotifyWait((uint32_t)-1, (uint32_t)-1, &uiNotyMask, tmDeltaTCurrent);
		uiOsTimerNew = osKernelSysTick();
		if (uiNotyMask & TASK_SCHEDULER_EVENT_FLUX_CNTR)
		{
		}
		bForceAction = false;
		if (uiNotyMask & TASK_SCHEDULER_EVENT_ACTION)
		{
			bForceAction = true;
		}

		if (stSCHEDULER_CTX.eMODE != stSCHEDULER_CTX.eMODE_NEW)
		{
			switch (stSCHEDULER_CTX.eMODE)
			{
			case SCHEDULER_MODE_INIT:
				break;
			case SCHEDULER_MODE_WORKING:
				schedulerWorkingCoreFree();
				break;
			case SCHEDULER_MODE_BOARD_TEST:
				schedulerTestBoardCoreFree();
				break;
			}
			stSCHEDULER_CTX.eMODE = stSCHEDULER_CTX.eMODE_NEW;
			switch (stSCHEDULER_CTX.eMODE)
			{
			case SCHEDULER_MODE_INIT:
				break;
			case SCHEDULER_MODE_WORKING:
				schedulerWorkingCoreInit(uiOsTimerNew);
				break;
			case SCHEDULER_MODE_BOARD_TEST:
				schedulerTestBoardCoreInit(uiOsTimerNew);
				break;
			}
		}

		switch (stSCHEDULER_CTX.eMODE)
		{
		case SCHEDULER_MODE_INIT:
			tmDeltaTRQST = 1000;
			if (stSTORAGE_IN_MEMORY.bREADY)
			{
				stSCHEDULER_CTX.eMODE_NEW = SCHEDULER_MODE_WORKING;
			}
			break;
		case SCHEDULER_MODE_WORKING:
			tmDeltaTRQST = schedulerWorkingCore(uiOsTimerNew,bForceAction);
			break;
		case SCHEDULER_MODE_BOARD_TEST:
			tmDeltaTRQST = schedulerTestBoardCore(uiOsTimerNew,bForceAction);
			break;
		}
		tmDeltaTCurrent = tmDeltaTRQST;
	 }
}



