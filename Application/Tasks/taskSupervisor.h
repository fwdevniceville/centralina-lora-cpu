/*
 * taskSupervisor.h
 *
 *  Created on: 16 apr 2018
 *      Author: daniele_parise
 */

#ifndef TASKS_TASKSUPERVISOR_H_
#define TASKS_TASKSUPERVISOR_H_

struct tagMENU_VARS_LORA_CFG;
struct tagCFG_LORAWAN;
struct tagCFG_SYSTEM_PARAMETERS;
void taskSupervisorSaveLoraCfgSimple(const struct tagMENU_VARS_LORA_CFG* pLoRa);
void taskSupervisorSaveLoraCfg(const struct tagCFG_LORAWAN* pLoRa);
void taskSupervisorSaveSysParamsCfg(const struct tagCFG_SYSTEM_PARAMETERS* pCFG_SYSTEM_PARAMETERS);
void taskSupervisorReboot(void);

#endif /* TASKS_TASKSUPERVISOR_H_ */
