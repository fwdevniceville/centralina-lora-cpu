/*
 * intcom_hw.h
 *
 *  Created on: 11 mar 2018
 *      Author: daniele_parise
 */

#ifndef SRC_APPLICATION_INTCOM_INTCOM_HW_H_
#define SRC_APPLICATION_INTCOM_INTCOM_HW_H_

#define USART_INTCOM                           USART2
#define USART_INTCOM_CLK_ENABLE()              __USART2_CLK_ENABLE();

#define USART_INTCOM_FORCE_RESET()             __USART2_FORCE_RESET()
#define USART_INTCOM_RELEASE_RESET()           __USART2_RELEASE_RESET()


#define USART_INTCOM_TX_PIN                  GPIO_PIN_5
#define USART_INTCOM_TX_GPIO_PORT            GPIOD
#define USART_INTCOM_TX_AF                   GPIO_AF0_USART2
#define USART_INTCOM_RX_PIN                  GPIO_PIN_6
#define USART_INTCOM_RX_GPIO_PORT            GPIOD
#define USART_INTCOM_RX_AF                   GPIO_AF0_USART2

#define USART_INTCOM_WAKEUP_FROM_STOP
#define USART_INTCOM_INVERT_COMM

#define USART_INTCOM_IRQn                    	USART2_IRQn
#define USART_INTCOM_IRQHandler              	USART2_IRQHandler
#define USART_INTCOM_IRQ_pri					(15)


void intcom_hwEnterStopMode(void);
void intcom_hwExitStopMode(void);

#endif /* SRC_APPLICATION_INTCOM_INTCOM_HW_H_ */
